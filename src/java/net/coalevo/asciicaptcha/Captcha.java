/***
 * Coalevo ASCII Captcha
 *
 * Copyright 2006 Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
 package net.coalevo.asciicaptcha;

/**
 * Interface that defines the contract for an ASCII Captcha.
 *
 * @author Dieter Wimberger
 * @version @version@ (@date@)
 */
public interface Captcha {
  
  /**
   * Returns the captcha as string that is build from
   * figlets.
   * 
   * @return the captcha to be displayed.
   */
  public String getDisplay();
  
  /**
   * Tests if the given string to the string represented by the captcha.
   * <p>
   * The actual captcha string that is returned by {@link #getDisplay()} will 
   * logically return false.
   * </p>
   */
  public boolean equals(String str);
 
}//Captcha