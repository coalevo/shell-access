/***
 * Coalevo ASCII Captcha
 *
 * Copyright 2006 Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
 package net.coalevo.asciicaptcha;

import java.util.Random;
import java.util.ArrayList;
import java.util.List;
import java.io.IOException;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Provides a generator for ASCII {@link Captcha} instances.
 * 
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class CaptchaGenerator {

  private static Logger log = LoggerFactory.getLogger(CaptchaGenerator.class);
  private List[] m_TypeLists;
  private Random m_TypeRnd;
  private Random m_ListRnd;
  private char[] m_CharMap;
  private byte[] m_Empty;

  public CaptchaGenerator() {
    m_TypeLists = new List[3];
    m_ListRnd = new Random();
    m_TypeRnd = new Random();
    prepare();
  }//constructor

  public Captcha generate() {
    return new CaptchaImpl();
  }//getCaptcha
  
  private void prepare() {
  try{
    initialize();
    final ClassLoader cl = getClass().getClassLoader();
     
    for(int i = 1; i<4; i++) {
     List l = new ArrayList(35);
     String prefix = NUM_PREFIX;
     for(int n=49;n<123;n++) {
       //jump to a
       if(n==58) {
         n = 97;
         prefix = CHAR_PREFIX;
       }
       String res =  prefix + (char) n + "_" + i + ".txt";
       log.debug("Loading " + res);
       l.add(load(cl.getResourceAsStream(res)));
       }
     m_TypeLists[i-1] = l;       
    }
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }//prepare
  
  private String load(InputStream in) throws IOException {
    byte[] buf = new byte[in.available()];
    in.read(buf);
    return new String(buf);
  }//load

  private void initialize() {
    m_CharMap = new char[35];
    m_Empty = new byte[35];
    int idx = 0;
    for(int n=49;n<123;n++) {
       //jump to a
       if(n==58) {
         n = 97;
       }
       m_Empty[idx] = (byte) ' ';
       m_CharMap[idx++] = (char) n;
    }
  }//initialize

  /*
  public static void main(String[] args) {
    CaptchaGenerator gen = new CaptchaGenerator();
    System.out.println(gen.generate().getDisplay());
    
    //Random rtype = new Random();
    //for(int i = 0; i<10;i++) {
    //  System.out.println(
    //   rtype.nextInt(3)
    //);
    //}
    if(args != null && args.length > 0) {
      long total = 0;
      long now = System.currentTimeMillis();
      for(int i = 0; i< 10000; i++) {
        now = System.currentTimeMillis();
        gen.generate();
        total += System.currentTimeMillis() - now;
      }
      System.out.println("Total generation time = " + total);
      System.out.println("Average per captcha = " + (total/10000));
    }
  }//main
  */

  private class CaptchaImpl implements Captcha {
        
    private String m_Chars;
    private String m_Display;
    
    public CaptchaImpl() { 
      generate();
    }//CaptchaImpl
  
    private void generate() {      
      final StringBuffer cs = new StringBuffer(4);
      //1. char
      int idx = m_ListRnd.nextInt(35);
      List l = m_TypeLists[m_TypeRnd.nextInt(3)];
      cs.append(m_CharMap[idx]);
      String c1 = (String) l.get(idx);
      //2. char
      idx = m_ListRnd.nextInt(35);
      l = m_TypeLists[m_TypeRnd.nextInt(3)];
      cs.append(m_CharMap[idx]);
      String c2 = (String) l.get(idx);
      //3. char
      idx = m_ListRnd.nextInt(35);
      l = m_TypeLists[m_TypeRnd.nextInt(3)];
      cs.append(m_CharMap[idx]);
      String c3 = (String) l.get(idx);
      //4. char
      idx = m_ListRnd.nextInt(35);
      l = m_TypeLists[m_TypeRnd.nextInt(3)];
      cs.append(m_CharMap[idx]);
      
      String c4 = (String) l.get(idx);
      m_Chars = cs.toString();
      //System.out.println(m_Chars.toString());
      //System.out.println(c1);
      //System.out.println(c2);
      //System.out.println(c3);
      //System.out.println(c4);
      String[] t1 = c1.split("\n");
      String[] t2 = c2.split("\n");
      String[] t3 = c3.split("\n");
      String[] t4 = c4.split("\n");

            
      StringBuffer sbuf = new StringBuffer();
      boolean done = false;
      idx = 0;
      String tmp = "";
      do {        
        // char 1
        //System.out.println("\n\n==>t1 length=" + t1.length + " ::idx="+ idx +"<==\n\n");
        if(t1.length > idx) {
          sbuf.append(' ');
          sbuf.append(getFixedWidth(18,t1[idx]));          
          sbuf.append(' ');
        } else {
          sbuf.append(getEmpty(18));
        }
        // char 2
        if(t2.length > idx) {
          sbuf.append(' ');
          sbuf.append(getFixedWidth(18,t2[idx]));          
          sbuf.append(' ');
        } else {
          sbuf.append(getEmpty(18));
        }
        // char 3
        if(t3.length > idx) {
          sbuf.append(' ');
          sbuf.append(getFixedWidth(18,t3[idx]));          
          sbuf.append(' ');
        } else {
          sbuf.append(getEmpty(18));
        }
        // char 4
        if(t4.length > idx) {
          sbuf.append(' ');
          sbuf.append(getFixedWidth(18,t4[idx]));          
          sbuf.append(' ');
        } else {
          sbuf.append(getEmpty(18));
        }
        sbuf.append(LINEBREAK);        
        done = (idx > t1.length 
                && idx > t2.length 
                && idx > t3.length 
                && idx > t4.length
               );      
        idx++;
      } while(!done); 
      m_Display = sbuf.toString();
      
    }//prepare
  
    private String getEmpty(int first) {
      return new String(m_Empty,0,first);
    }//getEmpty
    
    private String getFixedWidth(int first, String str) {
      int diff = first- str.length() ;
      return str + new String(m_Empty,0,diff);
    }//getFixedWidth
    
    public String getDisplay() {
      return m_Display;
    }//getDisplay
    
    public boolean equals(String str) {
      return m_Chars.equalsIgnoreCase(str);
    }//equals
  
  }//inner class CaptchaImpl
  
  private static final String CHAR_PREFIX = "net/coalevo/asciicaptcha/resources/chars/";
  private static final String NUM_PREFIX = "net/coalevo/asciicaptcha/resources/numbers/";
  private static final String LINEBREAK = "\n\r";
 
}//CaptchaGenerator