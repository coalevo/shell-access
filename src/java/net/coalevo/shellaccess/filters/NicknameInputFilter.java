/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.filters;

import java.io.IOException;

/**
 * This class implements a nickname filter.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class NicknameInputFilter
    extends ASCIIInputFilter {

  private boolean m_Capitalize = true;

  public NicknameInputFilter() {
  }//constructor

  public NicknameInputFilter(String errormessage) {
    super(errormessage);
  }//constructor

  public NicknameInputFilter(boolean nospecial) {
    super(nospecial);
  }//constructor

  public NicknameInputFilter(boolean nospecial, String errormessage) {
    super(nospecial, errormessage);
  }//constructor

   public int filterInput(int key) throws IOException {
     if((m_Capitalize && key > 64 && key < 91)) {
       key = key + 32;
       m_Capitalize = false;
     } else if (m_Capitalize && key > 96 & key < 123) {
       key = key - 32;
       m_Capitalize = false;
     } else if(m_Capitalize && key == 35) {
       m_Capitalize = false;
     } else if (key == 32 ) {
       m_Capitalize = true;
     }
     return super.filterInput(key);
   }//filterInput

}//class NicknameInputFilter
