/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.filters;

import net.wimpi.telnetd.io.toolkit.InputFilter;
import net.wimpi.telnetd.io.BasicTerminalIO;

import java.io.IOException;

/**
 * This class implements a UTF-8 {@link InputFilter}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class UTF8InputFilter
    extends BaseInputFilter {

  public UTF8InputFilter() {
   }//UTF8InputFilter

   public UTF8InputFilter(String errormessage) {
     m_Error = errormessage;
   }//UTF8nputFilter
 
  public int filterInput(int key) throws IOException {
    if(key == BasicTerminalIO.CTRL_A) {
      return key;
    }
    if(key == BasicTerminalIO.CTRL_E) {
      return key;
    }
    if(key == BasicTerminalIO.CTRL_U) {
      return key;
    }
    if(key == BasicTerminalIO.CTRL_W) {
      return key;
    }
    if(key == BasicTerminalIO.ENTER) {
      return key;
    }
    if(key == BasicTerminalIO.LEFT) {
      return key;
    }
    if(key == BasicTerminalIO.RIGHT) {
      return key;
    }
    if(key == BasicTerminalIO.BACKSPACE) {
      return key;
    }
    if(key == BasicTerminalIO.DELETE) {
      return key;
    }
    if (!isValidChar(key)) {
      return InputFilter.INPUT_INVALID;
    }
    return key;
  }//filterInput

  private static boolean isValidChar(int i) {
    return ((i == 0x9) ||
        (i == 0xA) ||
        (i == 0xD) ||
        ((i >= 0x20) && (i <= 0xD7FF)) ||
        ((i >= 0xE000) && (i <= 0xFFFD)) ||
        ((i >= 0x10000) && (i <= 0x10FFFF)));
  }//isValidChar

  public static String stripNonValidCharacters(String in) {
    StringBuilder out = new StringBuilder(); // Used to hold the output.
    char current; // Used to reference the current character.
    if (in == null || ("".equals(in))) return ""; // vacancy test.
    for (int i = 0; i < in.length(); i++) {
      current = in.charAt(i); // NOTE: No IndexOutOfBoundsException caught here; it should not happen.
      if (isValidChar(current)) {
        out.append(current);
      }
    }
    return out.toString();
  }//stripNonValidXMLCharacters

}//class UTF8InputFilter
