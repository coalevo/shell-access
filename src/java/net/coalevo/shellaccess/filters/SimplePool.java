/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.filters;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.slf4j.Marker;
import org.slf4j.MarkerFactory;
import net.coalevo.shellaccess.impl.Activator;


/**
 * Provides a simple pool basically thought for reusing filters.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class SimplePool<T> {

  private static final Marker c_LogMarker = MarkerFactory.getMarker(SimplePool.class.getName());
  private BlockingQueue<T> m_Completers;


  public SimplePool() {
    m_Completers = new LinkedBlockingQueue<T>();
  }//SimplePool

  public void clear() {
    m_Completers.clear();
  }//clear

  public T lease() {
     if(m_Completers.peek()!=null) {
       try {
         return m_Completers.take();
       } catch (InterruptedException e) {
         Activator.log().error(c_LogMarker,"lease()", e);
         return null;
       }
     } else {
       return null;
     }
  }//lease

  public void release(T t) {
    try {
      m_Completers.put(t);
    } catch (InterruptedException e) {
      Activator.log().error(c_LogMarker,"release()", e);
    }
  }//release

}//class SimplePool
