/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.filters;

import net.wimpi.telnetd.io.toolkit.Editfield;
import net.wimpi.telnetd.io.toolkit.TabCompletionFilter;
import net.wimpi.telnetd.util.StringCompletions;
import org.apache.commons.collections.iterators.LoopingIterator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * Provides a <tt>TabCompletionFilter</tt> that will
 * complete available forum names.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class StringListCompletionFilter
    extends TabCompletionFilter {

  private TernarySearchTree m_Strings;
  private boolean m_AlmostMatch = false;

  public StringListCompletionFilter(Collection<String> strs) {
    m_Strings = new TernarySearchTree();
    for (String str : strs) {
      m_Strings.put(str, str);
    }
  }//StringListCompletionFilter

  public StringListCompletionFilter(Collection<String> strs, int diff) {
    this(strs);
    setMatchingDifference(diff);
  }//StringListCompletionFilter

  public StringListCompletionFilter(Editfield editfield, Collection<String> strs) {
    super(editfield);
    m_Strings = new TernarySearchTree();
    for (String str : strs) {
      m_Strings.put(str, str);
    }
  }//StringListCompletionFilter

  public StringListCompletionFilter(Editfield editfield, Collection<String> strs, int diff) {
    this(editfield, strs);
    setMatchingDifference(diff);
  }//StringListCompletionFilter

  public void setMatchingDifference(int diff) {
    m_AlmostMatch = true;
    m_Strings.setMatchAlmostDiff(diff);
  }//setMatchingDifference

  public StringCompletions complete(String string) {
    final Iterator completions = getCompletingStrings(string);

    return new StringCompletions() {
      public String nextCompletion() {
        if (completions.hasNext()) {
          return (String) completions.next();
        } else {
          return null;
        }
      }
    };
  }//complete

  public Iterator getCompletingStrings(String str) {
    DoublyLinkedList dll = m_Strings.matchPrefix(str);
    if (m_AlmostMatch && dll.isEmpty()) {
      //try match almost
      dll = m_Strings.matchAlmost(str);
    }
    if (dll.isEmpty()) {
      return EmptyIterator.INSTANCE;
    } else {
      ArrayList<String> pref = new ArrayList<String>(dll.size());

      DoublyLinkedList.DLLIterator iterator = dll.iterator();
      while (iterator.hasNext()) {
        pref.add((String) iterator.next());
      }
      pref.add(str); //add the prefix
      return new LoopingIterator(pref);
    }
  }//getCompletions

  public String getErrorMessage() {
    return "";
  }//getErrorMessage

}//class StringListCompletionFilter
