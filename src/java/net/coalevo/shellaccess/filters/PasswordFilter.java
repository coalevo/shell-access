/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.filters;

import net.coalevo.foundation.service.RndStringGeneratorService;
import net.coalevo.shellaccess.impl.Activator;
import net.coalevo.shellaccess.model.Services;
import net.wimpi.telnetd.io.toolkit.Editfield;
import net.wimpi.telnetd.io.toolkit.TabCompletionFilter;
import net.wimpi.telnetd.util.StringCompletions;

/**
 * A filter that will not complete, but rather provide random
 * generated (pronounceable and unpronounceable) strings for
 * the password.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class PasswordFilter
    extends TabCompletionFilter {

  RndStringGeneratorService m_Generator;

  public PasswordFilter(Editfield ef) {
    super(ef);
    m_Generator = Activator.getServices().getRndStringGeneratorService(Services.NO_WAIT);
  }//PasswordFilter

  public StringCompletions complete(String string) {
    m_Editfield.setHiddenInput(false);
    int len = 8;
    try {
      if (string != null && string.length() > 0) {
        len = Integer.parseInt(string);
        if (len < 8) {
          len = 8;
        } else if (len > 25) {
          len = 25;
        }
      }
    } catch (Exception ex) {
    }

    final int length = len;

    return new StringCompletions() {
      private boolean m_Flip;

      public String nextCompletion() {
        if (m_Generator == null) {
          return "";
        } else {
          m_Flip = !m_Flip;
          if (m_Flip) {
            return m_Generator.getPronounceableString(length);
          } else {
            return m_Generator.getRandomString(length);
          }
        }
      }
    };
  }//complete

  public String getErrorMessage() {
    return "";
  }//getErrorMessage
  
}//class PasswordFilter
