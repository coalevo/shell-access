/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.filters;

import net.wimpi.telnetd.io.toolkit.InputFilter;
import net.wimpi.telnetd.io.BasicTerminalIO;

import java.io.IOException;

/**
 * Provides an {@link InputFilter} implementation
 * that will only pass through valid printable ASCII
 * characters (that is, [32..126]) and the editing
 * CTRL sequences and special Characters.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class ASCIIInputFilter
    extends BaseInputFilter {

  private boolean m_NoSpecial = false;

  public ASCIIInputFilter() {  
  }//ASCIIInputFilter

  public ASCIIInputFilter(String errormessage) {
    m_Error = errormessage;
  }//ASCIIInputFilter

  public ASCIIInputFilter(boolean nospecial) {
    m_NoSpecial = nospecial;
  }//ASCIIInputFilter

  public ASCIIInputFilter(boolean nospecial, String errormessage) {
    m_NoSpecial = nospecial;
    m_Error = errormessage;
  }//ASCIIInputFilter

  public int filterInput(int key) throws IOException {
    if(!m_NoSpecial) {
      if (key > 31 && key < 127) {
        return key;
      }
    } else {
      if(key == 32 || (key > 64 && key < 91) || (key > 96 && key < 123)) {
        return key;
      }
    }
    if(key == BasicTerminalIO.CTRL_A) {
      return key;
    }
    if(key == BasicTerminalIO.CTRL_E) {
      return key;
    }
    if(key == BasicTerminalIO.CTRL_U) {
      return key;
    }
    if(key == BasicTerminalIO.CTRL_W) {
      return key;
    }
    if(key == BasicTerminalIO.ENTER) {
      return key;
    }
    if(key == BasicTerminalIO.LEFT) {
      return key;
    }
    if(key == BasicTerminalIO.RIGHT) {
      return key;
    }
    if(key == BasicTerminalIO.BACKSPACE) {
      return key;
    }
    if(key == BasicTerminalIO.DELETE) {
      return key;
    }
    return INPUT_INVALID;
  }//filterInput

}//class ASCIIInputFilter