/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.model;

import java.util.HashMap;
import java.util.NoSuchElementException;
import java.util.Map;

/**
 * Provides an abstract base class for simple
 * {@link ShellCommandProvider} implementations.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public abstract class BaseShellCommandProvider implements ShellCommandProvider {

  protected String m_Namespace;
  protected Map<String,Class> m_Commands;

  public BaseShellCommandProvider(String namespace) {
    m_Namespace = namespace;
    m_Commands = new HashMap<String,Class>();
  }//BaseShellCommandProvider

  public String getNamespace() {
    return m_Namespace;
  }//getNamespace

  public boolean provides(String cid) {
    return m_Commands.containsKey(cid);
  }//provides

  public Command createCommand(String cid)
      throws CommandException {
    Class clazz = m_Commands.get(cid);
    if (clazz != null) {
      try {
        return (Command) clazz.newInstance();
      } catch (Exception e) {
        throw new CommandException();
      }
    } else {
      throw new NoSuchElementException();
    }
  }//createCommand

  public void addCommand(String cid, Class c) {
    m_Commands.put(cid, c);
  }//addCommand

}//class BaseShellCommandProvider
