/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.model;

import java.util.HashMap;

/**
 * This class implements Integer to String shortcuts, in both directions.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class Shortcuts {

  private HashMap<Integer, String> m_Short2String;
  private HashMap<String, Integer> m_String2Short;

  public Shortcuts() {
    m_Short2String = new HashMap<Integer,String>();
    m_String2Short = new HashMap<String,Integer>();
  }//Shortcuts

  public Shortcuts(String str) {
    this();
    parseFrom(str);
  }//Shortcuts

  public boolean contains(int i) {
    return m_Short2String.containsKey(i);
  }//contains

  public boolean contains(String str) {
    return m_String2Short.containsKey(str);
  }//contains

  public boolean isEmpty() {
    return m_Short2String.isEmpty();
  }//isEmpty

  public int size() {
    return m_Short2String.size();
  }//size
  
  public int get(String str) {
    return m_String2Short.get(str);
  }//get

  public String get(int i) {
    return m_Short2String.get(i);
  }//get

  public void add(int i, String str) {
    m_Short2String.put(i,str);
    m_String2Short.put(str,i);
  }//add

  public void remove(int i) {
    m_String2Short.remove(m_Short2String.remove(i));
  }//remove

  public void remove(String str) {
    m_Short2String.remove(m_String2Short.remove(str));
  }//remove

  public String toString() {
    StringBuilder sbuf = new StringBuilder();
    for(Integer i: m_Short2String.keySet()) {
      sbuf.append(Integer.toString(i));
      sbuf.append("=");
      sbuf.append(m_Short2String.get(i));
      sbuf.append(";");
    }
    return sbuf.toString();
  }//toString

  private void parseFrom(String str) {
    if(str != null && str.length() > 0) {
    String[] l = str.split(";");
      for (int i = 0; i< l.length; i++) {
        String[] e = l[i].split("=");
        add(Integer.parseInt(e[0]),e[1]);
      }
    }
  }//parseFrom

}//class Shortcuts
