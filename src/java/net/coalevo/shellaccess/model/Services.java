/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.model;

import net.coalevo.foundation.service.RndStringGeneratorService;
import net.coalevo.foundation.model.ServiceAgent;
import net.coalevo.messaging.service.MessagingService;
import net.coalevo.messaging.service.GroupMessagingService;
import net.coalevo.presence.service.PresenceService;
import net.coalevo.security.service.PolicyService;
import net.coalevo.security.service.SecurityManagementService;
import net.coalevo.security.service.SecurityService;
import net.coalevo.shellaccess.command.CommandShellManager;
import net.coalevo.shellaccess.impl.SessionManager;
import net.coalevo.system.service.ExecutionService;
import net.coalevo.system.service.MaintenanceService;
import net.coalevo.userdata.service.UserdataService;
import net.coalevo.iplocator.service.IPLocatorService;
import net.coalevo.discussion.service.DiscussionService;
import net.coalevo.discussion.service.ForumModerationService;
import net.coalevo.statistics.service.StatisticsService;
import net.coalevo.bank.service.BankService;
import net.coalevo.postoffice.service.PostOfficeService;
import net.wimpi.telnetd.service.TemplatesService;

import javax.xml.parsers.SAXParserFactory;

import org.osgi.service.prefs.PreferencesService;

/**
 * Defines an interface for a mediator that provides access
 * to services (obtained or tracked by the mediator through the
 * standard OSGi mechanisms).
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface Services {


  /**
   * Returns a <tt>SaxParserFactory</tt> to create SAX parsers if
   * required.
   *
   * @return a <tt>SAXParserFactory</tt>.
   * @throws Exception if the factory cannot be provided.
   */
  public SAXParserFactory getSAXParserFactory(long wait) throws Exception;

  /**
   * Returns the {@link PreferencesService}.
   *
   * @return a {@link PreferencesService} reference or null if
   *         it cannot be obtained from the container.
   * @throws Exception if the factory cannot be provided.
   * @see #WAIT_UNLIMITED
   * @see #NO_WAIT
   */
  public PreferencesService getPreferencesService() throws Exception;

  /**
   * Returns the {@link CommandShellManager}.
   *
   * @return the {@link CommandShellManager} reference.
   */
  public CommandShellManager getCommandShellManager();

  /**
   * Returns the {@link SecurityService}.
   *
   * @param wait
   * @return a {@link SecurityService} reference or null if
   *         it cannot be obtained in the given time frame.
   * @see #WAIT_UNLIMITED
   * @see #NO_WAIT
   */
  public SecurityService getSecurityService(long wait);

  /**
   * Returns the {@link SecurityManagementService}.
   *
   * @param wait
   * @return a {@link SecurityManagementService} reference or null if
   *         it cannot be obtained in the given time frame.
   * @see #WAIT_UNLIMITED
   * @see #NO_WAIT
   */
  public SecurityManagementService getSecurityManagementService(long wait);

  /**
   * Returns the {@link PolicyService}.
   *
   * @param wait
   * @return a {@link PolicyService} reference or null if
   *         it cannot be obtained in the given time frame.
   * @see #WAIT_UNLIMITED
   * @see #NO_WAIT
   */
  public PolicyService getPolicyService(long wait);

  /**
   * Returns the {@link PolicyService}.
   *
   * @param wait
   * @return a {@link PolicyService} reference or null if
   *         it cannot be obtained in the given time frame.
   * @see #WAIT_UNLIMITED
   * @see #NO_WAIT
   */
  public RndStringGeneratorService getRndStringGeneratorService(long wait);

  /**
   * Returns the {@link PolicyService}.
   *
   * @param wait
   * @return a {@link PolicyService} reference or null if
   *         it cannot be obtained in the given time frame.
   * @see #WAIT_UNLIMITED
   * @see #NO_WAIT
   */
  public ExecutionService getExecutionService(long wait);

  /**
   * Returns the {@link TemplatesService}.
   *
   * @param wait
   * @return a {@link TemplatesService} reference or null if
   *         it cannot be obtained in the given time frame.
   * @see #WAIT_UNLIMITED
   * @see #NO_WAIT
   */
  public TemplatesService getTemplatesService(long wait);

  /**
   * Returns the {@link PresenceService}.
   *
   * @param wait
   * @return a {@link PresenceService} reference or null if
   *         it cannot be obtained in the given time frame.
   * @see #WAIT_UNLIMITED
   * @see #NO_WAIT
   */
  public PresenceService getPresenceService(long wait);

  /**
   * Returns the {@link MessagingService}.
   *
   * @param wait
   * @return a {@link MessagingService} reference or null if
   *         it cannot be obtained in the given time frame.
   * @see #WAIT_UNLIMITED
   * @see #NO_WAIT
   */
  public MessagingService getMessagingService(long wait);

  /**
   * Returns the {@link GroupMessagingService}.
   *
   * @param wait
   * @return a {@link MessagingService} reference or null if
   *         it cannot be obtained in the given time frame.
   * @see #WAIT_UNLIMITED
   * @see #NO_WAIT
   */
  public GroupMessagingService getGroupMessagingService(long wait);

  /**
   * Returns the {@link UserdataService}.
   *
   * @param wait
   * @return a {@link UserdataService} reference or null if
   *         it cannot be obtained in the given time frame.
   * @see #WAIT_UNLIMITED
   * @see #NO_WAIT
   */
  public UserdataService getUserdataService(long wait);

  /**
   * Returns the {@link IPLocatorService}.
   * @param wait
   * @return a {@link IPLocatorService} reference or null if it cannot
   *         be obtained in the given time frame.
   */
  public IPLocatorService getIPLocatorService(long wait);

  /**
   * Returns the {@link DiscussionService}.
   * @param wait
   * @return a {@link DiscussionService} reference or null if it cannot
   *         be obtained in the given time frame.
   */
  public DiscussionService getDiscussionService(long wait);

  /**
   * Returns the {@link ForumModerationService}.
   * @param wait
   * @return a {@link ForumModerationService} reference or null if it cannot
   *         be obtained in the given time frame.
   */
  public ForumModerationService getForumModerationService(long wait);

  /**
   * Returns the {@link StatisticsService}.
   * @param wait
   * @return a {@link StatisticsService} reference or null if it cannot
   *         be obtained in the given time frame.
   */
  public StatisticsService getStatisticsService(long wait);

  /**
   * Returs the {@link BankService}.
   * @param wait
   * @return
   */
  public BankService getBankService(long wait);

  /**
   * Returns the shell access service agent.
   * @return the shell access {@link ServiceAgent}.
   */
  public ServiceAgent getServiceAgent();

  /**
   * Returns the session manager.
   */
  public SessionManager getSessionManager();

  /**
   * Returs the {@link MaintenanceService}.
   * @param wait
   * @return
   */
  public MaintenanceService getMaintenanceService(long wait);

  /**
   * Returns the {@link PostOfficeService}.
   * @param wait
   * @return
   */
  public PostOfficeService getPostOfficeService(long wait);


  /**
   * Defines the time span to be unlimited. Use with care, as it
   * will block the requesting thread (unlimited :).
   */
  public static long WAIT_UNLIMITED = -1;

  /**
   * Defines the time span to be zero. The method will return inmediately.
   */
  public static long NO_WAIT = 0;

}//interface Services
