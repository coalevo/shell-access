/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.model;

import java.io.IOException;

/**
 * Provides extended access to the source of script commands that are
 * handled by this <tt>ScriptShellCommandProvider</tt>.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface ScriptShellCommandProvider
    extends ShellCommandProvider {

  /**
   * Returns the source of the script command with the given identifier,
   * within the namespace of this <tt>ScriptShellCommandProvider</tt>.
   *
   * @param id the commands identifier.
   * @return the source of the script command.
   */
  public String getCommandScript(String id) throws IOException;

  /**
   * Sets the new source of the script command with the given identifier,
   * within the namespace of this <tt>ScriptShellCommandProvider</tt>.
   *
   * @param id     the command identifier.
   * @param script the new source of the script command to be set.
   */
  public void setCommandScript(String id, String script) throws IOException;

  /**
   * Returns the names of the script commands provided within the namespace
   * of this  <tt>ScriptShellCommandProvider</tt>.
   * @return the command identifiers as <tt>String[]</tt>.
   */
  public String[] listCommandScriptNames();

}//interface ScriptShellCommandProvider
