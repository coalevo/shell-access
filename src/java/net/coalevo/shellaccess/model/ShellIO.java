/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.model;

import net.wimpi.telnetd.io.BasicTerminalIO;
import net.wimpi.telnetd.io.TemplateAttributes;

import java.io.IOException;
import java.util.Locale;

/**
 * Defines the basic I/O functionality.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface ShellIO {

  /**
   * Returns the underlying <tt>BasicTerminalIO</tt>.
   *
   * @return a <tt>BasicTerminalIO</tt> instance.
   */
  public BasicTerminalIO getTerminalIO();

  /**
   * Returns the number of columns of the terminal.
   * <p/>
   * Convenience method.
   *
   * @return the number of columns as <tt>int</tt>.
   * @see BasicTerminalIO#getColumns()
   */
  public int getColumns();

  /**
   * Returns the number of rows of the terminal.
   * <p/>
   * Convenience method.
   *
   * @return the number of columns as <tt>int</tt>.
   * @see BasicTerminalIO#getRows()
   */
  public int getRows();

  /**
   * Returns the <tt>java.util.Locale</tt> of
   * this <tt>ShellIOImpl</tt>.
   * <p/>
   *
   * @return the <tt>java.util.Locale</tt>.
   */
  public Locale getLocale();

  /**
   * Sets the <tt>java.util.Locale</tt> of
   * this <tt>ShellIOImpl</tt>
   *
   * @param l <tt>java.util.Locale</tt> instance.
   */
  public void setLocale(Locale l);

  /**
   * Sets the language by index.
   * @param idx the index of the language.
   */
  public void setLanguageIndex(int idx);

  /**
   * Returns the index of the actual language.
   *
   * @return a language index as <tt>int</tt>.
   */
  public int getLanguageIndex();

  /**
   * Sets the terminal type.
   * <p/>
   * If the given type is not supported, the default terminal
   * type will be set (as configured).
   * <p/>
   * <em>WARNING:</em><br/>
   * This should be used with care; a negotiated terminal
   * type might be much more useful and correct.
   *
   * @param type the type of terminal to be set.
   * @throws IOException if an I/O error occurs.
   */
  public void setTerminalType(String type)
      throws IOException;

  /**
   * Tests if I/O should be done with bold colors.
   *
   * @return true if with bold colors, false otherwise.
   */
  public boolean isBold();

  /**
   * Sets the flag for bold color output.
   * <p/>
   *
   * @param bold true if bold color output, false otherwise.
   */
  public void setBold(boolean bold);

  /**
   * Sets the flag for asynchronous or synchronous event
   * output.
   * <p/>
   * If set, event outputs will be immediately written to
   * the terminal, or buffered otherwise.
   *
   * @param b true if asynchronous, false otherwise.
   */
  public void setAsyncEventIO(boolean b);

  /**
   * Tests the flag for asynchronous event output.
   * @return b true if asynchronous, false otherwise.
   */
  public boolean isAsyncEventIO();


  public boolean isAutoflushing();

  public void setAutoflushing(boolean b);


  public void print(String str) throws IOException;

  /**
   * Prints a newline.
   *
   * @throws IOException if an I/O error occurs.
   */
  public void println() throws IOException;

  public void println(String str) throws IOException;

  public void page(String str) throws IOException;

  /**
   * Prints or queues the given content.
   * <p/>
   * The given content will be printed immediately if
   * the event I/O is set to be asynchronous and
   * queue the content in the event buffer otherwise.
   *
   * @param content the content.
   * @throws IOException if an I/O error occurs.
   */
  public void printEvent(String content) throws IOException;

  /**
   * Tests if there are events in the buffer.
   *
   * @return true if there are events in the buffer, false otherwise.
   */
  public boolean hasEventsInBuffer();

  /**
   * Returns the number of events stored in the buffer.
   *
   * @return the number of events stored in the buffer.
   */
  public int getEventsInBufferCount();

  /**
   * Flushes buffered event outputs.
   *
   * @throws IOException if an I/O error occurs.
   */
  public void flushEventBuffer() throws IOException;

  public void flush() throws IOException;

  public void indicateProgress() throws IOException;

  public void scrollLines(int num) throws IOException;

  /**
   * Blanks out everything on the terminal.
   *
   * @throws IOException if an I/O error occurs.
   */
  public void blankTerminal()
      throws IOException;

  /**
   * Get a YES/NO decision, accepting only the respective
   * yes and no denoting characters as input.
   *
   * @return true if yes, false otherwise.
   * @throws IOException if an I/O error occurs.
   */
  public boolean getDecision() throws IOException;

  /**
   * Prompts for a key with the given prompt.
   *
   * @param promptKey the key of the prompt to be formatted.
   * @param attr      the {@link TemplateAttributes} for formatting.
   * @return the read character.
   * @throws IOException if an I/O error occurs.
   */
  public int prompt(String promptKey, TemplateAttributes attr) throws IOException;

  /**
   * Prompts for a key but may suppress reprinting the prompt message.
   *
   * @param promptKey the key of the prompt to be formatted.
   * @param attr      the {@link TemplateAttributes} for formatting.
   * @param supress   true if suppress prompting, else print.
   * @return the read character.
   * @throws IOException if an I/O error occurs.
   */
  public int prompt(String promptKey, TemplateAttributes attr, boolean supress)
      throws IOException;

  /**
   * Print a formatted template.
   *
   * @param key a <tt>String</tt> representing the template's key.
   * @throws IOException if an I/O error occurs.
   */
  public void printTemplate(String key) throws IOException;

  /**
   * Print a formatted template with the given
   * attributes.
   *
   * @param key  <tt>String</tt> representing the template's key.
   * @param attr the attributes to fill the "holes" in the template.
   * @throws IOException if an I/O error occurs.
   */
  public void printTemplate(String key, TemplateAttributes attr) throws IOException;

  /**
   * Convenience method that will print a template with a given
   * attribute.
   *
   * @param key     the template key.
   * @param attrkey the attribute key.
   * @param attrval the attribute value.
   * @throws IOException if an I/O error occurs.
   */
  public void printTemplate(String key, String attrkey, String attrval)
      throws IOException;

  /**
   * Convenience method that will print a template with two given
   * attributes.
   *
   * @param key      the template key.
   * @param attrkey  the attribute key.
   * @param attrval  the attribute value.
   * @param attr2key the attribute key.
   * @param attr2val the attribute value.
   * @throws IOException if an I/O error occurs.
   */
  public void printTemplate(String key, String attrkey, String attrval, String attr2key, String attr2val)
      throws IOException;

  /**
   * Page a formatted template.
   *
   * @param key a <tt>String</tt> representing the template's key.
   * @throws IOException if an I/O error occurs.
   */
  public void pageTemplate(String key) throws IOException;

  /**
   * Page a formatted template with the given
   * attributes.
   *
   * @param key  <tt>String</tt> representing the template's key.
   * @param attr the attributes to fill the "holes" in the template.
   * @throws IOException if an I/O error occurs.
   */
  public void pageTemplate(String key, TemplateAttributes attr) throws IOException;

  /**
   * Page a formatted template with the given
   * attributes without wrapping to terminal width.
   *
   * @param key  <tt>String</tt> representing the template's key.
   * @param attr the attributes to fill the "holes" in the template.
   * @param fixwidth flags a preformatted template (to terminal width),
   *        to avoid pager wrapping mechanisms.
   * @throws IOException if an I/O error occurs.
   */
  public void pageTemplate(String key, TemplateAttributes attr, boolean fixwidth) throws IOException;
  
  /**
   * Convenience method that will format a message with
   * the given template key and attributes, in the actual
   * I/O language.
   *
   * @param key  <tt>String</tt> representing the template's key.
   * @param attr the attributes to fill the "holes" in the template.
   * @return the formatted String.
   * @throws IOException if an I/O error occurs.
   */
  public String formatTemplate(String key, TemplateAttributes attr) throws IOException;

  /**
   * Convenience method that will format a message with
   * the given template key in the actual
   * I/O language.
   *
   * @param key <tt>String</tt> representing the template's key.
   * @return the formatted String.
   * @throws IOException if an I/O error occurs.
   */
  public String formatTemplate(String key) throws IOException;

  /**
   * Convenience method that will format a template with a given
   * attribute.
   *
   * @param key     the template key.
   * @param attrkey the attribute key.
   * @param attrval the attribute value.
   * @return the formatted String.
   * @throws IOException if an I/O error occurs.
   */
  public String formatTemplate(String key, String attrkey, String attrval)
      throws IOException;

  /**
   * Convenience method that will format a template with a given
   * attribute.
   *
   * @param key      the template key.
   * @param attrkey  the attribute key.
   * @param attrval  the attribute value.
   * @param attr2key the attribute key.
   * @param attr2val the attribute value.
   * @return the formatted String.
   * @throws IOException if an I/O error occurs.
   */
  public String formatTemplate(String key, String attrkey, String attrval, String attr2key, String attr2val)
      throws IOException;


  /**
   * Convenience method that will format a given string with
   * a given style.
   *
   * @param key <tt>String</tt> representing the template's key.
   * @param str the <tt>String</tt> to be formatted with the style.
   * @return the formatted String.
   * @throws IOException if an I/O error occurs.
   */
  public String formatStyle(String key, String str) throws IOException;

  /**
   * Leases a {@link TemplateAttributes} instance.
   *
   * @return a leased {@link TemplateAttributes} instance.
   */
  public TemplateAttributes leaseTemplateAttributes();

  /**
   * Returns all templates formatted for all available locales.
   *
   * @param key the key of the template.
   * @return a <tt>String[]</tt> with the formatted templates.
   */
  public String[] formatAllTemplates(String key);

  /**
   * Reads a character from the terminal I/O.
   *
   * @return the character read from stream.
   * @throws IOException if an I/O error occurs or end of stream is detected.
   */  
  public int read() throws IOException;

}//interface ShellIO
