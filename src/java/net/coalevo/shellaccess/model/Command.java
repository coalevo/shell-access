/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.model;

import net.coalevo.foundation.model.Identifiable;

/**
 * This interface defines the contract for a <tt>Command</tt>
 * instance.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface Command
    extends Identifiable {

  /**
   * Returns the public unique identifier of
   * this <tt>Command</tt>.
   *
   * @return the unique identifier as <tt>String</tt>.
   */
  public String getIdentifier();

  /**
   * Runs this <tt>Command</tt>, passing a parameter object
   * if required.
   *
   * @param shell the actual {@link BasicShell} instance.
   */
  public void run(BasicShell shell) throws CommandException;

  /**
   * Resets this <tt>Command</tt> for reuse.
   * The implementation of this method should clean
   * up all resources used for system, so that
   * the command is ready for being reused.
   */
  public void reset();

}//interface Command
