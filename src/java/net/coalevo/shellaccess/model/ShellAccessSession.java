/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.model;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.model.Session;
import net.coalevo.foundation.model.UserAgent;
import net.coalevo.security.model.AuthenticationException;
import net.coalevo.presence.model.PresenceStatusType;

import java.net.InetAddress;
import java.util.Set;
import java.util.TimeZone;

/**
 * Interface defining a <tt>ShellAccessSession</tt>.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface ShellAccessSession {

  /**
   * Login to the Coalevo Framework.
   *
   * @param username the username.
   * @param password the password.
   * @throws AuthenticationException if authentication fails.
   */
  public void login(String username, String password)
      throws AuthenticationException;

  /**
   * Indicate session activity.
   */
  public void activity();

  /**
   * Invalidates this session.
   * <p/>
   * If logged in, this will also invalidate the login/authentication.
   * </p>
   */
  public void invalidate();

  /**
   * Tests if this session is logged in.
   *
   * @return true if logged in, false otherwise.
   */
  public boolean isLoggedIn();

  /**
   * Returns the coalevo session that is handled by the
   * {@link net.coalevo.system.service.SessionService}.
   *
   * @return the associated {@link Session} or null.
   */
  public Session getCoalevoSession();

  /**
   * Returns the associated {@link UserAgent}.
   *
   * @return a {@link UserAgent} instance.
   */
  public UserAgent getUserAgent();

  /**
   * Returns the actual doing.
   *
   * @return the doing as String.
   */
  public String getDoing();

  /**
   * Sets the actual doing.
   * @param str the doing.
   */
  public void setDoing(String str);

  /**
   * Returns the username of this session.
   *
   * @return the username as <tt>String</tt>.
   */
  public String getUsername();

  /**
   * Tests if the session is in expert mode.
   *
   * @return true if expert, false otherwise.
   */
  public boolean isExpert();

  /**
   * Sets the expert flag of the session.
   * @param b true if expert, false otherwise.
   */
  public void setExpert(boolean b);

  /**
   * Tests if the session should print out events
   * while reading in links or entries.
   * @return b true if events while reading, false otherwise.
   */
  public boolean isEventsWhileReading();

  /**
   * Sets the events while reading flag of the session.
   * @param b true if events while reading, false otherwise.
   */
  public void setEventsWhileReading(boolean b);

  /**
   * Returns the session's boss key prompt.
   * @return the boss key prompt of the actual session.
   */
  public String getBossKeyPrompt();

  /**
   * Sets the boss key prompt of the actual session.
   * @param bossKeyPrompt the prompt string.
   */
  public void setBossKeyPrompt(String bossKeyPrompt);

  /**
   * Sets the auto away time of the actual session.
   * @param millis the number of milliseconds that make up the auto awaytime.
   */
  public void setAutoAwayTime(long millis);

  /**
   * Returns the auto away time of the actual session.
   * @return the auto away time in milliseconds.
   */
  public long getAutoAwayTime();

   /**
   * Tests if the session is away mode.
   *
   * @return true if away, false otherwise.
   */
  public boolean isAway();

  /**
   * Sets the away flag of the session.
   * @param b true if away, false otherwise.
   */
  public void setAway(boolean b);

  /**
   * Returns the unmodifiable start time of this session.
   *
   * @return the start time of this session as long.
   */
  public long getStartTime();

  /**
   * Returns the hostname of the connection.
   *
   * @return the hostname as String.
   */
  public String getHost();

  /**
   * Sets the hostname of the connection.
   * <p/>
   * This can be any type of freeform string, probably length limited
   * to fit into the node-local who-list.
   * </p>
   *
   * @param host the actually set host name.
   */
  public void setHost(String host);

  /**
   * Tests if this session is in host to country mode.
   * @return true if in host to country mode, false otherwise.
   */
  public boolean isHostToCountry();

  /**
   * Sets the sessions host to country mode.
   * @param b true if to be the country, false otherwise.
   */
  public void setHostToCountry(boolean b);

  /**
   * Returns the {@link PresenceStatusType} to be set when becoming present.
   *
   * @return a {@link PresenceStatusType}.
   */
  public PresenceStatusType getLoginPresenceStatusType();

  /**
   * Sets the {@link PresenceStatusType} to be set when becoming present.
   *
   * @param lpst a {@link PresenceStatusType}.
   */
  public void setLoginPresenceStatusType(PresenceStatusType lpst);

  /**
   * Returns the address of the connection.
   * @return the address of the connection.
   */
  public InetAddress getAddress();

  /**
   * Sets the address of the session.
   * @param addr the address.
   */
  public void setAddress(InetAddress addr);

  /**
   * Adds a flag for an agent that started writing a message to
   * the agent associated with this session.
   *
   * @param aid   the {@link AgentIdentifier} identifying the agent.
   * @param msgid the message identifier.
   */
  public void addMessaging(AgentIdentifier aid, String msgid);

  /**
   * Removes the flag for an agent that previously started writing
   * a message to the agent associated with this session.
   *
   * @param aid   the {@link AgentIdentifier} identifying the agent.
   * @param msgid the message identifier.
   */
  public void removeMessaging(AgentIdentifier aid, String msgid);

  /**
   * Tests if an identified agent is writing a message to the agent
   * associated with this session.
   *
   * @param aid the {@link AgentIdentifier} identifying the agent.
   * @return true if message was started, false otherwise.
   */
  public boolean isMessaging(AgentIdentifier aid);

  /**
   * Lists the agents that are currently writing a message to
   * the agent associated with this session.
   *
   * @return a <tt>Set</tt> of {@link AgentIdentifier} instances.
   */
  public Set listMessaging();

  /**
   * Tests if this session is displaying group message confirmations.
   * @return true if displaying, false otherwise.
   */
  public boolean isGroupConfirmations();

  /**
   * Sets the flag that determines if this session should display
   * group confirmations.
   * @param b true if confirmations are to be displayed, false otherwise.
   */
  public void setGroupConfirmations(boolean b);

  /**
   * Tests if this session is displaying message confirmations.
   * @return true if displaying, false otherwise.
   */
  public boolean isConfirmations();

  /**
   * Sets the flag that determines if this session should display
   * message confirmations.
   * @param b true if confirmations are to be displayed, false otherwise.
   */
  public void setConfirmations(boolean b);

  /**
   * Sets the flag that determines whether the sysguide status is active
   * or not.
   *
   * @param b true if active, false otherwise.
   */
  public void setSysguideActive(boolean b);

  /**
   * Tests if this session has active sysguide status.
   * @return true if active, false otherwise.
   */
  public boolean isSysguideActive();

  /**
   * Sets the flag that determines whether the group presence notification is active
   * or not.
   *
   * @param b true if notification active, false otherwise.
   */
  public void setGroupPresenceNotification(boolean b);

  /**
   * Tests if this session should do group presence notification.
   * @return true if notification active, false otherwise.
   */
  public boolean isGroupPresenceNotification();

  /**
   * Sets the flag that determines whether the presence notification is active
   * or not.
   *
   * @param b true if notification active, false otherwise.
   */
  public void setPresenceNotification(boolean b);

  /**
   * Tests if this session should do presence notification.
   * @return true if notification active, false otherwise.
   */
  public boolean isPresenceNotification();

  /**
   * Returns the timezone resolved for the connection of this
   * session.
   * @return a <tt>TimeZone</tt> corresponding to the client location.
   */
  public TimeZone getTimeZone();

  /**
   * Tests if the user of this session is a whiner.
   * @return true if whiner, false otherwise.
   */
  public boolean isWhiner();

  /**
   * Sets the whiner flag.
   * @param b true if the user of this session is a whiner, false otherwise.
   */
  public void setWhiner(boolean b);

  /**
   * Tests if this session should rebind DEL and BS.
   * @return true if rebind delete, false otherwise.
   */
  public boolean isRebindDelete();

  /**
   * Sets if this session should rebind DEL and BS.
   * @param b true if rebind delete, false otherwise.
   */
  public void setRebindDelete(boolean b);

  /**
   * Sets all groups to be automatically joined.
   * @param str the groups as comma separated list.
   */
  public void setAutoJoinGroups(String str);

  /**
   * Returns the set of all groups to be automatically joined.
   * @return a <tt>Set</tt> of <tt>String</tt> representing group names.
   */
  public Set<String> getAutoJoinGroups();

  /**
   * Tests if a given group is to be automatically joined.
   * @param str the groupname.
   * @return true if to be auto joined, false otherwise.
   */
  public boolean isAutoJoinGroup(String str);

}//class ShellAccessSession
