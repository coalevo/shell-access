/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.model;

import java.util.Iterator;


/**
 * Provides vital references for commands.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface BasicShell {

  /**
   * Starts a progress indicator.
   */
  public void startProgressIndicator();


  /**
   * Stops a progress indicator.
   */
  public void stopProgressIndicator();

  /**
   * Returns the {@link ShellAccessSession} of this
   * <tt>BasicShell</tt>.
   *
   * @return a {@link ShellAccessSession} instance.
   */
  public ShellAccessSession getSession();

  /**
   * Returns the {@link net.coalevo.shellaccess.impl.ShellIOImpl}
   * instance of the connection.
   *
   * @return the shell io.
   */
  public ShellIO getShellIO();

  /**
   * Returns a {@link Services} instance that mediates access
   * to framework provided services.
   *
   * @return {@link Services} instance.
   */
  public Services getServices();

  /**
   * Returns a variable from the shell's environment.
   * Note that when switching shells, the environment
   * variables will remain the same and accessible.
   *
   * @return the value of the variable as <tt>Object</tt>.
   */
  public Object getEnvironmentVariable(String key);

  /**
   * Sets a variable in the shell's environment.
   * Using a <tt>null</tt> value will unset the variable.
   *
   * @param key   the name of the variable as <tt>String</tt>.
   * @param value the value of the variable as <tt>Object</tt>.
   */
  public void setEnvironmentVariable(String key, Object value);

  /**
   * Unsets (e.g. removes) a variable from the shell's environment.
   *
   * @param key the name of the variable as <tt>String</tt>.
   */
  public void unsetEnvironmentVariable(String key);

  /**
   * Returns an <tt>Iterator</tt> over the set environment
   * variable names.
   *
   * @return an <tt>Iterator</tt> over the names of the variables
   *         set in the environment.
   */
  public Iterator environmentVariables();

  /**
   * Tests if a variable with a specific name is set.
   *
   * @param key the name of the variable as <tt>String</tt>.
   * @return true if set, false otherwise.
   */
  public boolean isEnviromentVariableSet(String key);

  public static final String PROMPT_ATTRIBUTES_KEY = "shell.promptattributes";

}//interface BasicShell
