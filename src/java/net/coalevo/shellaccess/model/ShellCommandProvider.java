/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.model;

/**
 * Defines a <tt>ShellCommandProvider</tt>, which is basically
 * a factory for commands within a certain namespace.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface ShellCommandProvider {

  /**
   * Returns the namespace of this <tt>ShellCommandProvider</tt>.
   *
   * @return the namespace as <tt>String</tt>.
   */
  public String getNamespace();

  /**
   * Tests if this <tt>ShellCommandProvider</tt> provides
   * the identified command within its namespace.
   *
   * @param cid the command identifier within the namespace.
   * @return true if provided, false otherwise.
   */
  public boolean provides(String cid);

  /**
   * Creates a new {@link Command} instance for the
   * given identifier.
   *
   * @param cid the command identifier within the namespace.
   * @return a newly create {@link Command} instance.
   */
  public Command createCommand(String cid) throws CommandException;

}//interface ShellCommandProvider
