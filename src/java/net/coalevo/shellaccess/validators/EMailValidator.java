/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.validators;

import java.util.regex.Pattern;

/**
 * Provides a string input validator that will
 * ensure that a String is not null and represents a valid email address.
 * <p/>
 * RegEx taken from http://www.regular-expressions.info/regexbuddy/email.html
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class EMailValidator
    extends RegExValidator {

  public EMailValidator() {
    m_Pattern = Pattern.compile(
        "\\b[A-Z0-9._%-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}\\b",
        Pattern.CASE_INSENSITIVE
    );
  }//constructor

  public EMailValidator(String errmsg) {
    m_Pattern = Pattern.compile(
        "\\b[A-Z0-9._%-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}\\b",
        Pattern.CASE_INSENSITIVE
    );
    m_Error = errmsg;
  }//constructor

}//class EMailValidator
