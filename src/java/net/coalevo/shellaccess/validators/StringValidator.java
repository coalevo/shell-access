/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.validators;

/**
 * Provides a string input validator that will
 * ensure that a String is not null and of a certain size.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class StringValidator
    extends BaseInputValidator {

  private int m_Size;

  /**
   * Constructs a new <tt>StringValidator</tt> that validates
   * strings that have at least one character.
   */
  public StringValidator() {
    this(1);
  }//constructor

  /**
   * Constructs a new <tt>StringValidator</tt> that validates
   * strings that have at least the given size.
   *
   * @param size the size as <tt>int</tt>.
   */
  public StringValidator(int size) {
    m_Size = size;
  }//constructor

  /**
   * Constructs a new <tt>StringValidator</tt> that validates
   * strings that have at least one character.
   *
   * @param errmsg the error message for invalid input.
   */
  public StringValidator(String errmsg) {
    this(1, errmsg);
  }//constructor

  /**
   * Constructs a new <tt>StringValidator</tt> that validates
   * strings that have at least the given size.
   *
   * @param size   the size as <tt>int</tt>.
   * @param errmsg the error message for invalid input.
   */
  public StringValidator(int size, String errmsg) {
    this(size);
    m_Error = errmsg;
  }//constructor

  public boolean validate(String string) {
    if (string == null || string.length() < m_Size) {
      return false;
    } else {
      return true;
    }
  }//validate

}//class StringValidator
