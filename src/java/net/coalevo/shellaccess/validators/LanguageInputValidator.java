/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.validators;

import net.wimpi.telnetd.io.toolkit.InputValidator;

import java.util.Arrays;
import java.util.Locale;


/**
 * Language {@link InputValidator}
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class LanguageInputValidator
    extends BaseInputValidator {

  private static final String[] c_Languages;

  static {
    c_Languages = Locale.getISOLanguages();
    Arrays.sort(c_Languages);
  }

  public LanguageInputValidator() {

  }//constructor

  public LanguageInputValidator(String errmsg) {
    m_Error = errmsg;
  }//constructor

  public boolean validate(String str) {
    return Arrays.binarySearch(c_Languages, str) >= 0;
  }//validate

}//class LanguageInputValidator
