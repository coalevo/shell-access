/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.validators;

import java.util.List;

/**
 * Provides an <tt>EMailValidator</tt> that validates
 * against a given <tt>List</tt> of <tt>String</tt> instances.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class StringListValidator
    extends BaseInputValidator {

  private List<String> m_Strings;

  public StringListValidator(List<String> strings) {
    m_Strings = strings;
  }//StringListValidator

  public boolean validate(String string) {
    if (!m_Strings.isEmpty()) {
      return m_Strings.contains(string);
    } else {
      return true;
    }
  }//validate

}//class StringListValidator
