/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.validators;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.shellaccess.model.Services;
import net.coalevo.userdata.service.UserdataService;

/**
 * This class implements a validator that will validate the input to
 * be a valid and new (i.e. not existing) agent identifier.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class NewAIDValidator
    extends BaseInputValidator {

  private Services m_Services;
  private String m_UDSError;
  private String m_SyntaxError;
  private String m_DuplicateError;

  public NewAIDValidator(Services s) {
    m_Services = s;
  }//constructor

  public NewAIDValidator(
      Services s,
      String udserr,
      String syntaxerr,
      String duplicaterr) {
    m_Services = s;
    m_UDSError = udserr;
    m_SyntaxError = syntaxerr;
    m_DuplicateError = duplicaterr;
  }//constructor

  public boolean validate(String s) {
    UserdataService ud = m_Services.getUserdataService(Services.NO_WAIT);
    if(ud == null) {
      m_Error = m_UDSError;
      return false;
    }
    if(!ud.isValidAgentIdentifier(m_Services.getServiceAgent(),s)) {
      m_Error = m_SyntaxError;
      return false;
    }
    if(ud.isUserdataAvailable(m_Services.getServiceAgent(), new AgentIdentifier(s))) {
      m_Error = m_DuplicateError;
      return false;
    }
    return true;
  }//validate

}//class NewAIDValidator