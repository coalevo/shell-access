/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.validators;

import net.wimpi.telnetd.io.toolkit.InputValidator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class implements a regular expression {@link InputValidator}
 * that will validate the input matching it against a regular expression.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class RegExValidator
    extends BaseInputValidator {

  protected Pattern m_Pattern;

  protected RegExValidator() {
  }//constructor

  public RegExValidator(String regex, String errmsg) {
    this(regex);
    m_Error = errmsg;
  }//constructor

  public RegExValidator(String regex) {
    m_Pattern = Pattern.compile(regex);
  }//constructor

  public boolean validate(String s) {
    Matcher m = m_Pattern.matcher(s);
    return m.matches();
  }//validate

}//class RegExValidator
