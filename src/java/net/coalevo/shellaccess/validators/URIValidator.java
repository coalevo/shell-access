/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.validators;


import java.net.URI;
import java.net.URISyntaxException;

/**
 * Provides a string input validator that will
 * ensure that a String is not null and represents a valid URL.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class URIValidator
    extends BaseInputValidator {

  private boolean m_Absolute;

  public URIValidator() {
    this(false);
  }//URIValdiator

  public URIValidator(boolean absolute) {
    m_Absolute = absolute;
  }//URIValidator

  public URIValidator(String errmsg) {
    this(false,errmsg);
  }//URIValidator

  public URIValidator(boolean absolute, String errmsg) {
    m_Error = errmsg;
    m_Absolute = absolute;
  }//URIValidator

  public boolean validate(String string) {
    if (string == null || string.length() == 0) {
      return false;
    } else {
      try {
        URI l = new URI(string);
        if (m_Absolute && !l.isAbsolute()) {
          throw new URISyntaxException(string, "not absolute");
        }
        return true;
      } catch (URISyntaxException usex) {
        return false;
      }
    }
  }//validate

}//class URIValidator
