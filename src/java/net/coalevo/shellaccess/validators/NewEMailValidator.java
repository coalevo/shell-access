/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.validators;

import net.coalevo.shellaccess.model.Services;
import net.coalevo.userdata.service.UserdataService;

import java.util.Arrays;

/**
 * Provides a string input validator that will
 * ensure that a String is not null and represents a valid email address.
 * <p/>
 * RegEx taken from http://www.regular-expressions.info/regexbuddy/email.html
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class NewEMailValidator
    extends EMailValidator {

  private Services m_Services;
  private String m_UDSError;
  private String m_SyntaxError;
  private String m_DuplicateError;
  private String m_AnonError;
  private String m_ActualEmail;

  private static final String[] ANON_MAIL = {
      "anonymouse.org",
      "despammed.com",
      "discardmail.com",
      "e4ward.com",
      "guerillamail.com",
      "mailexpire.com",
      "mailinator.com",
      "mytrashmail.com",
      "sneakemail.com",
      "spambog.com",
      "trashymail.com",
      "mt2009.com",
      "spammotel.com",
      "spamtrail.com",
      "mailinator2.com",
      "sogetthis.com",
      "mailin8r.com",
      "mailinator.net",
      "spamherelots.com",
      "thisisnotmyrealemail.com",
      "spamgourmet.com"
  };

  static {
    //sort, binary search will only work then
    Arrays.sort(ANON_MAIL);
  }//static initializer

  public NewEMailValidator(
      Services s,
      String udserr,
      String syntaxerr,
      String duplicaterr,
      String anonerr) {
    super();
    m_Services = s;
    m_UDSError = udserr;
    m_SyntaxError = syntaxerr;
    m_DuplicateError = duplicaterr;
    m_AnonError = anonerr;
  }//constructor

  public void setActualEmail(String s) {
    m_ActualEmail = s;
  }//setActualEmail

  public boolean validate(String str) {
    UserdataService ud = m_Services.getUserdataService(Services.NO_WAIT);
    if(ud == null) {
      m_Error = m_UDSError;
      return false;
    }
    if(!super.validate(str)) {
      m_Error = m_SyntaxError;
      return false;
    }
    //try match against given
    if(Arrays.binarySearch(ANON_MAIL,str.substring(str.indexOf('@')+1).toLowerCase()) >= 0) {
      m_Error = m_AnonError;
      return false;
    }
    if(m_ActualEmail != null && m_ActualEmail.equals(str)) {
      return true;
    }
    if(ud.existsEmail(m_Services.getServiceAgent(), str)) {
      m_Error = m_DuplicateError;
      return false;
    }
    return true;
  }//validate


  
}//class EMailValidator