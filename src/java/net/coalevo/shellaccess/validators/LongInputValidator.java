/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.validators;

import net.wimpi.telnetd.io.toolkit.InputValidator;

/**
 * Implements an {@link InputValidator} that will
 * only accept integers within a given input range.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class LongInputValidator
    extends BaseInputValidator {

  private long m_UpperLimit;
  private long m_LowerLimit;

  public LongInputValidator() {
    this(Long.MIN_VALUE, Long.MAX_VALUE);
  }//constructor

  public LongInputValidator(long lowerlimit, long upperlimit) {
    m_LowerLimit = lowerlimit;
    m_UpperLimit = upperlimit;
  }//constructor

  public LongInputValidator(String errmsg) {
    this(Long.MIN_VALUE, Long.MAX_VALUE, errmsg);
  }//constructor

  public LongInputValidator(long lowerlimit, long upperlimit, String errmsg) {
    m_LowerLimit = lowerlimit;
    m_UpperLimit = upperlimit;
    m_Error = errmsg;
  }//constructor

  public boolean validate(String string) {
    if (string == null) {
      return false;
    }
    string = string.trim();
    try {
      long i = Long.parseLong(string);
      return (i >= m_LowerLimit && i <= m_UpperLimit);
    } catch (NumberFormatException ex) {
      return false;
    }
  }//validate

}//class LongInputValidator