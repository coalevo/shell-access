/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.validators;

import net.coalevo.shellaccess.model.Services;
import net.coalevo.userdata.service.UserdataService;

/**
 * This class implements a validator that will validate the input to
 * be either a valid nickname or a valid agent identifier.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class AIDNickValidator
    extends BaseInputValidator {

  private Services m_Services;
  private boolean m_AllowEmpty;

  public AIDNickValidator(Services s) {
    m_Services = s;
  }//constructor

  public AIDNickValidator(Services s, String error) {
    m_Services = s;
    m_Error = error;
    m_AllowEmpty = false;
  }//constructor

  public AIDNickValidator(Services s, String error, boolean allowempty) {
    m_Services = s;
    m_Error = error;
    m_AllowEmpty = allowempty;
  }//constructor

  public boolean validate(String s) {
    if(m_AllowEmpty && (s == null || s.length() == 0) || s.contains(".service.")) {
      return true;
    }
    UserdataService ud = m_Services.getUserdataService(Services.NO_WAIT);
    return ud == null || (ud.isValidAgentIdentifier(m_Services.getServiceAgent(), s) ||
        ud.isValidNickname(m_Services.getServiceAgent(), s));
  }//validate

}//class AIDNickValidator
