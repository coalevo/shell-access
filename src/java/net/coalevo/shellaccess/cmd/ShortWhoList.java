/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd;

/**
 * Provides a node-local short who list.
 * <p/>
 * The actual implementation is in the super class.
 * This is just a tagging class for the who list type preference.
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class ShortWhoList
    extends WhoListCommand {

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

}//class ShortWhoList
