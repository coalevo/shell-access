/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd;

import net.coalevo.shellaccess.model.BaseShellCommandProvider;

/**
 * Extends {@link BaseShellCommandProvider} for the base package
 * commands.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class ShellCommandProviderImpl
    extends BaseShellCommandProvider {

  public ShellCommandProviderImpl() {
    super(ShellCommandProviderImpl.class.getPackage().getName());
    prepare();
  }//constructor

  private void prepare() {
    addCommand("DetailedWhoList", DetailedWhoList.class);
    addCommand("ShortWhoList", ShortWhoList.class);
    addCommand("ExitMenu", ExitMenu.class);
    addCommand("Logout", Logout.class);
    addCommand("Login", Login.class);
    addCommand("MenuHelp", MenuHelp.class);
    addCommand("ShowDateTime", ShowDateTime.class);
    addCommand("ShowUptime", ShowUptime.class);
    addCommand("CommandHelp",CommandHelp.class);
    addCommand("BossKey",BossKey.class);

  }//prepare


}//class ShellCommandProviderImpl
