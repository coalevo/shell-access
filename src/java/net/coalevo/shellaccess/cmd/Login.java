/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd;

import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.shellaccess.model.Services;
import net.coalevo.shellaccess.service.ShellAccessService;
import net.coalevo.shellaccess.impl.RoleUtility;
import net.coalevo.statistics.model.Counter;
import net.coalevo.statistics.model.NoSuchCounterException;
import net.coalevo.statistics.model.StatisticsServiceException;
import net.coalevo.statistics.service.StatisticsService;
import net.wimpi.telnetd.io.TemplateAttributes;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * Provides a login command that may do simple login tasks, like
 * keeping track of a login counter and printing some info related
 * to the login.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class Login
    extends BaseCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(Login.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell)
      throws CommandException {
    prepare(shell);
    try {
      //login stats and online users
      long logins = doLoginCount();
      if(logins >= 0) {
        TemplateAttributes attr = m_IO.leaseTemplateAttributes();
        attr.add("numlogins",Long.toString(logins));
        int sc = m_Shell.getServices().getSessionManager().getSessionCount();
        if(sc > 1) {
          attr.add("numonline",Integer.toString(sc));
        }
        m_IO.printTemplate(PREFIX + "logins",attr);
      }
      m_IO.printTemplate(PREFIX + "roles", "roles", RoleUtility.getRolesWithStyle(
          getRoles(m_AgentIdentifier)));
    } catch (Exception ex) {
      log.error(c_LogMarker,"run()", ex);
    }

  }//run

  private long doLoginCount() {
    //login stats
    Services s = m_Shell.getServices();
    StatisticsService stats = s.getStatisticsService(0);
    Counter c = null;
    if (stats != null) {
      try {
        try {
          c = stats.getAgentBoundCounter(
              s.getServiceAgent(), ShellAccessService.LOGIN_COUNTER_ID, m_AgentIdentifier);
        } catch (NoSuchCounterException nsc) {
          log.debug(c_LogMarker,"doLoginCount()::Attempting to create");
          c = stats.createAgentBoundCounter(s.getServiceAgent(), ShellAccessService.LOGIN_COUNTER_ID, m_AgentIdentifier);
        }
      } catch (StatisticsServiceException ex) {
        log.error(c_LogMarker,"doLoginCount()", ex);
      }
    }
    if(c!=null) {
      return c.increment();
    }
    return -1;
  }//doLoginCount


  private static final String PREFIX = "commands_net.coalevo.shellaccess.cmd.Login_";

}//class Login
