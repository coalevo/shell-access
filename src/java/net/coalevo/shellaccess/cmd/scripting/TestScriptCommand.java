/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.scripting;

import net.coalevo.shellaccess.model.Command;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.shellaccess.model.ShellIO;
import net.coalevo.shellaccess.command.CommandShellManager;
import net.coalevo.shellaccess.impl.Activator;
import net.coalevo.shellaccess.cmd.BaseCommand;
import net.wimpi.telnetd.io.BasicTerminalIO;
import net.wimpi.telnetd.io.toolkit.Editfield;
import net.wimpi.telnetd.io.toolkit.InputFilter;

import java.io.IOException;

import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * Provides a command that allows to test run a given
 * script command.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class TestScriptCommand
    extends BaseCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(TestScriptCommand.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell) throws CommandException {
    prepare(shell);
    CommandShellManager csmgr = Activator.getServices().getCommandShellManager();

    try {
      m_PromptWrap = false;
      String id = prompt(PREFIX + "idlabel");
      if (id == null || id.length() == 0) {
        m_IO.printTemplate(PREFIX + "aborted");
        return;
      }
      m_IO.println();
      //make sure it can be edited
      if (!csmgr.isScriptCommand(id)) {
        m_IO.printTemplate(PREFIX + "notscript");
      } else {
        Command cmd = csmgr.getScriptCommand(id);
        if(cmd == null) {
          m_IO.printTemplate(PREFIX + "nonexistant");
        } else {
          m_IO.printTemplate(PREFIX + "run");
          cmd.run(shell);
        }
        m_IO.printTemplate(PREFIX + "done");
      }
    } catch (Exception ex) {
      try {
        m_IO.printTemplate(PREFIX + "failed");
      } catch (IOException iex) {
        Activator.log().error(c_LogMarker,"run(BaseShell)", iex);
      }
      Activator.log().error(c_LogMarker,"run(BaseShell)", ex);
      throw new CommandException(ex.getMessage());
    }
  }//run

  public void reset() {
  }//reset

  private static final String PREFIX = "commands_net.coalevo.shellaccess.cmd.scripting.TestScriptCommand_";

}//class TestScriptCommand
