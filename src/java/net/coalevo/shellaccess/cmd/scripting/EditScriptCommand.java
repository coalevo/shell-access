/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.scripting;

import net.coalevo.shellaccess.cmd.admin.AdminCommand;
import net.coalevo.shellaccess.command.CommandShellManager;
import net.coalevo.shellaccess.impl.Activator;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.IOException;

/**
 * This class implements a command for editing
 * a <tt>ScriptCommand</tt>.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class EditScriptCommand
    extends AdminCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(EditScriptCommand.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell) throws CommandException {
    prepare(shell);
    CommandShellManager csmgr = Activator.getServices().getCommandShellManager();

    try {
      m_PromptWrap = false;
      String id = prompt(PREFIX + "idlabel");
      if (id == null || id.length() == 0) {
        m_IO.printTemplate(PREFIX + "aborted");
        return;
      }

      //make sure it can be edited
      if (!csmgr.isScriptCommand(id)) {
        if (promptDecision(PREFIX + "nonexistant")) {
          String source = editTransformableContent("", "");
          csmgr.saveScript(id, source);
          m_IO.printTemplate(PREFIX + "done");
        }
      } else {
        String source = csmgr.loadScript(id);
        editTransformableContent("", source);
        csmgr.saveScript(id, source);
        m_IO.printTemplate(PREFIX + "done");
      }
    } catch (Exception ex) {
      try {
        m_IO.printTemplate(PREFIX + "failed");
      } catch (IOException iex) {
        Activator.log().error(c_LogMarker, "run(BaseShell)", iex);
      }
      Activator.log().error(c_LogMarker, "run(BaseShell)", ex);
      throw new CommandException(ex.getMessage());
    }
  }//run

  public void reset() {
    //Clean up local resources
  }//reset

  private static final String PREFIX = "commands_net.coalevo.shellaccess.cmd.scripting.EditScriptCommand_";

}//class EditScriptCommand
