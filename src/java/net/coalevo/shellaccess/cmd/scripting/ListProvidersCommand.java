/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.scripting;

import net.coalevo.shellaccess.model.Command;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.shellaccess.model.ShellIO;
import net.coalevo.shellaccess.command.CommandShellManager;
import net.coalevo.shellaccess.impl.Activator;
import net.wimpi.telnetd.io.TemplateAttributes;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.IOException;
import java.util.Iterator;

/**
 * Provides a command that allows to list all existing script command
 * providers in a given namespace.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class ListProvidersCommand
    implements Command {

  private static Marker c_LogMarker = MarkerFactory.getMarker(ListProvidersCommand.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell) throws CommandException {
    ShellIO io = shell.getShellIO();
    CommandShellManager csmgr = Activator.getServices().getCommandShellManager();

    try {
      TemplateAttributes attr = io.leaseTemplateAttributes();
      Iterator iter = csmgr.getCommandProviderManager().listAvailableScriptProviders();
      while(iter.hasNext()) {
        attr.add("namespace", (String)iter.next());
      }
      io.pageTemplate(ListProvidersCommand.PREFIX + "list", attr);
    } catch (Exception ex) {
      try {
        io.printTemplate(ListProvidersCommand.PREFIX + "failed");
      } catch (IOException iex) {
        Activator.log().error(c_LogMarker,"run(BaseShell)", iex);
      }
      Activator.log().error(c_LogMarker,"run(BaseShell)", ex);
      throw new CommandException(ex.getMessage());
    }
  }//run

  public void reset() {
  }//reset

  private static final String PREFIX = "commands_net.coalevo.shellaccess.cmd.scripting.ListProvidersCommand_";

}//class ListProvidersCommand
