/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.scripting;

import net.coalevo.shellaccess.cmd.BaseCommand;
import net.coalevo.shellaccess.command.CommandShellManager;
import net.coalevo.shellaccess.impl.Activator;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.wimpi.telnetd.io.TemplateAttributes;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.IOException;

/**
 * Provides a command that allows to list all existing script commands
 * in a given namespace.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class ListScriptsCommand
    extends BaseCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(ListScriptsCommand.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell) throws CommandException {
    prepare(shell);

    CommandShellManager csmgr = Activator.getServices().getCommandShellManager();

    try {
      m_PromptWrap = false;
      String str = prompt(PREFIX + "nslabel");
      if (str == null || str.length() == 0) {
        m_IO.printTemplate(PREFIX + "aborted");
        return;
      }
      String[] names = csmgr.listScriptCommandNames(str);

      TemplateAttributes attr = m_IO.leaseTemplateAttributes();
      attr.add("namespace", str);
      for (int i = 0; i < names.length; i++) {
        attr.add("cid", names[i]);
      }
      m_IO.pageTemplate(PREFIX + "list", attr);

    } catch (Exception ex) {
      try {
        m_IO.printTemplate(ListScriptsCommand.PREFIX + "failed");
      } catch (IOException iex) {
        Activator.log().error(c_LogMarker, "run(BaseShell)", iex);
      }
      Activator.log().error(c_LogMarker, "run(BaseShell)", ex);
      throw new CommandException(ex.getMessage());
    }
  }//run

  public void reset() {
  }//reset

  private static final String PREFIX = "commands_net.coalevo.shellaccess.cmd.scripting.ListScriptsCommand_";

}//class ListScriptsCommand
