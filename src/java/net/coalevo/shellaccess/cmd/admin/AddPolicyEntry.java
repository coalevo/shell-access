/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.admin;

import net.coalevo.foundation.model.Action;
import net.coalevo.security.model.Policy;
import net.coalevo.security.service.PolicyService;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.IOException;

/**
 * This class implements a command that allows to add a policy entry.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class AddPolicyEntry
    extends EditPolicyEntry {

  private static Marker c_LogMarker = MarkerFactory.getMarker(AddPolicyEntry.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell)
      throws CommandException {
    prepare(shell);
    try {
      try {
        PolicyService ps = getPolicyService();
        if (ps == null) {
          m_IO.printTemplate(PREFIX + "serviceunavailable");
          return;
        }
        String policy = selectPolicy(ps);
        if (policy == null) {
          return;
        }

        Policy p = ps.leasePolicy(m_Agent, policy);
        try {
          String entryname = prompt(PREFIX + "entryprompt");
          if (entryname == null || entryname.length() == 0) {
            m_IO.printTemplate(PREFIX + "aborted");
            return;
          }
     
          Action a = new Action(entryname);
          editEntry(ps, p, a);
        } finally {
          ps.releasePolicy(p);
        }
      } catch (SecurityException secex) {
        log.error(c_LogMarker, "run()", secex);
        m_IO.printTemplate(PREFIX + "notallowed");
      }
    } catch (IOException ex) {
      log.error(c_LogMarker, "run()", ex);
    }
  }//run

  protected static final String PREFIX = TS_PREFIX + "AddPolicyEntry_";

}//class AddPolicyEntry
