/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.admin;

import net.coalevo.foundation.model.Action;
import net.coalevo.security.model.AuthorizationRule;
import net.coalevo.security.model.Policy;
import net.coalevo.security.model.SARLException;
import net.coalevo.security.service.PolicyService;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.wimpi.telnetd.io.TemplateAttributes;
import net.wimpi.telnetd.io.toolkit.MaskedSelection;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.IOException;
import java.util.Set;
import java.util.TreeSet;

/**
 * This class implements a command that allows to edit a policy entry.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class EditPolicyEntry
    extends AdminCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(EditPolicyEntry.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell)
      throws CommandException {
    prepare(shell);
    try {
      try {
        PolicyService ps = getPolicyService();
        if (ps == null) {
          m_IO.printTemplate(PREFIX + "serviceunavailable");
          return;
        }
        String policy = selectPolicy(ps);
        if (policy == null) {
          return;
        }

        Policy p = ps.leasePolicy(m_Agent, policy);
        try {
          Action a = selectEntry(p);
          if (a == null) {
            return;
          }
          editEntry(ps, p, a);
        } finally {
          ps.releasePolicy(p);
        }
      } catch (SecurityException secex) {
        log.error(c_LogMarker, "run()", secex);
        m_IO.printTemplate(PREFIX + "notallowed");
      }
    } catch (IOException ex) {
      log.error(c_LogMarker, "run()", ex);
    }
  }//run

  protected String selectPolicy(PolicyService ps)
      throws IOException, SecurityException {

    //2. prepare destroyable policies
    Set<String> policies = ps.listAvailablePolicies();
    TreeSet<String> selectables = new TreeSet<String>();
    selectables.addAll(policies);
    if (selectables.isEmpty()) {
      m_IO.printTemplate(PREFIX + "nopolicies");
      return null;
    }

    //2. prompt for selection
    m_PromptWrap = false;
    m_IO.printTemplate(PREFIX + "policyselect");
    MaskedSelection s = new MaskedSelection(m_IO.getTerminalIO(), "ms");
    String none = m_IO.formatTemplate(PREFIX + "noneoption");
    s.addOption(NONE, none);
    for (String p : selectables) {
      s.addOption(p, p);
    }
    s.run();
    Object o = s.getSelectedValue();
    if (NONE.equals(o)) {
      m_IO.printTemplate(PREFIX + "aborted");
      return null;
    } else {
      return (String) o;
    }
  }//selectPolicy

  protected Action selectEntry(Policy p)
      throws IOException, SecurityException {

    Set<Action> actions = (Set<Action>) p.getActions();
    if (actions.isEmpty()) {
      m_IO.printTemplate(PREFIX + "noentries");
      return null;
    }
    m_IO.printTemplate(PREFIX + "entryselect");
    MaskedSelection s = new MaskedSelection(m_IO.getTerminalIO(), "ms");
    String none = m_IO.formatTemplate(PREFIX + "noneoption");
    s.addOption(NONE, none);
    for (Action a : actions) {
      s.addOption(a, a.getIdentifier());
    }
    s.run();
    Object o = s.getSelectedValue();
    if (NONE.equals(o)) {
      m_IO.printTemplate(PREFIX + "aborted");
      return null;
    } else {
      return (Action) o;
    }
  }//selectEntry

  protected void editEntry(PolicyService ps, Policy p, Action a)
      throws IOException, SecurityException {


    AuthorizationRule r = p.getRuleFor(a);
    String rule = ps.getBaseAuthorizationRule().toSARL();
    if(r != null) {
      rule = r.toSARL();
    }
    boolean done = false;
    try {
      while (!done) {
        rule = editTransformableContent("",rule);
        try {
          r = ps.createAuthorizationRule(rule);
          done = true;
        } catch (SARLException e) {
          m_IO.printTemplate(PREFIX + "invalidsarl");
          if (!promptDecision(PREFIX + "docontinue")) {
            m_IO.printTemplate(PREFIX + "aborted");
            return;
          }
        }
      }
    } catch (Exception ex) {
      //should not happen
    }
    m_IO.println();
    if (!promptDecision(PREFIX + "doit")) {
      m_IO.printTemplate(PREFIX + "aborted");
      return;
    }
    if (ps.putPolicyEntry(m_Agent, p, a, r)) {
      logPolicyEntry(m_AgentIdentifier.getIdentifier(), p.getIdentifier(), a.getIdentifier(), rule);
      TemplateAttributes attr = m_IO.leaseTemplateAttributes();
      attr.add("policy", p.getIdentifier());
      attr.add("entry", a.getIdentifier());
      attr.add("rule", rule);
      m_IO.formatTemplate(PREFIX + "confirm", attr);
    } else {
      m_IO.printTemplate(PREFIX + "failed");
    }
  }//editEntry

  protected static final String PREFIX = TS_PREFIX + "EditPolicyEntry_";

}//class EditPolicyEntry