/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.admin;

import net.coalevo.security.model.Permission;
import net.coalevo.security.model.Role;
import net.coalevo.security.model.Permit;
import net.coalevo.security.service.SecurityManagementService;
import net.coalevo.shellaccess.impl.RoleUtility;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.wimpi.telnetd.io.toolkit.MaskedSelection;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.IOException;
import java.util.Set;
import java.util.TreeSet;

/**
 * This class implements a command that removes a role permission.
 * <p/>
 * Note that a permission may either be a permit or a deny.
 * 
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class RemoveRolePermission
    extends AdminCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(RemoveRolePermission.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell)
      throws CommandException {
    prepare(shell);
    try {
      try {
        SecurityManagementService sms = getSecurityManagementService();
        if (sms == null) {
          m_IO.printTemplate(PREFIX + "serviceunavailable");
          return;
        }

        //1. prepare selectable roles
        Set<Role> roles = sms.getRoles(m_Agent);
        TreeSet<Role> selectables = new TreeSet<Role>(ROLE_COMP);
        selectables.addAll(roles);
        if(selectables.isEmpty()) {
          m_IO.printTemplate(PREFIX + "noroles");
          return;
        }
        //2. prompt for selection
        m_PromptWrap = false;
        m_IO.printTemplate(PREFIX + "roleselect");
        MaskedSelection s = new MaskedSelection(m_IO.getTerminalIO(), "ms");
        String none = m_IO.formatTemplate(PREFIX + "noneoption");
        s.addOption(NONE, none);
        for (Role r : selectables) {
          s.addOption(r, RoleUtility.getRoleWithStyle(r));
        }
        s.run();
        Object o = s.getSelectedValue();
        if (NONE.equals(o)) {
          m_IO.printTemplate(PREFIX + "aborted");
          return;
        } else {
          Role r = (Role) o;
          //3. prompt for addable permission to add
          Set<Permission> roleperms = sms.getRolePermissions(m_Agent, r);
          TreeSet<Permission> permissions = new TreeSet<Permission>(PERM_COMP);
          permissions.addAll(roleperms);
          if(permissions.isEmpty()) {
            m_IO.printTemplate(PREFIX + "nopermissions");
            return;
          }
          m_IO.printTemplate(PREFIX + "permissionselect");
          s = new MaskedSelection(m_IO.getTerminalIO(), "ms");
          s.addOption(NONE, none);
          for (Permission p : permissions) {
            s.addOption(
                p,
                m_IO.formatTemplate(
                    PREFIX + ((p instanceof Permit)? "permit":"deny"),
                    "perm",
                    p.getIdentifier()
                )
            );
          }
          s.run();
          o = s.getSelectedValue();
          if (NONE.equals(o)) {
            m_IO.printTemplate(PREFIX + "aborted");
            return;
          } else {
            Permission p = (Permission) o;
            if (sms.removeRolePermission(m_Agent, r, p)) {
              logPermissionRevoke(m_AgentIdentifier.getIdentifier(),r.getIdentifier(),p.toString());              
              m_IO.formatTemplate(
                  PREFIX + "confirm",
                  "role", RoleUtility.getRoleWithStyle(r),
                  "perm", p.toString()
              );
            } else {
              m_IO.printTemplate(PREFIX + "failed");
            }
          }
        }
      } catch (SecurityException secex) {
        log.error(c_LogMarker, "run()", secex);
        m_IO.printTemplate(PREFIX + "notallowed");
      }
    } catch (IOException ex) {
      log.error(c_LogMarker, "run()", ex);
    }
  }//run

  protected static final String PREFIX = TS_PREFIX + "RemoveRolePermission_";

}//class RemoveRolePermission