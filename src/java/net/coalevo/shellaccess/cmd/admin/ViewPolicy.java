/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.admin;

import net.coalevo.security.service.PolicyService;
import net.coalevo.security.model.Policy;
import net.coalevo.security.model.Role;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.foundation.model.Action;
import net.wimpi.telnetd.io.TemplateAttributes;
import net.wimpi.telnetd.io.toolkit.MaskedSelection;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.IOException;
import java.util.Set;
import java.util.TreeSet;

/**
 * This class implements a command that will dump a policy.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class ViewPolicy extends AdminCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(ViewPolicy.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell)
      throws CommandException {
    prepare(shell);
    try {
      try {
        PolicyService ps = getPolicyService();
        if (ps == null) {
          m_IO.printTemplate(PREFIX + "serviceunavailable");
          return;
        }

         //1. prepare selectable roles
        Set<String> policies = ps.listAvailablePolicies();
        TreeSet<String> selectables = new TreeSet<String>();
        selectables.addAll(policies);
        if(selectables.isEmpty()) {
          m_IO.printTemplate(PREFIX + "nopolicies");
          return;
        }

        //2. prompt for selection
        m_PromptWrap = false;
        m_IO.printTemplate(PREFIX + "policyselect");
        MaskedSelection s = new MaskedSelection(m_IO.getTerminalIO(), "ms");
        String none = m_IO.formatTemplate(PREFIX + "noneoption");
        s.addOption(NONE, none);
        for (String str: selectables) {
          s.addOption(str, str);
        }
        s.run();
        Object o = s.getSelectedValue();
        if (NONE.equals(o)) {
          m_IO.printTemplate(PREFIX + "aborted");
          return;
        } else {
          String policy = (String) o;
          Policy p = null;
          try {
            p = ps.leasePolicy(m_Agent, policy);

            TemplateAttributes attr = m_IO.leaseTemplateAttributes();
            attr.add("policy", p.getIdentifier());

            Set<Action> actions = (Set<Action>) p.getActions();
            for(Action a:actions) {
              attr.add("actions.{id,rule}", new String[]{a.getIdentifier(),p.getRuleFor(a).toSARL()});
            }
            m_IO.printTemplate(PREFIX + "policy", attr);

          } catch (Exception ex) {
            log.error(c_LogMarker, "run()", ex);
            m_IO.printTemplate(PREFIX + "failed");
          } finally {
            if(p != null) {
              ps.releasePolicy(p);
            }
          } 
        }
      } catch (SecurityException secex) {
        log.error(c_LogMarker, "run()", secex);
        m_IO.printTemplate(PREFIX + "notallowed");
      }
    } catch (IOException ex) {
      log.error(c_LogMarker, "run()", ex);
    }
  }//run

  protected static final String PREFIX = TS_PREFIX + "ViewPolicy_";

}//class ViewPolicy
