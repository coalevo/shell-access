/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.admin;

import net.coalevo.security.service.PolicyService;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.wimpi.telnetd.io.TemplateAttributes;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.IOException;
import java.util.Set;
import java.util.TreeSet;

/**
 * This class implements a command that allows to list all roles.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class ListPolicies extends AdminCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(ListPolicies.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell)
      throws CommandException {
    prepare(shell);
    try {
      try {
        PolicyService ps = getPolicyService();
        if (ps == null) {
          m_IO.printTemplate(PREFIX + "serviceunavailable");
          return;
        }

        //1. prepare selectable roles
        Set<String> policies = ps.listAvailablePolicies();
        TreeSet<String> selectables = new TreeSet<String>();
        selectables.addAll(policies);
        if (selectables.isEmpty()) {
          m_IO.printTemplate(PREFIX + "nopolicies");
          return;
        }

        //2. output list
        TemplateAttributes attr = m_IO.leaseTemplateAttributes();
        for (String str : selectables) {
          attr.add("policies", str);
        }
        m_IO.printTemplate(PREFIX + "policylist", attr);

      } catch (SecurityException secex) {
        log.error(c_LogMarker, "run()", secex);
        m_IO.printTemplate(PREFIX + "notallowed");
      } catch (Exception ex) {
        log.error(c_LogMarker, "run()", ex);
        m_IO.printTemplate(PREFIX + "failed");
      }
    } catch (IOException ex) {
      log.error(c_LogMarker, "run()", ex);
    }
  }//run

  protected static final String PREFIX = TS_PREFIX + "ListPolicies_";

}//class ListPolicies