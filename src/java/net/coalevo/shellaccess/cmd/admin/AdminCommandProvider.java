/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.admin;

import net.coalevo.shellaccess.model.BaseShellCommandProvider;

/**
 * Extends {@link BaseShellCommandProvider} for the admin package
 * commands.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class AdminCommandProvider
     extends BaseShellCommandProvider {

    public AdminCommandProvider() {
      super(AdminCommandProvider.class.getPackage().getName());
      prepare();
    }//constructor

    private void prepare() {
      addCommand("DoMaintenance", DoMaintenance.class);

      //Roles
      addCommand("ListRoles", ListRoles.class);
      addCommand("ListAgentRoles", ListAgentRoles.class);
      addCommand("GrantRole", GrantRole.class);
      addCommand("RevokeRole", RevokeRole.class);
      addCommand("CreateRole", CreateRole.class);
      addCommand("DestroyRole", DestroyRole.class);
      //Permissions
      addCommand("CreatePermission", CreatePermission.class);
      addCommand("DestroyPermission", DestroyPermission.class);
      addCommand("ListPermissions", ListPermissions.class);
      addCommand("AddRolePermission", AddRolePermission.class);
      addCommand("RemoveRolePermission", RemoveRolePermission.class);
      addCommand("ListRolePermissions", ListRolePermissions.class);
      
      //Password
      addCommand("ResetUserPassword", ResetUserPassword.class);

      //Policies
      addCommand("ViewPolicy", ViewPolicy.class);
      addCommand("ListPolicies", ListPolicies.class);
      addCommand("CreatePolicy", CreatePolicy.class);
      addCommand("DestroyPolicy", DestroyPolicy.class);
      addCommand("AddPolicyEntry", AddPolicyEntry.class);
      addCommand("EditPolicyEntry", EditPolicyEntry.class);
      addCommand("RemovePolicyEntry", RemovePolicyEntry.class);
    }//prepare

}//class AdminCommandProvider
