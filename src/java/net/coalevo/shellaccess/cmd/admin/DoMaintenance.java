/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.admin;

import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.shellaccess.model.Services;
import net.coalevo.system.service.MaintenanceService;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.IOException;


/**
 * Implements a command that allows to run the maintenance routines
 * through the maintenance service.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class DoMaintenance
    extends AdminCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(DoMaintenance.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell)
      throws CommandException {
    prepare(shell);
    try {
      MaintenanceService ms = m_Shell.getServices().getMaintenanceService(Services.NO_WAIT);
      if (ms != null) {
        ms.doMaintenance(m_Agent);
        m_IO.printTemplate(PREFIX + "running");
      } else {
        m_IO.printTemplate(PREFIX + "noservice");
      }
    } catch (IOException ex) {
      log.error(c_LogMarker, "run()", ex);
    }
  }//run

  protected static final String PREFIX = TS_PREFIX + "DoMaintenance_";

}//class DoMaintenance
