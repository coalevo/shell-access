/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.admin;

import net.coalevo.security.service.PolicyService;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.wimpi.telnetd.io.toolkit.MaskedSelection;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.IOException;
import java.util.Set;
import java.util.TreeSet;

/**
 * This class provides an admin command that allows to destroy a policy.
 * Use with care.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class DestroyPolicy extends AdminCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(DestroyPolicy.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell)
      throws CommandException {
    prepare(shell);
    try {
      try {
        //1. confirm with warning
        m_PromptWrap = false;
        if (!promptDecision(PREFIX + "doit")) {
          m_IO.printTemplate(PREFIX + "aborted");
          return;
        }

        PolicyService ps = getPolicyService();
        if (ps == null) {
          m_IO.printTemplate(PREFIX + "serviceunavailable");
          return;
        }

        //2. prepare destroyable policies
        Set<String> policies = ps.listAvailablePolicies();
        TreeSet<String> destroyables = new TreeSet<String>();
        destroyables.addAll(policies);
        if(destroyables.isEmpty()) {
          m_IO.printTemplate(PREFIX + "nopolicies");
          return;
        }

        //3. prompt for selection
        m_IO.printTemplate(PREFIX + "policyselect");
        MaskedSelection s = new MaskedSelection(m_IO.getTerminalIO(), "ms");
        String none = m_IO.formatTemplate(PREFIX + "noneoption");
        s.addOption(NONE, none);
        for (String p : destroyables) {
          s.addOption(p, p);
        }
        s.run();
        Object o = s.getSelectedValue();
        if (NONE.equals(o)) {
          m_IO.printTemplate(PREFIX + "aborted");
          return;
        } else {
          String policy = (String) o;
          if (ps.deletePolicy(m_Agent, policy)) {
            logPolicyDestroy(
                m_AgentIdentifier.getIdentifier(),
                policy
            );
            m_IO.printTemplate(PREFIX + "confirm", "policy", policy);
            return;
          } else {
            m_IO.printTemplate(PREFIX + "failed");
          }
        }


      } catch (SecurityException secex) {
        log.error(c_LogMarker, "run()", secex);
        m_IO.printTemplate(PREFIX + "notallowed");
      }
    } catch (IOException ex) {
      log.error(c_LogMarker, "run()", ex);
    }
  }//run

  protected static final String PREFIX = TS_PREFIX + "DestroyPolicy_";

}//class DestroyPolicy