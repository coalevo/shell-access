/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.admin;

import net.coalevo.foundation.model.MessageAttributes;
import net.coalevo.foundation.model.Messages;
import net.coalevo.security.model.Deny;
import net.coalevo.security.model.Permission;
import net.coalevo.security.model.Permit;
import net.coalevo.security.model.Role;
import net.coalevo.security.service.SecurityManagementService;
import net.coalevo.security.service.PolicyService;
import net.coalevo.security.service.PolicyXMLService;
import net.coalevo.shellaccess.cmd.BaseCommand;
import net.coalevo.shellaccess.cmd.EmbeddedNano;
import net.coalevo.shellaccess.impl.Activator;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.Services;
import net.wimpi.telnetd.io.toolkit.BufferOverflowException;
import net.wimpi.telnetd.io.micro.Micro;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.util.Comparator;
import java.io.File;
import java.io.IOException;
import java.io.FileReader;
import java.io.FileWriter;

/**
 * Abstract base class for admin related commands.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public abstract class AdminCommand
    extends BaseCommand {

  protected Messages m_Messages;

  public boolean prepare(BasicShell s) {
    if (super.prepare(s)) {
      m_Messages = Activator.getBundleMessages();
      return true;
    }
    return false;
  }//prepare

  public void reset() {
    m_Messages = null;
    super.reset();
  }//reset

  protected SecurityManagementService getSecurityManagementService() {
    return Activator.getServices().getSecurityManagementService(Services.NO_WAIT);
  }//getSecurityManagementService

  protected PolicyService getPolicyService() {
    return Activator.getServices().getPolicyService(Services.NO_WAIT);
  }//getPolicyService

  protected PolicyXMLService getPolicyXMLService() {
    //return Activator.getServices().getPolicyXMLService(Services.NO_WAIT);
    return null;
  }//getPolicyService

  protected void logRoleGrant(String aidgranter, String aidgranted, String role) {
    MessageAttributes attr = m_Messages.leaseAttributes();
    attr.add("granter", aidgranter);
    attr.add("granted", aidgranted);
    attr.add("role", role);
    log.info(c_SecurityMarker, m_Messages.get("AdminCommand.security.rolegrant", attr));
  }//logRoleGrant

  protected void logRoleRevoke(String aidrevoker, String aidrevoked, String role) {
    MessageAttributes attr = m_Messages.leaseAttributes();
    attr.add("revoker", aidrevoker);
    attr.add("revoked", aidrevoked);
    attr.add("role", role);
    log.info(c_SecurityMarker, m_Messages.get("AdminCommand.security.rolerevoke", attr));
  }//logRoleRevoke

  protected void logRoleDestroy(String aid, String role) {
    MessageAttributes attr = m_Messages.leaseAttributes();
    attr.add("aid", aid);
    attr.add("role", role);
    log.info(c_SecurityMarker, m_Messages.get("AdminCommand.security.roledestroy", attr));
  }//logRoleDestroy

  protected void logRoleCreate(String aid, String role) {
    MessageAttributes attr = m_Messages.leaseAttributes();
    attr.add("aid", aid);
    attr.add("role", role);
    log.info(c_SecurityMarker, m_Messages.get("AdminCommand.security.rolecreate", attr));
  }//logRoleCreate

  protected void logPermissionCreate(String aid, String perm) {
    MessageAttributes attr = m_Messages.leaseAttributes();
    attr.add("aid", aid);
    attr.add("perm", perm);
    log.info(c_SecurityMarker, m_Messages.get("AdminCommand.security.permcreate", attr));
  }//logPermissionCreate

  protected void logPermissionDestroy(String aid, String perm) {
    MessageAttributes attr = m_Messages.leaseAttributes();
    attr.add("aid", aid);
    attr.add("perm", perm);
    log.info(c_SecurityMarker, m_Messages.get("AdminCommand.security.permdestroy", attr));
  }//logPermissionDestroy

  protected void logPermissionGrant(String aidgranter, String aidgranted, String perm) {
    MessageAttributes attr = m_Messages.leaseAttributes();
    attr.add("granter", aidgranter);
    attr.add("granted", aidgranted);
    attr.add("perm", perm);
    log.info(c_SecurityMarker, m_Messages.get("AdminCommand.security.permgrant", attr));
  }//logPermissionGrant

  protected void logPermissionRevoke(String aidrevoker, String aidrevoked, String perm) {
    MessageAttributes attr = m_Messages.leaseAttributes();
    attr.add("revoker", aidrevoker);
    attr.add("revoked", aidrevoked);
    attr.add("perm", perm);
    log.info(c_SecurityMarker, m_Messages.get("AdminCommand.security.permrevoke", attr));
  }//logPermissionRevoke

  protected void logPasswordReset(String resetter, String resetted) {
    MessageAttributes attr = m_Messages.leaseAttributes();
    attr.add("resetter", resetter);
    attr.add("resetted", resetted);
    log.info(c_SecurityMarker, m_Messages.get("AdminCommand.security.password", attr));
  }//logPasswordReset

  protected void logPolicyCreate(String creator, String policy) {
    MessageAttributes attr = m_Messages.leaseAttributes();
    attr.add("aid", creator);
    attr.add("policy", policy);
    log.info(c_SecurityMarker, m_Messages.get("AdminCommand.security.policycreate", attr));
  }//logPolicyCreate

  protected void logPolicyDestroy(String destroyer, String policy) {
    MessageAttributes attr = m_Messages.leaseAttributes();
    attr.add("aid", destroyer);
    attr.add("policy", policy);
    log.info(c_SecurityMarker, m_Messages.get("AdminCommand.security.policydestroy", attr));
  }//logPolicyDestroy

  protected void logPolicyEntryRemove(String remover, String policy, String action) {
    MessageAttributes attr = m_Messages.leaseAttributes();
    attr.add("aid", remover);
    attr.add("policy", policy);
    attr.add("entry", action);
    log.info(c_SecurityMarker, m_Messages.get("AdminCommand.security.policyentryremove", attr));
  }//logPolicyEntryRemove

  protected void logPolicyEntry(String remover, String policy, String action, String rule) {
    MessageAttributes attr = m_Messages.leaseAttributes();
    attr.add("aid", remover);
    attr.add("policy", policy);
    attr.add("entry", action);
    attr.add("rule",rule);
    log.info(c_SecurityMarker, m_Messages.get("AdminCommand.security.policyentry", attr));
  }//logPolicyEntry

  protected String editFile(File f)
      throws IOException, BufferOverflowException {

    EmbeddedNano en = null;
    Micro m = null;
    String c = "";

    if (EmbeddedNano.isAvailable()) {
      en = new EmbeddedNano();
      en.prepare(m_Shell, f);
      en.backup();
    } else {
      m = new Micro(m_Shell.getShellIO().getTerminalIO());
      m.setJustBackspace(true);
    }
    //backup
    if (EmbeddedNano.isAvailable()) {
      en.run();
      c = en.getContent();
    } else {
      m.readTextFrom(new FileReader(f));
      m.run();
      c = m.getText();
      f.renameTo(new File(f.getAbsolutePath() + "~"));
      FileWriter wf = new FileWriter(f);
      wf.write(c);
      wf.flush();
      wf.close();
    }
    //m_Shell.getShellIO().getTerminalIO().resetTerminal();
    m_Shell.getShellIO().blankTerminal();
    return c;
  }//editFile

  protected static final Comparator<Role> ROLE_COMP = new Comparator<Role>() {
    public int compare(Role r1, Role r2) {
      return r1.getIdentifier().compareTo(r2.getIdentifier());
    }//compare
  };

  protected static final Comparator<Permission> PERM_COMP = new Comparator<Permission>() {
    public int compare(Permission p1, Permission p2) {
      if (p1 instanceof Permit && p2 instanceof Deny) {
        return -1;
      }
      if (p1 instanceof Deny && p2 instanceof Permit) {
        return 1;
      }
      return p1.getIdentifier().compareTo(p2.getIdentifier());
    }//compare
  };

  protected static final Marker c_SecurityMarker = MarkerFactory.getMarker("SECURITY");
  protected static final Marker c_MaintenanceMarker = MarkerFactory.getMarker("MAINTENANCE");

  protected static final String TS_PREFIX = "commands_net.coalevo.shellaccess.cmd.admin.";

}//class AdminCommand
