/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.admin;

import net.coalevo.security.model.Role;
import net.coalevo.security.service.SecurityManagementService;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.IOException;

/**
 * This class provides an admin command that allows to create a role.
 * Use with care.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class CreateRole extends AdminCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(CreateRole.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell)
      throws CommandException {
    prepare(shell);
    try {
      try {
        SecurityManagementService sms = getSecurityManagementService();
        if (sms == null) {
          m_IO.printTemplate(PREFIX + "serviceunavailable");
          return;
        }

        //1. prompt for rolename
        String rolename = prompt(PREFIX + "roleprompt");
        if (rolename == null || rolename.length() == 0) {
          m_IO.printTemplate(PREFIX + "aborted");
          return;
        }

        //2. Create role
        Role r = sms.createRole(m_Agent, rolename);
        if (r == null) {
          m_IO.printTemplate(PREFIX + "failed");
          return;
        } else {
          logRoleCreate(m_AgentIdentifier.getIdentifier(),r.getIdentifier());
          m_IO.printTemplate(PREFIX + "confirm", "role", r.getIdentifier());
          return;
        }

      } catch (SecurityException secex) {
        log.error(c_LogMarker, "run()", secex);
        m_IO.printTemplate(PREFIX + "notallowed");
      }
    } catch (IOException ex) {
      log.error(c_LogMarker, "run()", ex);
    }
  }//run

  protected static final String PREFIX = TS_PREFIX + "CreateRole_";

}//class CreateRole