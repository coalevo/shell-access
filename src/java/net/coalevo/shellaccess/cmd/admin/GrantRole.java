/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.admin;

import net.coalevo.foundation.model.Agent;
import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.security.model.Role;
import net.coalevo.security.model.NoSuchAgentException;
import net.coalevo.security.service.SecurityManagementService;
import net.coalevo.shellaccess.impl.RoleUtility;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.wimpi.telnetd.io.toolkit.MaskedSelection;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.IOException;
import java.util.Set;
import java.util.TreeSet;

/**
 * This class implements a command that allows an admin to grant a role.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class GrantRole extends AdminCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(GrantRole.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell)
      throws CommandException {
    prepare(shell);
    try {
      try {
        SecurityManagementService sms = getSecurityManagementService();
        if (sms == null) {
          m_IO.printTemplate(PREFIX + "serviceunavailable");
          return;
        }
        //1. get agent
        m_PromptWrap = false;
        AgentIdentifier aid = promptAgent(PREFIX + "agentprompt");
        if(aid == null) {
          m_IO.printTemplate(PREFIX + "nosuchagent");
          return;
        }
        //Note: local security has no node identifier
        Agent a = sms.getAgent(m_Agent, aid.getName());

        //2. prepare grantable roles
        Set<Role> roles = sms.getRoles(m_Agent);
        Set<Role> agentroles = sms.getAgentRoles(m_Agent, a);

        TreeSet<Role> grantables = new TreeSet<Role>(ROLE_COMP);
        grantables.addAll(roles);
        grantables.removeAll(agentroles);
        if(grantables.isEmpty()) {
          m_IO.printTemplate(PREFIX + "noroles");
          return;
        }

        //3. prompt for selection
        m_IO.printTemplate(PREFIX + "roleselect");
        MaskedSelection s = new MaskedSelection(m_IO.getTerminalIO(), "ms");
        String none = m_IO.formatTemplate(PREFIX + "noneoption");
        s.addOption(NONE, none);
        for (Role r : grantables) {
          s.addOption(r, RoleUtility.getRoleWithStyle(r));
        }
        s.run();
        Object o = s.getSelectedValue();
        if (NONE.equals(o)) {
          m_IO.printTemplate(PREFIX + "unchanged");
          return;
        } else {
          Role r = (Role) o;
          if (sms.grantRole(m_Agent, a, r)) {
            logRoleGrant(
                m_AgentIdentifier.getIdentifier(),
                aid.getIdentifier(),
                r.getIdentifier()
            );
            m_IO.printTemplate(PREFIX + "confirm");
            return;
          } else {
            m_IO.printTemplate(PREFIX + "failed");
          }
        }
      } catch(NoSuchAgentException nsaex) {
        m_IO.printTemplate(PREFIX + "nosuchagent");
        return;
      } catch (SecurityException secex) {
        log.error(c_LogMarker, "run()", secex);
        m_IO.printTemplate(PREFIX + "notallowed");
      }
    } catch (IOException ex) {
      log.error(c_LogMarker, "run()", ex);
    }
  }//run

  protected static final String PREFIX = TS_PREFIX + "GrantRole_";

}//class GrantRole
