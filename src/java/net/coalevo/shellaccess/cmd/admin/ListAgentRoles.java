/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.admin;

import net.coalevo.security.model.Role;
import net.coalevo.security.model.NoSuchAgentException;
import net.coalevo.security.service.SecurityManagementService;
import net.coalevo.shellaccess.impl.RoleUtility;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.model.Agent;
import net.wimpi.telnetd.io.TemplateAttributes;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.IOException;
import java.util.Set;
import java.util.TreeSet;

/**
 * This class implements a command that allows to list all roles of a
 * specified agent.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class ListAgentRoles extends AdminCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(ListAgentRoles.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell)
      throws CommandException {
    prepare(shell);
    try {
      try {
        SecurityManagementService sms = getSecurityManagementService();
        if (sms == null) {
          m_IO.printTemplate(PREFIX + "serviceunavailable");
          return;
        }

        //1. Get agent
        m_PromptWrap = false;
        AgentIdentifier aid = promptAgent(PREFIX + "agentprompt");
        if(aid == null) {
          m_IO.printTemplate(PREFIX + "nosuchagent");
          return;
        }        
        //Note: local security has no node identifier
        Agent a = sms.getAgent(m_Agent, aid.getName());

        //2. prepare agent roles
        Set<Role> roles = sms.getAgentRoles(m_Agent,a);
        TreeSet<Role> sr = new TreeSet<Role>(ROLE_COMP);
        sr.addAll(roles);
        if(sr.isEmpty()) {
          m_IO.printTemplate(PREFIX + "noroles");
          return;
        }

        //2. output list
        TemplateAttributes attr = m_IO.leaseTemplateAttributes();
        for (Role r : sr) {
          attr.add("roles", RoleUtility.getRoleWithStyle(r));
        }
        m_IO.printTemplate(PREFIX + "rolelist", attr);

      } catch(NoSuchAgentException nsaex) {
        m_IO.printTemplate(PREFIX + "nosuchagent");
        return;
      } catch (SecurityException secex) {
        log.error(c_LogMarker, "run()", secex);
        m_IO.printTemplate(PREFIX + "notallowed");
      }
    } catch (IOException ex) {
      log.error(c_LogMarker, "run()", ex);
    }
  }//run

  protected static final String PREFIX = TS_PREFIX + "ListAgentRoles_";

}//class ListAgentRoles