/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.admin;

import org.slf4j.Marker;
import org.slf4j.MarkerFactory;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.shellaccess.model.Services;
import net.coalevo.shellaccess.impl.Activator;
import net.coalevo.security.service.SecurityManagementService;
import net.coalevo.security.model.NoSuchAgentException;
import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.model.Agent;

import java.io.IOException;

/**
 * This class implements a command that allows admins to reset user
 * passwords.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class ResetUserPassword
    extends AdminCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(ResetUserPassword.class.getName());

   public String getIdentifier() {
     return getClass().getName();
   }//getIdentifier

   public void run(BasicShell shell)
       throws CommandException {
     prepare(shell);
     try {
       try {
         SecurityManagementService sms = getSecurityManagementService();
         if (sms == null) {
           m_IO.printTemplate(PREFIX + "serviceunavailable");
           return;
         }

         //1. Get agent
         m_PromptWrap = false;
         AgentIdentifier aid = promptAgent(PREFIX + "agentprompt");
         if(aid == null) {
           m_IO.printTemplate(PREFIX + "nosuchagent");
           return;
         }
         //Note: local security has no node identifier
         Agent a = sms.getAgent(m_Agent, aid.getName());

         //Generate Password
         String passwd = Activator.getServices().getRndStringGeneratorService(Services.NO_WAIT).getRandomString(8);

         if (sms.updateAuthentication(m_Agent, a, passwd)) {
           logPasswordReset(m_Agent.getIdentifier(), a.getIdentifier());
           m_IO.printTemplate(PREFIX + "confirm", "agent", a.getIdentifier(),"passwd", passwd);
         } else {
           m_IO.printTemplate(PREFIX + "failed");
         }
      } catch(NoSuchAgentException nsaex) {
        m_IO.printTemplate(PREFIX + "nosuchagent");
        return;
      } catch (SecurityException secex) {
        log.error(c_LogMarker, "run()", secex);
        m_IO.printTemplate(PREFIX + "notallowed");
      }
    } catch (IOException ex) {
      log.error(c_LogMarker, "run()", ex);
    }
  }//run

  protected static final String PREFIX = TS_PREFIX + "ResetUserPassword_";

}//class ResetUserPassword
