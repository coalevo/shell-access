/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd;

import net.coalevo.shellaccess.impl.Activator;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.Command;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.shellaccess.model.ShellIO;
import net.wimpi.telnetd.io.TemplateAttributes;
import net.wimpi.telnetd.io.Templates;
import net.wimpi.telnetd.io.TemplatesUpdateListener;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.slf4j.MarkerFactory;
import org.slf4j.Marker;


/**
 * Command that shows the local time and date.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class ShowDateTime implements Command {

  private static SimpleDateFormat[] c_Formats;
  private static Marker c_LogMarker = MarkerFactory.getMarker(ShowDateTime.class.getName());

  static {
    Templates ts = Activator.getTemplates();

    ts.registerUpdateListener(new TemplatesUpdateListener() {
      public void updatedTemplates() {
        ShowDateTime.updateFormatCache();
      }//updated
    });
    updateFormatCache();
  }//static initializer

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell) throws CommandException {
    ShellIO io = shell.getShellIO();
    try {
      TemplateAttributes attr = io.leaseTemplateAttributes();
      Date d = new Date();
      DateFormat df = c_Formats[shell.getShellIO().getLanguageIndex()];
      attr.add("datetime", df.format(d));
      if(!shell.getSession().getTimeZone().equals(TimeZone.getDefault())) {
        df.setTimeZone(shell.getSession().getTimeZone());
        attr.add("tzdatetime", df.format(d));
        df.setTimeZone(TimeZone.getDefault());
      }
      attr.add("doing", shell.getSession().getDoing());
      attr.add("host", shell.getSession().getHost());
      io.printTemplate("commands_net.coalevo.shellaccess.cmd.ShowDateTime_datetime", attr);
    } catch (IOException ex) {
      Activator.log().error(c_LogMarker,"run(BaseShell)", ex);
      throw new CommandException(ex.getMessage());
    }
  }//run

  public void reset() {
    //Clean up local resources
  }//reset

  private static void updateFormatCache() {
    SimpleDateFormat[] backupFormats = c_Formats;
    Templates ts = Activator.getTemplates();
    if (ts == null) {
      return;
    }
    try {
      String[] patterns = ts.formatAll("commands_net.coalevo.shellaccess.cmd.ShowDateTime_pattern");
      c_Formats = new SimpleDateFormat[patterns.length];
      for (int i = 0; i < patterns.length; i++) {
        c_Formats[i] = new SimpleDateFormat(patterns[i]);
      }
    } catch (Exception ex) {
      Activator.log().error("updateFormatCache()", ex);
      c_Formats = backupFormats;
      Activator.log().info(c_LogMarker,"updateFormatCache():: Update failed, keeping working instances.");
    }
    Activator.log().debug(c_LogMarker,"updateFormatCache():: Updated format cache from updated templates.");
  }//updateFormatCache

}//class ShowDateTime
