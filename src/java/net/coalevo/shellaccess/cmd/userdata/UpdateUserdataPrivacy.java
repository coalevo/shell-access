/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.userdata;

import net.coalevo.security.model.NoSuchAgentException;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.text.util.ContentUtility;
import net.coalevo.userdata.model.EditableUserdata;
import net.coalevo.userdata.model.EditableUserdataPrivacy;
import net.coalevo.userdata.model.UserdataServiceException;
import net.coalevo.userdata.model.UserdataTokens;
import net.wimpi.telnetd.io.toolkit.CheckboxGroup;
import net.wimpi.telnetd.io.toolkit.Point;

import java.io.IOException;
import java.util.Date;

import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * Provides a command that allows to update the userdata privacy.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class UpdateUserdataPrivacy
    extends UserdataCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(UpdateUserdataPrivacy.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell)
      throws CommandException {
    prepare(shell);
    try {
      try {
        EditableUserdata ud =
            m_UserdataService.beginUserdataUpdate(m_Agent, m_AgentIdentifier);
        EditableUserdataPrivacy udp = ud.getEditablePrivacy();

        //Blank terminal and use single checkboxgroup
        m_IO.blankTerminal();
        CheckboxGroup cbg =
            new CheckboxGroup(m_IO.getTerminalIO(), "udprivacy", new Point(1, 1));

        cbg.setPrompt(m_IO.formatTemplate(PREFIX + "prompt"));
        //1. Personal Data
        if (check(ud.getPrefix())) {
          addCheckbox(cbg, UserdataTokens.ELEMENT_PREFIX, udp.getAllowsPrefix());
        }
        if (check(ud.getFirstnames())) {
          addCheckbox(cbg, UserdataTokens.ELEMENT_FIRSTNAME, udp.getAllowsFirstnames());
        }
        if (check(ud.getAdditionalnames())) {
          addCheckbox(cbg, UserdataTokens.ELEMENT_ADDITIONALNAME, udp.getAllowsAdditionalnames());
        }
        if (check(ud.getLastnames())) {
          addCheckbox(cbg, UserdataTokens.ELEMENT_LASTNAME, udp.getAllowsLastnames());
        }
        if (check(ud.getSuffix())) {
          addCheckbox(cbg, UserdataTokens.ELEMENT_SUFFIX, udp.getAllowsSuffix());
        }
        if (check(ud.getBirthdate())) {
          addCheckbox(cbg, UserdataTokens.ELEMENT_BIRTHDATE, udp.getAllowsBirthdate());
        }
        if (check(ud.getGender())) {
          addCheckbox(cbg, UserdataTokens.ELEMENT_GENDER, udp.getAllowsGender());
        }

        //2.Address Data
        if (check(ud.getStreet())) {
          addCheckbox(cbg, UserdataTokens.ELEMENT_STREET, udp.getAllowsStreet());
        }
        if (check(ud.getExtension())) {
          addCheckbox(cbg, UserdataTokens.ELEMENT_EXTENSION, udp.getAllowsExtension());
        }
        if (check(ud.getCity())) {
          addCheckbox(cbg, UserdataTokens.ELEMENT_CITY, udp.getAllowsCity());
        }
        if (check(ud.getPostalCode())) {
          addCheckbox(cbg, UserdataTokens.ELEMENT_POSTALCODE, udp.getAllowsPostalCode());
        }
        if (check(ud.getCountry())) {
          addCheckbox(cbg, UserdataTokens.ELEMENT_COUNTRY, udp.getAllowsCountry());
        }
        //3. Contact
        if (check(ud.getPhone())) {
          addCheckbox(cbg, UserdataTokens.ELEMENT_PHONE, udp.getAllowsPhone());
        }
        if (check(ud.getMobile())) {
          addCheckbox(cbg, UserdataTokens.ELEMENT_MOBILE, udp.getAllowsMobile());
        }
        if (check(ud.getEmail())) {
          addCheckbox(cbg, UserdataTokens.ELEMENT_EMAIL, udp.getAllowsEmail());
        }

        //4. Run
        cbg.run();

        //5. Update
        if (check(ud.getPrefix())) {
          udp.setAllowsPrefix(cbg.get(UserdataTokens.ELEMENT_PREFIX));
        }
        if (check(ud.getFirstnames())) {
          udp.setAllowsFirstnames(cbg.get(UserdataTokens.ELEMENT_FIRSTNAME));
        }
        if (check(ud.getAdditionalnames())) {
          udp.setAllowsAdditionalnames(cbg.get(UserdataTokens.ELEMENT_ADDITIONALNAME));
        }
        if (check(ud.getLastnames())) {
          udp.setAllowsLastnames(cbg.get(UserdataTokens.ELEMENT_LASTNAME));
        }
        if (check(ud.getSuffix())) {
          udp.setAllowsSuffix(cbg.get(UserdataTokens.ELEMENT_SUFFIX));
        }
        if (check(ud.getBirthdate())) {
          udp.setAllowsBirthdate(cbg.get(UserdataTokens.ELEMENT_BIRTHDATE));
        }
        if (check(ud.getGender())) {
          udp.setAllowsGender(cbg.get(UserdataTokens.ELEMENT_GENDER));
        }
        if (check(ud.getStreet())) {
          udp.setAllowsStreet(cbg.get(UserdataTokens.ELEMENT_STREET));
        }
        if (check(ud.getExtension())) {
          udp.setAllowsExtension(cbg.get(UserdataTokens.ELEMENT_EXTENSION));
        }
        if (check(ud.getCity())) {
          udp.setAllowsCity(cbg.get(UserdataTokens.ELEMENT_CITY));
        }
        if (check(ud.getPostalCode())) {
          udp.setAllowsPostalCode(cbg.get(UserdataTokens.ELEMENT_POSTALCODE));
        }
        if (check(ud.getCountry())) {
          udp.setAllowsCountry(cbg.get(UserdataTokens.ELEMENT_COUNTRY));
        }
        if (check(ud.getPhone())) {
          udp.setAllowsPhone(cbg.get(UserdataTokens.ELEMENT_PHONE));
        }
        if (check(ud.getMobile())) {
          udp.setAllowsMobile(cbg.get(UserdataTokens.ELEMENT_MOBILE));
        }
        if (check(ud.getEmail())) {
          udp.setAllowsEmail(cbg.get(UserdataTokens.ELEMENT_EMAIL));
        }
        //6. Commit
        m_UserdataService.commitUserdata(m_Agent, ud);
        m_IO.printTemplate(PREFIX + "confirm");

      } catch (NoSuchAgentException nse) {
        log.error(c_LogMarker,"run()", nse);
        m_IO.printTemplate(PREFIX + "nodata");
        return;
      } catch (SecurityException secex) {
        log.error(c_LogMarker,"run()", secex);
        m_IO.printTemplate(PREFIX + "notallowed");
        return;
      } catch (UserdataServiceException udex) {
        log.error(c_LogMarker,"run()", udex);
        m_IO.printTemplate(PREFIX + "failed");
        return;
      } catch (IllegalStateException iex) {
        log.error(c_LogMarker,"run()", iex);
        m_IO.printTemplate(PREFIX + "failed");
        return;
      }

    } catch (Exception ex) {
      log.error(c_LogMarker,"run()", ex);
    }
  }//run

  protected boolean check(String str) {
    return (str != null && str.length() != 0);
  }//check

  protected boolean check(Date d) {
    return d != null;
  }//check

  protected void addCheckbox(CheckboxGroup cbg, String elem, boolean b)
      throws IOException {
    cbg.addCheckbox(
        elem,
        ContentUtility.adjustLength(m_IO.formatTemplate(PREFIX + elem), 15),
        m_IO.formatTemplate(PREFIX + elem + "-key").charAt(0),
        b
    );
  }//addCheckbox

  public static final String PREFIX = UserdataCommand.TS_PREFIX + "UpdateUserdataPrivacy_";


}//class UpdateUserdataPrivacy
