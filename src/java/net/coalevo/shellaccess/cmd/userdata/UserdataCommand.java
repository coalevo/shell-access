/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.userdata;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.shellaccess.cmd.BaseCommand;
import net.coalevo.shellaccess.impl.RoleUtility;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.Services;
import net.coalevo.userdata.model.Userdata;
import net.coalevo.userdata.model.UserdataPrivacy;
import net.coalevo.userdata.model.UserdataTokens;
import net.coalevo.userdata.service.UserdataService;
import net.wimpi.telnetd.io.TemplateAttributes;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.IOException;
import java.util.Date;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Abstract base class for userdata related commands.
 * <p/>
 * Provides the functionality common to the service access.
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public abstract class UserdataCommand
    extends BaseCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(UserdataCommand.class.getName());

  protected UserdataService m_UserdataService;
  protected String m_Unspecified;
  protected String m_NotPublic;
  protected String m_FlagNP;

  public boolean prepare(BasicShell shell) {
    super.prepare(shell);
    m_UserdataService = shell.getServices().getUserdataService(Services.NO_WAIT);
    if (m_UserdataService == null) {
      try {
        m_IO.printTemplate(PREFIX + "serviceunavailable");
      } catch (Exception ex) {
        log.error(c_LogMarker, "prepare()", ex);
      }
      return false;
    }
    return true;
  }//prepare

  /**
   * Resets the instance members.
   * Be sure you call <tt>super.reset();</tt> in your subclass
   * if you override this method.
   */
  public void reset() {
    super.reset();
    m_UserdataService = null;
    m_Unspecified = null;
    m_NotPublic = null;
    m_FlagNP = null;
  }//reset

  protected void pageUserdata(AgentIdentifier aid) throws IOException {
    m_Unspecified = m_IO.formatTemplate(PREFIX + "unspecified");
    m_NotPublic = m_IO.formatTemplate(PREFIX + "notpublic");
    m_FlagNP = m_IO.formatTemplate(PREFIX + "flagnp");

    Userdata ud = null;
    try {
      ud = m_UserdataService.getUserdata(m_Agent, aid);
    } catch (Exception ex) {
      log.error(c_LogMarker, "pageUserdate(AgentIdentifier)", ex);
    }
    if (ud == null) {
      m_IO.printTemplate(PREFIX + "nosuch");
      return;
    }
    StringBuilder sbuf = new StringBuilder();
    sbuf.append(toString(ud, aid));
    m_IO.page(sbuf.toString());
  }//pageUserdata

  protected void pageUserdata(AgentIdentifier[] aid) throws IOException {
    m_Unspecified = m_IO.formatTemplate(PREFIX + "unspecified");
    m_NotPublic = m_IO.formatTemplate(PREFIX + "notpublic");
    m_FlagNP = m_IO.formatTemplate(PREFIX + "flagnp");

    StringBuilder sbuf = new StringBuilder();
    for (int i = 0; i < aid.length; i++) {
      Userdata ud = null;
      try {
        ud = m_UserdataService.getUserdata(m_Agent, aid[i]);
      } catch (NoSuchElementException nse) {

      }
      if (ud == null) {
        continue;
      }
      sbuf.append(toString(ud, aid[i]));
    }
    m_IO.page(sbuf.toString());
  }//printUserdata

  protected String toString(Userdata data, AgentIdentifier aid)
      throws IOException {
    //If the data is mutable, then the user is privileged to see it all
    //non public items should only be flagged.
    boolean ispriv = !data.isPublic();

    //1. transform to attributes
    TemplateAttributes attr = m_IO.leaseTemplateAttributes();
    UserdataPrivacy priv = data.getPrivacy();

    if (!data.isConfirmed()) {
      attr.add("unconfirmed", "");
    }
    if (!data.isReviewed()) {
      attr.add("notreviewed", "");
    }
    attr.add(UserdataTokens.ATTR_USERDATA_UID, data.getIdentifier());
    attr.add(UserdataTokens.ELEMENT_NICKNAME, data.getNickname());
    //flags
    Iterator iter = data.getFlags();
    while (iter.hasNext()) {
      String tmp = (String) iter.next();
      attr.add(UserdataTokens.ELEMENT_FLAG, tmp);
    }
    //roles
    attr.add("roles", RoleUtility.getRolesWithStyle(getRoles(aid)));
    if (data.getFirstLoginDate() != null) {
      attr.add(UserdataTokens.ELEMENT_FIRSTLOGINDATE, getStandardDateFormat().format(data.getFirstLoginDate()));
    } else {
      attr.add(UserdataTokens.ELEMENT_FIRSTLOGINDATE, m_Unspecified);
    }

    //Personal

    //Name special strategy
    String prefix = data.getPrefix();
    String suffix = data.getSuffix();
    String firstnames = data.getFirstnames();
    String lastnames = data.getLastnames();
    String addnames = data.getAdditionalnames();

    if ((prefix == null || prefix.length() == 0) &&
        (suffix == null || suffix.length() == 0) &&
        (firstnames == null || firstnames.length() == 0) &&
        (lastnames == null || lastnames.length() == 0) &&
        (addnames == null || addnames.length() == 0)) {
      if (!priv.getAllowsPrefix()
          && !priv.getAllowsFirstnames()
          && !priv.getAllowsAdditionalnames()
          && !priv.getAllowsLastnames()
          && !priv.getAllowsSuffix()) {
        //show prefix as not public
        attr.add("prefix", m_NotPublic);
      } else {
        attr.add("nofn", m_Unspecified);
      }
    } else {
      addName(attr, UserdataTokens.ELEMENT_PREFIX, prefix, priv.getAllowsPrefix(), ispriv);
      addName(attr, UserdataTokens.ELEMENT_FIRSTNAME, firstnames, priv.getAllowsFirstnames(), ispriv);
      addName(attr, UserdataTokens.ELEMENT_ADDITIONALNAME, addnames, priv.getAllowsAdditionalnames(), ispriv);
      addName(attr, UserdataTokens.ELEMENT_LASTNAME, lastnames, priv.getAllowsLastnames(), ispriv);
      addName(attr, UserdataTokens.ELEMENT_SUFFIX, suffix, priv.getAllowsSuffix(), ispriv);
    }
    Date d = data.getBirthdate();
    if (d != null) {
      String tmp = getStandardDateFormat().format(data.getBirthdate());
      attr.add(UserdataTokens.ELEMENT_BIRTHDATE,
          (priv.getAllowsBirthdate()) ? tmp : ((ispriv) ? m_FlagNP.concat(tmp) : m_NotPublic));
    } else {
      attr.add(UserdataTokens.ELEMENT_BIRTHDATE, m_Unspecified);
    }
    attr.add("photo", (data.hasPhoto()) ? "true" : "false");

    //Address
    add(attr, UserdataTokens.ELEMENT_STREET, data.getStreet(), priv.getAllowsStreet(), ispriv);
    add(attr, UserdataTokens.ELEMENT_EXTENSION, data.getExtension(), priv.getAllowsExtension(), ispriv);
    add(attr, UserdataTokens.ELEMENT_CITY, data.getCity(), priv.getAllowsCity(), ispriv);
    add(attr, UserdataTokens.ELEMENT_POSTALCODE, data.getPostalCode(), priv.getAllowsPostalCode(), ispriv);
    add(attr, UserdataTokens.ELEMENT_COUNTRY, data.getCountry(), priv.getAllowsCountry(), ispriv);

    //Communications
    add(attr, UserdataTokens.ELEMENT_PHONE, data.getPhone(), priv.getAllowsPhone(), ispriv);
    add(attr, UserdataTokens.ELEMENT_MOBILE, data.getMobile(), priv.getAllowsMobile(), ispriv);
    add(attr, UserdataTokens.ELEMENT_EMAIL, data.getEmail(), priv.getAllowsEmail(), ispriv);
    add(attr, UserdataTokens.ELEMENT_LINK, data.getLink(), true, ispriv);
    add(attr, "linktype", data.getLinkType(), true, ispriv);
    add(attr, UserdataTokens.ELEMENT_LANGUAGES, data.getLanguages(), true, ispriv);

    add(attr, UserdataTokens.ELEMENT_INFO, data.getInfo(), true, ispriv);

    //Review and Email Confirmation
    if (data.isReviewed()) {
      attr.add(UserdataTokens.ATTR_REVIEWED, "");
    }
    if (ispriv && data.isConfirmed()) {
      attr.add(UserdataTokens.ATTR_CONFIRMED, "");
    }

    return m_IO.formatTemplate(PREFIX + "userdata", attr);
  }//toString

  private void add(TemplateAttributes attr, String name, String val, boolean np, boolean ispriv) {
    boolean valid = (val != null && val.length() > 0);
    if (ispriv) {
      if (valid) {
        attr.add(name, (np) ? val : m_FlagNP.concat(val));
      } else {
        attr.add(name, m_Unspecified);
      }
    } else {
      if (np) {
        attr.add(name, (valid) ? val : m_Unspecified);
      } else {
        attr.add(name, m_NotPublic);
      }
    }
  }//add

  private void addName(TemplateAttributes attr, String name, String val, boolean np, boolean ispriv) {
    boolean valid = (val != null && val.length() > 0);
    if (ispriv) {
      if (valid) {
        attr.add(name, (np) ? val : m_FlagNP.concat(val));
      }
    } else {
      if (np && valid) {
        attr.add(name, val);
      }
    }
  }//add

  public static final String TS_PREFIX = "commands_net.coalevo.shellaccess.cmd.userdata.";
  public static final String PREFIX = UserdataCommand.TS_PREFIX + "UserdataCommand_";

}//class UserdataCommand
