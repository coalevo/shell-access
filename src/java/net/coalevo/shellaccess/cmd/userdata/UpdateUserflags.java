/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.userdata;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.security.model.NoSuchAgentException;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.shellaccess.validators.IntegerInputValidator;
import net.coalevo.userdata.model.EditableUserdata;
import net.coalevo.userdata.model.UserdataServiceException;
import net.wimpi.telnetd.io.TemplateAttributes;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.IOException;
import java.util.Iterator;

/**
 * Provides a command that allows to update the userdata privacy.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class UpdateUserflags
    extends UserdataCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(UpdateUserflags.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell)
      throws CommandException {
    prepare(shell);
    try {
      try {

        //1. Get AgentIdentifier
        AgentIdentifier aid = this.promptAgent(PREFIX + "agentid");
        if (aid == null) {
          m_IO.printTemplate(PREFIX + "nosuchuser");
          return;
        }
        //2. Get Userdata
        EditableUserdata ud = m_UserdataService.beginUserdataUpdate(m_Agent, aid);

        //3. Figure what should be done
        doUpdateFlags(ud);

        //4. Commit
        m_UserdataService.commitUserdata(m_Agent, ud);

        //5. Confirm stored
        m_IO.printTemplate(PREFIX + "confirmstored");

      } catch (NoSuchAgentException nse) {
        log.error(c_LogMarker, "run()", nse);
        m_IO.printTemplate(PREFIX + "nodata");
        return;
      } catch (SecurityException secex) {
        log.error(c_LogMarker, "run()", secex);
        m_IO.printTemplate(PREFIX + "notallowed");
        return;
      } catch (UserdataServiceException udex) {
        log.error(c_LogMarker, "run()", udex);
        m_IO.printTemplate(PREFIX + "failed");
        return;
      } catch (IllegalStateException iex) {
        log.error(c_LogMarker, "run()", iex);
        m_IO.printTemplate(PREFIX + "failed");
        return;
      }

    } catch (Exception ex) {
      log.error(c_LogMarker, "run()", ex);
    }
  }//run

  protected void doUpdateFlags(EditableUserdata ud)
      throws Exception {

    char delete = m_IO.formatTemplate("delete").charAt(0);
    char add = m_IO.formatTemplate("add").charAt(0);
    char update = m_IO.formatTemplate("update").charAt(0);
    int numflags = ud.getNumberOfFlags();
    boolean done = false;

    do {
      listFlags(ud);
      m_IO.printTemplate(PREFIX + "prompt");
      char in = (char) m_IO.read();
      if (in == add) {
        String flag = prompt(PREFIX + "newflag");
        ud.addFlag(flag);
        m_IO.println(PREFIX + "confirmadd");
      } else if (in == delete) {
        if (ud.getNumberOfFlags() == 0) {
          m_IO.println(PREFIX + "noneleft");
          continue;
        }
        int i = Integer.parseInt(
            prompt(
                PREFIX + "flagnumber",
                new IntegerInputValidator(
                    1,
                    numflags,
                    m_IO.formatTemplate(
                        "inputerror_intvalidator",
                        "min",
                        Integer.toString(1),
                        "max",
                        Integer.toString(numflags)
                    )
                )
            )
        );
        if (ud.removeFlag(i)) {
          m_IO.printTemplate(PREFIX + "confirmdelete");
        } else {
          m_IO.printTemplate(PREFIX + "faileddelete");
        }
      } else if (in == update) {
        if (ud.getNumberOfFlags() == 0) {
          m_IO.println(PREFIX + "noneleft");
          continue;
        }
        int i = Integer.parseInt(prompt(PREFIX + "flagnumber", new IntegerInputValidator(1, numflags)));
        String flag = prompt(PREFIX + "newflag", ud.getFlag(i));
        ud.updateFlag(i, flag);
        m_IO.printTemplate(PREFIX + "confirmupdate");
      } else {
        done = true;
      }
    } while (!done);
  }//doUpdateUserFlags

  protected void listFlags(EditableUserdata ud) throws IOException {
    if (ud.getNumberOfFlags() == 0) {
      m_IO.printTemplate(PREFIX + "noflags");
      return;
    }
    TemplateAttributes attr = m_IO.leaseTemplateAttributes();
    for (Iterator<String> iter = ud.getFlags(); iter.hasNext();) {
      attr.add("flags", iter.next());
    }
    m_IO.printTemplate(PREFIX + "listflags", attr);
  }//listFlags

  public static final String PREFIX = UserdataCommand.TS_PREFIX + "UpdateUserflags_";

}//class UpdateUserflags
