/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.userdata;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;

import java.util.List;

import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * Command that will show userdata for a review.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class ReviewUserdata
    extends UserdataCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(ReviewUserdata.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell) throws CommandException {
    prepare(shell);
    try {
      List<AgentIdentifier> list = m_UserdataService.listUnreviewedUserdata(m_Agent);

      m_PromptWrap = false;
      AgentIdentifier aid = promptAgent(PREFIX + "agentlabel", list);
      if (aid == null) {
        m_IO.printTemplate(PREFIX + "aborted");
        return;
      }
      if (!list.contains(aid)) {
        if(m_UserdataService.isUserdataAvailable(m_Agent,aid)) {
          m_IO.printTemplate(PREFIX + "isreviewed");
        } else {
          m_IO.printTemplate(PREFIX + "nosuchdata");
        }
        return;
      }
      pageUserdata(aid);

      if (promptDecision(PREFIX + "domarkreviewed")) {
        m_UserdataService.markReviewed(m_Agent, aid);
        m_IO.printTemplate(PREFIX + "markedreviewed");
      }

    } catch (Exception ex) {
      log.error(c_LogMarker,"run()", ex);
    }
  }//run

  public static final String PREFIX = UserdataCommand.TS_PREFIX + "ReviewUserdata_";

}//class ReviewUserdata
