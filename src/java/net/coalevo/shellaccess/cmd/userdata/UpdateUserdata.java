/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.userdata;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.security.model.NoSuchAgentException;
import net.coalevo.shellaccess.impl.Activator;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.shellaccess.validators.*;
import net.coalevo.userdata.model.EditableUserdata;
import net.coalevo.userdata.model.UserdataServiceException;
import net.coalevo.userdata.model.UserdataTokens;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.util.Date;

/**
 * Provides a command that allows to update the userdata privacy.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class UpdateUserdata
    extends UserdataCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(UpdateUserdata.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell)
      throws CommandException {
    prepare(shell);
    try {
      try {

        //get AgentIdentifier
        AgentIdentifier aid = this.promptAgent(PREFIX + "agentid");

        EditableUserdata ud = null;
        if (m_UserdataService.isUserdataAvailable(m_Agent, aid)) {
          ud = m_UserdataService.beginUserdataUpdate(m_Agent, aid);
          doUpdateUserdata(ud, false);
        } else {
          ud = m_UserdataService.beginUserdataCreate(m_Agent, aid);
          m_IO.printTemplate(PREFIX + "creation");
          doUpdateUserdata(ud, true);
        }

        //6. Commit
        m_UserdataService.commitUserdata(m_Agent, ud);
        //7. Mark as reviewed (admin made changes and reviews)
        if (promptDecision(PREFIX + "domarkreviewed")) {
          m_UserdataService.markReviewed(m_Agent, aid);
          m_IO.printTemplate(PREFIX + "markedreviewed");
        }

        //8. Confirms
        m_IO.printTemplate(PREFIX + "confirm");
        if (!ud.isConfirmed()) {
          m_IO.printTemplate(PREFIX + "confirmationnote");
        }

      } catch (NoSuchAgentException nse) {
        log.error(c_LogMarker, "run()", nse);
        m_IO.printTemplate(PREFIX + "nodata");
        return;
      } catch (SecurityException secex) {
        log.error(c_LogMarker, "run()", secex);
        m_IO.printTemplate(PREFIX + "notallowed");
        return;
      } catch (UserdataServiceException udex) {
        log.error(c_LogMarker, "run()", udex);
        m_IO.printTemplate(PREFIX + "failed");
        return;
      } catch (IllegalStateException iex) {
        log.error(c_LogMarker, "run()", iex);
        m_IO.printTemplate(PREFIX + "failed");
        return;
      }

    } catch (Exception ex) {
      log.error(c_LogMarker, "run()", ex);
    }
  }//run

  protected void doUpdateUserdata(EditableUserdata ud, boolean create)
      throws Exception {
    String nick = "";
    String email = "";
    NewEMailValidator mailValidator =
        new NewEMailValidator(
            Activator.getServices(),
            m_IO.formatTemplate("newuser_nouds"),
            m_IO.formatTemplate("newuser_emailsyntax"),
            m_IO.formatTemplate("newuser_emailduplicate"),
            m_IO.formatTemplate("newuser_emailanon")
        );
    NewNickValidator nickvalidator =
        new NewNickValidator(Activator.getServices(),
            m_IO.formatTemplate("newuser_nouds"),
            m_IO.formatTemplate("newuser_nicksyntax"),
            m_IO.formatTemplate("newuser_nickduplicate")
        );

    if (create) {
      //1. Nickname
      m_IO.printTemplate(PREFIX + "nickrequired");
      ud.setNickname(prompt(PREFIX + UserdataTokens.ELEMENT_NICKNAME, nick, nickvalidator));
      //2. eMail
      m_IO.printTemplate(PREFIX + "mailrequired");
      ud.setEmail(prompt(PREFIX + UserdataTokens.ELEMENT_EMAIL, email, mailValidator));
    } else {
      nick = ud.getNickname();
      nickvalidator.setActualNick(nick);
      ud.setNickname(prompt(PREFIX + UserdataTokens.ELEMENT_NICKNAME, nick, nickvalidator));
      email = ud.getEmail();
      mailValidator.setActualEmail(email);
      ud.setEmail(prompt(PREFIX + UserdataTokens.ELEMENT_EMAIL, email, mailValidator));
    }

    //1. Personal Information
    if (promptDecision(PREFIX + "dopersonal")) {
      ud.setPrefix(
          prompt(PREFIX + UserdataTokens.ELEMENT_PREFIX, ud.getPrefix())
      );
      ud.setFirstnames(
          prompt(PREFIX + UserdataTokens.ELEMENT_FIRSTNAME, ud.getFirstnames())
      );
      ud.setAdditionalNames(
          prompt(PREFIX + UserdataTokens.ELEMENT_ADDITIONALNAME, ud.getAdditionalnames())
      );
      ud.setLastnames(
          prompt(PREFIX + UserdataTokens.ELEMENT_LASTNAME, ud.getLastnames())
      );
      ud.setSuffix(
          prompt(PREFIX + UserdataTokens.ELEMENT_SUFFIX, ud.getSuffix())
      );
      Date d = ud.getBirthdate();
      if (d == null) {
        d = new Date(0);
      }
      ud.setBirthdate(
          promptShortDate(PREFIX + UserdataTokens.ELEMENT_BIRTHDATE, d)
      );
      ud.setGender(
          prompt(PREFIX + UserdataTokens.ELEMENT_GENDER, ud.getGender())
      );
    }

    //2. Address Information
    if (promptDecision(PREFIX + "doaddress")) {
      ud.setStreet(
          prompt(PREFIX + UserdataTokens.ELEMENT_STREET, ud.getStreet())
      );
      ud.setExtension(
          prompt(PREFIX + UserdataTokens.ELEMENT_EXTENSION, ud.getExtension())
      );
      ud.setCity(
          prompt(PREFIX + UserdataTokens.ELEMENT_CITY, ud.getCity())
      );
      ud.setPostalCode(
          prompt(PREFIX + UserdataTokens.ELEMENT_POSTALCODE, ud.getPostalCode())
      );
      ud.setCountry(
          prompt(PREFIX + UserdataTokens.ELEMENT_COUNTRY, ud.getCountry())
      );
    }

    //3. Communications Information
    if (promptDecision(PREFIX + "docommunications")) {
      ud.setPhone(
          prompt(PREFIX + UserdataTokens.ELEMENT_PHONE, ud.getPhone())
      );
      ud.setMobile(
          prompt(PREFIX + UserdataTokens.ELEMENT_MOBILE, ud.getMobile())
      );
      ud.setLanguages(
          prompt(PREFIX + UserdataTokens.ELEMENT_LANGUAGES, ud.getLanguages(),
              new LanguageListValidator(m_IO.formatTemplate("inputerror_languagelistvalidator")))
      );
    }
    if (promptDecision(PREFIX + "dolink")) {
      //Prompt Link Type
      ud.setLinkType(
          prompt(PREFIX + UserdataTokens.ATTR_TYPE, ud.getLinkType())
      );
      //should be absolute?
      ud.setLink(
          prompt(
              PREFIX + UserdataTokens.ELEMENT_LINK,
              ud.getLink(),
              new URIValidator(true, m_IO.formatTemplate("inputerror_absurivalidator")))
      );
    }

    //4. User Info
    if (promptDecision(PREFIX + "doinfo")) {
      //format
      String f = promptInputFormat(ud.getInfoFormat());
      ud.setInfoFormat(f);

      //content
      String txt = editTransformableContent(f, ud.getInfo());
      if (txt == null || txt.length() == 0) {
        m_IO.printTemplate(PREFIX + "aborted");
        return;
      }
      ud.setInfo(txt);
    }
  }//doUpdateUserdata

  protected boolean check(String str) {
    return (str != null && str.length() != 0);
  }//check

  protected boolean check(Date d) {
    return d != null;
  }//check

  public static final String PREFIX = UserdataCommand.TS_PREFIX + "UpdateUserdata_";


}//class UpdateUserdata
