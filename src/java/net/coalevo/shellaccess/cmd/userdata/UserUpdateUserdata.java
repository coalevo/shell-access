/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.userdata;

import net.coalevo.security.model.NoSuchAgentException;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.userdata.model.EditableUserdata;
import net.coalevo.userdata.model.UserdataServiceException;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * Provides a command that allows a user to update his/her userdata.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class UserUpdateUserdata
    extends UpdateUserdata {

  private static Marker c_LogMarker = MarkerFactory.getMarker(UserUpdateUserdata.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell)
      throws CommandException {
    prepare(shell);
    EditableUserdata ud = null;
    try {
      try {
        if (m_UserdataService.isUserdataAvailable(m_Agent, m_AgentIdentifier)) {
          ud = m_UserdataService.beginUserdataUpdate(m_Agent, m_AgentIdentifier);
          doUpdateUserdata(ud, false);
        } else {
          ud = m_UserdataService.beginUserdataCreate(m_Agent, m_AgentIdentifier);
          m_IO.printTemplate(PREFIX + "creation");
          doUpdateUserdata(ud, true);
        }
        //6. Commit
        m_UserdataService.commitUserdata(m_Agent, ud);


        m_IO.printTemplate(PREFIX + "confirm");

        //7. Confirms
        if (!ud.isConfirmed()) {
          //Post the revision and validation note (revision for changes, validation for email changed)
          m_IO.printTemplate(PREFIX + "confirmationnote");
        }
        if (!ud.isReviewed()) {
          m_IO.printTemplate(PREFIX + "reviewnote");
        }

      } catch (NoSuchAgentException nse) {
        m_UserdataService.cancelUserdataTransaction(m_Agent, ud);
        log.error(c_LogMarker,"run()", nse);
        m_IO.printTemplate(PREFIX + "nodata");
        return;
      } catch (SecurityException secex) {
        m_UserdataService.cancelUserdataTransaction(m_Agent, ud);
        log.error(c_LogMarker,"run()", secex);
        m_IO.printTemplate(PREFIX + "notallowed");
        return;
      } catch (UserdataServiceException udex) {
        m_UserdataService.cancelUserdataTransaction(m_Agent, ud);
        log.error(c_LogMarker,"run()", udex);
        m_IO.printTemplate(PREFIX + "failed");
        return;
      } catch (IllegalStateException iex) {
        m_UserdataService.cancelUserdataTransaction(m_Agent, ud);
        log.error(c_LogMarker,"run()", iex);
        m_IO.printTemplate(PREFIX + "failed");
        return;
      }
    } catch (Exception ex) {
      log.error(c_LogMarker,"run()", ex);
    }
  }//run

  public static final String PREFIX = UserdataCommand.TS_PREFIX + "UserUpdateUserdata_";

}//class UserUpdateUserdata
