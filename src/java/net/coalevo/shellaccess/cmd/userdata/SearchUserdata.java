/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.userdata;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.wimpi.telnetd.io.toolkit.Editline;

import java.util.NoSuchElementException;

import org.slf4j.MarkerFactory;
import org.slf4j.Marker;

/**
 * Command that allows to search userdata using lucene query language.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class SearchUserdata extends UserdataCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(SearchUserdata.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell) throws CommandException {
    prepare(shell);
    try {
      m_IO.printTemplate(SearchUserdata.PREFIX + "querylabel");
      Editline el = new Editline(m_IO.getTerminalIO(), "query");
      el.setIgnoreDelete(true);
      el.run();
      String query = el.getValue();

      if (query == null || query.length() == 0) {
        m_IO.printTemplate(SearchUserdata.PREFIX + "aborted");
      } else {
        try {
          AgentIdentifier[] aid = m_UserdataService.searchUserdata(m_Agent, query);
          if (aid == null || aid.length == 0) {
            m_IO.printTemplate(SearchUserdata.PREFIX + "nosuch");
          } else {
            pageUserdata(aid);
          }
        } catch (NoSuchElementException nse) {
          m_IO.printTemplate(SearchUserdata.PREFIX + "nosuch");
        }
      }
    } catch (Exception ex) {
      log.error(c_LogMarker,"run()", ex);
    }
  }//run

  public void reset() {
    super.reset();
    m_Unspecified = null;
    m_NotPublic = null;
    m_FlagNP = null;
  }//reset

  public static final String PREFIX = UserdataCommand.TS_PREFIX + "SearchUserdata_";

}//class ShowUserdata
