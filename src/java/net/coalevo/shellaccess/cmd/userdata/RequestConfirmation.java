/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.userdata;

import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.userdata.model.Userdata;
import org.slf4j.MarkerFactory;
import org.slf4j.Marker;

/**
 * This class implements a command that allows to request a confirmation
 * again.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class RequestConfirmation
    extends UserdataCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(RequestConfirmation.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell)
      throws CommandException {
    prepare(shell);
    try {

      try {
        Object o = m_Shell.getEnvironmentVariable(PREFIX + "lastrequest");
        if (o != null) {
          long diff = System.currentTimeMillis() - ((Long) o).longValue();
          if (diff < MAX_REPEAT) {
            m_IO.printTemplate(PREFIX + "toosoon");
            return;
          }
        }
        Userdata ud = m_UserdataService.getUserdata(m_Agent, m_AgentIdentifier);
        if (ud.isConfirmed()) {
          m_IO.printTemplate(PREFIX + "isconfirmed");
          return;
        }

        m_UserdataService.retryConfirmation(m_Agent, m_AgentIdentifier);
        m_IO.printTemplate(PREFIX + "confirm");
        m_Shell.setEnvironmentVariable(PREFIX + "lastrequest", new Long(System.currentTimeMillis()));

      } catch (SecurityException ex) {
        m_IO.printTemplate(PREFIX + "notallowed");
        return;
      }
    } catch (Exception ex) {
      log.error(c_LogMarker,"run()", ex);
    }
  }//run

  private static final String PREFIX = TS_PREFIX + "RequestConfirmation_";
  private static final long MAX_REPEAT = 60 * 60 * 1000; //60 mins * 60 secs * 1000 millisecs

}//class RequestConfirmation
