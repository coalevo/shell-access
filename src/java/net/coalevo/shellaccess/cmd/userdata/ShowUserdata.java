/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.userdata;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * Command that will show userdata for a given nickname.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class ShowUserdata
    extends UserdataCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(ShowUserdata.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell) throws CommandException {
    prepare(shell);
    try {
      m_PromptWrap = false;
      AgentIdentifier aid;
      if(shell.isEnviromentVariableSet("profile.lastuser")) {
        aid = promptAgentUpdate(
            PREFIX + "agentlabel",
            (AgentIdentifier) shell.getEnvironmentVariable("profile.lastuser")
        );
      } else {
        aid = promptAgent(PREFIX + "agentlabel");
      }
      if (aid == null) {
        m_IO.printTemplate(PREFIX + "nosuchagent");
        return;
      }
      pageUserdata(aid);
    } catch (Exception ex) {
      log.error(c_LogMarker,"run()", ex);
    }
  }//run

  public static final String PREFIX = UserdataCommand.TS_PREFIX + "ShowUserdata_";

}//class ShowUserdata
