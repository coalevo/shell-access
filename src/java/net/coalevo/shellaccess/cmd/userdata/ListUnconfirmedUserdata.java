/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.userdata;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.IOException;
import java.util.List;

/**
 * Implements a command that lists unconfirmed userdata.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class ListUnconfirmedUserdata
    extends UserdataCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(ListUnconfirmedUserdata.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell) throws CommandException {
    if (!prepare(shell)) {
      return;
    }
    try {
      List<AgentIdentifier> list =
          m_UserdataService.listUnconfirmedUserdata(m_Agent);

      listRestrictable(list, 50, PREFIX + "none", PREFIX + "listheader");

    } catch (IOException ex) {
      try {
        m_IO.printTemplate(PREFIX + "failed");
      } catch (IOException iex) {
        log.error(c_LogMarker, "run(BaseShell)", iex);
      }
      log.error(c_LogMarker, "run(BaseShell)", ex);
      throw new CommandException(ex.getMessage());
    }
  }//run

  private static final String PREFIX = TS_PREFIX + "ListUnconfirmedUserdata_";

}//class ListUnconfirmedUserdata
