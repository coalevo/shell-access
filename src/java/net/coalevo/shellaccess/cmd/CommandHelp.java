/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd;

import net.coalevo.shellaccess.model.Command;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.shellaccess.impl.CommandShell;

/**
 * Provides help for commands within the actual menu.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class CommandHelp
    implements Command {

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell) throws CommandException {
    try {
      //prompt:
      int key = shell.getShellIO().prompt(PREFIX + "prompt",null);
      if (shell instanceof CommandShell) {
        ((CommandShell) shell).printCommandHelp(key,PREFIX);
      }
    } catch (Exception ex) {
      throw new CommandException(ex.getMessage());
    }
  }//run

  public void reset() {
    //Clean up local resources
  }//reset

  private final static String PREFIX = "commands_net.coalevo.shellaccess.cmd.CommandHelp_";

}//class CommandHelp
