/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd;

import net.coalevo.shellaccess.impl.Activator;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.Command;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.shellaccess.model.ShellIO;
import net.wimpi.telnetd.io.TemplateAttributes;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * This class implements a command that shows the
 * uptime of the proxy server.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class ShowUptime
    implements Command {

  private static SimpleDateFormat c_TimeFormat = new SimpleDateFormat("HH:mm");

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell) throws CommandException {
    ShellIO io = shell.getShellIO();
    try {
      final long since = Activator.getStartTime();
      final long now = System.currentTimeMillis();
      long elapsedSeconds = (now - since) / 1000;
      long days = elapsedSeconds / 86400;
      elapsedSeconds -= (days * 86400);
      long hours = elapsedSeconds / 3600;
      elapsedSeconds -= (hours * 3600);
      long minutes = elapsedSeconds / 60;
      elapsedSeconds -= (minutes * 60);

      TemplateAttributes attr = io.leaseTemplateAttributes();
      attr.add("time", c_TimeFormat.format(new Date(now)));
      attr.add("days", String.valueOf(days));
      attr.add("hours", getString(hours));
      attr.add("minutes", getString(minutes));
      attr.add("seconds", getString(elapsedSeconds));
      attr.add("freemem", toMemoryString(Runtime.getRuntime().freeMemory()));
      attr.add("totalmem", toMemoryString(Runtime.getRuntime().totalMemory()));
      attr.add("maxmem", toMemoryString(Runtime.getRuntime().maxMemory()));
      io.printTemplate("commands_net.coalevo.shellaccess.cmd.ShowUptime_uptime", attr);

    } catch (IOException iex) {
      throw new CommandException(iex.getMessage());
    }
  }//run

  public void reset() {
    //Clean up local resources
  }//reset

  private final long[] translateBytes(long bytes) {
    long mB = bytes / 1048576;
    bytes -= mB * 1048576;
    long kB = bytes / 1024;
    bytes -= kB * 1024;
    return new long[]{mB, kB, bytes};
  }//translateBytes

  private final String toMemoryString(long bytes) {
    long[] mem = translateBytes(bytes);
    return new StringBuilder()
        .append(mem[0]).append("MB ")
        .append(mem[1]).append("kB")
        .toString();
  }//toMemoryString

  private String getString(long in) {
    final StringBuilder sbuf = new StringBuilder();
    if (in < 10) {
      sbuf.append('0');
    }
    return sbuf.append(in).toString();
  }//getTwo

}//class ShowUptime
