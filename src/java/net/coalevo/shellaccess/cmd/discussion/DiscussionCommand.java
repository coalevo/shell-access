/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.discussion;

import net.coalevo.discussion.model.*;
import net.coalevo.discussion.service.DiscussionService;
import net.coalevo.discussion.service.ForumModerationService;
import net.coalevo.shellaccess.cmd.BaseCommand;
import net.coalevo.shellaccess.filters.SimplePool;
import net.coalevo.shellaccess.filters.StringListCompletionFilter;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.Services;
import net.wimpi.telnetd.io.toolkit.TabCompletionFilter;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.IOException;
import java.util.*;

/**
 * Abstract base class for discussion related commands.
 * <p/>
 * Provides the functionality common to the service access.
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public abstract class DiscussionCommand
    extends BaseCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(DiscussionCommand.class.getName());
  protected static SimplePool<TabCompletionFilter> c_ForumCompletionFilterPool = new SimplePool<TabCompletionFilter>();
  protected DiscussionService m_DiscussionService;
  protected ForumModerationService m_ForumModerationService;
  protected ForumDescriptor m_ActualForum;

  public boolean prepare(BasicShell shell) {
    if (!super.prepare(shell)) {
      return false;
    }
    m_DiscussionService = shell.getServices().getDiscussionService(Services.NO_WAIT);
    m_ForumModerationService = shell.getServices().getForumModerationService(Services.NO_WAIT);

    if (m_DiscussionService == null || m_ForumModerationService == null) {
      try {
        m_IO.printTemplate(PREFIX + "serviceunavailable");
      } catch (Exception ex) {
        log.error(c_LogMarker,"prepare()", ex);
      }
      return false;
    }
    //Actual Forum
    resetActualForum();
    if(m_ActualForum != null) {
      if(!m_DiscussionService.isSubscribed(m_Agent,m_ActualForum.getIdentifier(),m_AgentIdentifier)) {
        try {
          skipToNext();
        } catch (Exception ex) {
          return false;
        }
      }
    }
    return true;
  }//prepare

  /**
   * Resets the instance members.
   * Be sure you call <tt>super.reset();</tt> in your subclass
   * if you override this method.
   */
  public void reset() {
    super.reset();
    m_DiscussionService = null;
    m_ForumModerationService = null;
    m_Agent = null;
    m_AgentIdentifier = null;
    m_ActualForum = null;
    m_IO = null;
  }//reset

  private void setActualForumPrompt() {
    if (m_ActualForum != null) {
      HashMap<String, String> map = new HashMap<String, String>();
      map.put("seqnum", Integer.toString(m_ActualForum.getSequenceNumber()));
      map.put("name", m_ActualForum.getName());
      m_Shell.setEnvironmentVariable(BasicShell.PROMPT_ATTRIBUTES_KEY, map);
    }
  }//setActualForumPrompt

  protected void setActualForum(ForumDescriptor fd) {
    m_Shell.setEnvironmentVariable(ENV_ACTUALFORUM_KEY, fd);
    m_ActualForum = fd;
    setActualForumPrompt();
  }//setActualForum

  protected void resetActualForum() {
    Object o = m_Shell.getEnvironmentVariable(ENV_ACTUALFORUM_KEY);
    if (o != null) {
      m_ActualForum = (ForumDescriptor) o;
      if (m_ActualForum.isInvalidated()) {
        m_ActualForum = m_DiscussionService.getForumDescriptor(m_Agent, m_ActualForum.getIdentifier());
      }
    }
  }//resetActualForum

  protected ForumDescriptor promptForum()
      throws IOException, NoSuchForumException {
    ForumDescriptor fd = null;
    TabCompletionFilter fcf = c_ForumCompletionFilterPool.lease();
    if (fcf == null) {
      List<ForumDescriptor> fds = m_DiscussionService.listFora(m_Agent);
      List<String> list = new ArrayList<String>(fds.size() * 2); //3 for mnemonics
      for (ForumDescriptor fdes : fds) {
        if(fdes.isInviteOnly()) {
          continue;
        }
        list.add(fdes.getName());
        list.add(Integer.toString(fdes.getSequenceNumber()));
      }
      fcf = new StringListCompletionFilter(list);
    }
    String str = null;
    try {
      str = prompt(PREFIX + "promptforum", null, fcf);
    } catch (IOException ex) {
      log.error(c_LogMarker,"promptForum()", ex);
      throw ex;
    } finally {
      c_ForumCompletionFilterPool.release(fcf);
    }
    if(str == null || str.length() == 0) {
      m_IO.printTemplate(PREFIX + "aborted");
      return null;
    }
    if (Character.isDigit(str.charAt(0))) {
      try {
        int seqnum = Integer.parseInt(str);
        try {
          fd = m_DiscussionService.getForumDescriptorBySequenceNumber(m_Agent, seqnum);
        } catch (NoSuchForumException ex) {
          m_IO.printTemplate(PREFIX + "invalidseqnum");
          throw ex;
        }
      } catch (NumberFormatException ex) {
        //do nothing, still may be name or mnemonic (more difficult to distinguish
      }
    }
    if (fd == null) {
      //try name first, then mnemonic and finally to jump to anything tha
      //somehow matches a completion?.
      try {
        fd = m_DiscussionService.getForumDescriptorByName(m_Agent, str);
      } catch (NoSuchForumException ex) {
        try {
          fd = m_DiscussionService.getForumDescriptorByMnemonic(m_Agent, str);
        } catch (NoSuchForumException nsfex) {

            //Emulating jkara behavior
            List<ForumDescriptor> fds = m_DiscussionService.listFora(m_Agent);
            for (Iterator<ForumDescriptor> iterator = fds.iterator(); iterator.hasNext();) {
              ForumDescriptor forumDescriptor =  iterator.next();
              if(forumDescriptor.getName().toLowerCase().contains(str.toLowerCase())) {
                fd = forumDescriptor;
                break;
              }
            }
            if(fd == null) {
              throw new NoSuchForumException(str);
            }
        }
      }
    }
    m_IO.println();
    return fd;
  }//promptForum

  public void blowForumCompletions() {
    c_ForumCompletionFilterPool.clear();
  }//blowForumCompletions

  public ForumDescriptor getMainForum() {
    //get main forum
     return m_DiscussionService.getForumDescriptorBySequenceNumber(m_Agent, 0);
  }//getMainForum

  protected void skipToNext()
      throws DiscussionServiceException, IOException {

    List<ForumIdentifier> fids;

    if(!m_Shell.isEnviromentVariableSet(ENV_SKIPLIST_KEY)) {
      fids = m_DiscussionService.listSubscriptions(m_Agent, m_AgentIdentifier);
      m_Shell.setEnvironmentVariable(ENV_SKIPLIST_KEY,fids);
    } else {
      fids = (List<ForumIdentifier>) m_Shell.getEnvironmentVariable(ENV_SKIPLIST_KEY);
    }
    //skip actual
    fids.remove(m_ActualForum.getIdentifier());
    //implicitly ordered by sequence number by implementation!
    for (Iterator<ForumIdentifier> iterator = fids.listIterator(); iterator.hasNext();) {
      ForumIdentifier forumIdentifier = iterator.next();
      Subscription s = m_DiscussionService.getSubscription(m_Agent, forumIdentifier, m_AgentIdentifier);
      //This should be cached as well in memory
      if (m_DiscussionService.hasNewEntries(m_Agent, forumIdentifier, s.getLastRead())) {
        //goto forum and start reading?
        //Descriptor is cached
        setActualForum(m_DiscussionService.getForumDescriptor(m_Agent, forumIdentifier));
        return;
      }
    }
    //3. Nothing goto main, old behavior
    try {
      m_Shell.unsetEnvironmentVariable(ENV_SKIPLIST_KEY);
      ForumDescriptor fd = getMainForum();
      setActualForum(fd);
    } catch (NoSuchForumException nsfex) {
      m_IO.printTemplate(PREFIX + "nomainforum");
      log.error(c_LogMarker,"run()", nsfex);
      return;
    }
  }//skip  
  
  public static final String TS_PREFIX = "commands_net.coalevo.shellaccess.cmd.discussion.";
  private static final String PREFIX = TS_PREFIX + "DiscussionCommand_";
  private static final String ENV_ACTUALFORUM_KEY = "net.coalevo.discussion.actualforum";
  protected static final String ENV_SKIPLIST_KEY = "net.coalevo.discussion.skiplist";

}//class DiscussionCommand
