/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.discussion;

import net.coalevo.discussion.model.ForumDescriptor;
import net.coalevo.discussion.model.ForumIdentifier;
import net.coalevo.discussion.model.NoSuchForumException;
import net.coalevo.discussion.model.Subscription;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.shellaccess.impl.CommandShell;
import net.wimpi.telnetd.io.TemplateAttributes;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

/**
 * Provides a command to jump to the next unread message.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class NextNewEntry
    extends ReadEntries {

  private static Marker c_LogMarker = MarkerFactory.getMarker(NextNewEntry.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell)
      throws CommandException {
    prepare(shell);
    try {
      //1. Check for new mail
      shell.setEnvironmentVariable("CommandShell.start","true"); //silence command
      try {
        ((CommandShell)shell).runCommand("net.coalevo.shellaccess.cmd.postoffice.CheckForNewMessages");
      } catch (Exception ex) {
        log.error(c_LogMarker,"run()",ex);
      } finally {
        shell.unsetEnvironmentVariable("CommandShell.start");
      }

      //2. Check the actual forum (should also have last read)
      Subscription s = m_DiscussionService.getSubscription(
          m_Agent, m_ActualForum.getIdentifier(), m_AgentIdentifier);
      if (m_DiscussionService.hasNewEntries(m_Agent, m_ActualForum.getIdentifier(), s.getLastRead())) {
        List<String> eids = m_DiscussionService.listEntriesFrom(m_Agent, m_ActualForum.getIdentifier(), s.getLastRead());
        navigateEntries(eids);
        return;
      }

      //3. Use subscribed forums or stored skiplist
      List<ForumIdentifier> fids;

      if(!m_Shell.isEnviromentVariableSet(DiscussionCommand.ENV_SKIPLIST_KEY)) {
        //Hopefully this list is in the LRU cache if not for the next space it is
        fids = m_DiscussionService.listSubscriptions(m_Agent, m_AgentIdentifier);
        m_Shell.setEnvironmentVariable(DiscussionCommand.ENV_SKIPLIST_KEY,fids);
      } else {
        fids = (List<ForumIdentifier>) m_Shell.getEnvironmentVariable(ENV_SKIPLIST_KEY);
      }
      //actual already checked
      fids.remove(m_ActualForum.getIdentifier());
      //implicitly ordered by sequence number by implementation!
      for (Iterator<ForumIdentifier> iterator = fids.listIterator(); iterator.hasNext();) {
        ForumIdentifier forumIdentifier = iterator.next();
        s = m_DiscussionService.getSubscription(m_Agent, forumIdentifier, m_AgentIdentifier);
        //This should be cached as well in memory
        if (m_DiscussionService.hasNewEntries(m_Agent, forumIdentifier, s.getLastRead())) {
          //goto forum and start reading?
          //Descriptor is cached
          setActualForum(m_DiscussionService.getForumDescriptor(m_Agent, forumIdentifier));
          //get list
          List<String> eids = m_DiscussionService.listEntriesFrom(m_Agent, m_ActualForum.getIdentifier(), s.getLastRead());

          //Print out info for the user CSAB-70
          TemplateAttributes attr = m_IO.leaseTemplateAttributes();
          attr.add("newmsg", Integer.toString(eids.size()));
          attr.add("seqnum", "" + m_ActualForum.getSequenceNumber());
          attr.add("name", m_ActualForum.getName());
          attr.add("mnemonic", m_ActualForum.getMnemonic());
          attr.add("language", m_ActualForum.getLanguage().getDisplayLanguage(m_IO.getLocale()));
          attr.add("moderator", this.getFormattedAgent(m_ActualForum.getModerator()));
          m_IO.printTemplate(PREFIX + "forumnewinfo", attr);
          return;
        }
      }
    } catch (Exception ex) {
      log.error(c_LogMarker, "run()", ex);
    }
    //4. Nothing goto main, old behavior
    try {
      m_Shell.unsetEnvironmentVariable(ENV_SKIPLIST_KEY);
      ForumDescriptor fd = m_DiscussionService.getForumDescriptorBySequenceNumber(m_Agent, 0);
      setActualForum(fd);
    } catch (NoSuchForumException nsfex) {
      try {
        m_IO.printTemplate(PREFIX + "nomainforum");
      } catch (IOException e) {
        log.error("run()", e);
      }
      log.error(c_LogMarker, "run()", nsfex);
      return;
    }
  }//run


}//class NextNewEntry
