/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.discussion;

import net.coalevo.discussion.model.DiscussionServiceException;
import net.coalevo.discussion.model.ForumDescriptor;
import net.coalevo.discussion.model.ForumIdentifier;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.util.List;

/**
 * This class implements a {@link DiscussionCommand} that allows to
 * unsubscribe all sunscribed fora.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class UnsubscribeAllFora
    extends DiscussionCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(UnsubscribeAllFora.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell)
      throws CommandException {
    prepare(shell);
    try {
      m_PromptWrap = false;
      if (!isUserExpert()) {
        if (!promptDecision(PREFIX + "doit")) {
          m_IO.printTemplate(PREFIX + "aborted");
          return;
        }
      }
      List<ForumDescriptor> fora = m_DiscussionService.listFora(m_Agent);
      List<ForumIdentifier> subscribed = m_DiscussionService.listSubscriptions(m_Agent, m_AgentIdentifier);
      ForumIdentifier mainforum = getMainForum().getIdentifier();
      boolean noneunsubscribed = true;
      for (ForumDescriptor fd : fora) {
        ForumIdentifier fid = fd.getIdentifier();
        //can't unsubscribe non-subscribed or main forum, don't unsubscribe invite only fora
        if (!subscribed.contains(fid) ||
            fid.equals(mainforum) ||
            fd.isInviteOnly()) {
          continue;
        }
        try {
          m_DiscussionService.cancelSubscription(m_Agent, fid, m_AgentIdentifier);
          noneunsubscribed = false;
        } catch (DiscussionServiceException dex) {
          m_IO.printTemplate(PREFIX + "failedunsubscribe",
              "name", fd.getName(),
              "seqnum", Integer.toString(fd.getSequenceNumber())
          );
          log.error(c_LogMarker, "run()", dex);
        }
      }
      if(!noneunsubscribed) {
        m_IO.printTemplate(PREFIX + "done");
      } else {
        m_IO.printTemplate(PREFIX + "allunsubscribed");        
      }

    } catch (Exception ex) {
      log.error(c_LogMarker, "run()", ex);
    }
  }//run

  protected static final String PREFIX = TS_PREFIX + "UnsubscribeAllFora_";

}//class UnsubscribeAllFora