/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.discussion;

import net.coalevo.discussion.model.ForumDescriptor;
import net.coalevo.discussion.model.ForumIdentifier;
import net.coalevo.discussion.model.Subscription;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.wimpi.telnetd.io.TemplateAttributes;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Implements a command to list fora
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class ListFora
    extends DiscussionCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(ListFora.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell)
      throws CommandException {

    prepare(shell);
    try {
      TemplateAttributes attr = m_IO.leaseTemplateAttributes();

      List<ForumDescriptor> foras = m_DiscussionService.listFora(m_Agent);
      //1. Sort by name
      Collections.sort(foras, FORUM_SEQNUM_COMPARATOR);
      int totalnew = 0;
      for (ForumDescriptor fd : foras) {
        ForumIdentifier fid = fd.getIdentifier();
        String[] items;
        boolean subscribed = m_DiscussionService.isSubscribed(m_Agent, fid, m_AgentIdentifier);

        if (subscribed) {
          Subscription s = m_DiscussionService.getSubscription(m_Agent, fid, m_AgentIdentifier);
          boolean newentries = m_DiscussionService.hasNewEntries(m_Agent, fid, s.getLastRead());
          if (newentries) {
            int newmsg = m_DiscussionService.getNewEntryCount(m_Agent, fid, s.getLastRead());
            totalnew += newmsg;
            if (fd.isInviteOnly()) {
              items = new String[]{
                  Integer.toString(fd.getSequenceNumber()),
                  fd.getName(),
                  fd.getMnemonic(),
                  Integer.toString(newmsg),
                  ""
              };
            } else {
              items = new String[]{
                  Integer.toString(fd.getSequenceNumber()),
                  fd.getName(),
                  fd.getMnemonic(),
                  Integer.toString(newmsg),
                  null
              };
            }
            attr.add("snitems.{seqnum,name,mnemonic,new,inviteonly}", items);
          } else {
            if (fd.isInviteOnly()) {
              items = new String[]{
                  Integer.toString(fd.getSequenceNumber()),
                  fd.getName(),
                  fd.getMnemonic(),
                  ""
              };
            } else {
              items = new String[]{
                  Integer.toString(fd.getSequenceNumber()),
                  fd.getName(),
                  fd.getMnemonic(),
                  null
              };
            }
            attr.add("sitems.{seqnum,name,mnemonic,inviteonly}", items);
          }
        } else {
          //Don't display the invite only fora
          if (!fd.isInviteOnly()) {
            items = new String[]{
                Integer.toString(fd.getSequenceNumber()),
                fd.getName(),
                fd.getMnemonic()
            };
            attr.add("items.{seqnum,name,mnemonic}", items);
          }
        }
      }//endfor
      if (totalnew > 0) {
        attr.add("totalnew", Integer.toString(totalnew));
      }
      m_IO.pageTemplate(PREFIX + "list", attr);

    } catch (Exception ex) {
      log.error(c_LogMarker, "run()", ex);
    }
  }//run

  public static final Comparator<ForumDescriptor> FORUM_NAME_COMPARATOR = new Comparator<ForumDescriptor>() {
    public int compare(ForumDescriptor fd1, ForumDescriptor fd2) {
      return (fd1.getName().compareTo(fd2.getName()));
    }//compare
  };

  public static final Comparator<ForumDescriptor> FORUM_SEQNUM_COMPARATOR = new Comparator<ForumDescriptor>() {
    public int compare(ForumDescriptor fd1, ForumDescriptor fd2) {
      return fd1.getSequenceNumber() - fd2.getSequenceNumber();
    }//compare
  };

  private static final String PREFIX = TS_PREFIX + "ListFora_";


}//class ListFora
