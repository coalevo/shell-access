/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.discussion;

import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.discussion.model.Subscription;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * Provides a command to mark messages as read or unread.
 * <p/>
 * In reality this is a misnomer; all it does is manipulate
 * the subscription timestamp of the last read message.
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class MarkMessagesRead
    extends DiscussionCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(MarkMessagesRead.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell)
      throws CommandException {

    prepare(shell);
    try {

      if(isUserExpert() || promptDecision(PREFIX + "doit")) {
        Subscription s = m_DiscussionService.getSubscription(
            m_Agent, m_ActualForum.getIdentifier(), m_AgentIdentifier);
        s.setLastRead(System.currentTimeMillis()); //up to now all
        m_IO.printTemplate(PREFIX + "confirm");
      } else {
        m_IO.printTemplate(PREFIX + "abort");
      }

    } catch (Exception ex) {
      log.error(c_LogMarker,"run()", ex);
    }
  }//run

  protected static final String PREFIX = TS_PREFIX + "MarkMessagesRead_";

}//class MarkMessagesRead
