/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.discussion;

import net.coalevo.discussion.model.*;
import net.coalevo.shellaccess.impl.CommandShell;
import net.coalevo.shellaccess.impl.Formatter;
import net.coalevo.shellaccess.impl.RoleUtility;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.shellaccess.validators.IntegerInputValidator;
import net.coalevo.text.util.TagUtility;
import net.wimpi.telnetd.io.BasicTerminalIO;
import net.wimpi.telnetd.io.TemplateAttributes;
import net.wimpi.telnetd.io.toolkit.Editfield;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;

/**
 * Provides a base command for reading entries.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class ReadEntries
    extends DiscussionCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(ReadEntries.class.getName());

  protected boolean m_Pending = false;
  protected Subscription m_Subscription;
  protected boolean m_Backreading = false;
  protected String m_LastAncestor = null;

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell)
      throws CommandException {
    prepare(shell);
    try {
      try {

        int maxnum = m_DiscussionService.getEntryCount(m_Agent, m_ActualForum.getIdentifier());
        if (maxnum <= 0) {
          m_IO.printTemplate(PREFIX + "noentries");
          return;
        }
        //prompt
        TemplateAttributes attr = m_IO.leaseTemplateAttributes();
        attr.add("max", Integer.toString(maxnum));
        m_IO.printTemplate(PREFIX + "numentries", attr);

        Editfield ef = new Editfield(m_IO.getTerminalIO(), "input", 15, 50);
        ef.setIgnoreDelete(true);
        ef.setInputValidator(
            new IntegerInputValidator(
                0,
                maxnum,
                m_IO.formatTemplate(
                    "inputerror_intvalidator",
                    "min",
                    Integer.toString(0),
                    "max",
                    Integer.toString(maxnum)
                ),
                true
            )
        );
        ef.run();
        String val = ef.getValue();
        int nummsg = 0;
        if(val != null && val.trim().length() > 0) {
          nummsg = Integer.parseInt(ef.getValue().trim());
        }
        if (nummsg == 0) {
          m_IO.printTemplate(PREFIX + "aborted");
          return;
        }
        List<String> eids =
            m_DiscussionService.listEntries(m_Agent, m_ActualForum.getIdentifier(), nummsg);
        navigateEntries(eids);
      } catch (SecurityException ex) {
        m_IO.printTemplate(PREFIX + "notallowed");
      } catch (DiscussionServiceException e) {
        m_IO.printTemplate(PREFIX + "failed");
      }
    } catch (Exception ex) {
      log.error(c_LogMarker, "run()", ex);
    }
  }//run

  protected void navigateEntries(List<String> ids) throws Exception {
    m_Subscription = m_DiscussionService.getSubscription(m_Agent, m_ActualForum.getIdentifier(), m_AgentIdentifier);
    boolean done = false;
    boolean forward = true;
    String id = null;
    ListIterator<String> iter = ids.listIterator();

    if (m_Backreading) {
      //1. spool iterator to last
      while (iter.hasNext()) iter.next();
      //2. set reading direction to backwards
      forward = false;
      id = iter.previous();
    } else {
      id = iter.next();
    }

    char fwd = m_IO.formatTemplate("forward").charAt(0);
    char bwd = m_IO.formatTemplate("backward").charAt(0);
    char stop = m_IO.formatTemplate("stop").charAt(0);
    char delete = m_IO.formatTemplate("delete").charAt(0);
    char approve = m_IO.formatTemplate("approve").charAt(0);
    char disapprove = m_IO.formatTemplate("disapprove").charAt(0);
    char move = m_IO.formatTemplate("move").charAt(0);
    char skip = m_IO.formatTemplate("skip").charAt(0);
    char identifier = m_IO.formatTemplate("identifier").charAt(0);
    char reply = m_IO.formatTemplate("reply").charAt(0);
    char quickreply = m_IO.formatTemplate("quickreply").charAt(0);
    char ancestor = m_IO.formatTemplate("ancestor").charAt(0);

    char next = ' '; //always space
    BasicTerminalIO bio = m_IO.getTerminalIO();
    boolean skipprompt = false;
    //display first entry
    displayEntry(id, m_ActualForum.getIdentifier());

    do {
      TemplateAttributes attr = null;
      if (!skipprompt) {
        //print prompt
        attr = m_IO.leaseTemplateAttributes();
        attr.add("fseqnum", Integer.toString(m_ActualForum.getSequenceNumber()));
        attr.add("fname", m_ActualForum.getName());
        attr.add("numentries", Integer.toString(ids.size()));
        if (forward) {
          attr.add("numremain", Integer.toString(iter.nextIndex()));
          attr.add("forward", "");
        } else {
          attr.add("numremain", Integer.toString(iter.nextIndex() + 1));
          attr.add("backward", "");
        }
      }
      //Allow async event I/O
      m_IO.setAsyncEventIO(true);
      char ch = (char) m_IO.prompt(PREFIX + "navprompt", attr, skipprompt);
      m_Shell.getSession().activity();
      m_IO.setAsyncEventIO(false);

      if (ch == fwd) {
        if (!forward) {
          iter.next();
          forward = true;
        }
      } else if (ch == bwd) {
        if (forward) {
          iter.previous();
          forward = false;
        }
      } else if (ch == skip) {
        done = true;
        skipToNext();
        continue;
      } else if (ch == identifier) {
        m_IO.printTemplate(PREFIX + "identifier","uuid", id);
        skipprompt = false;
        continue;
      } else if(ch == reply) {
        m_Shell.setEnvironmentVariable("posts.replyto",id);
        m_IO.println();
        ((CommandShell) m_Shell).runCommand("net.coalevo.shellaccess.cmd.discussion.AddEntry");
        continue;
      } else if(ch == quickreply) {
        m_Shell.setEnvironmentVariable("posts.replyto",id);
        m_IO.println();
        ((CommandShell) m_Shell).runCommand("net.coalevo.shellaccess.cmd.discussion.QuickAddEntry");
        continue;
      } else if(ch == ancestor) {
        if(m_LastAncestor != null) {
          displayEntry(m_LastAncestor, m_ActualForum.getIdentifier());
          skipprompt = false;
        }
        continue;
      } else if (ch == delete) {
        try {
          if (promptDecision(PREFIX + "dodelete")) {
            m_DiscussionService.removeEntry(m_Agent, m_ActualForum.getIdentifier(), id);
            m_IO.printTemplate(PREFIX + "confirmdelete");
            iter.remove();
          } else {
            continue;
          }
        } catch (SecurityException ex) {
          m_IO.printTemplate(PREFIX + "notalloweddelete");
          continue;  //prompt
        } catch (DiscussionServiceException e) {
          m_IO.printTemplate(PREFIX + "faileddelete");
          continue; //prompt
        }
        //drop through to get the next entry in actual direction!
      } else if (ch == move) {
        if (!move(id)) {
          continue;
        } else {
          iter.remove();
        }
      } else if (ch == stop) {
        done = true;
        continue;
      } else if (ch == approve && m_Pending) {
        if (!approval(id, true)) {
          continue;
        } else {
          iter.remove();
        }
        //drop through to get the next entry in the actual direction!
      } else if (ch == disapprove && m_Pending) {
        if (!approval(id, false)) {
          continue;
        } else {
          iter.remove();
        }
        //drop through to get the next  entry in the actual direction!
      } else if (ch != next) {
        //try run longprompt command
        if (!((CommandShell) m_Shell).runLongPromptCommand((int) ch)) {
          bio.bell();
          skipprompt = true;
        }
        continue;
      }
      if (forward && iter.hasNext()) {
        id = iter.next();
        skipprompt = false;
        //print entry
        displayEntry(id, m_ActualForum.getIdentifier());
        continue;
      }
      if (!forward && iter.hasPrevious()) {
        id = iter.previous();
        skipprompt = false;
        //print entry
        displayEntry(id, m_ActualForum.getIdentifier());
        continue;
      }
      done = true;
    } while (!done);
    m_IO.println();
  }//navigateEntries

  protected void displayEntry(String eid, ForumIdentifier fid) throws Exception {
    boolean sub = (fid != null);
    //1. Descriptor
    EntryDescriptor e =
        m_DiscussionService.getEntryDescriptor(m_Agent, fid, eid);
    //Get ForumIdentifier from encountered descriptor
    if(fid == null) {
      fid = e.getForumIdentifier();
    }
    //Ancestor logic
    String anid = e.getAncestor();
    if(anid != null && anid.length() > 0) {
      m_LastAncestor = anid;
    }
    //2. Content
    Content c =
        m_DiscussionService.getEntryContent(m_Agent, fid, eid);
    //3. Prepare date format
    DateFormat df = getStandardDateTimeFormat();
    //4. Attributes and page template
    TemplateAttributes attr = m_IO.leaseTemplateAttributes();
    attr.add("eid", eid);
    if(!sub) {
      ForumDescriptor fd = m_DiscussionService.getForumDescriptor(m_Agent, fid);
      attr.add("fname", fd.getName());
      attr.add("fmnemonic", fd.getMnemonic());      
    }
    if (e.isOfficial()) {
      attr.add("official", RoleUtility.getPriorityRoleWithStyle(getRoles(e.getAuthor())));
    }
    //Retain last author
    m_Shell.setEnvironmentVariable("profile.lastuser",e.getAuthor());
    attr.add("author", this.getFormattedAgent(e.getAuthor()));
    attr.add("created", df.format(new Date(e.getCreated())));
    if (m_ActualForum.isModeratedStrict()) {
      attr.add("approved", df.format(new Date(e.getApproved())));
    }
    String tmp = e.getCaption();
    if (tmp != null && tmp.length() > 0) {
      attr.add("caption", tmp);
    }
    tmp = TagUtility.fromSet(e.getTags());
    if (tmp != null && tmp.length() > 0) {
      attr.add("tags", tmp);
    }
    if (!c.isEmpty()) {
      attr.add("content", Formatter.format(e.getContentFormat(), eid, c.getContent()));
    }

    boolean ewr = m_Shell.getSession().isEventsWhileReading();
    m_IO.setAsyncEventIO(ewr);
    m_IO.pageTemplate(PREFIX + "entry", attr);
    m_IO.setAsyncEventIO(true);
    m_IO.flushEventBuffer();

    //mark as read, note that this is done in-memory if cached,
    //and only synched later on
    long approved = e.getApproved();
    if (sub && (m_Subscription.getLastRead() < approved)) {
      m_Subscription.setLastRead(approved);
    }
  }//displayEntry

  private boolean approval(String eid, boolean approved) throws IOException {

    try {
      //Todo: Notification
      if (approved) {
        m_ForumModerationService.approveEntry(
            m_Agent, m_ActualForum.getIdentifier(), eid, false);
        m_IO.printTemplate(PREFIX + "confirmapproved");
      } else {
        if (promptDecision(PREFIX + "dodisapprove")) {
          m_ForumModerationService.disapproveEntry(
              m_Agent, m_ActualForum.getIdentifier(), eid, false);
          m_IO.printTemplate(PREFIX + "confirmdisapproved");
        } else {
          return false;
        }
      }
      return true;
    } catch (SecurityException ex) {
      m_IO.printTemplate(PREFIX + "notallowedapproval");
      return false;
    } catch (DiscussionServiceException e) {
      m_IO.printTemplate(PREFIX + "failedapproval");
      return false;
    }

  }//approval

  private boolean move(String eid)
      throws IOException {
    try {
      ForumDescriptor to = promptForum();
      if (to == null) {
        return false;
      }
      m_ForumModerationService.moveEntry(
          m_Agent, m_ActualForum.getIdentifier(), to.getIdentifier(), eid);
      m_IO.printTemplate(PREFIX + "confirmmoved");
      return true;
    } catch (SecurityException ex) {
      m_IO.printTemplate(PREFIX + "notallowedmove");
      return false;
    } catch (DiscussionServiceException e) {
      m_IO.printTemplate(PREFIX + "failedmove");
      return false;
    }
  }//move

  public void reset() {
    m_Pending = false;
    m_Subscription = null;
    m_Backreading = false;
    m_LastAncestor = null;
    super.reset();
  }//reset

  protected static final String PREFIX = TS_PREFIX + "ReadEntries_";

}//class ReadEntries
