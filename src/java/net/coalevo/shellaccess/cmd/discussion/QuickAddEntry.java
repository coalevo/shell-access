/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.discussion;

import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.discussion.model.EditableEntry;
import net.coalevo.discussion.model.AnonymousNotAllowedException;
import net.coalevo.discussion.model.DiscussionServiceException;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * Provides a command for adding a link to
 * the actual forum.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class QuickAddEntry
    extends AddEntry {
  
  private static Marker c_LogMarker = MarkerFactory.getMarker(QuickAddEntry.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell)
      throws CommandException {
    prepare(shell);
    try {
      EditableEntry een = null;
      try {
        //1.Begin create entry
        een = m_DiscussionService.beginEntryCreate(m_Agent, m_ActualForum.getIdentifier());
        m_Quick = true;
        doEntry(een);

      } catch (AnonymousNotAllowedException anaex) {
        m_IO.printTemplate(PREFIX + "anonymousnotallowed");
        return;
      } catch (DiscussionServiceException dsex) {
        if (een != null) {
          m_DiscussionService.cancelEntryCreate(m_Agent, een);
        }
        m_IO.printTemplate(PREFIX + "failed");
        return;
      } catch (SecurityException ex) {
        m_IO.printTemplate(PREFIX + "notallowed");
        return;
      }
    } catch (Exception ex) {
      log.error(c_LogMarker,"run()", ex);
    }
  }//run

}//class AddEntry
