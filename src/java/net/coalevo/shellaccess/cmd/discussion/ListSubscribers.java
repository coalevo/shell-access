/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.discussion;

import net.coalevo.discussion.model.DiscussionServiceException;
import net.coalevo.discussion.model.ForumDescriptor;
import net.coalevo.discussion.model.NoSuchForumException;
import net.coalevo.discussion.model.Subscription;
import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.shellaccess.validators.StringValidator;
import net.wimpi.telnetd.io.TemplateAttributes;

import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * Provides a command that allows to list subscribers.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class ListSubscribers
    extends DiscussionCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(ListSubscribers.class.getName());
  protected ForumDescriptor m_Forum;

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell)
      throws CommandException {
    prepare(shell);
    try {
      try {
        m_PromptWrap = false;
        m_Forum = promptForum();
        if(m_Forum == null) {
          return;
        }
      } catch (NoSuchForumException nsfex) {
        return; //we are done
      }
      doListSubscribers();
    } catch (Exception ex) {
      log.error(c_LogMarker,"run()", ex);
    }
  }//run

  public void doListSubscribers()
      throws Exception {

    try {
      //1. get agent
      List<AgentIdentifier> subscribed =
          m_DiscussionService.listSubscribers(m_Agent, m_Forum.getIdentifier());

      if (subscribed.isEmpty()) {
        m_IO.printTemplate(PREFIX + "nonesubscribed");
        return;
      } else if (subscribed.size() > 75) {  //wimpi: arbitrary size, thats about three pages
        if (promptDecision(PREFIX + "filter")) {
          String startswith = prompt(PREFIX + "startwith", new StringValidator());
          for (Iterator<AgentIdentifier> iterator = subscribed.iterator(); iterator.hasNext();) {
            AgentIdentifier agentIdentifier = iterator.next();
            if (!agentIdentifier.getName().startsWith(startswith)) {
              iterator.remove();
            }
          }
        }
      }
      SimpleDateFormat sdf = (SimpleDateFormat) SimpleDateFormat.getDateInstance(
          SimpleDateFormat.SHORT, m_IO.getLocale());
      TemplateAttributes attr = m_IO.leaseTemplateAttributes();
      for (AgentIdentifier aid : subscribed) {
        Subscription s = m_DiscussionService.getSubscription(m_Agent, m_Forum.getIdentifier(), aid);
        String[] items = new String[]{
            aid.getIdentifier(), //who
            sdf.format(s.getCreated()) //since
        };
        attr.add("items.{who,since}", items);
      }
      m_IO.pageTemplate(PREFIX + "list", attr);
    } catch (DiscussionServiceException ex) {
      m_IO.printTemplate(PREFIX + "failed");
    } catch (SecurityException ex) {
      m_IO.printTemplate(PREFIX + "notallowed");
    }
  }//doListSubscribers

  public void reset() {
    m_Forum = null;
    super.reset();
  }//reset

  protected static final String PREFIX = TS_PREFIX + "ListSubscribers_";

}//class ListSubscribers
