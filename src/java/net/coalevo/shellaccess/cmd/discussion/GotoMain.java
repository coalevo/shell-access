/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.discussion;

import net.coalevo.discussion.model.ForumDescriptor;
import net.coalevo.discussion.model.NoSuchForumException;
import net.coalevo.discussion.model.Subscription;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.wimpi.telnetd.io.TemplateAttributes;

import java.io.IOException;

import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * Implements a start command for taking the user to
 * the main forum (sequence number 0).
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class GotoMain
    extends DiscussionCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(GotoMain.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell)
      throws CommandException {
    prepare(shell);
    try {

      //1. Get main forum
      ForumDescriptor fd = m_DiscussionService.getForumDescriptorBySequenceNumber(m_Agent, 0);
      //2. Ensure subscription
      if (!m_DiscussionService.isSubscribed(m_Agent, fd.getIdentifier(), m_AgentIdentifier)) {
        Subscription s =
            m_DiscussionService.beginSubscriptionCreate(m_Agent, fd.getIdentifier(), m_AgentIdentifier);
        s.setNote(m_IO.formatTemplate(PREFIX + "subnote"));
        s.setNotifying(true);
        m_DiscussionService.commitSubscription(m_Agent, s);
        String nick = this.resolveNickname(m_AgentIdentifier);
        if (nick == null || nick.length() == 0) {
          nick = m_AgentIdentifier.getName();
        }
        TemplateAttributes attr = m_IO.leaseTemplateAttributes();
        attr.add("name", nick);
        m_IO.printTemplate(PREFIX + "welcometomain", attr);
      }
      //3. Make it the actual forum
      setActualForum(fd);

    } catch (NoSuchForumException nsfex) {
      try {
        m_IO.printTemplate(PREFIX + "nomainforum");
      } catch (IOException e) {
        log.error(c_LogMarker,"run()", e);
      }
      log.error(c_LogMarker,"run()", nsfex);
      return;
    } catch (Exception ex) {
      log.error(c_LogMarker,"run()", ex);
    }
  }//run

  private static final String PREFIX = TS_PREFIX + "GotoMain_";

}//class GotoMain
