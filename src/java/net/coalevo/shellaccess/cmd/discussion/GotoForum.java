/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.discussion;

import net.coalevo.discussion.model.ForumDescriptor;
import net.coalevo.discussion.model.NoSuchForumException;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.wimpi.telnetd.io.TemplateAttributes;
import org.slf4j.MarkerFactory;
import org.slf4j.Marker;

/**
 * Implements a command for jumping to forum the basic way.
 * <p/>
 * Note that this derives from the subscription command
 * {@link SubscribeForum} because it will ask if you wish to subscribe.
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class GotoForum
    extends SubscribeForum {

  private static Marker c_LogMarker = MarkerFactory.getMarker(GotoForum.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell)
      throws CommandException {
    prepare(shell);
    try {
      ForumDescriptor fd = null;
      try {
        m_PromptWrap = false; //don't wrap the first
        fd = promptForum();
        if(fd == null) {
          return;
        }
      } catch (NoSuchForumException nsfex) {
        return; //we are done
      }
      if(fd == null) {
        m_IO.printTemplate(PREFIX + "aborted");
        return;
      }
      //check ban, pro-active, may be done via exception
      if (m_DiscussionService.isBanned(m_Agent, fd.getIdentifier(), m_AgentIdentifier)) {
        m_IO.printTemplate(PREFIX + "banned");
        return;
      }
      //check subscription
      if (!m_DiscussionService.isSubscribed(m_Agent, fd.getIdentifier(), m_AgentIdentifier)) {
        //check invite only
        if(m_DiscussionService.isInviteOnly(m_Agent,fd.getIdentifier())) {
          m_IO.printTemplate(PREFIX + "notallowed");
          return;
        }
        //gather forum info for template
        TemplateAttributes attr = m_IO.leaseTemplateAttributes();
        attr.add("name", fd.getName());
        attr.add("mnemonic", fd.getMnemonic());
        attr.add("seqnum",Integer.toString(fd.getSequenceNumber()));
        attr.add("language", fd.getLanguage().getDisplayLanguage(m_IO.getLocale()));
        m_IO.printTemplate(PREFIX + "notsubscribed",attr);

        if(isUserExpert()) {
          m_Forum = fd;
          m_Who = m_AgentIdentifier;
          subscribe(false);
        } else {
          if (promptDecision(PREFIX + "asksubscribe")) {
            m_Forum = fd;
            m_Who = m_AgentIdentifier;
            subscribe(false);
          } else {
            m_IO.println();
            return;
          }
        }
      }
      //else we are there
      setActualForum(fd);
    } catch (Exception ex) {
      log.error(c_LogMarker,"run()", ex);
    }
  }//run

  private static final String PREFIX = TS_PREFIX + "GotoForum_";

}//class GotoForum
