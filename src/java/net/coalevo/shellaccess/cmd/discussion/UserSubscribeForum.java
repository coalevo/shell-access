/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.discussion;

import net.coalevo.discussion.model.ForumIdentifier;
import net.coalevo.discussion.model.NoSuchForumException;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;

import java.util.List;

import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * Implements a user command for subscribing a forum.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class UserSubscribeForum
    extends SubscribeForum {

  private static Marker c_LogMarker = MarkerFactory.getMarker(UserSubscribeForum.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell)
      throws CommandException {
    prepare(shell);
    try {
      //Get forum descriptor through name with tab completion (yeah) :)
      try {
        List<String> fnames = m_DiscussionService.listForaNames(m_Agent);
        List<ForumIdentifier> fidsub = m_DiscussionService.listSubscriptions(m_Agent, m_AgentIdentifier);
        fnames.removeAll(
            m_DiscussionService.resolveIdentifiers(m_Agent, fidsub, true)
        );
        if (fnames.size() == 0) {
          m_IO.printTemplate(PREFIX + "allsubscribed");
          return;
        }
        m_PromptWrap = false;
        m_Forum = promptForum();
        if(m_Forum == null) {
          return;
        }
        if(m_Forum == null) {
           m_IO.printTemplate(PREFIX + "aborted");
          return;
        }

        if(m_DiscussionService.isInviteOnly(m_Agent,m_Forum.getIdentifier())) {
          m_IO.printTemplate(PREFIX + "notallowed");
        }
        if (m_DiscussionService.isSubscribed(m_Agent, m_Forum.getIdentifier(), m_AgentIdentifier)) {
          m_IO.printTemplate(PREFIX + "subscribed");
          return;
        }
      } catch (NoSuchForumException nsfex) {
        m_IO.printTemplate(PREFIX + "nosuchforumname");
        log.error(c_LogMarker,"run()", nsfex);
        return;
      }
      m_Who = m_AgentIdentifier;
      subscribe(false);
    } catch (Exception ex) {
      log.error(c_LogMarker,"run()", ex);
    }
  }//run

}//class SubscribeForum
