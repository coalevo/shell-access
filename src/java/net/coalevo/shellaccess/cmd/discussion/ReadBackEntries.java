/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.discussion;

import net.coalevo.discussion.model.DiscussionServiceException;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.shellaccess.validators.IntegerInputValidator;
import net.wimpi.telnetd.io.TemplateAttributes;
import net.wimpi.telnetd.io.toolkit.Editfield;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.util.List;

/**
 * This class extends {@link ReadEntries} to read back a certain amount of entries.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class ReadBackEntries
    extends ReadEntries {

  private static Marker c_LogMarker = MarkerFactory.getMarker(ReadBackEntries.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell)
      throws CommandException {
    prepare(shell);
    try {
      try {

        int maxnum = m_DiscussionService.getEntryCount(m_Agent, m_ActualForum.getIdentifier());
        if (maxnum <= 0) {
          m_IO.printTemplate(PREFIX + "noentries");
          return;
        }
        //prompt
        TemplateAttributes attr = m_IO.leaseTemplateAttributes();
        attr.add("max", Integer.toString(maxnum));
        m_IO.printTemplate(PREFIX + "numentries", attr);

        Editfield ef = new Editfield(m_IO.getTerminalIO(), "input", 15, 50);
        ef.setIgnoreDelete(true);
        ef.setInputValidator(
            new IntegerInputValidator(
                0,
                maxnum,
                m_IO.formatTemplate(
                    "inputerror_intvalidator",
                    "min",
                    Integer.toString(0),
                    "max",
                    Integer.toString(maxnum)
                ),
                true
            )
        );
        ef.run();
        String val = ef.getValue();
        int nummsg = 0;
        if(val != null && val.trim().length() > 0) {
          nummsg = Integer.parseInt(ef.getValue().trim());      
        }
        if (nummsg == 0) {
          m_IO.printTemplate(PREFIX + "aborted");
          return;
        }
        List<String> eids =
            m_DiscussionService.listEntries(m_Agent, m_ActualForum.getIdentifier(), nummsg);
        m_Backreading = true;
        navigateEntries(eids);
      } catch (SecurityException ex) {
        m_IO.printTemplate(PREFIX + "notallowed");
      } catch (DiscussionServiceException e) {
        m_IO.printTemplate(PREFIX + "failed");
      }
    } catch (Exception ex) {
      log.error(c_LogMarker, "run()", ex);
    }
  }//run

}//class ReadBackEntries
