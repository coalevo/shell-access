/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.discussion;

import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.discussion.model.DiscussionServiceException;

import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * This class implements a command that allows to read a message with a given
 * identifier in the actual forum.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class ReadEntry
    extends ReadEntries {

  private static Marker c_LogMarker = MarkerFactory.getMarker(ReadEntry.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell)
      throws CommandException {
    prepare(shell);
    try {
      try {

        int maxnum = m_DiscussionService.getEntryCount(m_Agent, m_ActualForum.getIdentifier());
        if (maxnum <= 0) {
          m_IO.printTemplate(PREFIX + "noentries");
          return;
        }
        //prompt
        m_PromptWrap = false;
        String eid = prompt(PREFIX + "prompteid");
        if (eid == null || eid.length() == 0) {
          m_IO.printTemplate(PREFIX + "aborted");
          return;
        }
        displayEntry(eid, null);
      } catch (SecurityException ex) {
        m_IO.printTemplate(PREFIX + "notallowed");
      } catch (DiscussionServiceException e) {
        m_IO.printTemplate(PREFIX + "failed");
      }
    } catch (Exception ex) {
      log.error(c_LogMarker,"run()", ex);
    }
  }//run

  protected static final String PREFIX = TS_PREFIX + "ReadEntry_";

}//class ReadEntry
