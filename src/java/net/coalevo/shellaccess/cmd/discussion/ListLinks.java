/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.discussion;

import net.coalevo.discussion.model.DiscussionServiceException;
import net.coalevo.discussion.model.EditableLink;
import net.coalevo.discussion.model.ForumDescriptor;
import net.coalevo.discussion.model.Link;
import net.coalevo.shellaccess.impl.CommandShell;
import net.coalevo.shellaccess.impl.Formatter;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.shellaccess.validators.StringValidator;
import net.coalevo.text.util.TagUtility;
import net.wimpi.telnetd.io.BasicTerminalIO;
import net.wimpi.telnetd.io.TemplateAttributes;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;

/**
 * Provides a command for listing the links in a given forum.
 * <p/>
 * Navigation:
 * back  (change dir)
 * forward (change dir)
 * space (one into direction)
 * stop (break out of navigation loop)
 * delete (delete)
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class ListLinks
    extends DiscussionCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(ListLinks.class.getName());
  boolean m_Pending = false;

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell)
      throws CommandException {
    prepare(shell);
    try {
      try {
        List<String> lids = m_DiscussionService.listLinks(m_Agent,
            m_ActualForum.getIdentifier());
        if (lids.isEmpty()) {
          m_IO.printTemplate(PREFIX + "nolinks");
          return;
        } else {
          navigateLinks(lids);
          m_IO.println();
        }
      } catch (SecurityException ex) {
        m_IO.printTemplate(PREFIX + "notallowed");
        return;
      } catch (DiscussionServiceException e) {
        m_IO.printTemplate(PREFIX + "failed");
        return;
      }
    } catch (Exception ex) {
      log.error(c_LogMarker, "run()", ex);
    }
  }//run

  protected void navigateLinks(List<String> lids) throws Exception {
    ListIterator<String> iter = lids.listIterator();
    boolean done = false;
    boolean forward = true;
    String id = iter.next();
    char fwd = m_IO.formatTemplate("forward").charAt(0);
    char bwd = m_IO.formatTemplate("backward").charAt(0);
    char stop = m_IO.formatTemplate("stop").charAt(0);
    char delete = m_IO.formatTemplate("delete").charAt(0);
    char approve = m_IO.formatTemplate("approve").charAt(0);
    char disapprove = m_IO.formatTemplate("disapprove").charAt(0);
    char update = m_IO.formatTemplate("update").charAt(0);
    char move = m_IO.formatTemplate("move").charAt(0);

    char next = ' '; //always space
    BasicTerminalIO bio = m_IO.getTerminalIO();
    boolean skipprompt = false;
    //display first link
    displayLink(id);

    do {
      TemplateAttributes attr = null;
      if (!skipprompt) {
        //print prompt
        attr = m_IO.leaseTemplateAttributes();
        attr.add("fseqnum", Integer.toString(m_ActualForum.getSequenceNumber()));
        attr.add("fname", m_ActualForum.getName());
        attr.add("numlinks", Integer.toString(lids.size()));
        if (forward) {
          attr.add("numremain", Integer.toString(iter.nextIndex()));
          attr.add("forward", "");
        } else {
          attr.add("numremain", Integer.toString(iter.nextIndex() + 1));
          attr.add("backward", "");
        }
      }
      m_IO.setAsyncEventIO(true);      
      char ch = (char) m_IO.prompt(PREFIX + "navprompt", attr, skipprompt);
      m_Shell.getSession().activity();
      m_IO.setAsyncEventIO(false);

      if (ch == fwd) {
        if (!forward) {
          iter.next();
          forward = true;
        }
      } else if (ch == bwd) {
        if (forward) {
          iter.previous();
          forward = false;
        }
      } else if (ch == delete) {
        try {
          if (promptDecision(PREFIX + "dodelete")) {
            m_DiscussionService.removeLink(m_Agent, m_ActualForum.getIdentifier(), id);
            m_IO.printTemplate(PREFIX + "confirmdelete");
            iter.remove();
          } else {
            continue;
          }
        } catch (SecurityException ex) {
          m_IO.printTemplate(PREFIX + "notalloweddelete");
          continue;  //prompt
        } catch (DiscussionServiceException e) {
          m_IO.printTemplate(PREFIX + "faileddelete");
          continue; //prompt
        }
        //drop through to get the next link in actual direction!
      } else if (ch == move) {
        if (!move(id)) {
          continue;
        } else {
          iter.remove();
        }
      } else if (ch == update) {
        if (update(id)) {
          continue;
        }
      } else if (ch == stop) {
        done = true;
        continue;
      } else if (ch == approve && m_Pending) {
        if (!approval(id, true)) {
          continue;
        } else {
          iter.remove();
        }
        //drop through to get the next link in the actual direction!
      } else if (ch == disapprove && m_Pending) {
        if (!approval(id, false)) {
          continue;
        } else {
          iter.remove();
        }
        //drop through to get the next link in the actual direction!
      } else if (ch != next) {
        //try run longprompt command
        if (!((CommandShell) m_Shell).runLongPromptCommand((int) ch)) {
          bio.bell();
          skipprompt = true;
        }
        continue;
      }
      if (forward && iter.hasNext()) {
        id = iter.next();
        skipprompt = false;
        //print link
        displayLink(id);
        continue;
      }
      if (!forward && iter.hasPrevious()) {
        id = iter.previous();
        skipprompt = false;
        //print link
        displayLink(id);
        continue;
      }
      done = true;
    } while (!done);
  }//navigateLinks

  protected void displayLink(String lid) throws Exception {
    Link l = m_DiscussionService.getLink(m_Agent, m_ActualForum.getIdentifier(), lid);
    DateFormat df = getStandardDateFormat();
    TemplateAttributes attr = m_IO.leaseTemplateAttributes();
    attr.add("author", l.getAuthor().getIdentifier());
    attr.add("nick", resolveFormattedNickname(l.getAuthor()));
    attr.add("caption", l.getCaption());
    attr.add("created", df.format(new Date(l.getCreated())));
    attr.add("modified", df.format(new Date(l.getModified())));
    if (l.isFunctional()) {
      attr.add("functional", "");
    }
    if (l.getLastChecked() > 0) {
      attr.add("lastchecked", df.format(new Date(l.getLastChecked())));
    }
    attr.add("link", l.getLink());
    attr.add("tags", TagUtility.fromSet(l.getTags()));
    if (l.getDescription() != null) {
      attr.add("description", Formatter.format(l.getDescriptionFormat(), l.getIdentifier(), l.getDescription()));
    }
    boolean ewr = m_Shell.getSession().isEventsWhileReading();
    m_IO.setAsyncEventIO(ewr);
    m_IO.pageTemplate(PREFIX + "link", attr);
    m_IO.setAsyncEventIO(true);
    m_IO.flushEventBuffer();    
  }//displayLink

  private boolean approval(String lid, boolean approved) throws IOException {
    try {
      //Todo: Notification
      if (approved) {
        m_ForumModerationService.approveLink(m_Agent, m_ActualForum.getIdentifier(), lid, false);
        m_IO.printTemplate(PREFIX + "confirmapproved");
      } else {
        if (promptDecision(PREFIX + "dodisapprove")) {
          m_ForumModerationService.disapproveLink(m_Agent, m_ActualForum.getIdentifier(), lid, false);
          m_IO.printTemplate(PREFIX + "confirmdisapproved");
        } else {
          return false;
        }
      }
      return true;
    } catch (SecurityException ex) {
      m_IO.printTemplate(PREFIX + "notallowedapproval");
      return false;
    } catch (DiscussionServiceException e) {
      m_IO.printTemplate(PREFIX + "failedapproval");
      return false;
    }
  }//approval

  private boolean update(String lid) {
    try {
      EditableLink el = null;
      try {
        //1.Start Link Add
        el = m_ForumModerationService.beginLinkUpdate(
            m_Agent, m_ActualForum.getIdentifier(), lid);
        StringValidator sv = new StringValidator(m_IO.formatTemplate("inputerror_stringvalidator","size","1"));
        el.setCaption(prompt(PREFIX + "caption", el.getCaption(), sv));
        String link = prompt(PREFIX + "editlink", el.getLink(), sv, m_IO.getColumns() - 1, 2048);
        try {
          URI l = new URI(link);
          if (!l.isAbsolute()) {
            throw new URISyntaxException(link, "not absolute");
          }
          el.setLink(l.toString());
        } catch (URISyntaxException usex) {
          m_IO.printTemplate(PREFIX + "invaliduri");
          return false;
        }
        if (promptDecision(PREFIX + "askdescription")) {
          String format = promptInputFormat(el.getDescriptionFormat());
          String txt = editTransformableContent(format,el.getDescription());

          if (txt == null || txt.length() == 0) {
            m_IO.printTemplate(PREFIX + "aborted");
            return false;
          }
          el.setDescription(txt);
        }
        el.setTags(TagUtility.toSet(prompt(PREFIX + "tags", TagUtility.fromSet(el.getTags()))));
        //commit
        if (promptDecision(PREFIX + "doupdate")) {
          m_ForumModerationService.commitLinkUpdate(m_Agent, el);
          m_IO.printTemplate(PREFIX + "confirmupdate");
          String format = el.getDescriptionFormat();
          String id = el.getIdentifier();
          Formatter.invalidateCache(format, id);
          return true;
        } else {
          m_ForumModerationService.cancelLinkUpdate(m_Agent, el);
          m_IO.printTemplate(PREFIX + "aborted");
          return false;
        }
      } catch (DiscussionServiceException dsex) {
        if (el != null) {
          m_ForumModerationService.cancelLinkUpdate(m_Agent, el);
        }
        m_IO.printTemplate(PREFIX + "failedupdate");
        return false;
      } catch (SecurityException ex) {
        m_IO.printTemplate(PREFIX + "notallowedupdate");
        return false;
      }
    } catch (Exception ex) {
      return false;
    }
  }//update

  private boolean move(String lid)
      throws IOException {
    try {
      ForumDescriptor to = promptForum();
      if (to == null) {
        return false;
      }
      m_ForumModerationService.moveLink(
          m_Agent, m_ActualForum.getIdentifier(), to.getIdentifier(), lid);
      m_IO.printTemplate(PREFIX + "confirmmoved");
      return true;
    } catch (SecurityException ex) {
      m_IO.printTemplate(PREFIX + "notallowedmove");
      return false;
    } catch (DiscussionServiceException e) {
      m_IO.printTemplate(PREFIX + "failedmove");
      return false;
    }
  }//move

  public void reset() {
    m_Pending = false;
    super.reset();
  }//reset

  protected static final String PREFIX = TS_PREFIX + "ListLinks_";

}//class ListLinks
