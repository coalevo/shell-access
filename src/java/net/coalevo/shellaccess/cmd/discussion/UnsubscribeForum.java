/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.discussion;

import net.coalevo.discussion.model.DiscussionServiceException;
import net.coalevo.discussion.model.ForumDescriptor;
import net.coalevo.discussion.model.NoSuchForumException;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.foundation.model.AgentIdentifier;

import java.io.IOException;

import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * Implements a command for unsubscribing a forum.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class UnsubscribeForum
    extends DiscussionCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(UnsubscribeForum.class.getName());

  protected ForumDescriptor m_MainForum;
  protected ForumDescriptor m_Forum;
  protected AgentIdentifier m_Who;

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell)
      throws CommandException {
    prepare(shell);
    try {
      //get main forum
      m_MainForum = getMainForum();
      try {
        m_PromptWrap = false;
        m_Forum = promptForum();
        if(m_Forum == null) {
          return;
        }
        String agent = prompt(PREFIX + "agent");
        m_Who = new AgentIdentifier(agent);
      } catch (NoSuchForumException nsfex) {
        //template already printed
        return;
      }
      unsubscribe();
    } catch (Exception ex) {
      log.error(c_LogMarker,"run()", ex);
    }
  }//run

  protected void unsubscribe() throws IOException {
    //Assert subscription and that the main forum isn't unsubscribed
    if (!m_DiscussionService.isSubscribed(m_Agent, m_Forum.getIdentifier(), m_Who)) {
      m_IO.printTemplate(PREFIX + "notsubscribed");
      return;
    }
    if (m_Forum.getIdentifier().equals(m_MainForum.getIdentifier())) {
      m_IO.printTemplate(PREFIX + "notmainforum");
      return;
    }
    if(m_Forum.getModerator().getIdentifier().equals(m_Who.getIdentifier())) {
      m_IO.printTemplate(PREFIX + "notmoderator");
      return;
    }
    //check if really
    if (isUserExpert() || promptDecision(PREFIX + "doit")) {
      try {
        m_DiscussionService.cancelSubscription(m_Agent, m_Forum.getIdentifier(), m_Who);
      } catch (DiscussionServiceException ex) {
        m_IO.printTemplate(PREFIX + "failed");
        log.error(c_LogMarker,"run()", ex);
      }
      m_IO.printTemplate(PREFIX + "confirm");
    } else {
      m_IO.printTemplate(PREFIX + "aborted");
    }
  }//unsubscribe

  public void reset() {
    super.reset();
    m_Forum = null;
    m_MainForum = null;
    m_Who = null;
  }//reset

  protected static final String PREFIX = TS_PREFIX + "UnsubscribeForum_";

}//class UnsubscribeForum
