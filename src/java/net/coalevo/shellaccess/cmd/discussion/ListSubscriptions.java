/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.discussion;

import net.coalevo.discussion.model.DiscussionServiceException;
import net.coalevo.discussion.model.ForumIdentifier;
import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.shellaccess.validators.StringValidator;
import net.wimpi.telnetd.io.TemplateAttributes;

import java.util.List;

import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * Provides a command that allows to list subscriptions of a
 * given user.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class ListSubscriptions
    extends DiscussionCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(ListSubscriptions.class.getName());
  protected AgentIdentifier m_Who;

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell)
      throws CommandException {
    prepare(shell);
    try {
      String agent = prompt(PREFIX + "agent", new StringValidator());
      m_Who = new AgentIdentifier(agent);
      doListSubscriptions();
    } catch (Exception ex) {
      log.error(c_LogMarker,"run()", ex);
    }
  }//run

  public void doListSubscriptions()
      throws Exception {

    try {
      //1. get agent
      List<ForumIdentifier> subscriptions =
          m_DiscussionService.listSubscriptions(m_Agent, m_Who);

      if (subscriptions.isEmpty()) {
        m_IO.printTemplate(PREFIX + "nosubscriptions");
        return;
      }
      //resolve to names
      List<String> fnames = m_DiscussionService.resolveIdentifiers(m_Agent, subscriptions, true);

      TemplateAttributes attr = m_IO.leaseTemplateAttributes();
      for (String fname : fnames) {
        attr.add("names", fname);
      }
      m_IO.pageTemplate(PREFIX + "list", attr);
    } catch (DiscussionServiceException ex) {
      m_IO.printTemplate(PREFIX + "failed");
    } catch (SecurityException ex) {
      m_IO.printTemplate(PREFIX + "notallowed");
    }
  }//doListSubscriptions

  public void reset() {
    m_Who = null;
    super.reset();
  }//reset

  protected static final String PREFIX = TS_PREFIX + "ListSubscriptions_";

}//class ListSubscriptions
