/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.discussion;

import net.coalevo.shellaccess.model.BaseShellCommandProvider;
import net.coalevo.shellaccess.model.ShellCommandProvider;

/**
 * A {@link ShellCommandProvider} for discussion commands.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class DiscussionCommandProvider
    extends BaseShellCommandProvider {

  public DiscussionCommandProvider() {
    super(DiscussionCommandProvider.class.getPackage().getName());
    prepare();
  }//constructor

  private void prepare() {
    //Start command
    addCommand("GotoMain", GotoMain.class);

    //Admin commands
    addCommand("CreateForum", CreateForum.class);
    addCommand("DestroyForum", DestroyForum.class);
    addCommand("UpdateForum", UpdateForum.class);
    addCommand("UpdateForumInfo", UpdateForumInfo.class);
    addCommand("SubscribeForum", SubscribeForum.class);
    addCommand("AddBan", AddBan.class);
    addCommand("UpdateBan", UpdateBan.class);
    addCommand("RemoveBan", RemoveBan.class);
    addCommand("ListBans", ListBans.class);
    addCommand("ListSubscribers", ListSubscribers.class);
    addCommand("ListSubscriptions", ListSubscriptions.class);
    addCommand("UnsubscribeForum", UnsubscribeForum.class);
    addCommand("RebuildEntryIndex", RebuildEntryIndex.class);
    addCommand("UpdateEntryIndex", UpdateEntryIndex.class);

    //Forum Moderator commands
    addCommand("FMUpdateForumInfo", FMUpdateForumInfo.class);
    addCommand("FMAddBan", FMAddBan.class);
    addCommand("FMUpdateBan", FMUpdateBan.class);
    addCommand("FMRemoveBan", FMRemoveBan.class);
    addCommand("FMListBans", FMListBans.class);
    addCommand("FMSubscribeForum", FMSubscribeForum.class);
    addCommand("FMListSubscribers", FMListSubscribers.class);
    addCommand("ListPendingLinks", ListPendingLinks.class);
    addCommand("CheckLinks", CheckLinks.class);
    addCommand("AddOfficialEntry", AddOfficialEntry.class);
    addCommand("FMUnsubscribeForum", FMUnsubscribeForum.class);

    //User commands
    addCommand("ListFora", ListFora.class);
    addCommand("UserSubscribeForum", UserSubscribeForum.class);
    addCommand("GotoForum", GotoForum.class);
    addCommand("ShowForumInfo", ShowForumInfo.class);
    addCommand("AddLink", AddLink.class);
    addCommand("ListLinks", ListLinks.class);
    addCommand("AddEntry", AddEntry.class);
    addCommand("QuickAddEntry", QuickAddEntry.class);
    addCommand("AddAnonymousEntry", AddAnonymousEntry.class);
    addCommand("NextNewEntry", NextNewEntry.class);
    addCommand("ReadPendingEntries", ReadPendingEntries.class);
    addCommand("ReadEntries", ReadEntries.class);
    addCommand("ReadBackEntries", ReadBackEntries.class);
    addCommand("SearchEntries", SearchEntries.class);
    addCommand("SkipForum", SkipForum.class);
    addCommand("UserUnsubscribeForum", UserUnsubscribeForum.class);
    addCommand("UserUnsubscribeActualForum", UserUnsubscribeActualForum.class);
    addCommand("MarkMessagesRead", MarkMessagesRead.class);
    addCommand("ToggleNotification",ToggleNotification.class);
    addCommand("ReadEntry", ReadEntry.class);
    addCommand("SubscribeAllFora", SubscribeAllFora.class);
    addCommand("UnsubscribeAllFora", UnsubscribeAllFora.class);

  }//prepare

}//class DiscussionCommandProvider
