/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.discussion;

import net.coalevo.discussion.model.DiscussionServiceException;
import net.coalevo.discussion.model.EditableForumDescriptor;
import net.coalevo.text.util.TagUtility;
import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.shellaccess.validators.IntegerInputValidator;
import net.coalevo.shellaccess.validators.LanguageInputValidator;

import java.io.IOException;
import java.util.Locale;

import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * Implements a command for creating a forum.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class CreateForum
    extends DiscussionCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(CreateForum.class.getName());
  
  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell)
      throws CommandException {
    prepare(shell);
    try {
      //1. begin transaction
      EditableForumDescriptor fd = m_DiscussionService.beginForumCreate(m_Agent);

      //2. collect info
      m_PromptWrap = false;
      fd.setSequenceNumber(
          Integer.parseInt(
              prompt(
                  PREFIX + "seqnum",
                  new IntegerInputValidator(m_IO.formatTemplate("inputerror_integervalidator"))
              ).trim()
          )
      );
      fd.setName(prompt(PREFIX + "name"));
      fd.setMnemonic(prompt(PREFIX + "mnemonic"));
      fd.setModerator(
          new AgentIdentifier(prompt(PREFIX + "moderator"))
      );
      fd.setLanguage(
          new Locale(
              prompt(
                  PREFIX + "language",
                  new LanguageInputValidator(m_IO.formatTemplate("inputerror_languagevalidator"))
              )
          )
      );
      fd.setInviteOnly(promptDecision(PREFIX + "inviteonly"));
      fd.setModeratedStrict(promptDecision(PREFIX + "strictmod"));
      fd.setAnonymousAllowed(promptDecision(PREFIX + "anonallowed"));
      fd.setPublic(promptDecision(PREFIX + "public"));
      fd.setTags(TagUtility.toSet(prompt(PREFIX + "tags")));
      fd.setInfoFormat(promptInputFormat(fd.getInfoFormat()));
      fd.setMaxActive(
            Integer.parseInt(
                prompt(
                    PREFIX + "maxactive",
                    new IntegerInputValidator(
                        200,
                        1000,
                        m_IO.formatTemplate(
                            "inputerror_intvalidator",
                            "min",
                            Integer.toString(200),
                            "max",
                            Integer.toString(1000)
                        )
                    )
                ).trim()
            )
      );

      //3. commit transaction
      if (promptDecision(PREFIX + "doit")) {
        try {
          m_DiscussionService.commitForum(m_Agent, fd);
          blowForumCompletions();
        } catch (DiscussionServiceException ex) {
          m_IO.printTemplate(PREFIX + "failed");
          log.error(c_LogMarker,"run()", ex);
        }
        m_IO.printTemplate(PREFIX + "confirm");
      } else {
        m_DiscussionService.cancelForumCreate(m_Agent, fd);
        m_IO.printTemplate(PREFIX + "aborted");
      }
    } catch (SecurityException ex) {
      try {
        m_IO.printTemplate(PREFIX + "notallowed", null);
      } catch (IOException e) {
        log.error(c_LogMarker,"run()", e);
      }
    } catch (Exception ex) {
      log.error(c_LogMarker,"run()", ex);
    }
  }//run

  private static final String PREFIX = TS_PREFIX + "CreateForum_";

}//class CreateForum
