/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.discussion;

import net.coalevo.discussion.model.EditableForumDescriptor;
import net.coalevo.discussion.model.ForumDescriptor;
import net.coalevo.discussion.model.NoSuchForumException;
import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.shellaccess.validators.IntegerInputValidator;
import net.coalevo.shellaccess.validators.LanguageInputValidator;
import net.coalevo.text.util.TagUtility;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.IOException;
import java.util.Locale;

/**
 * Implements a command for creating a forum.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class UpdateForum
    extends DiscussionCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(UpdateForum.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell)
      throws CommandException {
    prepare(shell);
    try {
      ForumDescriptor oldfd = null;
      try {
        m_PromptWrap = false;
        oldfd = promptForum();
        if (oldfd == null) {
          return;
        }
      } catch (NoSuchForumException nsfex) {
        return; //we are done
      }
      //1. begin transaction
      EditableForumDescriptor fd = null;
      try {
        fd = m_ForumModerationService.beginForumUpdate(m_Agent, oldfd.getIdentifier());
      } catch (IllegalStateException ex) {
        m_IO.printTemplate(PREFIX + "beingedited");
        return;
      }
      try {
        //2. collect info
        fd.setSequenceNumber(
            Integer.parseInt(prompt(PREFIX + "seqnum", Integer.toString(fd.getSequenceNumber()),
                new IntegerInputValidator(m_IO.formatTemplate("inputerror_integervalidator"))).trim())
        );
        fd.setName(prompt(PREFIX + "name", fd.getName()));
        fd.setMnemonic(prompt(PREFIX + "mnemonic", fd.getMnemonic()));
        fd.setModerator(
            new AgentIdentifier(prompt(PREFIX + "moderator", fd.getModerator().getIdentifier()))
        );
        fd.setLanguage(
            new Locale(prompt(PREFIX + "language", fd.getLanguage().getLanguage(),
                new LanguageInputValidator(m_IO.formatTemplate("inputerror_languagevalidator"))))
        );
        fd.setInviteOnly(promptDecision(PREFIX + "inviteonly"));
        fd.setModeratedStrict(promptDecision(PREFIX + "strictmod"));
        fd.setAnonymousAllowed(promptDecision(PREFIX + "anonallowed"));
        fd.setPublic(promptDecision(PREFIX + "public"));
        fd.setTags(TagUtility.toSet(prompt(PREFIX + "tags", TagUtility.fromSet(fd.getTags()))));
        fd.setInfoFormat(promptInputFormat(fd.getInfoFormat()));
        fd.setMaxActive(
            Integer.parseInt(prompt(PREFIX + "maxactive", Integer.toString(fd.getMaxActive()),
                new IntegerInputValidator(
                    200,
                    1000, m_IO.formatTemplate(
                    "inputerror_intvalidator",
                    "min",
                    Integer.toString(200),
                    "max",
                    Integer.toString(1000)
                )
                )).trim())
        );

        //3. commit transaction
        if (promptDecision(PREFIX + "doit")) {
          m_ForumModerationService.commitForumUpdate(m_Agent, fd);
          blowForumCompletions();
          //just in case
          setActualForum(m_DiscussionService.getForumDescriptor(m_Agent, fd.getIdentifier()));
          m_IO.printTemplate(PREFIX + "confirm");
        } else {
          m_ForumModerationService.cancelForumUpdate(m_Agent, fd);
          m_IO.printTemplate(PREFIX + "aborted");
        }
      } catch (Exception ex) {
        m_ForumModerationService.cancelForumUpdate(m_Agent, fd);
        m_IO.printTemplate(PREFIX + "failed");
        log.error(c_LogMarker, "run()", ex);
      }

    } catch (SecurityException ex) {
      try {
        m_IO.printTemplate(PREFIX + "notallowed", null);
      } catch (IOException e) {
        log.error(c_LogMarker, "run()", e);
      }
    } catch (Exception ex) {
      log.error(c_LogMarker, "run()", ex);
    }
  }//run

  private static final String PREFIX = TS_PREFIX + "UpdateForum_";

}//class UpdateForum
