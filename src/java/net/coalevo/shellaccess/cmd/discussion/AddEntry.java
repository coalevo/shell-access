/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.discussion;

import net.coalevo.discussion.model.AnonymousNotAllowedException;
import net.coalevo.discussion.model.DiscussionServiceException;
import net.coalevo.discussion.model.EditableEntry;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.text.util.TagUtility;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * Provides a command for adding an entry to
 * the actual forum.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class AddEntry
    extends DiscussionCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(AddEntry.class.getName());

  protected boolean m_Quick = false;
  protected String m_Content;

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell)
      throws CommandException {
    prepare(shell);
    try {
      EditableEntry een = null;
      try {
        //1.Begin create entry
        een = m_DiscussionService.beginEntryCreate(m_Agent, m_ActualForum.getIdentifier());
        doEntry(een);

      } catch (AnonymousNotAllowedException anaex) {
        m_IO.printTemplate(PREFIX + "anonymousnotallowed");
        return;
      } catch (DiscussionServiceException dsex) {
        if (een != null) {
          m_DiscussionService.cancelEntryCreate(m_Agent, een);
        }
        m_IO.printTemplate(PREFIX + "failed");
        return;
      } catch (SecurityException ex) {
        m_IO.printTemplate(PREFIX + "notallowed");
        return;
      }
    } catch (Exception ex) {
      log.error(c_LogMarker, "run()", ex);
    }
  }//run

  public void reset() {
    m_Quick = false;
    super.reset();
  }//reset

  protected void doEntry(EditableEntry een) throws Exception {
    m_PromptWrap = false; //don't wrap the first
    //check content
    if (een.getContent().isEmpty()) {
      // Reset reply environment
      if (m_Shell.isEnviromentVariableSet("posts.replyto")) {
        een.setAncestor(m_Shell.getEnvironmentVariable("posts.replyto").toString());
        m_Shell.unsetEnvironmentVariable("posts.replyto");
      }
      if (m_Quick) {
        een.setContentFormat("");
        m_IO.println();
        m_IO.printTemplate(PREFIX + "body");
        String val = simpleEdit(null);
        if (val == null || val.length() == 0) {
          m_IO.printTemplate(PREFIX + "aborted");
          return;
        } else {
          een.getContent().setContent(val);
          m_IO.println();
        }
      } else {
        String tmp = prompt(PREFIX + "caption");
        if (tmp == null || tmp.length() == 0) {
          m_IO.printTemplate(PREFIX + "aborted");
          return;
        } else {
          een.setCaption(tmp);
        }
        String f = promptInputFormat(null);
        een.setContentFormat(f);
        String txt = editTransformableContent(f, null);

        if (txt == null || txt.length() == 0) {
            m_IO.printTemplate(PREFIX + "aborted");
            return;
        }
        een.getContent().setContent(txt);
      }
    } else {
      String f = een.getContentFormat();
      String txt = editTransformableContent(f, een.getContent().getContent());

      if (txt == null || txt.length() == 0) {
          m_IO.printTemplate(PREFIX + "aborted");
          return;
      }
      een.getContent().setContent(txt);
    }
    //Prepare prompt
    char save = m_IO.formatTemplate("save").charAt(0);
    char tags = m_IO.formatTemplate("tags").charAt(0);
    char caption = m_IO.formatTemplate("caption").charAt(0);
    char abort = m_IO.formatTemplate("abort").charAt(0);
    char edit = m_IO.formatTemplate("edit").charAt(0);

    boolean skipprompt = false;
    boolean done = false;
    m_PromptWrap = true;
    do {
      char ch = (char) m_IO.prompt(PREFIX + "addprompt", null, skipprompt);
      if (ch == save) {
        m_DiscussionService.commitEntry(m_Agent, een);
        if (m_ActualForum.isModeratedStrict() && !m_AgentIdentifier.equals(m_ActualForum.getModerator())) {
          m_IO.printTemplate(PREFIX + "confirmapproval");
        } else {
          m_IO.printTemplate(PREFIX + "confirm");
        }
        done = true;
      } else if (ch == tags) {
        een.setTags(
            TagUtility.toSet(
                prompt(
                    PREFIX + "tags",
                    TagUtility.fromSet(een.getTags())
                )
            )
        );
        skipprompt = false;
      } else if (ch == caption) {
        een.setCaption(prompt(PREFIX + "caption", een.getCaption()));
        skipprompt = false;
      } else if (ch == abort) {
        m_DiscussionService.cancelEntryCreate(m_Agent, een);
        m_IO.printTemplate(PREFIX + "aborted");
        done = true;
      } else if (ch == edit) {
        m_Quick = false;
        doEntry(een);
        done = true;
      } else {
        skipprompt = true;
      }
    } while (!done);
  }//doEntry

  protected static final String PREFIX = TS_PREFIX + "AddEntry_";

}//class AddEntry
