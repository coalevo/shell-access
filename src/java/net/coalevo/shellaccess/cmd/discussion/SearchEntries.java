/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.discussion;

import net.coalevo.discussion.model.*;
import net.coalevo.shellaccess.impl.CommandShell;
import net.coalevo.shellaccess.impl.Formatter;
import net.coalevo.shellaccess.impl.RoleUtility;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.text.util.TagUtility;
import net.wimpi.telnetd.io.BasicTerminalIO;
import net.wimpi.telnetd.io.TemplateAttributes;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;

/**
 * Provides a command for searching entries.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class SearchEntries
    extends DiscussionCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(SearchEntries.class.getName());
  protected boolean m_Pending = false;
  protected Subscription m_Subscription;

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell)
      throws CommandException {
    prepare(shell);
    try {
      try {
        //prompt
        m_PromptWrap = false;
        String query = prompt(PREFIX + "query");
        if (query == null || query.length() == 0) {
          m_IO.printTemplate(PREFIX + "aborted");
          return;
        }
        List<EntryHit> hits =
            m_DiscussionService.searchEntries(m_Agent, query, 50);
        if (hits.isEmpty()) {
          m_IO.printTemplate(PREFIX + "noentries");
          return;
        }
        navigateEntries(hits, query);
      } catch (SecurityException ex) {
        m_IO.printTemplate(PREFIX + "notallowed");
      } catch (DiscussionServiceException e) {
        m_IO.printTemplate(PREFIX + "failed");
      }
    } catch (Exception ex) {
      log.error(c_LogMarker, "run()", ex);
    }
  }//run

  protected void navigateEntries(List<EntryHit> hits, String query) throws Exception {
    ListIterator<EntryHit> iter = hits.listIterator();
    boolean done = false;
    boolean forward = true;
    EntryHit hit = iter.next();
    char fwd = m_IO.formatTemplate("forward").charAt(0);
    char bwd = m_IO.formatTemplate("backward").charAt(0);
    char stop = m_IO.formatTemplate("stop").charAt(0);
    char delete = m_IO.formatTemplate("delete").charAt(0);
    char move = m_IO.formatTemplate("move").charAt(0);
    char skip = m_IO.formatTemplate("skip").charAt(0);
    char identifier = m_IO.formatTemplate("identifier").charAt(0);
    //TODO: Add move?

    char next = ' '; //always space
    BasicTerminalIO bio = m_IO.getTerminalIO();
    boolean skipprompt = false;
    //display first link
    displayEntry(hit);

    do {
      TemplateAttributes attr = null;
      if (!skipprompt) {
        //print prompt
        attr = m_IO.leaseTemplateAttributes();
        attr.add("query", query);
        attr.add("numentries", Integer.toString(hits.size()));
        if (forward) {
          attr.add("numremain", Integer.toString(iter.nextIndex()));
          attr.add("forward", "");
        } else {
          attr.add("numremain", Integer.toString(iter.nextIndex() + 1));
          attr.add("backward", "");
        }
      }
      //Allow async event I/O
      m_IO.setAsyncEventIO(true);
      char ch = (char) m_IO.prompt(PREFIX + "navprompt", attr, skipprompt);
      m_Shell.getSession().activity();
      m_IO.setAsyncEventIO(false);

      if (ch == fwd) {
        if (!forward) {
          iter.next();
          forward = true;
        }
      } else if (ch == bwd) {
        if (forward) {
          iter.previous();
          forward = false;
        }
      } else if (ch == skip) {
        done = true;
        skipToNext();
        continue;
      } else if (ch == identifier) {
        m_IO.printTemplate(PREFIX + "identifier","uuid", hit.getEntryIdentifier());
        skipprompt = false;
        continue;
      } else if (ch == delete) {
        try {
          if (promptDecision(PREFIX + "dodelete")) {
            m_DiscussionService.removeEntry(
                m_Agent, hit.getForumIdentifier(),
                hit.getEntryIdentifier()
            );
            m_IO.printTemplate(PREFIX + "confirmdelete");
            iter.remove();
          } else {
            continue;
          }
        } catch (SecurityException ex) {
          m_IO.printTemplate(PREFIX + "notalloweddelete");
          continue;  //prompt
        } catch (DiscussionServiceException e) {
          m_IO.printTemplate(PREFIX + "faileddelete");
          continue; //prompt
        }
        //drop through to get the next link in actual direction!
      } else if (ch == move) {
        if (!move(hit)) {
          continue;
        } else {
          iter.remove();
        }
      } else if (ch == stop) {
        done = true;
        continue;
      } else if (ch != next) {
        //try run longprompt command
        if (!((CommandShell) m_Shell).runLongPromptCommand((int) ch)) {
          bio.bell();
          skipprompt = true;
        }
        continue;
      }
      if (forward && iter.hasNext()) {
        hit = iter.next();
        skipprompt = false;
        //print link
        displayEntry(hit);
        if (iter.hasNext()) {
          continue;
        } else {
          done = true;
        }
      }
      if (!forward && iter.hasPrevious()) {
        hit = iter.previous();
        skipprompt = false;
        //print link
        displayEntry(hit);
        if (iter.hasPrevious()) {
          continue;
        } else {
          done = true;
        }
      }
      done = true;
    } while (!done);
    m_IO.println();
  }//navigateLinks

  protected void displayEntry(EntryHit hit) throws Exception {
    //1. Descriptors
    ForumDescriptor fd =
        m_DiscussionService.getForumDescriptor(m_Agent, hit.getForumIdentifier());

    EntryDescriptor e =
        m_DiscussionService.getEntryDescriptor(m_Agent, hit.getForumIdentifier(), hit.getEntryIdentifier());

    //2. Content
    Content c =
        m_DiscussionService.getEntryContent(m_Agent, hit.getForumIdentifier(), hit.getEntryIdentifier());
    //3. Prepare date format

    DateFormat df = getStandardDateTimeFormat();

    //4. Attributes and page template
    TemplateAttributes attr = m_IO.leaseTemplateAttributes();
    attr.add("eid", hit.getEntryIdentifier());
    if (e.isOfficial()) {
      attr.add("official", RoleUtility.getPriorityRoleWithStyle(getRoles(e.getAuthor())));
    }
    attr.add("fname", fd.getName());
    attr.add("fmnemonic", fd.getMnemonic());

    attr.add("author", getFormattedAgent(e.getAuthor()));
    attr.add("created", df.format(new Date(e.getCreated())));
    if (fd.isModeratedStrict()) {
      attr.add("approved", df.format(new Date(e.getApproved())));
    }

    String tmp = e.getCaption();
    if (tmp != null && tmp.length() > 0) {
      attr.add("caption", tmp);
    }
    tmp = TagUtility.fromSet(e.getTags());
    if (tmp != null && tmp.length() > 0) {
      attr.add("tags", tmp);
    }
    if (!c.isEmpty()) {
      attr.add("content", Formatter.format(e.getContentFormat(), hit.getEntryIdentifier(), c.getContent()));
    }
    boolean ewr = m_Shell.getSession().isEventsWhileReading();    
    m_IO.setAsyncEventIO(ewr);
    m_IO.pageTemplate(PREFIX + "entry", attr);
    m_IO.setAsyncEventIO(true);
    m_IO.flushEventBuffer();    
  }//displayEntry

  private boolean move(EntryHit hit)
      throws IOException {
    try {
      ForumDescriptor to = promptForum();
      if (to == null) {
        return false;
      }
      m_ForumModerationService.moveEntry(
          m_Agent, hit.getForumIdentifier(),
          to.getIdentifier(),
          hit.getEntryIdentifier()
      );
      m_IO.printTemplate(PREFIX + "confirmmoved");
      return true;
    } catch (SecurityException ex) {
      m_IO.printTemplate(PREFIX + "notallowedmove");
      return false;
    } catch (DiscussionServiceException e) {
      m_IO.printTemplate(PREFIX + "failedmove");
      return false;
    }
  }//move

  public void reset() {
    m_Pending = false;
    m_Subscription = null;
    super.reset();
  }//reset

  protected static final String PREFIX = TS_PREFIX + "SearchEntries_";

}//class SearchEntries
