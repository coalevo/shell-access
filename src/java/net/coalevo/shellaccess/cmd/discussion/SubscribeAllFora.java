/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.discussion;

import net.coalevo.discussion.model.*;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.util.List;

/**
 * This class implements a {@link DiscussionCommand} that allows to
 * subscribe all fora.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class SubscribeAllFora
    extends DiscussionCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(SubscribeAllFora.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell)
      throws CommandException {
    prepare(shell);
    try {
      m_PromptWrap = false;
      if (!isUserExpert()) {
        if (!promptDecision(PREFIX + "doit")) {
          m_IO.printTemplate(PREFIX + "aborted");
          return;
        }
      }
      List<ForumDescriptor> fora = m_DiscussionService.listFora(m_Agent);
      List<ForumIdentifier> subscribedfora = m_DiscussionService.listSubscriptions(m_Agent,m_AgentIdentifier);
      boolean allsubscribed = true;
      for (ForumDescriptor fd : fora) {
        try {
          if (!fd.isInviteOnly() &&
              !subscribedfora.contains(fd.getIdentifier())) {
            //subscribe
            Subscription s = m_DiscussionService.beginSubscriptionCreate(
                m_Agent,
                fd.getIdentifier(),
                m_AgentIdentifier
            );
            m_DiscussionService.commitSubscription(m_Agent, s);
            allsubscribed = false;
          }
        } catch (BannedException bex) {
          m_IO.printTemplate(PREFIX + "banned",
              "name", fd.getName(),
              "seqnum", Integer.toString(fd.getSequenceNumber())
          );

        } catch (DiscussionServiceException dex) {
          m_IO.printTemplate(PREFIX + "failedsubscribe",
              "name", fd.getName(),
              "seqnum", Integer.toString(fd.getSequenceNumber())
          );
          log.error(c_LogMarker, "run()", dex);
        }
      }
      if(allsubscribed) {
        m_IO.printTemplate(PREFIX + "allsubscribed");        
      } else {
        m_IO.printTemplate(PREFIX + "done");
      }
    } catch (Exception ex) {
      log.error(c_LogMarker, "run()", ex);
    }
  }//run

  protected static final String PREFIX = TS_PREFIX + "SubscribeAllFora_";

}//class SubscribeAllFora
