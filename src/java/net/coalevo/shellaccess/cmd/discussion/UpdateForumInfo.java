/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.discussion;

import net.coalevo.discussion.model.EditableContent;
import net.coalevo.discussion.model.EditableForumDescriptor;
import net.coalevo.discussion.model.ForumDescriptor;
import net.coalevo.discussion.model.NoSuchForumException;
import net.coalevo.shellaccess.impl.Formatter;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * Provides a command to update the forum info.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class UpdateForumInfo
    extends DiscussionCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(UpdateForumInfo.class.getName());

  protected ForumDescriptor m_Forum;

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell) throws CommandException {
    prepare(shell);
    try {
      try {
        m_PromptWrap = false;
        m_Forum = promptForum();
        if (m_Forum == null) {
          return;
        }
      } catch (NoSuchForumException nsfex) {
        return; //we are done
      }
      update();
    } catch (Exception ex) {
      log.error(c_LogMarker, "run()", ex);
    }
  }//run

  public void update() throws Exception {
    //Body/Content
    if (m_Forum == null) {
      return;
    }
    EditableForumDescriptor fd;
    try {
      fd = m_ForumModerationService.beginForumUpdate(m_Agent, m_Forum.getIdentifier());
    } catch (IllegalStateException ex) {
      m_IO.printTemplate(PREFIX + "failed");
      return;
    }

    if (fd == null) {
      m_IO.printTemplate(PREFIX + "failed");
      return;
    }

    try {
      fd.setInfoFormat(promptInputFormat(fd.getInfoFormat()));
      m_ForumModerationService.commitForumUpdate(m_Agent, fd);
    } catch (Exception ex) {
      m_ForumModerationService.cancelForumUpdate(m_Agent, fd);
      m_IO.printTemplate(PREFIX + "failed");
      log.error(c_LogMarker, "run()", ex);
    }


    EditableContent ec =
        m_ForumModerationService.beginForumInfoUpdate(m_Agent, m_Forum.getIdentifier());

    if (ec == null) {
      m_IO.printTemplate(PREFIX + "failed");
      return;
    }

    String format = m_Forum.getInfoFormat();
    String id = m_Forum.getIdentifier().getIdentifier();

    try {
      String txt = editTransformableContent(format, ec.getContent());
      if (txt == null || txt.length() == 0) {
        m_ForumModerationService.cancelForumInfoUpdate(m_Agent, m_Forum.getIdentifier());
        m_IO.printTemplate(PREFIX + "aborted");
        return;
      }
      ec.setContent(txt);

      //commit
      m_ForumModerationService.commitForumInfoUpdate(m_Agent, m_Forum.getIdentifier(), ec);
      Formatter.invalidateCache(format, id);
      m_IO.printTemplate(PREFIX + "confirm");
    } catch (Exception dex) {
      m_ForumModerationService.cancelForumInfoUpdate(m_Agent, m_Forum.getIdentifier());
      m_IO.printTemplate(PREFIX + "failed");
      log.error(c_LogMarker, "run()", dex);
    }
  }//update

  public void reset() {
    m_Forum = null;
    super.reset();
  }//reset

  protected static final String PREFIX = TS_PREFIX + "UpdateForumInfo_";

}//class UpdateForumInfo
