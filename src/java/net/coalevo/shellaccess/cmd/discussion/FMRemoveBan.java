/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.discussion;

import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import org.slf4j.MarkerFactory;
import org.slf4j.Marker;

/**
 * Provides a command for the forum moderator
 * to remove a ban.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class FMRemoveBan
    extends RemoveBan {

  private static Marker c_LogMarker = MarkerFactory.getMarker(FMRemoveBan.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell) throws CommandException {
    prepare(shell);
    try {
      if (m_ActualForum == null) {
        log.error(c_LogMarker,"ActualForum==NULL");
        return;
      }
      if (!m_ForumModerationService.isAllowedToModerate(m_Agent, m_ActualForum.getIdentifier())) {
        m_IO.printTemplate(PREFIX + "notallowed");
        return;
      }
      m_Forum = m_ActualForum;
      doRemoveBan();
    } catch (Exception ex) {
      log.error(c_LogMarker,"run()", ex);
    }
  }//run

}//class FMRemoveBan
