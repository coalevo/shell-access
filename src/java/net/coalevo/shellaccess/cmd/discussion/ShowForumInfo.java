/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.discussion;

import net.coalevo.discussion.model.Content;
import net.coalevo.text.util.TagUtility;
import net.coalevo.shellaccess.impl.Formatter;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.wimpi.telnetd.io.TemplateAttributes;

import java.text.DateFormat;
import java.util.Date;

import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * Provides a command to show the forum info.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class ShowForumInfo
    extends DiscussionCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(ShowForumInfo.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell) throws CommandException {
    prepare(shell);
    try {
      if (m_ActualForum == null) {
        log.error(c_LogMarker,"ActualForum==NULL");
        return;
      }

      StringBuilder sbuf = new StringBuilder();

      //Descriptor
      TemplateAttributes attr = m_IO.leaseTemplateAttributes();
      attr.add("seqnum", "" + m_ActualForum.getSequenceNumber());
      attr.add("name", m_ActualForum.getName());
      attr.add("mnemonic", m_ActualForum.getMnemonic());
      attr.add("language", m_ActualForum.getLanguage().getDisplayLanguage(m_IO.getLocale()));
      attr.add("moderator", m_ActualForum.getModerator().getIdentifier());
      attr.add("moderatornick", resolveFormattedNickname(m_ActualForum.getModerator()));
      attr.add("maxactive", Integer.toString(m_ActualForum.getMaxActive()));

      if (m_ActualForum.isAnonymousAllowed()) {
        attr.add("anonymous", "");
      }
      if (m_ActualForum.isInviteOnly()) {
        attr.add("inviteonly", "");
      }
      if (m_ActualForum.isModeratedStrict()) {
        attr.add("strictmod", "");
      }
      if(m_ActualForum.isPublic()) {
        attr.add("public","");
      }
      
      DateFormat df = getStandardDateFormat();
      attr.add("created", df.format(new Date(m_ActualForum.getCreated())));
      attr.add("modified", df.format(new Date(m_ActualForum.getModified())));
      attr.add("tags", TagUtility.fromSet(m_ActualForum.getTags()));

      sbuf.append(m_IO.formatTemplate(PREFIX + "descriptor", attr));

      //Body/Content
      Content c = m_DiscussionService.getForumInfo(m_Agent, m_ActualForum.getIdentifier());
      if (c != null && !c.isEmpty()) {
        sbuf.append(Formatter.format(m_ActualForum.getInfoFormat(),m_ActualForum.getIdentifier().getIdentifier(),c.getContent()));
      }
      m_IO.page(sbuf.toString());
      m_IO.println();
    } catch (Exception ex) {
      log.error(c_LogMarker,"run()", ex);
    }
  }//run

  private static final String PREFIX = TS_PREFIX + "ShowForumInfo_";

}//class ShowForumInfo
