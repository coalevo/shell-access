/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.discussion;

import net.coalevo.discussion.model.*;
import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * Implements a user command for subscribing a forum.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class SubscribeForum
    extends DiscussionCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(SubscribeForum.class.getName());

  protected ForumDescriptor m_Forum;
  protected AgentIdentifier m_Who;

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell)
      throws CommandException {
    prepare(shell);
    try {
      try {
        m_PromptWrap = false;
        m_Forum = promptForum();
        if(m_Forum == null) {
          return;
        }
      } catch (NoSuchForumException nsfex) {
        return; //we are done
      }
      String agent = prompt(PREFIX + "agent");
      m_Who = new AgentIdentifier(agent);
      subscribe(true);
    } catch (Exception ex) {
      log.error(c_LogMarker,"run()", ex);
    }
  }//run

  public void subscribe(boolean note) throws Exception {
    try {
      //1. Begin transaction
      Subscription s = m_DiscussionService.beginSubscriptionCreate(
          m_Agent,
          m_Forum.getIdentifier(),
          m_Who
      );
      if(note) {
        s.setNote(prompt(PREFIX + "note"));
      }
      if(!isUserExpert()) {
        s.setNotifying(promptDecision(PREFIX + "notifying"));
      }

      //check if really; weird way, but we skip if this is used from a GotoForum command.
      if ((this instanceof GotoForum)
          || isUserExpert()
          || promptDecision(PREFIX + "doit")) {
        try {
          m_DiscussionService.commitSubscription(m_Agent, s);
        } catch (DiscussionServiceException ex) {
          m_IO.printTemplate(PREFIX + "failed");
          log.error("run()", ex);
        }
        m_IO.printTemplate(PREFIX + "confirm");
      } else {
        m_IO.printTemplate(PREFIX + "aborted");
      }

    } catch (BannedException bex) {
      m_IO.printTemplate(PREFIX + "banned");
    } catch (Exception ex) {
      log.error(c_LogMarker,"run()", ex);
    }
  }//subscribe

  public void reset() {
    m_Forum = null;
    m_Who = null;
    super.reset();
  }//reset

  protected static final String PREFIX = TS_PREFIX + "SubscribeForum_";

}//class SubscribeForum
