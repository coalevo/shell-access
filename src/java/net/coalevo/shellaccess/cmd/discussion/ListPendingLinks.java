/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.discussion;

import net.coalevo.discussion.model.DiscussionServiceException;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;

import java.util.List;

import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * Provides a command for listing the pending
 * links in a given forum.
 * <p/>
 * Navigation:
 * back  (change dir)
 * forward (change dir)
 * space (one into direction)
 * stop (break out of navigation loop)
 * delete (delete the actual link)
 * approve (approve the actual link)
 * disapprove (disapprove the actual link)
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class ListPendingLinks
    extends ListLinks {

  private static Marker c_LogMarker = MarkerFactory.getMarker(ListPendingLinks.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell)
      throws CommandException {
    prepare(shell);
    try {
      if (!m_ForumModerationService.isAllowedToModerate(m_Agent, m_ActualForum.getIdentifier())) {
        m_IO.printTemplate(PREFIX + "notallowed");
        return;
      }
      try {
        m_Pending = true;
        List<String> lids = m_ForumModerationService.listPendingLinks(m_Agent, m_ActualForum.getIdentifier());
        if (lids.isEmpty()) {
          m_IO.printTemplate(PREFIX + "nolinks");
          return;
        } else {
          navigateLinks(lids);
        }
      } catch (SecurityException ex) {
        m_IO.printTemplate(PREFIX + "notallowed");
        return;
      } catch (DiscussionServiceException e) {
        m_IO.printTemplate(PREFIX + "failed");
        return;
      }
    } catch (Exception ex) {
      log.error(c_LogMarker,"run()", ex);
    }
  }//run

}//class ListPendingLinks
