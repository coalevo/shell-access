/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.discussion;

import net.coalevo.discussion.model.Ban;
import net.coalevo.discussion.model.BannedException;
import net.coalevo.discussion.model.ForumDescriptor;
import net.coalevo.discussion.model.NoSuchForumException;
import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.shellaccess.validators.DateFormatValidator;
import net.coalevo.shellaccess.validators.StringValidator;
import net.wimpi.telnetd.io.TemplateAttributes;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * Provides a command implementation for adding a ban.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class AddBan
    extends DiscussionCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(AddBan.class.getName());

  protected ForumDescriptor m_Forum;

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell)
      throws CommandException {
    prepare(shell);
    try {
      try {
        m_Forum = promptForum();
        if(m_Forum == null) {
          return;
        }
      } catch (NoSuchForumException nsfex) {
        return; //we are done
      }
      doBan();
    } catch (Exception ex) {
      log.error(c_LogMarker,"run()", ex);
    }
  }//run

  protected void doBan()
      throws Exception {

    //1. get agent
    String agent = prompt(PREFIX + "agent", new StringValidator());

    //2. start ban
    Ban ban = null;
    try {
      ban = m_ForumModerationService.beginBanCreate(
          m_Agent, m_Forum.getIdentifier(), new AgentIdentifier(agent));
    } catch (SecurityException sex) {
      m_IO.printTemplate(PREFIX + "notallowed");
      return;
    } catch (BannedException bex) {
      m_IO.printTemplate(PREFIX + "banned");
      return;
    }
    try {
      ban.setByWhom(m_Agent.getAgentIdentifier());
      ban.setReason(prompt(PREFIX + "reason"));
      long time = -1;
      if (promptDecision(PREFIX + "askuntil")) {
        SimpleDateFormat sdf = (SimpleDateFormat) SimpleDateFormat.getDateInstance(
            SimpleDateFormat.SHORT, m_IO.getLocale());
        TemplateAttributes attr = m_IO.leaseTemplateAttributes();
        attr.add("date", sdf.format(new Date()));
        attr.add("dateformat", sdf.toPattern());
        m_IO.printTemplate(PREFIX + "bantime", attr);
        String date = prompt(PREFIX + "until",
            new DateFormatValidator(sdf, m_IO.formatTemplate("inputerror_datevalidator","format",sdf.toPattern()))
        );
        time = sdf.parse(date).getTime();
        if (time < System.currentTimeMillis() + ONE_DAY) {
          m_IO.printTemplate(PREFIX + "minimumbantime");
          return;
        }
      }
      ban.setUntil(time);

      if (!promptDecision(PREFIX + "doit")) {
        m_ForumModerationService.cancelBanCreate(m_Agent, ban);
        m_IO.printTemplate(PREFIX + "aborted");
        return;
      }
      m_ForumModerationService.commitBan(m_Agent, ban);
      m_IO.printTemplate(PREFIX + "confirm");

    } catch (Exception ex) {
      m_ForumModerationService.cancelBanCreate(m_Agent, ban);
      m_IO.printTemplate(PREFIX + "failed");
    }
  }//doBan


  public void reset() {
    m_Forum = null;
    super.reset();
  }//reset

  protected static final String PREFIX = TS_PREFIX + "AddBan_";
  protected static long ONE_DAY = 1000 * 60 * 60 * 24;

}//class AddBan
