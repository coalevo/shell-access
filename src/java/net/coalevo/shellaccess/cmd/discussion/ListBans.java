/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.discussion;

import net.coalevo.discussion.model.Ban;
import net.coalevo.discussion.model.DiscussionServiceException;
import net.coalevo.discussion.model.ForumDescriptor;
import net.coalevo.discussion.model.NoSuchForumException;
import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.wimpi.telnetd.io.TemplateAttributes;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * Provides a command for listing bans of a given forum.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class ListBans
    extends DiscussionCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(ListBans.class.getName());
  protected ForumDescriptor m_Forum;

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell)
      throws CommandException {
    prepare(shell);
    try {
      try {
        m_Forum = promptForum();
        if(m_Forum == null) {
          return;
        }
      } catch (NoSuchForumException nsfex) {
        return; //we are done
      }
      doListBans();
    } catch (Exception ex) {
      log.error(c_LogMarker,"run()", ex);
    }
  }//run

  public void doListBans()
      throws Exception {

    try {
      //1. get agent
      List<AgentIdentifier> banned =
          m_ForumModerationService.listBanned(m_Agent, m_Forum.getIdentifier());

      if (banned.isEmpty()) {
        m_IO.printTemplate(PREFIX + "nonebanned");
        return;
      }
      SimpleDateFormat sdf = (SimpleDateFormat) SimpleDateFormat.getDateInstance(
          SimpleDateFormat.SHORT, m_IO.getLocale());
      TemplateAttributes attr = m_IO.leaseTemplateAttributes();
      for (AgentIdentifier aid : banned) {
        Ban b = m_ForumModerationService.getBan(m_Agent, m_Forum.getIdentifier(), aid);
        String date = "";
        if (b.getUntil() < 0) {
          date = m_IO.formatTemplate(PREFIX + "forever");
        } else {
          date = sdf.format(new Date(b.getUntil()));
        }
        String[] items = new String[]{
            aid.getIdentifier(),
            b.getByWhom().getIdentifier(),
            b.getReason(),
            date
        };
        attr.add("items.{aid,bywhom,reason,until}", items);
      }
      m_IO.pageTemplate(PREFIX + "list", attr);
    } catch (DiscussionServiceException ex) {
      m_IO.printTemplate(PREFIX + "failed");
    } catch (SecurityException ex) {
      m_IO.printTemplate(PREFIX + "notallowed");
    }
  }//doListBans


  public void reset() {
    m_Forum = null;
    super.reset();
  }//reset

  protected static final String PREFIX = TS_PREFIX + "ListBans_";

}//class ListBans
