/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.discussion;

import net.coalevo.discussion.model.DiscussionServiceException;
import net.coalevo.discussion.model.EditableLink;
import net.coalevo.text.util.TagUtility;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.shellaccess.validators.StringValidator;

import java.net.URI;
import java.net.URISyntaxException;

import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * Provides a command for adding a link to
 * the actual forum.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class AddLink
    extends DiscussionCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(AddLink.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell)
      throws CommandException {
    prepare(shell);
    try {
      EditableLink el = null;
      try {
        //1.Start Link Add
        el = m_DiscussionService.beginLinkCreate(
            m_Agent, m_ActualForum.getIdentifier());
        StringValidator sv = new StringValidator(0, m_IO.formatTemplate("inputerror_stringvalidator"));
        m_PromptWrap = false;
        String cap = prompt(PREFIX + "caption", sv);
        if(cap == null || cap.length() == 0) {
           m_IO.printTemplate(PREFIX + "aborted");
           return;
        }
        el.setCaption(cap);

        String link = prompt(PREFIX + "link", sv, m_IO.getColumns() - 1, 2048);
        try {
          URI l = new URI(link);
          if (!l.isAbsolute()) {
            throw new URISyntaxException(link, "not absolute");
          }
          el.setLink(l.toString());
        } catch (URISyntaxException usex) {
          m_IO.printTemplate(PREFIX + "invaliduri");
          return;
        }
        if (promptDecision(PREFIX + "askdescription")) {

          String f = promptInputFormat(null);
          el.setDescriptionFormat(f);

          String txt = editTransformableContent(f, null);

          if (txt == null || txt.length() == 0) {
            m_IO.printTemplate(PREFIX + "aborted");
            return;
          }
          el.setDescription(txt);
        }
        el.setTags(TagUtility.toSet(prompt(PREFIX + "tags")));
        //commit
        if (isUserExpert() || promptDecision(PREFIX + "doit")) {
          m_DiscussionService.commitLink(m_Agent, el);
          m_IO.printTemplate(PREFIX + "confirm");
        } else {
          m_DiscussionService.cancelLinkCreate(m_Agent, el);
          m_IO.printTemplate(PREFIX + "aborted");
        }
      } catch (DiscussionServiceException dsex) {
        if (el != null) {
          m_DiscussionService.cancelLinkCreate(m_Agent, el);
        }
        m_IO.printTemplate(PREFIX + "failed");
        return;
      } catch (SecurityException ex) {
        m_IO.printTemplate(PREFIX + "notallowed");
        return;
      }
    } catch (Exception ex) {
      log.error(c_LogMarker,"run()", ex);
    }
  }//run

  private static final String PREFIX = TS_PREFIX + "AddLink_";

}//class AddLink
