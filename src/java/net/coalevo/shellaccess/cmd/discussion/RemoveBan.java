/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.discussion;

import net.coalevo.discussion.model.DiscussionServiceException;
import net.coalevo.discussion.model.ForumDescriptor;
import net.coalevo.discussion.model.NoSuchForumException;
import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.wimpi.telnetd.io.toolkit.MaskedSelection;

import java.util.List;

import org.slf4j.MarkerFactory;
import org.slf4j.Marker;

/**
 * Provides a command implementation for removing a ban.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class RemoveBan
    extends DiscussionCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(RemoveBan.class.getName());

  protected ForumDescriptor m_Forum;

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell)
      throws CommandException {
    prepare(shell);
    try {
      try {
        m_Forum = promptForum();
        if(m_Forum == null) {
          return;
        }
      } catch (NoSuchForumException nsfex) {
        return; //we are done
      }
      doRemoveBan();
    } catch (Exception ex) {
      log.error(c_LogMarker,"run()", ex);
    }
  }//run

  protected void doRemoveBan()
      throws Exception {

    //1. get agent
    List<AgentIdentifier> banned =
        m_ForumModerationService.listBanned(m_Agent, m_Forum.getIdentifier());

    if (banned.isEmpty()) {
      m_IO.printTemplate(PREFIX + "nonebanned");
      return;
    }
    m_IO.printTemplate(PREFIX + "agent");
    MaskedSelection msel = new MaskedSelection(m_IO.getTerminalIO(), "banned");
    for (AgentIdentifier aid : banned) {
      msel.addOption(aid, aid.getIdentifier());
    }
    msel.run();
    AgentIdentifier tbub = (AgentIdentifier) msel.getSelectedValue();

    //2. remove ban
    if (!promptDecision(PREFIX + "doit")) {
      m_IO.printTemplate(PREFIX + "aborted");
      return;
    }
    try {
      m_ForumModerationService.removeBan(
          m_Agent, m_Forum.getIdentifier(), tbub);
      m_IO.printTemplate(PREFIX + "confirm");
    } catch (DiscussionServiceException ex) {
      m_IO.printTemplate(PREFIX + "failed");
    } catch (SecurityException ex) {
      m_IO.printTemplate(PREFIX + "notallowed");
    }
  }//doRemoveBan

  public void reset() {
    m_Forum = null;
    super.reset();
  }//reset

  protected static final String PREFIX = TS_PREFIX + "RemoveBan_";

}//class RemoveBan
