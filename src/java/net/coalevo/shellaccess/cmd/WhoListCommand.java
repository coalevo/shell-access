/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.presence.model.PresenceStatusTypes;
import net.coalevo.shellaccess.cmd.config.ConfigurationCommand;
import net.coalevo.shellaccess.cmd.presence.PresenceCommand;
import net.coalevo.shellaccess.cmd.presence.PresenceInfo;
import net.coalevo.shellaccess.impl.Activator;
import net.coalevo.shellaccess.impl.SessionManager;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.shellaccess.model.ShellAccessSession;
import net.coalevo.text.util.ContentUtility;
import net.wimpi.telnetd.io.TemplateAttributes;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

/**
 * Provides a base class for the two who list commands.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public abstract class WhoListCommand
    extends BaseCommand {

  private static SimpleDateFormat c_TimeFormat = new SimpleDateFormat("HH:mm (Z z)");
  private static Marker c_LogMarker = MarkerFactory.getMarker(WhoListCommand.class.getName());

  public void run(BasicShell shell) throws CommandException {
    super.prepare(shell);
    boolean toggle = false;
    if (shell.isEnviromentVariableSet(ConfigurationCommand.KEY_COMMANDS_WHOLISTSHORT)) {
      toggle = new Boolean(
          (String) shell.getEnvironmentVariable(
              ConfigurationCommand.KEY_COMMANDS_WHOLISTSHORT
          )).booleanValue();
    }
    //if called long and there is no toggle, it is the long.
    if (getIdentifier().equals(DetailedWhoList.class.getName())) {
      if (toggle) {
        doShortList(shell);
      } else {
        doDetailedList(shell);
      }
    } else {
      if (toggle) {
        doDetailedList(shell);
      } else {
        doShortList(shell);
      }
    }
  }//run

  protected void doDetailedList(BasicShell shell)
      throws CommandException {

    boolean lw = false;
    try {
      lw = m_IO.getTerminalIO().isLineWrapping();
      m_IO.getTerminalIO().setLineWrapping(false);
      SessionManager smgr = Activator.getServices().getSessionManager();
      int numses = smgr.getSessionCount();
      //prepare template base
      TemplateAttributes attr = m_IO.leaseTemplateAttributes();
      attr.add("now", c_TimeFormat.format(new Date()));
      if (numses > 1) {
        attr.add("users", "" + numses);
        int cols = m_IO.getColumns();
        if (cols < 132) {
          //header
          attr.add("username", ContentUtility.adjustLength(m_IO.formatTemplate(PREFIX + "username"), 18));
          attr.add("ind", " ");
          attr.add("host", ContentUtility.adjustLength(m_IO.formatTemplate(PREFIX + "host"), 24));
          attr.add("time", ContentUtility.adjustLength(m_IO.formatTemplate(PREFIX + "time"), 5));
          attr.add("doing", ContentUtility.adjustLength(m_IO.formatTemplate(PREFIX + "doing"), 25));
          attr.add("sep", ContentUtility.padding(null, 79, '-'));
        } else {
          //header
          attr.add("username", ContentUtility.adjustLength(m_IO.formatTemplate(PREFIX + "username"), 23));
          attr.add("ind", " ");
          attr.add("host", ContentUtility.adjustLength(m_IO.formatTemplate(PREFIX + "host"), 39));
          attr.add("time", ContentUtility.adjustLength(m_IO.formatTemplate(PREFIX + "time"), 8));
          attr.add("doing", ContentUtility.adjustLength(m_IO.formatTemplate(PREFIX + "doing"), 54));
          attr.add("sep", ContentUtility.padding(null, 131, '-'));
        }
        ShellAccessSession mysession = m_Shell.getSession();
        PresenceInfo pi = (PresenceInfo) shell.getEnvironmentVariable(
            PresenceCommand.ENVKEY_PRESENCEINFO
        );
        for (Iterator iterator = smgr.sessions(); iterator.hasNext();) {
          ShellAccessSession session = (ShellAccessSession) iterator.next();
          AgentIdentifier other = session.getUserAgent().getAgentIdentifier();
          String ind = " ";
          //Indicators
          if (pi.isPresent(other)) {
            if (pi.isAvailable(other)) {
              if (pi.isTemporaryUnavailable(other)) {
                ind = "-";
              } else {
                ind = "+";
              }
            } else {
              ind = "*";
            }
          } else if (m_AgentIdentifier.equals(other)) {
            //self
            if (pi.getOwner().isAvailable()) {
              if (pi.getOwner().getStatus().isType(PresenceStatusTypes.TemporarilyUnavailable)
                  || pi.getOwner().getStatus().isType(PresenceStatusTypes.AdvertisedTemporarilyUnavailable)) {
                ind = "-";
              } else {
                ind = "+";
              }
            } else {
              ind = "*";
            }

          }
          //Interactive Message on the way overrides others
          if (mysession.isMessaging(other)) {
            ind = "X";
          }
          if (cols < 132) {
            String[] items = new String[]{
                ContentUtility.adjustLength(getUserWithStyle(session.getUsername(), other), 18),
                ind,
                ContentUtility.adjustLength(session.getHost(), 24),
                ContentUtility.adjustLength(getOnlineTime(session.getStartTime()), 5),
                ContentUtility.adjustLength(session.getDoing().replaceAll("@", "\001"), 25)
            };
            attr.add("items.{uname,indicator,host,online,doing}", items);
          } else {
            String[] items = new String[]{
                ContentUtility.adjustLength(getUserWithStyle(session.getUsername(), other), 23),
                ind,
                ContentUtility.adjustLength(session.getHost(), 39),
                ContentUtility.adjustLength(getOnlineTime(session.getStartTime()), 8),
                ContentUtility.adjustLength(session.getDoing().replaceAll("@", "\001"), 54)
            };
            attr.add("items.{uname,indicator,host,online,doing}", items);
          }
        }
      }
      m_IO.pageTemplate(PREFIX + "wltable", attr, true);
      m_IO.flush();
    } catch (IOException iex) {
      log.error(c_LogMarker, "run(BaseShell)", iex);
      throw new CommandException(iex.getMessage());
    } finally {
      try {
        m_IO.getTerminalIO().setLineWrapping(lw);
      } catch (IOException ex) {

      }
    }
  }//doDetailedList

  protected void doShortList(BasicShell shell) throws CommandException {
    boolean lw = false;
    try {
      lw = m_IO.getTerminalIO().isLineWrapping();
      m_IO.getTerminalIO().setLineWrapping(false);
      SessionManager smgr = Activator.getServices().getSessionManager();
      int numses = smgr.getSessionCount();
      //prepare template base
      TemplateAttributes attr = m_IO.leaseTemplateAttributes();
      attr.add("now", c_TimeFormat.format(new Date()));

      if (numses > 1) {
        attr.add("users", "" + numses);

        StringBuilder line = new StringBuilder();
        int counter = 0;
        PresenceInfo pi = (PresenceInfo) shell.getEnvironmentVariable(
            PresenceCommand.ENVKEY_PRESENCEINFO
        );
        for (Iterator iterator = smgr.sessions(); iterator.hasNext();) {
          ShellAccessSession session = (ShellAccessSession) iterator.next();
          AgentIdentifier other = session.getUserAgent().getAgentIdentifier();
          String ind = " ";
          //Indicators
          if (pi.isPresent(other)) {
            if (pi.isAvailable(other)) {
              if (pi.isTemporaryUnavailable(other)) {
                ind = "-";
              } else {
                ind = "+";
              }
            } else {
              ind = "*";
            }
          } else if (m_AgentIdentifier.equals(other)) {
            //self
            if (pi.getOwner().isAvailable()) {
              if (pi.getOwner().getStatus().isType(PresenceStatusTypes.TemporarilyUnavailable)
                  || pi.getOwner().getStatus().isType(PresenceStatusTypes.AdvertisedTemporarilyUnavailable)) {
                ind = "-";
              } else {
                ind = "+";
              }
            } else {
              ind = "*";
            }
          }
          //X indicator for messaging status overrides others
          if (shell.getSession().isMessaging(other)) {
            ind = "X";
          }
          line.append(ind).append(
              ContentUtility.adjustLength(
                  getUserWithStyle(session.getUsername(), other), 24));
          counter++;
          if (m_IO.getColumns() < 132) {
            if (counter == 3) {
              attr.add("lines", line.toString());
              line = new StringBuilder();
              counter = 0;
            }
          } else {
            if (counter == 5) {
              attr.add("lines", line.toString());
              line = new StringBuilder();
              counter = 0;
            }
          }
        }
        if (line.length() > 0) {
          attr.add("lines", line.toString());
        }
      }
      m_IO.pageTemplate(PREFIX + "wlshort", attr);
      m_IO.flush();
    } catch (IOException iex) {
      log.error(c_LogMarker, "run(BaseShell)", iex);
      throw new CommandException(iex.getMessage());
    } finally {
      try {
        m_IO.getTerminalIO().setLineWrapping(lw);
      } catch (IOException ex) {

      }
    }
  }//run

  public String getUserWithStyle(String name, AgentIdentifier aid) {
    try {
      //Handle self
      if (m_Agent.getAgentIdentifier().equals(aid)) {
        return m_IO.formatStyle("Myself", resolveUserNickname());
      } else {
        return resolveFormattedNickname(aid);
      }
    } catch (IOException ex) {
      return name;
    }
  }//styleUser

  private String getOnlineTime(long start) {
    final StringBuilder sbuf = new StringBuilder();
    long elapsedSeconds = (System.currentTimeMillis() - start) / 1000;
    long days = elapsedSeconds / 86400;
    elapsedSeconds -= (days * 86400);
    long hours = elapsedSeconds / 3600;
    elapsedSeconds -= (hours * 3600);
    long minutes = elapsedSeconds / 60;
    //elapsedSeconds -= (minutes * 60);
    if (days > 99) {
      sbuf.append(days).append('d');
    } else if (days > 0) {
      sbuf.append(days).append('d');
      if (hours < 10) {
        sbuf.append('0');
      }
      sbuf.append(hours);
    } else {
      if (hours < 10) {
        sbuf.append('0');
      }
      sbuf.append(hours).append(':');
      if (minutes < 10) {
        sbuf.append('0');
      }
      sbuf.append(minutes);
    }
    return sbuf.toString();
  }//getOnlineTime

  private static final String PREFIX = "commands_net.coalevo.shellaccess.cmd.WhoListCommand_";

}//class WhoListCommand
