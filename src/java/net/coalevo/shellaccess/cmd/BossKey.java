/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd;

import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.shellaccess.cmd.presence.PresenceCommand;
import net.coalevo.foundation.model.Session;
import net.coalevo.presence.model.*;
import org.slf4j.MarkerFactory;
import org.slf4j.Marker;

/**
 * Provides a command that will pretend you are
 * using a pretty standard shell.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class BossKey
    extends BaseCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(BossKey.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell)
      throws CommandException {
    prepare(shell);
    try {
      //1. Retrieve Presence
      Presence presence = null;
      PresenceStatusType pst = null;
      Session s = shell.getSession().getUserAgent().getSession();
      if (s != null && s.isValid()) {
        presence = (Presence) s.getAttribute(PresenceConstants.PRESENCE_SESSION_ATTRIBUTE);
      } else if (shell.isEnviromentVariableSet(PresenceCommand.ENVKEY_PRESENCE)) {
        presence = (Presence) shell.getEnvironmentVariable(PresenceCommand.ENVKEY_PRESENCE);
      }
      
      //2. Become unavailable
      boolean pstupd = false;
      if(presence != null && presence.isAvailable()) {
        pstupd = true;
        try {
          PresenceStatus ps = presence.getStatus();
          //store old
          pst = ps.getType();
          ps.setType(PresenceStatusTypes.Unavailable);
          presence.updateStatus(ps);
        } catch (Exception ex) {
          log.error(c_LogMarker,"run()",ex);
        }
      }
      //3. Blank and write shell prompt
      m_IO.blankTerminal();
      m_IO.print(m_Shell.getSession().getBossKeyPrompt().replaceAll("@","\001"));
      
      //4.Wait for any key
      m_IO.read();

      //5. Reset auto-away flag in session (CSAB-124).
      m_Shell.getSession().setAway(false);

      //6. Become available
      if(presence != null && pstupd) {
        try {
          PresenceStatus ps = presence.getStatus();
          ps.setType((pst !=null)?pst:PresenceStatusTypes.Available);
          presence.updateStatus(ps);
        } catch (Exception ex) {
          log.error(c_LogMarker,"run()",ex);
        }
      }
      m_IO.println();
    } catch (Exception ex) {
      log.error(c_LogMarker,"run()",ex);
    }
  }//run

  private static final String PREFIX = "commands_net.coalevo.shellaccess.cmd.BossKey_";

}//class BossKey
