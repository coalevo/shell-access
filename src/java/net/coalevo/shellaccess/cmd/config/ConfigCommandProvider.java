/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.config;

import net.coalevo.shellaccess.model.BaseShellCommandProvider;

/**
 * A {@link net.coalevo.shellaccess.model.ShellCommandProvider}
 * for the configuration commands.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class ConfigCommandProvider
    extends BaseShellCommandProvider {

  public ConfigCommandProvider() {
    super(ConfigCommandProvider.class.getPackage().getName());
    prepare();
  }//constructor

  private void prepare() {
    addCommand("ChangePassword", ChangePassword.class);
    addCommand("ApplyPreferences", ApplyPreferences.class);
    addCommand("SetDoing", SetDoing.class);
    addCommand("SetHost", SetHost.class);
    addCommand("ToggleBoldcolors",ToggleBoldcolors.class);
    addCommand("ToggleColors",ToggleColors.class);
    addCommand("ToggleExpert",ToggleExpert.class);
    addCommand("ToggleConfirmGroupMessages",ToggleConfirmGroupMessages.class);
    addCommand("ToggleAcousticNotification",ToggleAcousticNotification.class);
    addCommand("ToggleWholistDefault",ToggleWholistDefault.class);
    addCommand("ToggleHost2Country",ToggleHost2Country.class);
    addCommand("ToggleEventsWhileReading",ToggleEventsWhileReading.class);
    addCommand("SetBossKeyPrompt", SetBossKeyPrompt.class);
    addCommand("SetAutoAwayTime", SetAutoAwayTime.class);
    addCommand("SetLanguage", SetLanguage.class);
    addCommand("SetLoginPresenceStatusType", SetLoginPresenceStatusType.class);
    addCommand("EditShortcuts", EditShortcuts.class);
    addCommand("ToggleSysguideActive",ToggleSysguideActive.class);
    addCommand("ToggleGroupPresenceNotification",ToggleGroupPresenceNotification.class);
    addCommand("ToggleMessagingHistory",ToggleMessagingHistory.class);
    addCommand("ToggleWhiner",ToggleWhiner.class);
    addCommand("ToggleASCII",ToggleASCII.class);
    addCommand("TogglePresenceNotification",TogglePresenceNotification.class);
    addCommand("ToggleConfirmMessages",ToggleConfirmMessages.class);
    addCommand("SetAutoJoinGroups",SetAutoJoinGroups.class);
    addCommand("ToggleRebindDelete", ToggleRebindDelete.class);

    //addCommand("", .class);
  }//prepare

}//class ConfigCommandProvider
