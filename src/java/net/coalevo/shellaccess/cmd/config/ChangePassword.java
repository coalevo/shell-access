/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.config;

import net.coalevo.foundation.model.UserAgent;
import net.coalevo.security.model.AuthenticationException;
import net.coalevo.security.service.SecurityManagementService;
import net.coalevo.security.service.SecurityService;
import net.coalevo.shellaccess.impl.Activator;
import net.coalevo.shellaccess.model.*;
import net.coalevo.shellaccess.filters.PasswordFilter;
import net.wimpi.telnetd.io.toolkit.Editfield;
import net.wimpi.telnetd.io.toolkit.InputValidator;

import java.io.IOException;

import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * Implementation of {@link Command} that allows to change
 * the actual users password.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class ChangePassword
    implements Command {

  private static Marker c_LogMarker = MarkerFactory.getMarker(ChangePassword.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell) throws CommandException {
    final ShellIO io = shell.getShellIO();
    try {
      SecurityManagementService sms = Activator.getServices().getSecurityManagementService(Services.NO_WAIT);
      SecurityService ss = Activator.getServices().getSecurityService(Services.NO_WAIT);
      UserAgent agent = shell.getSession().getUserAgent();
      if (ss == null || sms == null) {
        io.printTemplate(PREFIX + "securityunavailable");
        return;
      }
      io.printTemplate(PREFIX + "oldpasslabel");
      Editfield ef = new Editfield(io.getTerminalIO(), "oldpass", 30, 255);
      ef.setHiddenInput(true);
      ef.setIgnoreDelete(true);
      ef.run();
      String oldpass = ef.getValue();
      try {
        ss.authenticate(agent.getAgentIdentifier().getName(), oldpass);
      } catch (AuthenticationException ex) {
        io.printTemplate(PREFIX + "incorrectoldpass");
        return;
      }
      io.printTemplate(PREFIX + "newpasslabel");
      ef = new Editfield(io.getTerminalIO(), "newpass", 30, 255);
      ef.setIgnoreDelete(true);
      ef.setInputFilter(new PasswordFilter(ef));
      final String pwderr = io.formatTemplate("inputerror_passwordvalidator");
      ef.setInputValidator(new InputValidator() {

        public boolean validate(String string) {
          if (string == null || string.length() < 8 || string.length() > 25) {
            return false;
          }
          //todo: probably add other password checks?
          return true;
        }
        public String getErrorMessage() {
          return pwderr;
        }
      });
      ef.setHiddenInput(true); //if tabbed, will be set to false!
      ef.run();
      String newpass = ef.getValue();
      io.printTemplate(PREFIX + "newpassagainlabel");
      ef = new Editfield(io.getTerminalIO(), "newpassagain", 30, 255);
      ef.setHiddenInput(true);
      ef.setIgnoreDelete(true);
      ef.run();
      if (!newpass.equals(ef.getValue())) {
        io.printTemplate(PREFIX + "newpassmismatch");
        return;
      }
      boolean set = false;
      try {
        set = sms.updateAuthentication(agent, agent, newpass);
      } catch (Exception ex) {
        set = false;
      }
      if (set) {
        io.printTemplate(PREFIX + "setnewpass");
      } else {
        io.printTemplate(PREFIX + "notsetnewpass");
      }
    } catch (IOException iex) {
      throw new CommandException(iex.getMessage());
    }
  }//run

  public void reset() {
  }//reset

  private static final String PREFIX = "commands_net.coalevo.shellaccess.cmd.config.ChangePassword_";

}//class ChangePassword
