/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.config;

import net.coalevo.presence.model.PresenceStatusType;
import net.coalevo.presence.model.PresenceStatusTypes;
import net.coalevo.shellaccess.cmd.BaseCommand;
import net.coalevo.shellaccess.impl.Activator;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.ShellAccessSession;
import net.coalevo.shellaccess.model.Shortcuts;
import org.osgi.service.prefs.Preferences;
import org.osgi.service.prefs.PreferencesService;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.UnsupportedEncodingException;
import java.util.Locale;
import java.util.Properties;

/**
 * Implements an abstract base class for configuration shell commands.
 * <p/>
 * Provides basic elements like simpler access to the preferences of a user.
 * Commands that need this access should be designed as subclasses of this
 * class and call prepare(BasicShell) as first statement in the run() method.
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public abstract class ConfigurationCommand
    extends BaseCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(ConfigurationCommand.class.getName());

  protected PreferencesService m_PrefsService;
  protected ShellAccessSession m_Session;
  protected Preferences m_Preferences;
  protected Properties m_Defaults;

  public boolean prepare(BasicShell shell) {
    if (!super.prepare(shell)) {
      return false;
    }

    try {
      m_PrefsService = shell.getServices().getPreferencesService();
    } catch (Exception ex) {
      log.error(c_LogMarker, "prepare()", ex);
    }
    if (m_PrefsService == null) {
      try {
        m_IO.printTemplate(PREFIX + "prefserviceunavailable");
      } catch (Exception ex) {
        log.error(c_LogMarker, "prepare()", ex);
      }
      return false;
    }
    m_Defaults = Activator.getDefaultPreferences();

    //prepare session (some prefs will be applied there)
    m_Session = shell.getSession();
    //prepare user identifier, prefs will be stored as user
    String uid = m_Session.getUserAgent().getAgentIdentifier().getIdentifier();
    //prepare preferences
    m_Preferences = m_PrefsService.getUserPreferences(uid);
    if (m_Preferences == null) {
      try {
        m_IO.printTemplate(PREFIX + "prefsunavailable");
      } catch (Exception ex) {
        log.error(c_LogMarker, "prepare()", ex);
      }
      return false;
    }
    return true;
  }//prepare

  public void reset() {
    super.reset();
    m_Shell = null;
    m_IO = null;
    m_Session = null;
    m_Preferences = null;
    m_PrefsService = null;
    m_Defaults = null;
  }//reset

  protected boolean save() {
    try {
      m_Preferences.flush();
      //m_Preferences.sync();
      return true;
    } catch (Exception ex) {
      log.error(c_LogMarker, "save()", ex);
      return false;
    }
  }//save

  protected void apply(boolean b) throws Exception {

    //Expert mode
    applyExpert();
    //Host 2 Country
    applyHost2Country();
    //Doing
    applyDoing();
    //Language
    applyLanguage();
    //Colors
    applyColors();
    //Boldcolors
    applyBoldcolors();
    //Acoustic notification
    applyAcoustic();
    //Key Commands Wholistshort
    applyWholistDefault();
    //Events While Reading
    applyEventsWhileReading();
    //Boss Key Prompt
    applyBossKeyPrompt();
    //Apply Auto Away Time
    applyAutoAwayTime();
    //Apply presence status moving it into a session setting?
    applyLoginPresenceStatus();
    //apply group confirmations
    applyGroupConfirmations();
    //apply shortcuts
    applyShortcuts();
    //apply sysguide status
    applySysguideActive();
    //apply group notification
    applyGroupPresenceNotification();
    //apply messaging history type
    applyMessagingHistoryType();
    //apply whiner mode
    applyAmIAWhiner();
    //apply ASCII only mode
    applyASCIIOnly();
    //apply presence notification
    applyPresenceNotification();
    //apply message confirmations
    applyConfirmations();
    //apply autojoin groups
    applyAutoJoinGroups();
    //apply rebind delete
    applyRebindDelete();
    
    if (b) {
      m_IO.printTemplate(PREFIX + "prefsunavailable");
    }
  }//apply

  protected void applyDoing() {
    m_Session.setDoing(
        m_Preferences.get(
            KEY_SESSION_DOING,
            m_Defaults.getProperty(KEY_SESSION_DOING)
        )
    );
  }//applyDoing

  protected void applyBoldcolors() {
    m_IO.setBold(
        m_Preferences.getBoolean(
            KEY_DISPLAY_BOLDCOLORS,
            new Boolean(m_Defaults.getProperty(KEY_DISPLAY_BOLDCOLORS)).booleanValue()
        )
    );
  }//applyBoldcolors

  protected void applyColors() {
    m_IO.getTerminalIO().setColorsEnabled(
        m_Preferences.getBoolean(
            KEY_DISPLAY_COLORS,
            new Boolean(m_Defaults.getProperty(KEY_DISPLAY_COLORS)).booleanValue()
        )
    );
  }//applyColors

  protected void applyExpert() {
    m_Session.setExpert(
        m_Preferences.getBoolean(
            KEY_SESSION_EXPERT,
            new Boolean(m_Defaults.getProperty(KEY_SESSION_EXPERT)).booleanValue()
        )
    );
  }//applyExpert

  protected void applyHost2Country() {
    m_Session.setHostToCountry(
        m_Preferences.getBoolean(
            KEY_SESSION_HOST2COUNTRY,
            new Boolean(m_Defaults.getProperty(KEY_SESSION_HOST2COUNTRY)).booleanValue()
        )
    );
  }//applyHost2Country

  protected void applyAcoustic() {
    m_IO.getTerminalIO().setSignalling(
        m_Preferences.getBoolean(
            KEY_NOTIFICATIONS_ACOUSTIC,
            new Boolean(m_Defaults.getProperty(KEY_NOTIFICATIONS_ACOUSTIC)).booleanValue()
        )
    );
  }//applyAcoustic

  protected void applyWholistDefault() {
    m_Shell.setEnvironmentVariable(
        KEY_COMMANDS_WHOLISTSHORT,
        m_Preferences.get(
            KEY_COMMANDS_WHOLISTSHORT,
            m_Defaults.getProperty(KEY_COMMANDS_WHOLISTSHORT)
        )
    );
  }//applyWholistDefault

  protected void applyLanguage() {
    String lang = m_Preferences.get(
        KEY_SESSION_LANG,
        m_Defaults.getProperty(KEY_SESSION_LANG)
    );
    Locale l = new Locale(lang);
    m_IO.setLocale(l);
  }//applyLanguage

  protected void applyEventsWhileReading() {
    m_Session.setEventsWhileReading(
        m_Preferences.getBoolean(
            KEY_SESSION_EVENTSWHILEREADING,
            new Boolean(m_Defaults.getProperty(KEY_SESSION_EXPERT)).booleanValue()
        )
    );
  }//applyExpert

  protected void applyBossKeyPrompt() {
    m_Session.setBossKeyPrompt(
        m_Preferences.get(
            KEY_SESSION_BOSSKEYPROMPT,
            m_Defaults.getProperty(KEY_SESSION_BOSSKEYPROMPT)
        )
    );
  }//applyBossKeyPrompt

  protected void applyAutoAwayTime() {
    m_Session.setAutoAwayTime(
        Long.parseLong(m_Preferences.get(
            KEY_SESSION_AUTOAWAYTIME,
            m_Defaults.getProperty(KEY_SESSION_AUTOAWAYTIME)
        ))
    );
  }//applyAutoAwayTime

  protected void applyLoginPresenceStatus() {
    String actstatus = m_Preferences.get(
        KEY_SESSION_PRESENCESTATUSTYPE,
        m_Defaults.getProperty(KEY_SESSION_PRESENCESTATUSTYPE)
    );
    PresenceStatusType pt =
        PresenceStatusTypes.getPresenceStatusType(actstatus);
    if (pt == null) {
      pt = PresenceStatusTypes.Available;
    }
    m_Session.setLoginPresenceStatusType(pt);
  }//applyLoginPresenceStatus

  protected void applyGroupConfirmations() {
    m_Session.setGroupConfirmations(
        m_Preferences.getBoolean(
            KEY_CONFIRMATIONS_GROUPMESSAGES,
            new Boolean(m_Defaults.getProperty(KEY_CONFIRMATIONS_GROUPMESSAGES)).booleanValue()
        )
    );
  }//applyGroupConfirmations

  protected void applyConfirmations() {
    m_Session.setConfirmations(
        m_Preferences.getBoolean(
            KEY_CONFIRMATIONS_MESSAGES,
            new Boolean(m_Defaults.getProperty(KEY_CONFIRMATIONS_MESSAGES)).booleanValue()
        )
    );
  }//applyConfirmations

  protected void applySysguideActive() {
    m_Session.setSysguideActive(
        m_Preferences.getBoolean(
            KEY_SESSION_SYSGUIDE_ACTIVE,
            new Boolean(m_Defaults.getProperty(KEY_SESSION_SYSGUIDE_ACTIVE)).booleanValue()
        )
    );
  }//applySysguideActive

  protected void applyShortcuts() {
    String scl = m_Preferences.get(KEY_SHORTCUTS, "");
    m_Shell.setEnvironmentVariable(KEY_SHORTCUTS, new Shortcuts(scl));
  }//applyShortcuts

  protected void applyGroupPresenceNotification() {
    m_Session.setGroupPresenceNotification(
        m_Preferences.getBoolean(
            KEY_SESSION_GROUPPRESENCENOTIFICATION,
            new Boolean(m_Defaults.getProperty(KEY_SESSION_GROUPPRESENCENOTIFICATION)).booleanValue()
        )
    );
  }//applyGroupPresenceNotification

  protected void applyPresenceNotification() {
    m_Session.setPresenceNotification(
        m_Preferences.getBoolean(
            KEY_SESSION_PRESENCENOTIFICATION,
            new Boolean(m_Defaults.getProperty(KEY_SESSION_PRESENCENOTIFICATION)).booleanValue()
        )
    );
  }//applyPresenceNotification

  protected void applyMessagingHistoryType() {
    m_Shell.setEnvironmentVariable(
        KEY_MESSAGINGHISTORY_TYPE,
        m_Preferences.get(
            KEY_MESSAGINGHISTORY_TYPE,
            m_Defaults.getProperty(KEY_MESSAGINGHISTORY_TYPE)
        )
    );
  }//applyMessagingHistoryType

  protected void applyAmIAWhiner() {
    m_Session.setWhiner(
        m_Preferences.getBoolean(
            KEY_AMIAWHINER,
            new Boolean(m_Defaults.getProperty(KEY_AMIAWHINER)).booleanValue()
        )
    );
  }//applyAmIAWhiner

  protected void applyRebindDelete() {
    m_Session.setRebindDelete(
        m_Preferences.getBoolean(
            KEY_SESSION_REBINDDELETE,
            new Boolean(m_Defaults.getProperty(KEY_SESSION_REBINDDELETE)).booleanValue()
        )
    );
  }//applyRebindDelete

  protected void applyASCIIOnly() {
    if (m_Preferences.getBoolean(
        KEY_ASCIIONLY,
        new Boolean(m_Defaults.getProperty(KEY_ASCIIONLY)).booleanValue()
    )) {
      try {
        m_IO.getTerminalIO().getBaseIO().setEncoding("US-ASCII");
      } catch (Exception ex) {
        log.error("applyASCIIOnly()", ex);
      }
    } else {
      try {
        m_IO.getTerminalIO().getBaseIO().setEncoding("UTF-8");
      } catch (UnsupportedEncodingException e) {
        log.error("applyASCIIOnly()", e);
      }
    }
  }//applyASCIIOnly

  protected void applyAutoJoinGroups() {
    m_Session.setAutoJoinGroups(m_Preferences.get(KEY_SESSION_AUTOJOINGROUPS, ""));
  }//applyAutoJoinGroups

  public static final String TPREFIX = "commands_net.coalevo.shellaccess.cmd.config.";
  private static final String PREFIX = "ConfigurationCommmand_";

  public static final String KEY_SESSION_EXPERT = "options.session.expert=false";
  public static final String KEY_SESSION_DOING = "options.session.doing";
  public static final String KEY_SESSION_LANG = "options.session.language";
  public static final String KEY_DISPLAY_COLORS = "options.display.boldcolors";
  public static final String KEY_DISPLAY_BOLDCOLORS = "options.display.colors";
  public static final String KEY_NOTIFICATIONS_ACOUSTIC = "options.notification.acoustic";
  public static final String KEY_COMMANDS_WHOLISTSHORT = "options.commands.wholist.short";
  public static final String KEY_SESSION_HOST2COUNTRY = "options.session.host2country";
  public static final String KEY_SESSION_EVENTSWHILEREADING = "options.session.eventswhilereading";
  public static final String KEY_SESSION_BOSSKEYPROMPT = "options.session.bosskeyprompt";
  public static final String KEY_SESSION_AUTOAWAYTIME = "options.session.autoawaytime";
  public static final String KEY_SESSION_SYSGUIDE_ACTIVE = "options.session.sysguide.active";
  public static final String KEY_SESSION_PRESENCESTATUSTYPE = "options.session.presencestatustype";
  public static final String KEY_CONFIRMATIONS_GROUPMESSAGES = "options.confirmation.groupmsg";
  public static final String KEY_CONFIRMATIONS_MESSAGES = "options.confirmation.msg";
  public static final String KEY_SHORTCUTS = "options.shortcuts";
  public static final String KEY_SESSION_GROUPPRESENCENOTIFICATION = "options.session.grouppresencenotification";
  public static final String KEY_MESSAGINGHISTORY_TYPE = "options.session.messaginghistorytype";
  public static final String KEY_AMIAWHINER = "options.commands.amiawhiner";
  public static final String KEY_ASCIIONLY = "options.display.asciionly";
  public static final String KEY_SESSION_PRESENCENOTIFICATION = "options.session.presencenotification";
  public static final String KEY_SESSION_AUTOJOINGROUPS = "options.session.autojoingroups";
  public static final String KEY_SESSION_REBINDDELETE = "options.session.rebinddelete";

  public static final String MESSAGINGHISTORY_TYPE_DISK = "disk";
  public static final String MESSAGINGHISTORY_TYPE_MEMORY = "mem";

}//class ConfigurationCommand
