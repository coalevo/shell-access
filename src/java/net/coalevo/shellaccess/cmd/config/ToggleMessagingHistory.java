/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.config;

import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.shellaccess.cmd.messaging.MessagingCommand;
import net.coalevo.shellaccess.cmd.messaging.MessagingHistory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * Command that allows to toggle the expert preference
 * setting.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class ToggleMessagingHistory extends ConfigurationCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(ToggleMessagingHistory.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell)
      throws CommandException {
    if (!prepare(shell)) {
      return;
    }
    try {
      boolean mem = true;
      Object o = shell.getEnvironmentVariable(ConfigurationCommand.KEY_MESSAGINGHISTORY_TYPE);
      if (o != null && ConfigurationCommand.MESSAGINGHISTORY_TYPE_DISK.equalsIgnoreCase(((String) o))) {
        mem = false;
      }

      m_IO.printTemplate(PREFIX + "ask", (mem) ? "mem" : "disk", "");
      if (m_IO.getDecision()) {
        String type = ConfigurationCommand.MESSAGINGHISTORY_TYPE_MEMORY;
        if(mem) {
          type = ConfigurationCommand.MESSAGINGHISTORY_TYPE_DISK;
        }
        //toggle pref
        m_Preferences.put(
            KEY_MESSAGINGHISTORY_TYPE,
            type
        );
        applyMessagingHistoryType();
        save();
        //update actual index
        MessagingHistory hist = (MessagingHistory) shell.getEnvironmentVariable(MessagingCommand.ENVKEY_HISTORY);
        hist.setIndex(!mem);

        m_IO.printTemplate(PREFIX + "confirm", (!mem) ? "mem" : "disk", "");
      } else {
        //not toggle
        m_IO.printTemplate(PREFIX + "unchanged");
      }
    } catch (Exception ex) {
      log.error(c_LogMarker, "ToggleMessagingHistory::run()", ex);
    }

  }//run

  private static final String PREFIX = TPREFIX + "ToggleMessagingHistory_";

}//class ToggleMessagingHistory