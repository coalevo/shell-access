/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.config;

import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * Command that allows to toggle if you are a whiner or not.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class ToggleWhiner extends ConfigurationCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(ToggleWhiner.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell)
      throws CommandException {
    if(!prepare(shell)) {
      return;
    }
    try {
      boolean gc = m_Session.isWhiner();
      m_IO.printTemplate(PREFIX + "ask",(gc)?"on":"off","");
      if(m_IO.getDecision()) {
        //toggle
        m_Preferences.putBoolean(KEY_AMIAWHINER,!gc);
        applyAmIAWhiner();
        save();
        m_IO.printTemplate(PREFIX + "confirm",(!gc)?"on":"off","");
      } else {
        //not toggle
        m_IO.printTemplate(PREFIX + "unchanged");
      }
    } catch (Exception ex) {
      log.error(c_LogMarker,"ToggleWhiner::run()",ex);
    }

  }//run

  private static final String PREFIX = TPREFIX + "ToggleWhiner_";

}//class ToggleWhiner