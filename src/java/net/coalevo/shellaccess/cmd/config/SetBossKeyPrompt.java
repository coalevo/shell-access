/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.config;

import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.shellaccess.filters.UTF8InputFilter;
import net.wimpi.telnetd.io.toolkit.Editfield;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * Command that allows to set the session's boss key prompt.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class SetBossKeyPrompt
    extends ConfigurationCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(SetBossKeyPrompt.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell) throws CommandException {
    if(!prepare(shell)) {
      return;
    }
    try {
      m_IO.printTemplate(PREFIX + "promptlabel");
      Editfield ef = new Editfield(m_IO.getTerminalIO(),"prompt",50,200);
      ef.setValue(m_Session.getBossKeyPrompt());
      ef.setIgnoreDelete(true);
      ef.setInputFilter(new UTF8InputFilter(m_IO.formatTemplate("inputerror_utf8filter")));
      ef.run();
      String prompt = ef.getValue();
      if(prompt.contains("@") && !prompt.endsWith("@a")) {
        prompt = prompt + "@a";
      }
      m_Preferences.put(KEY_SESSION_BOSSKEYPROMPT, prompt);
      applyBossKeyPrompt();
      save();
      m_IO.printTemplate(PREFIX + "confirm","prompt", prompt.replaceAll("@","\001"));
    } catch (Exception ex) {
      log.error(c_LogMarker,"run()",ex);
      throw new CommandException(ex.getMessage());
    }
  }//run

  private static final String PREFIX = TPREFIX + "SetBossKeyPrompt_";


}//class SetBossKeyPrompt
