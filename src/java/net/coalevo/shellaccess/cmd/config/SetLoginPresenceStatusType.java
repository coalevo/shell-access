/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.config;

import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.shellaccess.cmd.presence.PresenceCommand;
import net.coalevo.presence.model.PresenceStatusType;
import net.coalevo.presence.model.PresenceStatusTypes;
import net.wimpi.telnetd.io.toolkit.MaskedSelection;

import java.io.IOException;

import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * This class implements a command that allows to switch
 * the presence status type at login.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class SetLoginPresenceStatusType
    extends ConfigurationCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(SetLoginPresenceStatusType.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell)
      throws CommandException {
    if(!prepare(shell)) {
      return;
    }
    try {
      //1. Actual status
       String actstatus  = m_Preferences.get(
           KEY_SESSION_PRESENCESTATUSTYPE,
        m_Defaults.getProperty(KEY_SESSION_PRESENCESTATUSTYPE)
      );
      PresenceStatusType pt =
          PresenceStatusTypes.getPresenceStatusType(actstatus);

      if(pt == null) {
        pt = PresenceStatusTypes.Available;
      }
      
      //2. Get type and description
      m_IO.printTemplate(PREFIX + "typelabel");
      MaskedSelection sel = getTypeSelection();
      sel.initialSelect(pt);
      sel.run();
      PresenceStatusType selected = (PresenceStatusType) sel.getSelectedValue();

      if(selected.equals(pt)) {
        m_IO.printTemplate(PREFIX + "nochange");
        return;
      } else {
        m_Preferences.put(KEY_SESSION_PRESENCESTATUSTYPE,selected.getIdentifier());
        save();
        applyLoginPresenceStatus();
        m_IO.printTemplate(PREFIX + "confirm","type",m_IO.formatTemplate(
            PresenceCommand.STATUS_PREFIX + selected.getIdentifier()
        ));
      }
    } catch (Exception ex) {
      log.error(c_LogMarker,"run()",ex);
      throw new CommandException(ex.getMessage());
    }
  }//run

  private MaskedSelection getTypeSelection() throws IOException {
     MaskedSelection sel = new MaskedSelection(m_IO.getTerminalIO(), "type");
     sel.addOption(
         PresenceStatusTypes.Available,
         m_IO.formatTemplate(
             PresenceCommand.STATUS_PREFIX + PresenceStatusTypes.Available.getIdentifier()
         )
     );
     sel.addOption(
         PresenceStatusTypes.AdvertisedAvailable,
         m_IO.formatTemplate(
             PresenceCommand.STATUS_PREFIX + PresenceStatusTypes.AdvertisedAvailable.getIdentifier()
         )
     );
     sel.addOption(
         PresenceStatusTypes.TemporarilyUnavailable,
         m_IO.formatTemplate(
             PresenceCommand.STATUS_PREFIX + PresenceStatusTypes.TemporarilyUnavailable.getIdentifier()
         )
     );
     sel.addOption(
         PresenceStatusTypes.Unavailable,
         m_IO.formatTemplate(
             PresenceCommand.STATUS_PREFIX + PresenceStatusTypes.Unavailable.getIdentifier()
         )
     );
     sel.addOption (
         NO_CHANGE,
         m_IO.formatTemplate(PREFIX + "noneoption")
     );
     return sel;
   }//getTypeSelection

   private static final PresenceStatusType NO_CHANGE = new PresenceStatusType(){
     public String getIdentifier() {
       return "none";
     }//getIdentifier
   };


  private static final String PREFIX = TPREFIX + "SetLoginPresenceStatusType_";

}//class SetLoginPresenceStatusType
