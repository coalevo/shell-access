/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.config;

import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.shellaccess.filters.UTF8InputFilter;
import net.coalevo.text.util.ContentUtility;
import net.wimpi.telnetd.io.toolkit.Editfield;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * Command that allows to set the doing.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class SetDoing
    extends ConfigurationCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(SetDoing.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell) throws CommandException {
    if(!prepare(shell)) {
      return;
    }
    try {
      String olddoing = shell.getSession().getDoing();
      m_IO.printTemplate(PREFIX + "doinglabel");
      Editfield ef = new Editfield(m_IO.getTerminalIO(),"doing",50,200);
      ef.setValue(m_Session.getDoing());
      ef.setIgnoreDelete(true);
      ef.setInputFilter(new UTF8InputFilter(m_IO.formatTemplate("inputerror_utf8filter")));
      if(olddoing !=null && olddoing.length() > 0) {
        ef.setValue(olddoing);
      }
      ef.run();
      String doing = ef.getValue();
      if(doing.contains("@") && !doing.endsWith("@a")) {
        doing = doing + "@a";
      }
      m_Preferences.put(KEY_SESSION_DOING,doing);
      applyDoing();
      save();
      m_IO.printTemplate(PREFIX+ "confirm","doing",
          ContentUtility.translateColorCodes(doing));
    } catch (Exception ex) {
      log.error(c_LogMarker,"run()",ex);
      throw new CommandException(ex.getMessage());
    }
  }//run

  private static final String PREFIX = TPREFIX + "SetDoing_";

}//class SetDoing
