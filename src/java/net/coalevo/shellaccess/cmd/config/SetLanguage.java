/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.config;

import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.shellaccess.impl.Activator;
import net.wimpi.telnetd.io.Templates;
import net.wimpi.telnetd.io.toolkit.Selection;

import java.util.Iterator;

import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * This class implements a command that allows to switch
 * the language within the available ones.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class SetLanguage
    extends ConfigurationCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(SetLanguage.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell)
      throws CommandException {
    if(!prepare(shell)) {
      return;
    }
    try {
      Templates ts = Activator.getTemplates();
      final Iterator iter = ts.getLanguages();
      m_IO.printTemplate(PREFIX + "langlabel");
      //TODO: This way of handling selections is just too complicated :)
      // Make it more easy, really what were you thinking when you wrote it?
      String actlang  = m_Preferences.get(
        KEY_SESSION_LANG,
        m_Defaults.getProperty(KEY_SESSION_LANG)
      );
      Selection sel = new Selection(m_IO.getTerminalIO(),"langsel");
      String none = m_IO.formatTemplate(PREFIX + "noneoption");
      sel.addOption(none);
      int idx = 1;
      while (iter.hasNext()) {
        String lang = iter.next().toString();
        sel.addOption(lang);
        if(lang.equals(actlang)) {
          sel.setSelected(idx);
        }
        idx++;
       }
      sel.run();
      String selected = sel.getOption(sel.getSelected());
      if(none.equals(selected) || actlang.equals(selected)) {
        m_IO.printTemplate(PREFIX + "nochange");
        return;
      } else {
        m_Preferences.put(KEY_SESSION_LANG,selected);
        save();
        applyLanguage();
        m_IO.printTemplate(PREFIX+ "confirm","lang",
            m_IO.getLocale().getDisplayLanguage(m_IO.getLocale()));
      }
    } catch (Exception ex) {
      log.error(c_LogMarker,"run()",ex);
      throw new CommandException(ex.getMessage());
    }
  }//run

  private static final String PREFIX = TPREFIX + "SetLanguage_";

}//class SetLanguage
