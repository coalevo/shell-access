/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.config;

import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.shellaccess.model.Command;
import net.coalevo.shellaccess.model.ShellIO;
import net.coalevo.shellaccess.filters.UTF8InputFilter;
import net.coalevo.text.util.ContentUtility;
import net.wimpi.telnetd.io.toolkit.Editfield;

/**
 * Command that allows to set the host (non permanent atm).
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class SetHost
    implements Command {

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell) throws CommandException {
    ShellIO io = shell.getShellIO();
    try {
      String oldhost = shell.getSession().getHost();
      io.printTemplate(SetHost.PREFIX + "hostlabel");
      Editfield ef = new Editfield(io.getTerminalIO(),"host",50,50);
      ef.setInputFilter(new UTF8InputFilter(io.formatTemplate("inputerror_utf8filter")));
      ef.setIgnoreDelete(true);
      if(oldhost != null && oldhost.length() > 0) {
        ef.setValue(oldhost);
      }
      ef.run();
      String host = ef.getValue();
      if (oldhost == null && oldhost.length() == 0 && host.length() == 0) {
        io.printTemplate(PREFIX+ "abort");
        return;
      }  
      shell.getSession().setHost(host);
      io.printTemplate(PREFIX+ "confirm","host",host);
    } catch (Exception ex) {
      throw new CommandException(ex.getMessage());
    }
  }//run

  public void reset() {
  }//reset

  private static final String PREFIX = ConfigurationCommand.TPREFIX + "SetHost_";

}//class SetDoing
