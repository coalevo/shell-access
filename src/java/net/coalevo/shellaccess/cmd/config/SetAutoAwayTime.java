/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.config;

import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.shellaccess.validators.IntegerInputValidator;
import net.wimpi.telnetd.io.toolkit.Editfield;

/**
 * Command that allows to set auto away time.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class SetAutoAwayTime
    extends ConfigurationCommand {

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell) throws CommandException {
    prepare(shell);
    try {
      m_IO.printTemplate(PREFIX + "timelabel");
      Editfield ef = new Editfield(m_IO.getTerminalIO(),"time",3,3);
      ef.setIgnoreDelete(true);
      ef.setValue(Long.toString(m_Session.getAutoAwayTime()/60000));
      ef.setInputValidator(
          new IntegerInputValidator(
              5,
              60,
              m_IO.formatTemplate("inputerror_intvalidator","min",Integer.toString(5),"max",Integer.toString(60)),
              true
          )
      );
      ef.run();
      String val = ef.getValue();
      if(val == null || val.trim().length() == 0) {
        m_IO.printTemplate(PREFIX + "unchanged");
        return;
      }
      String in = val.trim();
      m_Preferences.put(
          KEY_SESSION_AUTOAWAYTIME,
          Long.toString(Long.parseLong(in)*60000)
      );
      applyAutoAwayTime();
      save();
      m_IO.printTemplate(PREFIX+ "confirm","time",in);

    } catch (Exception ex) {
      throw new CommandException(ex.getMessage());
    }
  }//run

  private static final String PREFIX = ConfigurationCommand.TPREFIX + "SetAutoAwayTime_";

}//class SetAutoAwayTime
