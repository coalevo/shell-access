/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.config;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.messaging.service.GroupMessagingService;
import net.coalevo.shellaccess.impl.Activator;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.shellaccess.model.Services;
import net.coalevo.shellaccess.model.Shortcuts;
import net.wimpi.telnetd.io.TemplateAttributes;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.IOException;

/**
 * This class implements a configuration command that allows to edit number
 * shortcuts for addressees.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class EditShortcuts
    extends ConfigurationCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(EditShortcuts.class.getName());

  private Shortcuts m_Shortcuts;

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell) throws CommandException {
    if (!prepare(shell)) {
      return;
    }
    //prepare map
    Object o = m_Shell.getEnvironmentVariable(ConfigurationCommand.KEY_SHORTCUTS);
    if (o == null) {
      m_Shortcuts = new Shortcuts();
    } else {
      m_Shortcuts = (Shortcuts) o;
    }
    editShortcuts();
  }//run

  public void reset() {
    super.reset();
    m_Shortcuts = null;
  }//reset

  private void editShortcuts() {
    try {
      //Add shortcut, remove shortcut,
      boolean done = false;
      char remove = m_IO.formatTemplate(PREFIX + "remove").charAt(0);
      char edit = m_IO.formatTemplate(PREFIX + "edit").charAt(0);

      do {
        listShortcuts();

        //prompt shortcut
        m_IO.printTemplate(PREFIX + "prompt");
        int shortcut = readShortcut();

        //handle save and exit
        if (shortcut == -1) {
          m_Preferences.put(KEY_SHORTCUTS, m_Shortcuts.toString());
          //apply them from store
          applyShortcuts();
          save();
          done = true;
          continue;
        } else if (shortcut == -2) {
          //restore
          applyShortcuts();
          //end
          done = true;
          continue;
        }

        //check if its empty its an add
        if (!m_Shortcuts.contains(shortcut)) {
          //add operation
          String str = prompt(PREFIX + "addressee");
          if (str != null && str.length() > 0) {
            if (!str.startsWith("#")) {
              //aid
              AgentIdentifier aidn = getAgentIdentifier(str);
              if (aidn != null) {
                if (!str.contains(".service.")) {
                  if (!m_UserdataService.isUserdataAvailable(m_Agent, aidn)) {
                    aidn = null;
                    str = ";"; //to invalidate entry
                  } else {
                    str = aidn.getIdentifier();
                  }
                }
              } else {
                str = ";"; //invalidate entry
              }
            } else {
              GroupMessagingService gms = Activator.getServices().getGroupMessagingService(Services.NO_WAIT);
              if (gms == null || !gms.existsGroup(m_Agent, str.substring(1))) {
                str = ";"; //invalidate entry
              }
            }
            if (str.contains(";") || str.contains("=")) {
              m_IO.printTemplate(PREFIX + "invalidaddressee");
              continue;
            }
            m_Shortcuts.add(shortcut, str);
            m_IO.printTemplate(PREFIX + "added");
          }
          continue;
        }

        //Otherwise ask if delete or update
        char ch;
        do {
          ch = (char) m_IO.prompt(PREFIX + "promptaction", null);
          if (ch != edit && ch != remove) {
            m_IO.getTerminalIO().bell();
          }
        } while (ch != edit && ch != remove);

        if (ch == remove) {
          //remove
          m_Shortcuts.remove(shortcut);
          m_IO.printTemplate(PREFIX + "removed");
        } else if (ch == edit) {
          //update operation
          String str = prompt(PREFIX + "addressee", m_Shortcuts.get(shortcut).toString());
          if (str != null && str.length() > 0) {
            if (str.contains(";") || str.contains("=")) {
              m_IO.printTemplate(PREFIX + "invalidaddressee");
              continue;
            }
            m_Shortcuts.add(shortcut, str);
            m_IO.printTemplate(PREFIX + "updated");
          }
        }
      } while (!done);
    } catch (IOException ex) {
      try {
        m_IO.printTemplate(PREFIX + "failed");
      } catch (IOException iex) {
        log.error(c_LogMarker, "editShortcuts()", iex);
      }
      log.error(c_LogMarker, "editShortcuts()", ex);
    }
  }//editShortcuts

  private void listShortcuts() {
    try {
      //Maybe not most elegant, but it's sorted and does the job.
      TemplateAttributes attr = m_IO.leaseTemplateAttributes();
      for (int i = 0; i < 100; i++) {
        Object o = m_Shortcuts.get(i);
        if (o != null) {
          String str = o.toString();
          String idx = String.format("%02d", i);
          String[] items = {idx, str};
          attr.add("items.{shortcut,addressee}", items);
        }
      }
      m_IO.pageTemplate(PREFIX + "list", attr);
    } catch (IOException ex) {
      try {
        m_IO.printTemplate(PREFIX + "failed");
      } catch (IOException iex) {
        log.error(c_LogMarker, "listShortcuts()", iex);
      }
      log.error(c_LogMarker, "listShortcuts()", ex);
    }
  }//listShortcuts

  private int readShortcut() throws IOException {
    StringBuilder sb = new StringBuilder();
    do {
      int i = m_IO.read();
      //0-9 = 48-57 in ASCII
      if (i >= 48 && i <= 57) {
        sb.append((char) i);
        m_IO.print(Character.toString((char) i));
      } else if (i == 32) {
        return -1;
      } else if (i == 'a') {
        return -2;
      } else {
        m_IO.getTerminalIO().bell();
      }
    } while (sb.length() < 2);
    return Integer.parseInt(sb.toString());
  }//readShortcut

  private static final String PREFIX = TPREFIX + "EditShortcuts_";

}//class EditShortcuts
