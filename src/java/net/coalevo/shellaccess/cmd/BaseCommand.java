/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd;

import net.coalevo.foundation.model.Agent;
import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.model.AgentIdentifierInstanceCache;
import net.coalevo.security.service.SecurityService;
import net.coalevo.shellaccess.cmd.presence.AvailableUserFilter;
import net.coalevo.shellaccess.cmd.presence.PresenceCommand;
import net.coalevo.shellaccess.cmd.presence.PresenceInfo;
import net.coalevo.shellaccess.filters.NicknameInputFilter;
import net.coalevo.shellaccess.filters.StringListCompletionFilter;
import net.coalevo.shellaccess.filters.UTF8InputFilter;
import net.coalevo.shellaccess.impl.Activator;
import net.coalevo.shellaccess.impl.Formatter;
import net.coalevo.shellaccess.impl.RoleUtility;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.Command;
import net.coalevo.shellaccess.model.Services;
import net.coalevo.shellaccess.model.ShellIO;
import net.coalevo.shellaccess.validators.AIDNickValidator;
import net.coalevo.shellaccess.validators.DateFormatValidator;
import net.coalevo.shellaccess.validators.StringValidator;
import net.coalevo.text.model.TransformationException;
import net.coalevo.userdata.service.UserdataService;
import net.wimpi.telnetd.io.BasicTerminalIO;
import net.wimpi.telnetd.io.TemplateAttributes;
import net.wimpi.telnetd.io.micro.Micro;
import net.wimpi.telnetd.io.micro.MicroEd;
import net.wimpi.telnetd.io.terminal.ColorHelper;
import net.wimpi.telnetd.io.toolkit.*;
import org.slf4j.Logger;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Provides an abstract base class for shell commands.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public abstract class BaseCommand
    implements Command {

  protected static final Marker c_LogMarker = MarkerFactory.getMarker(BaseCommand.class.getName());
  protected Logger log;
  protected SecurityService m_SecurityService;
  protected UserdataService m_UserdataService;
  protected ShellIO m_IO;
  protected Agent m_Agent;
  protected AgentIdentifier m_AgentIdentifier;
  protected BasicShell m_Shell;
  protected boolean m_PromptWrap = true;

  private static Class c_JKEditorClass;

  protected static AgentIdentifierInstanceCache c_AgentIdentifierCache =
      new AgentIdentifierInstanceCache();

  protected boolean prepare(BasicShell shell) {
    log = Activator.log();
    m_Shell = shell;
    m_IO = shell.getShellIO();
    m_SecurityService = shell.getServices().getSecurityService(Services.NO_WAIT);
    m_UserdataService = shell.getServices().getUserdataService(Services.NO_WAIT);
    m_Agent = shell.getSession().getUserAgent();
    m_AgentIdentifier = m_Agent.getAgentIdentifier();
    return true;
  }//prepare

  public void reset() {
    m_IO = null;
    m_Agent = null;
    m_AgentIdentifier = null;
    m_SecurityService = null;
    m_UserdataService = null;
    m_Shell = null;
    log = null;
  }//reset

  //*** Security utility methods ***//

  /**
   * Returns a <tt>Set</tt> of roles of the specified agent.
   *
   * @param aid an {@link AgentIdentifier} specifying the agent.
   * @return a <tt>Set</tt> of role names (strings).
   */
  protected Set getRoles(AgentIdentifier aid) {
    return m_SecurityService.getAgentRolesByName(m_Agent, aid);
  }//getRoles

  //*** END: Security utility methods ***//

  //*** Nickname utility methods ***//

  /**
   * Resolves the nickname of the given agent from the
   * {@link UserdataService}.
   *
   * @param aid an {@link AgentIdentifier} specifying the agent.
   * @return the nickname.
   */
  protected String resolveNickname(AgentIdentifier aid) {
    if (aid.getIdentifier().startsWith("net.coalevo.")) {
      //need to resolve a service
      try {
        return m_IO.formatTemplate(String.format("services_%s", aid.getName()));
      } catch (IOException e) {
      }
    }
    if (aid.isLocal() || m_UserdataService == null) {
      try {
        return m_UserdataService.getNickname(m_Agent, aid);
      } catch (Exception ex) {
        return aid.getName();
      }
    } else {
      return aid.getName();
    }
  }//resolveNickname

  /**
   * Resolves the nickname formatted with the style that corresponds
   * to the role of the highest priority that the user has.
   *
   * @param aid an {@link AgentIdentifier}.
   * @return the formatted nickname.
   */
  protected String resolveFormattedNickname(AgentIdentifier aid) {
    return RoleUtility.getUserWithPriorityRoleStyle(
        resolveNickname(aid), getRoles(aid));
  }//resolveFormattedNickname

  /**
   * Resolves the user's nickname from the {@link UserdataService}.
   *
   * @return the users nickname.
   */
  protected String resolveUserNickname() {
    return resolveNickname(m_AgentIdentifier);
  }//resolveUserNickname

  /**
   * Resolves the user's nickname formatted with the style that corresponds
   * to the role of the highest priority that the user has.
   *
   * @return the formatted nickname.
   */
  protected String resolveFormattedUserNickname() {
    return resolveFormattedNickname(m_AgentIdentifier);
  }//resolveFormattedNickname

  protected String getFormattedAgent(AgentIdentifier aid)
      throws IOException {
    //resolve services
    if (aid.getIdentifier().startsWith("net.coalevo.")) {
      //need to resolve a service
      return resolveFormattedNickname(aid);
    }
    TemplateAttributes attr = m_IO.leaseTemplateAttributes();
    attr.add("aid", aid.getIdentifier());
    attr.add("name", resolveFormattedNickname(aid));
    if (!aid.isLocal()) {
      attr.add("remote", "");
    }
    return m_IO.formatTemplate("agent", attr);
  }//getFormattedAgent

  //*** END: Nickname utility methods ***//

  //*** Date Formatting utility methods ***//

  /**
   * Returns a standard locale specific date only format.
   *
   * @return a <tt>DateFormat</tt> for formatting dates.
   */
  protected DateFormat getStandardDateFormat() {
    return SimpleDateFormat.getDateInstance(SimpleDateFormat.MEDIUM, m_IO.getLocale());
  }//getStandardDateFormat

  /**
   * Returns a standard locale specific time only format.
   *
   * @return a <tt>DateFormat</tt> for formatting dates.
   */
  protected DateFormat getStandardTimeFormat() {
    return SimpleDateFormat.getTimeInstance(SimpleDateFormat.MEDIUM, m_IO.getLocale());
  }//getStandardDateFormat

  /**
   * Returns a standard locale specific date time format.
   *
   * @return a <tt>DateFormat</tt> for formatting dates.
   */
  protected DateFormat getStandardDateTimeFormat() {
    return SimpleDateFormat.getDateTimeInstance(SimpleDateFormat.MEDIUM, SimpleDateFormat.MEDIUM, m_IO.getLocale());
  }//getStandardDateTimeFormat

  //*** END: Date Formatting utility methods ***//

  //*** I/O utility methods ***//

  /**
   * Prompts for a yes/no type decision using the given template key.
   *
   * @param key specifying the prompt's template.
   * @return true if positive, false if negative.
   * @throws IOException if an I/O error occurs.
   */
  protected boolean promptDecision(String key)
      throws IOException {
    if (m_PromptWrap) {
      m_IO.println();
    } else {
      m_PromptWrap = true;
    }
    m_IO.printTemplate(key, null);
    return m_IO.getDecision();
  }//promptDecision

  protected String prompt(String key)
      throws IOException {
    if (m_PromptWrap) {
      m_IO.println();
    } else {
      m_PromptWrap = true;
    }
    m_IO.printTemplate(key);

    Editfield ef = new Editfield(m_IO.getTerminalIO(), "input", 50, 255);
    ef.setIgnoreDelete(true);
    ef.run();
    return ef.getValue();
  }//prompt

  protected String prompt(String key, String value)
      throws IOException {
    if (m_PromptWrap) {
      m_IO.println();
    } else {
      m_PromptWrap = true;
    }
    m_IO.printTemplate(key);

    Editfield ef = new Editfield(m_IO.getTerminalIO(), "input", 50, 255);
    ef.setIgnoreDelete(true);
    if (value != null) {
      try {
        ef.setValue(value);
      } catch (BufferOverflowException e) {
        log.error(c_LogMarker, "prompt()", e);
      }
    }
    ef.run();
    return ef.getValue();
  }//prompt

  protected String prompt(String key, InputValidator iv)
      throws IOException {
    if (m_PromptWrap) {
      m_IO.println();
    } else {
      m_PromptWrap = true;
    }
    m_IO.printTemplate(key);

    Editfield ef = new Editfield(m_IO.getTerminalIO(), "input", 50, 255);
    ef.setIgnoreDelete(true);
    if (iv != null) {
      ef.setInputValidator(iv);
    }

    ef.run();
    return ef.getValue();
  }//prompt

  protected String prompt(String key, InputValidator iv, int visize, int size)
      throws IOException {
    if (m_PromptWrap) {
      m_IO.println();
    } else {
      m_PromptWrap = true;
    }
    m_IO.printTemplate(key);

    Editfield ef = new Editfield(m_IO.getTerminalIO(), "input", visize, size);
    ef.setIgnoreDelete(true);
    if (iv != null) {
      ef.setInputValidator(iv);
    }

    ef.run();
    return ef.getValue();
  }//prompt

  protected String prompt(String key, String value, InputValidator iv, int visize, int size)
      throws IOException {
    if (m_PromptWrap) {
      m_IO.println();
    } else {
      m_PromptWrap = true;
    }
    m_IO.printTemplate(key);

    Editfield ef = new Editfield(m_IO.getTerminalIO(), "input", visize, size);
    ef.setIgnoreDelete(true);
    if (value != null) {
      try {
        ef.setValue(value);
      } catch (BufferOverflowException e) {
        log.error(c_LogMarker, "prompt()", e);
      }
    }
    if (iv != null) {
      ef.setInputValidator(iv);
    }

    ef.run();
    return ef.getValue();
  }//prompt

  protected String prompt(String key, String val, InputValidator iv)
      throws IOException {
    if (m_PromptWrap) {
      m_IO.println();
    } else {
      m_PromptWrap = true;
    }
    m_IO.printTemplate(key);

    Editfield ef = new Editfield(m_IO.getTerminalIO(), "input", 50, 255);
    ef.setIgnoreDelete(true);
    if (val != null) {

      try {
        ef.setValue(val);
      } catch (BufferOverflowException e) {
        log.error(c_LogMarker, "prompt()", e);
      }
    }
    if (iv != null) {
      ef.setInputValidator(iv);
    }

    ef.run();
    return ef.getValue();
  }//prompt

  protected String prompt(String key, InputValidator iv, List<String> list)
      throws IOException {
    if (m_PromptWrap) {
      m_IO.println();
    } else {
      m_PromptWrap = true;
    }
    m_IO.printTemplate(key);

    Editfield ef = new Editfield(m_IO.getTerminalIO(), "input", 50, 255);
    ef.setIgnoreDelete(true);
    if (iv != null) {
      ef.setInputValidator(iv);
    }
    if (list != null) {
      ef.setInputFilter(new StringListCompletionFilter(ef, list));
    }

    ef.run();
    return ef.getValue();
  }//prompt

  protected String prompt(String key, InputValidator iv, TabCompletionFilter infil)
      throws IOException {
    if (m_PromptWrap) {
      m_IO.println();
    } else {
      m_PromptWrap = true;
    }
    m_IO.printTemplate(key);

    Editfield ef = new Editfield(m_IO.getTerminalIO(), "input", 50, 255);
    ef.setIgnoreDelete(true);
    if (iv != null) {
      ef.setInputValidator(iv);
    }
    if (infil != null) {
      infil.assignEditfield(ef);
      ef.setInputFilter(infil);
    }

    ef.run();
    return ef.getValue();
  }//prompt


  protected String prompt(String key, String val, InputValidator iv, List<String> list)
      throws IOException {
    if (m_PromptWrap) {
      m_IO.println();
    } else {
      m_PromptWrap = true;
    }
    m_IO.printTemplate(key);

    Editfield ef = new Editfield(m_IO.getTerminalIO(), "input", 50, 255);
    ef.setIgnoreDelete(true);
    if (val != null) {
      try {
        ef.setValue(val);
      } catch (BufferOverflowException e) {
        log.error(c_LogMarker, "prompt()", e);
      }
    }
    if (iv != null) {
      ef.setInputValidator(iv);
    }
    if (list != null) {
      ef.setInputFilter(new StringListCompletionFilter(ef, list));
    }

    ef.run();
    return ef.getValue();
  }//prompt


  public Date promptShortDate(String key, Date d) throws Exception {
    SimpleDateFormat sdf = (SimpleDateFormat) SimpleDateFormat.getDateInstance(
        SimpleDateFormat.SHORT, m_IO.getLocale());
    String pattern = sdf.toPattern();
    TemplateAttributes attr = m_IO.leaseTemplateAttributes();
    attr.add("dateformat", pattern);
    InputValidator iv = new DateFormatValidator(
        sdf,
        m_IO.formatTemplate("inputerror_datevalidator", "format", pattern)
    );
    if (m_PromptWrap) {
      m_IO.println();
    } else {
      m_PromptWrap = true;
    }
    m_IO.printTemplate(key, attr);

    Editfield ef = new Editfield(m_IO.getTerminalIO(), "input", 50, 255);
    ef.setIgnoreDelete(true);
    if (iv != null) {
      ef.setInputValidator(iv);
    }
    ef.setValue(sdf.format(d));
    ef.run();
    String date = ef.getValue();
    return sdf.parse(date);
  }//promptShortDate

  public AgentIdentifier promptAgent(String key) throws IOException {
    if (m_PromptWrap) {
      m_IO.println();
    } else {
      m_PromptWrap = true;
    }
    m_IO.printTemplate(key);
    Editfield eField = new Editfield(m_IO.getTerminalIO(), "username", 50, 255);
    eField.setInputValidator(new AIDNickValidator(
        m_Shell.getServices(), m_IO.formatTemplate("inputerror_aidnickvalidator"), true));
    if (m_Shell.isEnviromentVariableSet(PresenceCommand.ENVKEY_PRESENCEINFO)) {
      PresenceInfo presenceInfo = (PresenceInfo) m_Shell.getEnvironmentVariable(
          PresenceCommand.ENVKEY_PRESENCEINFO);
      eField.setInputFilter(new AvailableUserFilter(eField, presenceInfo));
    } else {
      eField.setInputFilter(new NicknameInputFilter(false, m_IO.formatTemplate("inputerror_asciifilter")));
    }
    eField.setInsertMode(true);
    eField.setIgnoreDelete(true);
    eField.run();
    String uname = eField.getValue();
    if (uname == null || uname.length() == 0) {
      return null;
    }
    AgentIdentifier aidn = getAgentIdentifier(uname);
    if (aidn != null) {
      if (uname.contains(".service.")) {
        return aidn;
      }
      if (!m_UserdataService.isUserdataAvailable(m_Agent, aidn)) {
        aidn = null;
      }
    }
    return aidn;
  }//promptAgent

  public AgentIdentifier promptAgent(String key, List<AgentIdentifier> list)
      throws IOException {
    if (m_PromptWrap) {
      m_IO.println();
    } else {
      m_PromptWrap = true;
    }
    m_IO.printTemplate(key);
    //prepare filter
    List<String> names = new ArrayList<String>();
    for (Iterator<AgentIdentifier> iterator = list.iterator(); iterator.hasNext();) {
      AgentIdentifier agentIdentifier = iterator.next();
      names.add(this.resolveNickname(agentIdentifier));
    }
    Editfield eField = new Editfield(m_IO.getTerminalIO(), "username", 50, 255);
    eField.setInsertMode(true);
    eField.setIgnoreDelete(true);
    eField.setInputFilter(new StringListCompletionFilter(eField, names));
    eField.run();
    String uname = eField.getValue();
    if (uname == null || uname.length() == 0) {
      return null;
    }
    AgentIdentifier aidn = getAgentIdentifier(uname);
    if (aidn != null) {
      if (!m_UserdataService.isUserdataAvailable(m_Agent, aidn)) {
        aidn = null;
      }
    }
    return aidn;
  }//promptAgent

  public AgentIdentifier promptAgentUpdate(String key, AgentIdentifier aid) throws IOException {
    if (m_PromptWrap) {
      m_IO.println();
    } else {
      m_PromptWrap = true;
    }
    m_IO.printTemplate(key);
    Editfield eField = new Editfield(m_IO.getTerminalIO(), "username", 50, 255);
    if (m_Shell.isEnviromentVariableSet(PresenceCommand.ENVKEY_PRESENCEINFO)) {
      PresenceInfo presenceInfo = (PresenceInfo) m_Shell.getEnvironmentVariable(
          PresenceCommand.ENVKEY_PRESENCEINFO);
      eField.setInputFilter(new AvailableUserFilter(eField, presenceInfo));
    } else {
      eField.setInputFilter(new NicknameInputFilter(false, m_IO.formatTemplate("inputerror_asciifilter")));
    }
    eField.setInsertMode(true);
    eField.setIgnoreDelete(true);
    try {
      eField.setValue(aid.toString());
    } catch (BufferOverflowException e) {
      log.error(c_LogMarker, "promptAgentUpdate()", e);
    }
    eField.run();
    String uname = eField.getValue();
    AgentIdentifier aidn = getAgentIdentifier(uname);
    if (aidn != null) {
      if (!m_UserdataService.isUserdataAvailable(m_Agent, aidn)) {
        aidn = null;
      }
    }
    return aidn;
  }//promptAgentUpdate

  public AgentIdentifier getAgentIdentifier(String str) {
    //shortcut if identifier
    if (str.contains("@")) {
      return c_AgentIdentifierCache.get(str);
    }
    //go for aid
    AgentIdentifier[] ids = null;
    try {
      ids = m_UserdataService.getIdentifiers(m_Agent, str);
    } catch (SecurityException sex) {
      return c_AgentIdentifierCache.get(str);
    }
    if (ids != null && ids.length > 0) {
      return ids[0];
    }

    return c_AgentIdentifierCache.get(str);
  }//getAgentIdentifier


  public void listRestrictable(List<AgentIdentifier> list, int restrictif, String nonekey, String lhkey)
      throws IOException {
    if (list.isEmpty()) {
      m_IO.printTemplate(nonekey);
      return;
    } else if (list.size() > restrictif) {  //wimpi: arbitrary size, thats about three pages
      if (promptDecision(PREFIX + "filter")) {
        String startswith = prompt(PREFIX + "startwith", new StringValidator());
        for (Iterator<AgentIdentifier> iterator = list.iterator(); iterator.hasNext();) {
          AgentIdentifier agentIdentifier = iterator.next();
          if (!agentIdentifier.getName().startsWith(startswith)) {
            iterator.remove();
          }
        }
      }
    }
    TemplateAttributes attr = m_IO.leaseTemplateAttributes();
    for (Iterator iter = list.iterator(); iter.hasNext();) {
      AgentIdentifier aid = (AgentIdentifier) iter.next();
      attr.add("agents", getFormattedAgent(aid));
    }
    m_IO.printTemplate(lhkey);
    m_IO.pageTemplate(PREFIX + "list", attr);
  }//listRestrictable

  protected String editTransformableContent(String inf, String content)
      throws IOException, BufferOverflowException {
    boolean done = false;

    EmbeddedNano en = null;
    Micro m = null;
    String c = "";

    if (EmbeddedNano.isAvailable()) {
      en = new EmbeddedNano();
      en.prepare(m_Shell);
    } else {
      m = new Micro(m_Shell.getShellIO().getTerminalIO());
      m.setJustBackspace(true);
    }
    while (!done) {
      if (EmbeddedNano.isAvailable()) {
        if(content != null && content.length() > 0) {
          en.setContent(content);
        }
        en.run();
        c = en.getContent();
      } else {
        if(content != null && content.length() > 0) {
          m.setText(content);
        }
        m.run();
        c = m.getText();
      }
      if (inf != null && inf.length() > 0 && !"plain".equals(inf)) {
        //Preview?
        try {
          String pc = Formatter.formatPreview(inf, c);
          m_IO.blankTerminal();
          m_PromptWrap = false;
          if (promptDecision(PREFIX + "preview")) {
            m_IO.page(pc);
          }
          if (promptDecision(PREFIX + "continueedit")) {
            done = false;
            if (!EmbeddedNano.isAvailable()) {
              m.setText(c);
            }
            m_IO.println();
          } else {
            done = true;
          }
        } catch (TransformationException ex) {
          m_IO.blankTerminal();
          TemplateAttributes attr = m_IO.leaseTemplateAttributes();
          attr.add("linenumber", "" + ex.getLineNumber());
          attr.add("err", ex.getMessage());
          m_IO.printTemplate(PREFIX + "editerror", attr);
          if (!promptDecision(PREFIX + "askeditcorrect")) {
            return "";
          }
        }
      } else {
        done = true;
      }
    }
    if (EmbeddedNano.isAvailable() && en != null) {
      en.cleanup();
    }
    return c;
  }//editTransformableContent

  protected String promptInputFormat(String str)
      throws IOException {
    if (m_PromptWrap) {
      m_IO.println();
    } else {
      m_PromptWrap = true;
    }
    Set<String> formats = Formatter.getAvailableFormats();
    //Prompt
    m_IO.printTemplate(PREFIX + "promptformat");
    MaskedSelection s = new MaskedSelection(m_IO.getTerminalIO(), "inf");
    String none = m_IO.formatTemplate(PREFIX + "noneoption");
    s.addOption(NONE, none);
    int i = 0;
    for (String format : formats) {
      s.addOption(format, m_IO.formatTemplate(PREFIX + format));
      if (format.equals(str)) {
        s.setSelected(i);
      }
      i++;
    }
    s.run();
    Object o = s.getSelectedValue();
    if (NONE == o) {
      return "";
    } else {
      return (String) o;
    }
  }//promptInputFormat

  protected String simpleEdit(String flag) throws IOException {
    boolean whiner = m_Shell.getSession().isWhiner();
    if (whiner) {
      m_IO.println();
      try {
        ActiveComponent editor = getJKEditor(m_IO.getTerminalIO());
        editor.run();
        return UTF8InputFilter.stripNonValidCharacters(editor.toString());
      } catch (Exception e) {
        e.printStackTrace(System.err);
      }
    } else {
      MicroEd ed = new MicroEd(m_IO.getTerminalIO(), 25);
      ed.run();
      if (ed.isSwitchEditor()) {
        if (EmbeddedNano.isAvailable()) {
          EmbeddedNano en = new EmbeddedNano();
          try {
            en.prepare(m_Shell);
            en.setContent(ed.getValue());
            en.run();
            return UTF8InputFilter.stripNonValidCharacters(en.getContent());
          } finally {
            en.cleanup();
          }
        } else {
          Micro m = new Micro(m_IO.getTerminalIO());
          if (flag != null) {
            m.setTitleString(flag);
          }
          m.setJustBackspace(true);
          try {
            m.setText(ed.getValue());
            m.run();
            return UTF8InputFilter.stripNonValidCharacters(m.getText());
          } catch (Exception ex) {
            //should not happen
            return m.getText();
          }
        }
      } else {
        return ed.getValue();
      }
    }
    return "FAILED";
  }//simpleEdit

  protected void showError(String error) throws IOException {
    Titlebar t = new Titlebar(m_IO.getTerminalIO(), error);
    t.setBackgroundColor(ColorHelper.RED);
    t.setForegroundColor(ColorHelper.WHITE);
    t.setAlignment(Titlebar.ALIGN_LEFT);
    t.draw();
  }//showError

  //*** END: I/O utility methods ***//

  public boolean isUserExpert() {
    return m_Shell.getSession().isExpert();
  }//isUserExpert

  private static void loadJKEditorClass() {
    try {
      c_JKEditorClass =
          Activator.getBundle().loadClass(
              "jkara.telnet.toolkit.JKDynamicEditarea"
          );
    } catch (Throwable t) {
      t.printStackTrace(System.err);
    }
  }//loadJKEditorClass

  private static ActiveComponent getJKEditor(BasicTerminalIO io) {
    try {
      if (c_JKEditorClass == null) {
        loadJKEditorClass();
      }
      Constructor c = c_JKEditorClass.getConstructor(BasicTerminalIO.class);
      return (ActiveComponent) c.newInstance(io);
    } catch (Throwable t) {
      t.printStackTrace(System.err);
      return null;
    }
  }//getJKEditor

  private static final String PREFIX = "commands_net.coalevo.shellaccess.cmd.BaseCommand_";
  protected static Object NONE = new Object();

}//class BaseCommand
