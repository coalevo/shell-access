/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.messaging;

import net.coalevo.messaging.model.EditableInteractiveMessage;
import net.coalevo.messaging.model.InteractiveMessage;
import net.coalevo.messaging.model.InteractiveMessageTypes;
import net.coalevo.messaging.model.MessagingException;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.wimpi.telnetd.io.TemplateAttributes;

import java.io.IOException;
import java.util.Locale;

import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * Command implementation that allows to send broadcast messages.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class SendBroadcastMessage
    extends MessagingCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(SendBroadcastMessage.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell) throws CommandException {
    prepare(shell);
    EditableInteractiveMessage em = null;
    try {
      //1. create the message instance
      em = m_MessagingService.create(m_Presence,
          InteractiveMessageTypes.CHAT, null);
      em.setTo(c_AgentIdentifierCache.get("all"));
      //2. request confirmation
      m_IO.printTemplate(PREFIX + "requestconfirm");
      em.setConfirmationRequired(m_IO.getDecision());

      //4. get message
      String msg = getMessage();
      if (msg != null && msg.length() > 0 && !msg.equals("\n") && !msg.equals("\n\n")) {
        em.setBody(msg);
        remPendingOut(em);
        m_MessagingService.broadcast(m_Presence, em);
        m_IO.printTemplate(PREFIX + "sent");
      } else {
        m_IO.printTemplate(PREFIX + "aborted");
      }

    } catch (MessagingException mex) {
      try {
        m_IO.printTemplate(PREFIX + "cannot");
      } catch (Exception ex) {
        log.error(c_LogMarker,"run()", ex);
      }
    } catch (Exception ex) {
      log.error(c_LogMarker,"run()", ex);
    }
  }//run

  protected String getMessage() throws IOException {
    return simpleEdit(null);
  }//getMessage

  protected void remPendingOut(InteractiveMessage m)
      throws IOException {
    TemplateAttributes attr = m_IO.leaseTemplateAttributes();
    long st = System.currentTimeMillis();
    Locale l = m_IO.getLocale();
    String fmsg = m_IO.formatTemplate("MessagingServiceListener_sent",
        MessageConversion.toAttributes(
            st,
            m,
            getFormattedAgent(m.getFrom()),
            getFormattedAgent(m.getTo()),
            l,
            attr
        )
    );
    m_MessagingHistory.addOutgoingMessage(m, st, l, fmsg, false);
  }//remPendingOut

  private static final String PREFIX = TS_PREFIX + "SendBroadcastMessage_";

}//class SendBroadcastMessage
