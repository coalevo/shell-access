/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.messaging;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.messaging.model.EventInfo;
import net.coalevo.messaging.model.InteractiveMessage;
import net.coalevo.messaging.model.InteractiveMessageTypes;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.Hit;
import org.apache.lucene.search.Hits;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Implements a messaging history based on
 * a Lucene In-Memory Index.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class MessagingHistoryImpl implements MessagingHistory {

  private AgentIdentifier m_Owner;
  private AtomicInteger m_InCounter;
  private AtomicInteger m_OutCounter;
  private AgentIdentifier m_LastSender;
  private String m_LastGroupSender;
  private AgentIdentifier m_LastAddressee;
  private String m_LastGroupAddressee;
  private BoundedIndex m_BoundedIndex;
  private Map<String, Document> m_PendingOut;
  private QueryParser m_QueryParser;

  public MessagingHistoryImpl(AgentIdentifier owner, boolean mem) {
    m_Owner = owner;
    m_InCounter = new AtomicInteger(0);
    m_OutCounter = new AtomicInteger(0);
    if (mem) {
      m_BoundedIndex = new BoundedMemoryIndex(100);
    } else {
      m_BoundedIndex = new BoundedDiskIndex(m_Owner, 250);
    }
    m_PendingOut = new WeakHashMap<String, Document>();
    m_QueryParser = new QueryParser(MessageConversion.FIELD_BODY, m_BoundedIndex.getAnalyzer());
  }//constructor

  public int addIncomingMessage(InteractiveMessage m, EventInfo ei, long time, Locale l, String fmsg) {
    m_BoundedIndex.add(MessageConversion.toDocument(true, time, m, ei, l, fmsg));
    if (m.getType().equals(InteractiveMessageTypes.CHAT) ||
        m.getType().equals(InteractiveMessageTypes.QUESTION) ||
        m.getType().equals(InteractiveMessageTypes.OFFLINE)) {
      m_LastSender = m.getFrom();
    }
    if (m.getType().equals(InteractiveMessageTypes.GROUPCHAT)) {
      m_LastGroupSender = m.getThread();
    } else {
      m_LastGroupSender = null;
    }
    return m_InCounter.getAndIncrement();
  }//addIncomingMessage

  public void addOutgoingMessage(InteractiveMessage m, long time, Locale l, String fmsg, boolean index) {
    if (index) {
      m_BoundedIndex.add(MessageConversion.toDocument(false, time, m, m.getEventInfo(), l, fmsg));
    } else {
      m_PendingOut.put(m.getIdentifier(),
          MessageConversion.toDocument(false, time, m, m.getEventInfo(), l, fmsg));
    }
    if (m.getType().equals(InteractiveMessageTypes.CHAT)) {
      m_LastAddressee = m.getTo();
    }
    if (m.getType().equals(InteractiveMessageTypes.GROUPCHAT)) {
      m_LastGroupAddressee = m.getThread();
    } else {
      m_LastGroupAddressee = null;
    }
  }//addOutgoingMessage

  public int confirmOutgoingMessage(String id) {
    Object o = m_PendingOut.remove(id);
    if (o != null) {
      m_BoundedIndex.add((Document) o);
    }
    return m_OutCounter.getAndIncrement();
  }//confirmOutgoingMessage

  /**
   * Returns the sender of the last received/incoming message.
   *
   * @return an {@link AgentIdentifier}.
   */
  public AgentIdentifier getLastSender() {
    return m_LastSender;
  }//getLastSender

  /**
   * Returns the name of the last group message that
   * was received.
   *
   * @return the name of the last group a message was received from.
   */
  public String getLastGroupSender() {
    return m_LastGroupSender;
  }//getLastGroupSender

  /**
   * Returns the addressee of the last sent/outgoing message.
   *
   * @return an {@link AgentIdentifier}.
   */
  public AgentIdentifier getLastAddressee() {
    return m_LastAddressee;
  }//getLastAddressee

  /**
   * Returns the name of the last group message that
   * was sent.
   *
   * @return the name of the last group a message was received from.
   */
  public String getLastGroupAddressee() {
    return m_LastGroupAddressee;
  }//getLastGroupAddressee

  /**
   * Returns the total count of all incoming and outgoing messages.
   *
   * @return the total count of all messages.
   */
  public int getTotalMessageCount() {
    return m_InCounter.get() + m_OutCounter.get();
  }//getTotalMessageCount

  /**
   * Returns the count of all incoming messages.
   *
   * @return the number of received/incoming messages.
   */
  public int getIncomingMessageCount() {
    return m_InCounter.get();
  }//getIncomingMessageCount

  /**
   * Returns the count of all outgoing messages.
   *
   * @return the total number of sent/outgoing messages.
   */
  public int getOutgoingMessageCount() {
    return m_OutCounter.get();
  }//getOutgoingMessageCount

  public int size() {
    return m_BoundedIndex.size();
  }//size

  public String toString() {
    StringBuilder sbuf = new StringBuilder();
    try {
      IndexReader ir = m_BoundedIndex.getReader();
      for (int i = 0; i < ir.maxDoc(); i++) {
        if (!ir.isDeleted(i)) {
          Document d = ir.document(i);
          //System.out.println("i="+i + "::"+d.toString());
          //sbuf.append(TerminalIO.CRLF);
          sbuf.append(MessageConversion.getMessage(d));
          //sbuf.append(TerminalIO.CRLF);
        }
      }
      ir.close();
    } catch (Exception ex) {

    }
    return sbuf.toString();
  }//toString

  public Query parseQuery(String query) {
    try {
      return m_QueryParser.parse(query);
    } catch (ParseException ex) {
      return null;
    }
  }//parseQuery

  public String getMessages(Query q) {
    StringBuilder sbuf = new StringBuilder();
    try {
      IndexSearcher is = m_BoundedIndex.getSearcher();
      Hits hits = is.search(q);
      for (Iterator iterator = hits.iterator(); iterator.hasNext();) {
        Hit h = (Hit) iterator.next();
        //sbuf.append(TerminalIO.CRLF);
        sbuf.append(h.get(MessageConversion.FIELD_MSG));
        //sbuf.append(TerminalIO.CRLF);
      }
    } catch (Exception ex) {
    }
    return sbuf.toString();
  }//getMessages

  public ListIterator<String> iterateMessages() {
    IndexReader ir = m_BoundedIndex.getReader();
    ArrayList<String> list = new ArrayList<String>(ir.maxDoc());
    for (int i = ir.maxDoc(); i >= 0; i--) {
      if (!ir.isDeleted(i)) {
        try {
          list.add(ir.document(i).getField(MessageConversion.FIELD_MSG).stringValue());
        } catch (IOException e) {
        }
      }
    }
    return list.listIterator();
  }//iterateMessages

  public void setIndex(boolean mem) {
    if (mem) {
      try {
        ((BoundedDiskIndex) m_BoundedIndex).delete();
      } catch (IOException ex) {
        ex.printStackTrace(System.err);
      }
      m_BoundedIndex = new BoundedMemoryIndex(100);
    } else {
      m_BoundedIndex = new BoundedDiskIndex(m_Owner, 250);
    }
  }//setIndex

  public void closeIndex() {
    m_BoundedIndex.close();
  }//closeIndex

}//class MessagingHistoryImpl
