/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.messaging;

import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.shellaccess.cmd.config.ConfigurationCommand;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * Registers the caller to the messaging service.
 * <p/>
 * This command should be registered as a start auto-command
 * for the command shell.
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class Register
    extends MessagingCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(Register.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell) throws CommandException {
    //System.err.println("Registering with MessagingService.");
    prepare(shell);
    boolean mem = true;
    Object o = shell.getEnvironmentVariable(ConfigurationCommand.KEY_MESSAGINGHISTORY_TYPE);
    if(o != null && ConfigurationCommand.MESSAGINGHISTORY_TYPE_DISK.equalsIgnoreCase(((String)o))) {
      mem = false;
    }
    m_MessagingHistory = new MessagingHistoryImpl(m_AgentIdentifier, mem);
    m_MessagingServiceListener = new MessagingServiceListenerImpl(
        m_Presence, shell, shell.getSession(), m_MessagingHistory
    );
    //store in environment (don't forget to purge it?)
    shell.setEnvironmentVariable(
        MessagingCommand.ENVKEY_LISTENER, m_MessagingServiceListener);
    shell.setEnvironmentVariable(
        MessagingCommand.ENVKEY_HISTORY, m_MessagingHistory);

    try {
      m_MessagingService.register(m_Presence, m_MessagingServiceListener);
    } catch (Exception ex) {
      log.error(c_LogMarker,"run()", ex);
    }
  }//run

}//class Register
