/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.messaging;

import net.coalevo.shellaccess.impl.SessionManager;
import net.coalevo.shellaccess.impl.Activator;
import net.coalevo.shellaccess.model.ShellAccessSession;
import net.coalevo.foundation.model.AgentIdentifier;
import net.wimpi.telnetd.io.TemplateAttributes;

import java.util.ListIterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Random;
import java.io.IOException;

/**
 * This class provides a command that allows to ask a sysguide for help.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class AskSysguide
    extends SendInteractiveMessage {

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  protected AgentIdentifier getAddressee() throws IOException {
    //select a random guide
    SessionManager smgr = Activator.getServices().getSessionManager();
    List<AgentIdentifier> guides = new ArrayList<AgentIdentifier>();
    for(ListIterator iter =  (ListIterator) smgr.sessions(); iter.hasNext();) {
      ShellAccessSession ses = (ShellAccessSession) iter.next();
      AgentIdentifier aid = ses.getUserAgent().getAgentIdentifier();
      if(!aid.equals(m_AgentIdentifier) && ses.isSysguideActive() && (isAdvertisedAvailable(aid) || m_PresenceInfo.isAvailable(aid))) {
        guides.add(aid);
      }
    }
    if(guides.isEmpty()) {
      m_IO.printTemplate(PREFIX + "nosysguides");
      return null;
    } else {
      Random r = new Random();
      AgentIdentifier addressee = guides.get(r.nextInt(guides.size()));
      TemplateAttributes attr = m_IO.leaseTemplateAttributes();
      attr.add("to", getFormattedAgent(addressee));
      m_IO.printTemplate(PREFIX + "addressee", attr);
      return addressee;
    }
  }//getAddressee

  private static final String PREFIX = TS_PREFIX + "AskSysguide_"; 
  
}//class AskSysguide
