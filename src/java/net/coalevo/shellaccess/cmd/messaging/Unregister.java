/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.messaging;

import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;

/**
 * Unregisters the caller from the messaging service.
 * <p/>
 * This command should be registered as a stop auto-command
 * of the command shell.
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class Unregister
    extends MessagingCommand {

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell) throws CommandException {
    prepare(shell);
    //clean close
    MessagingHistory hist = (MessagingHistory) shell.getEnvironmentVariable(MessagingCommand.ENVKEY_HISTORY);
    hist.closeIndex();
    
    shell.unsetEnvironmentVariable(
        MessagingCommand.ENVKEY_HISTORY);
    shell.unsetEnvironmentVariable(
        MessagingCommand.ENVKEY_LISTENER);

    m_MessagingService.unregister(m_Presence);
  }//run

}//class Unregister
