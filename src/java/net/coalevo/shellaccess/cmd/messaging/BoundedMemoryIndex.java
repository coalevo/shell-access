/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.messaging;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.SimpleAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.IndexModifier;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.apache.lucene.store.RAMDirectory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Implements a bounded in-memory index based on Lucene.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class BoundedMemoryIndex implements BoundedIndex {

  private static Logger log = LoggerFactory.getLogger(BoundedMemoryIndex.class);
  private RAMDirectory m_Index;
  private Analyzer m_Analyzer;
  private Sort m_Sort;
  private IndexModifier m_IndexModifier;

  private int m_MaxDocuments;

  public BoundedMemoryIndex(int maxdocs) {
    m_MaxDocuments = maxdocs;
    m_Index = new RAMDirectory();
    m_Analyzer = new SimpleAnalyzer();
    m_Sort = new Sort(new SortField(FIELD_NUM, SortField.INT));
    try {
      m_IndexModifier = new IndexModifier(m_Index, m_Analyzer, true);
    } catch (IOException ex) {
      log.error("BoundedMemoryIndex()", ex);
    }
  }//constructor

  public void add(Document d) {
    try {
      //add index num field
      d.add(new Field(FIELD_NUM, Long.toString(System.currentTimeMillis()),
          Field.Store.NO, Field.Index.UN_TOKENIZED));

      synchronized (m_IndexModifier) {
        m_IndexModifier.addDocument(d);
        m_IndexModifier.flush();
        if (m_IndexModifier.docCount() > m_MaxDocuments) {
          m_IndexModifier.deleteDocument(0);
        }
        m_IndexModifier.optimize();
      }
    } catch (Exception ex) {
      log.error("add()", ex);
    }
  }//add

  public int size() {
    return m_IndexModifier.docCount();
  }//size

  public Sort getDefaultSort() {
    return m_Sort;
  }//getDefaultSort

  public IndexSearcher getSearcher() {
    try {
      return new IndexSearcher(m_Index);
    } catch (Exception ex) {
      log.error("getSearcher()", ex);
      return null;
    }
  }//getSearcher

  public IndexReader getReader() {
    try {
      return IndexReader.open(m_Index);
    } catch (Exception ex) {
      log.error("getReader()");
      return null;
    }
  }//getReader

  public Analyzer getAnalyzer() {
    return m_Analyzer;
  }//getAnalyzer

  public void close() {
    try {
      m_IndexModifier.close();
    } catch (Exception ex) {
      log.error("close()");
    }
  }//close

}//class BoundedMemoryIndex
