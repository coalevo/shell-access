/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.messaging;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.messaging.model.MessagingException;
import net.coalevo.messaging.model.NoSuchGroupException;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.IOException;

/**
 * Provides a command for inviting another agent to a
 * messaging group.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class InviteToGroup
    extends MessagingCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(InviteToGroup.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell) throws CommandException {
    prepare(shell);
    try {
      //1. prompt agent
      m_PromptWrap = false;
      String gname = prompt(PREFIX + "groupname");
      if(gname == null || gname.length() == 0) {
        m_IO.printTemplate(PREFIX + "aborted");
        return;
      }
      doInvites(gname);
    } catch (Exception ex) {
      log.error(c_LogMarker, "run()", ex);
    }
  }//run

  public void doInvites(String gname) throws IOException {
    boolean hasinvite = false;
    do {
        AgentIdentifier aid = promptAgent(PREFIX + "agent");
        if (aid == null) {
          if(!hasinvite) {
            m_IO.printTemplate(PREFIX + "nosuchagent");
          }
          return;
        } else {
          hasinvite = true;
        }
        try {
          m_GroupMessagingService.inviteToGroup(m_Agent, gname, aid);
          m_IO.printTemplate(PREFIX + "confirm");
        } catch (NoSuchGroupException nsgex) {
          m_IO.printTemplate(PREFIX + "nosuchgroup");
          return;
        } catch (SecurityException ex) {
          m_IO.printTemplate(PREFIX + "notallowed");
          return;
        } catch (MessagingException mex) {
          m_IO.printTemplate(PREFIX + "failed");
          return;
        }
      } while (true);
  }//invite

  private static final String PREFIX = TS_PREFIX + "InviteToGroup_";

}//class InviteToGroup
