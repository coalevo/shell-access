/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.messaging;

import net.coalevo.foundation.model.Agent;
import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.messaging.model.*;
import net.coalevo.messaging.service.GroupMessagingService;
import net.coalevo.messaging.service.MessagingService;
import net.coalevo.presence.model.Presence;
import net.coalevo.security.service.SecurityService;
import net.coalevo.shellaccess.impl.Activator;
import net.coalevo.shellaccess.impl.RoleUtility;
import net.coalevo.shellaccess.impl.ServiceMediator;
import net.coalevo.shellaccess.model.ShellAccessSession;
import net.coalevo.shellaccess.model.ShellIO;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.text.util.DateFormatter;
import net.coalevo.userdata.service.UserdataService;
import net.wimpi.telnetd.io.TemplateAttributes;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.IOException;
import java.util.Date;
import java.util.Locale;

/**
 * Implements a {@link MessagingServiceListener} that will
 * output events to the associated {@link ShellIO}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class MessagingServiceListenerImpl
    implements MessagingServiceListener {

  private static Marker c_LogMarker = MarkerFactory.getMarker(MessagingServiceListenerImpl.class.getName());
  private static DateFormatter c_OfflineFormat = new DateFormatter("dd/MM HH:mm:ss", 5);

  private BasicShell m_Shell;
  private ShellIO m_IO;
  private ShellAccessSession m_Session;
  private MessagingHistory m_MessagingHistory;
  private MessagingService m_MessagingService;
  private GroupMessagingService m_GroupMessagingService;
  private Presence m_Presence;
  private UserdataService m_UserdataService;
  private SecurityService m_SecurityService;
  private Agent m_Agent;

  public MessagingServiceListenerImpl(Presence p, BasicShell shell, ShellAccessSession s, MessagingHistory m) {
    m_Presence = p;
    m_Shell = shell;
    m_IO = shell.getShellIO();
    m_Session = s;
    m_MessagingHistory = m;
    m_MessagingService = Activator.getServices().getMessagingService(ServiceMediator.NO_WAIT);
    m_GroupMessagingService = Activator.getServices().getGroupMessagingService(ServiceMediator.NO_WAIT);
    m_UserdataService = Activator.getServices().getUserdataService(0);
    m_SecurityService = Activator.getServices().getSecurityService(0);
    m_Agent = m_Session.getUserAgent();
  }//constructor

  public boolean received(InteractiveMessage m) {
    try {
      boolean group = false;
      boolean discussion = false;
      boolean mail = false;
      boolean notification = false;
      EventInfo ei = m.getEventInfo();
      AgentIdentifier from = ei.getFrom();

      long rt = System.currentTimeMillis();
      try {
        if (m.isConfirmationRequired()) {
          rt = m_MessagingService.confirm(m_Presence, ei);
        } else {
        }
      } catch (MessagingException mex) {
      }

      try {
        if (m_GroupMessagingService.isGroupMessagingAgent(from)) {
          group = true;
        } else if (from.getIdentifier().startsWith("net.coalevo.discussion.service.DiscussionService")) {
          discussion = true;
        } else if(from.getIdentifier().startsWith("net.coalevo.postoffice.service.PostOfficeService")) {
          mail = true;
        }
      } catch (Exception ex) {
        //do nothing
      }
      TemplateAttributes attr = MessageConversion.toAttributes(
          rt,
          m,
          getFormattedAgent(ei.getFrom()),
          getFormattedAgent(ei.getTo()),
          m_IO.getLocale(),
          m_IO.leaseTemplateAttributes()
      );

      String str = null;
      if (InteractiveMessageTypes.NOTIFICATION.equals(m.getType())) {
        notification = true;
        String sub = m.getSubject(LANG_MACHINE);
        if (group) {
          if (!m_Session.isGroupPresenceNotification() &&
              ("presence".equals(sub) ||"absence".equals(sub))) {
            //supress output
            return true;
          }
          if("invite".equals(sub)) {
            //check auto join
            if(m_Session.isAutoJoinGroup(m.getThread())) {
              //join
              try {
                m_Shell.getServices().getGroupMessagingService(ServiceMediator.NO_WAIT).joinGroup(
                    m_Presence,m.getThread()
                );
                str = m_IO.formatTemplate(PREFIX + "autojoin", "group", m.getThread());
              } catch (Exception ex) {
                Activator.log().error("received()::auto-join", ex);
              }
            }
            if(str == null && !m_Session.isExpert()) {
              attr.add("invite","");
            }
          }
          if(str == null) {
            str = m_IO.formatTemplate(PREFIX + "groupchange", attr);
          }
        } else if (discussion) {
          str = m_IO.formatTemplate(PREFIX + "discussion", attr);
        } else if (mail) {
          str = m_IO.formatTemplate(PREFIX + "mail", attr);
        } else {
          str = m_IO.formatTemplate(PREFIX + "notification", attr);
        }
      } else if (InteractiveMessageTypes.GROUPCHAT.equals(m.getType())) {
        str = m_IO.formatTemplate(PREFIX + "group", attr);
      } else if (InteractiveMessageTypes.QUESTION.equals(m.getType())) {
        str = m_IO.formatTemplate(PREFIX + "question", attr);
      } else if (InteractiveMessageTypes.OFFLINE.equals(m.getType())) {
        attr.add("timedate", c_OfflineFormat.format(Long.parseLong(m.getThread())));
        str = m_IO.formatTemplate(PREFIX + "offline", attr);
      } else {
        str = m_IO.formatTemplate(PREFIX + "received", attr);
      }
      if (!notification) {
        m_MessagingHistory.addIncomingMessage(m, ei, rt, m_IO.getLocale(), str);
        if(!group) {
          //retain last sender
          m_Shell.setEnvironmentVariable("profile.lastuser",m.getFrom());
        }
      }
      m_IO.printEvent(str);
      return true;
    } catch (IOException ex) {
      Activator.log().error(c_LogMarker, "received()", ex);
      return false;
    }
  }//received

  public boolean received(Request r) {
    //this will probably be interesting when adding services?
    return false;
  }//received

  public boolean received(Response r) {
    return false;
  }//received

  public void confirmed(EventInfo ei, long timestamp) {
    try {
      m_MessagingHistory.confirmOutgoingMessage(ei.getIdentifier());
      TemplateAttributes attr = m_IO.leaseTemplateAttributes();
      attr.add("msgid", ei.getIdentifier());
      attr.add("addressee", getFormattedAgent(ei.getTo()));
      attr.add("time", MessageConversion.TIME_FORMAT.format(new Date(timestamp)));
      //timestamp?
      String str = m_IO.formatTemplate(PREFIX + "confirmation", attr);
      m_IO.printEvent(str);
    } catch (IOException ex) {
      Activator.log().error(c_LogMarker, "confirmed()", ex);
    }
  }//confirmed

  public void failedDelivery(Message m) {
    try {
      TemplateAttributes attr = m_IO.leaseTemplateAttributes();
      AgentIdentifier to = m.getTo();
      attr.add("to", getFormattedAgent(to));
      String str = m_IO.formatTemplate(PREFIX + "failed", attr);
      m_IO.printEvent(str);
    } catch (IOException ex) {
      Activator.log().error(c_LogMarker, "failedDelivery()", ex);
    }
  }//failedDelivery

  public void startedMessage(AgentIdentifier from, String msgid) {
    m_Session.addMessaging(from, msgid);
  }//startedMessage

  public void stoppedMessage(AgentIdentifier from, String msgid) {
    m_Session.removeMessaging(from, msgid);
  }//stoppedMessage

  private String getFormattedAgent(AgentIdentifier aid)
      throws IOException {
    //resolve services
    if (aid.getIdentifier().startsWith("net.coalevo.")) {
      //need to resolve a service
      return resolveFormattedServicename(aid);
    }
    TemplateAttributes attr = m_IO.leaseTemplateAttributes();
    attr.add("aid", aid.getIdentifier());
    attr.add("name", resolveFormattedNickname(aid));
    if (!aid.isLocal()) {
      attr.add("remote", "");
    }
    return m_IO.formatTemplate("agent", attr);
  }//getFormattedAgent

  private String resolveFormattedNickname(AgentIdentifier aid) {
    if (m_SecurityService == null) {
      return resolveNickname(aid);
    }
    return RoleUtility.getUserWithPriorityRoleStyle(
        resolveNickname(aid),
        m_SecurityService.getAgentRolesByName(m_Agent, aid));
  }//resolveFormattedNickname

  private String resolveFormattedServicename(AgentIdentifier aid)
      throws IOException {
    String snick = m_IO.formatTemplate(String.format("services_%s", aid.getName()));
    if (m_SecurityService == null) {
      return snick;
    }
    return RoleUtility.getUserWithPriorityRoleStyle(
        snick,
        m_SecurityService.getAgentRolesByName(m_Agent, aid));
  }//resolveFormattedServicename

  private String resolveNickname(AgentIdentifier aid) {
    if (aid.isLocal() || m_UserdataService == null) {
      try {
        return m_UserdataService.getNickname(m_Agent, aid);
      } catch (Exception ex) {
        return aid.getName();
      }
    } else {
      return aid.getName();
    }
  }//resolveNickname

  private static String PREFIX = "MessagingServiceListener_";
  private static final Locale LANG_MACHINE = new Locale("machine");

}//class MessagingServiceListenerImpl
