/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.messaging;

import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.wimpi.telnetd.io.BasicTerminalIO;
import net.wimpi.telnetd.io.TemplateAttributes;

import java.util.ListIterator;

import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * Provides a command for browsing the messaging history.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class BrowseHistory
    extends MessagingCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(BrowseHistory.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell) throws CommandException {
    prepare(shell);
    try {
      navigateEntries();
    } catch (Exception ex) {
      log.error(c_LogMarker,"BrowseHistory::run()",ex);    
    }
  }//run

  protected void navigateEntries() throws Exception {
    int size = m_MessagingHistory.size();
    if(size == 0) {
      m_IO.printTemplate(PREFIX + "nomessages");
      return;
    }
    ListIterator<String> iter = m_MessagingHistory.iterateMessages();
    
    boolean done = false;
    boolean forward = true;
    String msg = iter.next();

    char fwd = m_IO.formatTemplate("forward").charAt(0);
    char bwd = m_IO.formatTemplate("backward").charAt(0);
    char stop = m_IO.formatTemplate("stop").charAt(0);
    char next = ' '; //always space

    BasicTerminalIO bio = m_IO.getTerminalIO();
    boolean skipprompt = false;
    //display first link
    m_IO.println(msg);

    do {
      if (!skipprompt) {
        //print prompt
        TemplateAttributes attr = m_IO.leaseTemplateAttributes();
        attr.add("numentries", Integer.toString(size));
        if (forward) {
          attr.add("numremain", Integer.toString(iter.nextIndex()));
          attr.add("forward", "");
        } else {
          attr.add("numremain", Integer.toString(iter.nextIndex() + 1));
          attr.add("backward", "");
        }
        m_IO.printTemplate(PREFIX + "navprompt", attr);
      }
      char ch = (char) m_IO.read();
      if (ch == fwd) {
        if (!forward) {
          iter.next();
          forward = true;
        }
      } else if (ch == bwd) {
        if (forward) {
          iter.previous();
          forward = false;
        }
      } else if (ch == stop) {
        done = true;
        continue;
      } else if (ch != next) {
        bio.bell();
        skipprompt = true;
        continue;//do nothing
      }
      if (forward && iter.hasNext()) {
        msg = iter.next();
        skipprompt = false;
        m_IO.println(msg);
        continue;
      }
      if (!forward && iter.hasPrevious()) {
        msg = iter.previous();
        skipprompt = false;
        //print link
        m_IO.println(msg);
        continue;
      }
      done = true;
    } while (!done);
    m_IO.println();
  }//navigateEntries

  private static final String PREFIX = TS_PREFIX + "BrowseHistory_";

}//class BrowseHistory
