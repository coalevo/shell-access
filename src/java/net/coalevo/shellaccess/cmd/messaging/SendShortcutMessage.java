/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.messaging;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.shellaccess.impl.CommandShell;
import net.coalevo.shellaccess.cmd.config.ConfigurationCommand;
import net.coalevo.shellaccess.model.Shortcuts;
import net.wimpi.telnetd.io.TemplateAttributes;

import java.io.IOException;

/**
 * Overrides {@link SendInteractiveMessage} to allow shortcut messaging using
 * numbers.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class SendShortcutMessage
    extends SendInteractiveMessage {

   public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  protected AgentIdentifier getAddressee() throws IOException {
    //1. Get actual key, casting is a hack, but then....
    StringBuilder sb = new StringBuilder();
    sb.append((char)((CommandShell)m_Shell).getCommandKey());
    m_IO.printTemplate(PREFIX + "promptshortcut","num",sb.toString());
    //0-9 = 48-57 in ASCII
    int i2 = m_IO.read();
    if (i2 >= 48 && i2 <= 57) {
      sb.append((char)i2);
       m_IO.print(Character.toString((char)i2));
    } else {
      m_IO.printTemplate(PREFIX + "aborted");
      return null;    
    }
    int shortcut = Integer.parseInt(sb.toString());

    Object o =  m_Shell.getEnvironmentVariable(ConfigurationCommand.KEY_SHORTCUTS);
    if(o == null) {
      //note and go
      m_IO.printTemplate(PREFIX + "noshortcuts");
      return null;
    }
    Shortcuts shortcuts = (Shortcuts)o;
    o = shortcuts.get(shortcut);

    if(o==null) {
      //note and go
      m_IO.printTemplate(PREFIX + "notshortcut");
      return null;
    }
    String adr = o.toString();
    if(adr.startsWith("#")) {
      m_GroupName = adr.substring(1);
      TemplateAttributes attr = m_IO.leaseTemplateAttributes();
      attr.add("to", m_GroupName);
      m_IO.printTemplate(PREFIX + "groupaddressee", attr);
      return null;
    }
    AgentIdentifier addressee = getAgentIdentifier(o.toString());
    if(addressee != null) {
      TemplateAttributes attr = m_IO.leaseTemplateAttributes();
      attr.add("to", getFormattedAgent(addressee));
      m_IO.printTemplate(PREFIX + "addressee", attr);
    }
    return addressee;
  }//getAddressee

  private static final String PREFIX = TS_PREFIX + "SendShortcutMessage_";

}//class SendShortcutMessage
