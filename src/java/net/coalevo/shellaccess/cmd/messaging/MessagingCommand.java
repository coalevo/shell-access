/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.messaging;

import net.coalevo.foundation.model.Session;
import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.messaging.model.MessagingServiceListener;
import net.coalevo.messaging.service.MessagingService;
import net.coalevo.messaging.service.GroupMessagingService;
import net.coalevo.presence.model.Presence;
import net.coalevo.presence.model.PresenceConstants;
import net.coalevo.presence.service.PresenceService;
import net.coalevo.shellaccess.cmd.presence.PresenceCommand;
import net.coalevo.shellaccess.cmd.presence.PresenceInfo;
import net.coalevo.shellaccess.cmd.BaseCommand;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.Services;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * Abstract base class for messaging related commands.
 * <p/>
 * Provides the functionality common to the service access.
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public abstract class MessagingCommand
    extends BaseCommand {


  private static Marker c_LogMarker = MarkerFactory.getMarker(MessagingCommand.class.getName());

  protected MessagingService m_MessagingService;
  protected GroupMessagingService m_GroupMessagingService;
  protected Presence m_Presence;
  protected PresenceInfo m_PresenceInfo;
  protected MessagingServiceListener m_MessagingServiceListener;
  protected MessagingHistory m_MessagingHistory;


  public boolean prepare(BasicShell shell) {
    super.prepare(shell);
    m_MessagingService = shell.getServices().getMessagingService(Services.NO_WAIT);
    if (m_MessagingService == null) {
      try {
        m_IO.printTemplate(PREFIX + "serviceunavailable");
      } catch (Exception ex) {
        log.error(c_LogMarker,"prepare()", ex);
      }
      return false;
    }
    m_GroupMessagingService = shell.getServices().getGroupMessagingService(Services.NO_WAIT);
    if (m_GroupMessagingService == null) {
      try {
        m_IO.printTemplate(PREFIX + "serviceunavailable");
      } catch (Exception ex) {
        log.error(c_LogMarker,"prepare()", ex);
      }
      return false;
    }
    //user coalevo session
    Session s = shell.getSession().getUserAgent().getSession();
    if (s != null && s.isValid()) {
      m_Presence = (Presence) s.getAttribute(PresenceConstants.PRESENCE_SESSION_ATTRIBUTE);
    } else if (shell.isEnviromentVariableSet(PresenceCommand.ENVKEY_PRESENCE)) {
      m_Presence = (Presence) shell.getEnvironmentVariable(PresenceCommand.ENVKEY_PRESENCE);
    } else {
      try {
        m_IO.printTemplate(PREFIX + "presenceunavailable");
      } catch (Exception ex) {
        log.error(c_LogMarker,"prepare()", ex);
      }
      return false;
    }
    if (!m_Presence.isPresent()) {
      try {
        m_IO.printTemplate(PREFIX + "notpresent");
      } catch (Exception ex) {
        log.error(c_LogMarker,"prepare()", ex);
      }
      return false;
    }
    m_MessagingServiceListener = (MessagingServiceListener) shell.getEnvironmentVariable(
        MessagingCommand.ENVKEY_LISTENER);
    m_MessagingHistory = (MessagingHistory) shell.getEnvironmentVariable(
        MessagingCommand.ENVKEY_HISTORY);
    m_PresenceInfo = (PresenceInfo) shell.getEnvironmentVariable(
        PresenceCommand.ENVKEY_PRESENCEINFO);
    return true;
  }//prepare

  /**
   * Resets the instance members.
   * Be sure you call <tt>super.reset();</tt> in your subclass
   * if you override this method.
   */
  public void reset() {
    super.reset();
    m_MessagingService = null;
    m_Presence = null;
    m_MessagingServiceListener = null;
    m_MessagingHistory = null;
    m_PresenceInfo = null;
  }//reset

  protected boolean isAdvertisedAvailable(AgentIdentifier aid) {
    PresenceService ps = m_Shell.getServices().getPresenceService(Services.NO_WAIT);
    return ps != null && (ps.isAdvertisedAvailable(m_Agent, aid));
  }//checkAdvertisedAvailable

  public static final String TS_PREFIX = "commands_net.coalevo.shellaccess.cmd.messaging.";
  public static final String PREFIX = TS_PREFIX + "MessagingCommand_";
  public static final String ENVKEY_LISTENER = MessagingServiceListener.class.getName();
  public static final String ENVKEY_HISTORY = MessagingHistory.class.getName();

}//class MessagingCommand
