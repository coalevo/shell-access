/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.messaging;

import net.coalevo.messaging.model.MessagingException;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.shellaccess.validators.LanguageInputValidator;
import net.coalevo.shellaccess.validators.StringValidator;

import java.util.Locale;

import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * Provides a command for creating a temporary group.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class CreateTemporaryGroup
    extends InviteToGroup {

  private static Marker c_LogMarker = MarkerFactory.getMarker(CreateTemporaryGroup.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell) throws CommandException {
    prepare(shell);
    try {
      String name;
      boolean done = false;
      m_PromptWrap = false;
      do {
        name = prompt(PREFIX + "name", new StringValidator(3,m_IO.formatTemplate(PREFIX + "gnamelength")));
        if (m_GroupMessagingService.existsGroup(m_Agent, name)) {
          if (!promptDecision(PREFIX + "changename")) {
            m_IO.printTemplate(PREFIX + "aborted");
            return;
          }
        } else {
          done = true;
        }
      } while (!done);
      Locale l = new Locale(prompt(PREFIX + "language", new LanguageInputValidator(
          m_IO.formatTemplate("inputerror_languagevalidator")
      )));
      String tags = prompt(PREFIX + "tags");
      boolean inviteonly = promptDecision(PREFIX + "inviteonly");
      boolean invitemod = false;
      if (inviteonly) {
        invitemod = promptDecision(PREFIX + "invitemoderated");
      }
      if (isUserExpert() || promptDecision(PREFIX + "doit")) {
        try {
          m_GroupMessagingService.createTemporaryGroup(
              m_Agent,
              name,
              l,
              "",
              "",
              tags,
              inviteonly,
              invitemod
          );
          m_IO.printTemplate(PREFIX + "confirm");
        } catch (SecurityException ex) {
          m_IO.printTemplate(PREFIX + "notallowed");
          return;
        } catch (MessagingException mex) {
          m_IO.printTemplate(PREFIX + "failed");
          return;
        }
      } else {
        m_IO.printTemplate(PREFIX + "aborted");
        return;
      }
      if(isUserExpert() || promptDecision(PREFIX + "invite")) {
        doInvites(name);
      }
    } catch (Exception ex) {
      log.error(c_LogMarker,"run()", ex);
    }
  }//run

  private static final String PREFIX = TS_PREFIX + "CreateTemporaryGroup_";

}//class CreateTemporaryGroup
