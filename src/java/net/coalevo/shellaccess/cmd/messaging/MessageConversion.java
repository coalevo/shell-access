/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.messaging;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.messaging.model.InteractiveMessage;
import net.coalevo.messaging.model.InteractiveMessageTypes;
import net.coalevo.messaging.model.EventInfo;
import net.coalevo.text.util.DateFormatter;
import net.coalevo.shellaccess.impl.Formatter;
import net.wimpi.telnetd.io.TemplateAttributes;
import net.wimpi.telnetd.io.TerminalIO;
import org.apache.lucene.document.DateTools;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;

import java.util.Date;
import java.util.Locale;

/**
 * Utility class that provides conversions between messages
 * and index documents.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class MessageConversion {

  public static final String getMessage(Document d) {
    return d.get(FIELD_MSG);
  }//getMessage

  public static final Document toDocument(boolean incoming,
                                          long time,
                                          InteractiveMessage m,
                                          EventInfo ei,
                                          Locale l,
                                          String fmsg) {
    Document d = new Document();

    String temp = "";
    //incoming
    temp = (incoming) ? "inc" : "out";
    d.add(
        new Field(FIELD_DIR, temp, Field.Store.NO, Field.Index.UN_TOKENIZED)
    );
    //from
    AgentIdentifier aid = ei.getFrom();
    if (aid.isLocal()) {
      temp = aid.getName();
    } else {
      temp = aid.getIdentifier();
    }
    d.add(
        new Field(FIELD_FROM, temp, Field.Store.NO, Field.Index.TOKENIZED)
    );
    //to
    if(m.getType().equals(InteractiveMessageTypes.GROUPCHAT)) {
      temp = String.format("#%s",m.getThread());
    } else {
      aid = ei.getTo();
      if (aid.isLocal()) {
        temp = aid.getName();
      } else {
        temp = aid.getIdentifier();
      }
    }
    d.add(
        new Field(FIELD_TO, temp, Field.Store.NO, Field.Index.TOKENIZED)
    );
    //subject
    if (m.hasSubject()) {
      if (m.hasSubject(l)) {
        temp = m.getSubject(l);
      } else {
        temp = m.getSubject();
      }
      d.add(
          new Field(FIELD_SUBJECT, temp, Field.Store.NO, Field.Index.TOKENIZED)
      );
    }
    //body
    if (m.hasBody()) {
      if (m.hasBody(l)) {
        temp = m.getBody(l);
      } else {
        temp = m.getBody();
      }
      d.add(
          new Field(FIELD_BODY, temp, Field.Store.NO, Field.Index.TOKENIZED)
      );
    }
    //thread
    if (m.hasThread()) {
      temp = m.getThread();
      d.add(
          new Field(FIELD_THREAD, temp, Field.Store.NO, Field.Index.TOKENIZED)
      );
    }
    //time
    temp = DateTools.timeToString(time, DateTools.Resolution.SECOND);
    d.add(
        new Field(FIELD_TIME, temp, Field.Store.NO, Field.Index.UN_TOKENIZED)
    );

    //formatted message
    d.add(
        new Field(FIELD_MSG, fmsg, Field.Store.YES, Field.Index.UN_TOKENIZED)
    );
    return d;
  }//toDocument


  public static TemplateAttributes toAttributes(
      long time,
      InteractiveMessage m,
      String ffrom,
      String fto,
      Locale l,
      TemplateAttributes attr) {

    attr.add(FIELD_FROM, ffrom);
    attr.add(FIELD_TO, fto);
    if (m.hasSubjects()) {
      if (m.hasSubject(l)) {
        attr.add(FIELD_SUBJECT, convertLineWraps(m.getSubject(l)));
      } else {
        attr.add(FIELD_SUBJECT, convertLineWraps(m.getSubject()));
      }
    }
    if (m.hasBodies()) {
      //System.err.println("BODY 1: " + m.getBody());
      //System.err.println("BODY 2: " + m.getBody(l));
      String bd;
      if (m.hasBody(l)) {
        bd = m.getBody(l);
      } else {
        bd= m.getBody();
      }
      if(m.hasFormattingHint()) {
        bd = Formatter.format(m.getFormattingHint(), bd);
      }
      attr.add(FIELD_BODY, convertLineWraps(bd));
    }
    if (m.hasThread()) {
      attr.add(FIELD_THREAD, m.getThread());
    }
    if (m.isBroadcast()) {
      attr.add(FIELD_BROADCAST, "true");
    }
    attr.add(FIELD_TIME, TIME_FORMAT.format(new Date(time)));
    return attr;
  }//toAttributes

  private static String convertLineWraps(String cnt) {
    return cnt.replaceAll("\n", TerminalIO.CRLF);
  }//convertLineWraps

  private static final String FIELD_FROM = "from";
  private static final String FIELD_TO = "to";
  private static final String FIELD_SUBJECT = "subject";
  public static final String FIELD_BODY = "body";
  private static final String FIELD_DIR = "dir";
  private static final String FIELD_THREAD = "thread";
  private static final String FIELD_TIME = "time";
  public static final String FIELD_MSG = "msg";
  private static final String FIELD_BROADCAST = "broadcast";
  public static final DateFormatter TIME_FORMAT = new DateFormatter("HH:mm:ss dd/MM/yy",10);

}//class MessageConversion
