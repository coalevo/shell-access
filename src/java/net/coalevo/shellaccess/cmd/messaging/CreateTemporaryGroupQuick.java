/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.messaging;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.messaging.model.MessagingException;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.util.Locale;

/**
 * Provides a command for creating a temporary group.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class CreateTemporaryGroupQuick
    extends MessagingCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(CreateTemporaryGroupQuick.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell) throws CommandException {
    prepare(shell);
    try {
      String str;
      m_PromptWrap = false;

      str = prompt(PREFIX + "gstring");
      if(str == null || str.length() == 0) {
        m_IO.printTemplate(PREFIX + "aborted");
        return;
      }
      String[] split = str.split(":");
      if (split.length != 2) {
        m_IO.printTemplate(PREFIX + "syntax");
        return;
      }
      String gname = split[0];
      if(gname.length() < 3) {
        m_IO.printTemplate(PREFIX + "namelength");
        return;
      }
      if (m_GroupMessagingService.existsGroup(m_Agent, gname)) {
        m_IO.printTemplate(PREFIX + "groupexists");
        return;
      }
      try {
        //1. Create group
        Locale l = m_IO.getLocale();
        m_GroupMessagingService.createTemporaryGroup(
            m_Agent,
            gname,
            l,
            "",
            "",
            "",
            true,
            true
        );

        //2. Invite people
        split = split[1].split(",");
        for (String usr : split) {
          AgentIdentifier aid = getAgentIdentifier(usr);
          if (aid == null) {
            m_IO.printTemplate(PREFIX + "nosuchagent", "aid",usr);
          } else {
            m_GroupMessagingService.inviteToGroup(m_Agent, gname, aid);
          }
        }
        m_IO.printTemplate(PREFIX + "confirm");
      } catch (SecurityException ex) {
        m_IO.printTemplate(PREFIX + "notallowed");
        return;
      } catch (MessagingException mex) {
        m_IO.printTemplate(PREFIX + "failed");
        return;
      }
    } catch (Exception ex) {
      log.error(c_LogMarker, "run()", ex);
    }
  }//run

  private static final String PREFIX = TS_PREFIX + "CreateTemporaryGroupQuick_";

}//class CreateTemporaryGroupQuick