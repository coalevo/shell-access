/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.messaging;

import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.wimpi.telnetd.io.TemplateAttributes;

import java.util.Set;

import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * Provides a command for listing all groups sorted by permanent or
 * temporary and name.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class ListGroups
    extends MessagingCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(ListGroups.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell) throws CommandException {
    prepare(shell);
    try {
      Set<String> tempgroups = m_GroupMessagingService.listGroups(m_Agent,false);
      Set<String> permgroups = m_GroupMessagingService.listGroups(m_Agent,true);

      if(tempgroups.isEmpty() && permgroups.isEmpty()) {
        m_IO.printTemplate(PREFIX + "none");
        return;
      }

      TemplateAttributes attr = m_IO.leaseTemplateAttributes();
      //permanent
      if(!permgroups.isEmpty()) {
        for(String gname:permgroups) {
          attr.add("permgroups",gname);
        }
      }
      //temporary
      if(!tempgroups.isEmpty()) {
        for(String gname:tempgroups) {
          attr.add("tempgroups",gname);
        }
      }
      m_IO.printTemplate(PREFIX + "list",attr);

    } catch (Exception ex) {
      log.error(c_LogMarker,"run()",ex);
    }
  }//run

  private static final String PREFIX = TS_PREFIX + "ListGroups_";

}//class ListGroups
