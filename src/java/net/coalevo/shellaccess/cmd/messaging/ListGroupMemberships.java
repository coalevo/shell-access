/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.messaging;

import net.coalevo.shellaccess.cmd.config.ConfigurationCommand;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.shellaccess.model.Shortcuts;
import net.wimpi.telnetd.io.TemplateAttributes;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.util.Set;

/**
 * Provides a command for listing all group invitations.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class ListGroupMemberships
    extends MessagingCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(ListGroupMemberships.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell) throws CommandException {
    prepare(shell);
    try {
      Set<String> groups = m_GroupMessagingService.listGroupMemberships(
          m_Agent, m_AgentIdentifier);
      if (groups.isEmpty()) {
        m_IO.printTemplate(PREFIX + "none");
        return;
      }
      // Prepare shortcuts
      Shortcuts shortcuts;
      Object o = m_Shell.getEnvironmentVariable(ConfigurationCommand.KEY_SHORTCUTS);
      if (o == null) {
        shortcuts = new Shortcuts();
      } else {
        shortcuts = (Shortcuts) o;
      }
      TemplateAttributes attr = m_IO.leaseTemplateAttributes();
      for (String gname : groups) {
        String sc = null;
        String name = "#" + gname;
        if (shortcuts.contains(name)) {
          sc = String.format("%02d", shortcuts.get(name));
        }
        String[] items;
        if (m_GroupMessagingService.isPermanentGroup(m_Agent, gname)) {
          items = new String[]{
              gname,
              name,
              sc,
              ""
          };
        } else {
          items = new String[]{
              gname,
              name,
              sc,
              null
          };
        }
        attr.add("groups.{name,channel,shortcut,permanent}", items);
      }
      m_IO.printTemplate(PREFIX + "list", attr);

    } catch (Exception ex) {
      log.error(c_LogMarker, "run()", ex);
    }
  }//run

  private static final String PREFIX = TS_PREFIX + "ListGroupMemberships_";

}//class ListGroupMemberships
