/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.messaging;

import net.coalevo.shellaccess.model.BaseShellCommandProvider;

/**
 * A {@link net.coalevo.shellaccess.model.ShellCommandProvider}
 * for the messaging commands.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class MessagingCommandProvider
    extends BaseShellCommandProvider {

  public MessagingCommandProvider() {
    super(MessagingCommandProvider.class.getPackage().getName());
    prepare();
  }//constructor

  private void prepare() {
    //Registration
    addCommand("Register", Register.class);
    addCommand("Unregister", Unregister.class);

    //Messaging commands
    addCommand("SendInteractiveMessage", SendInteractiveMessage.class);
    addCommand("ReplyInteractiveMessage", ReplyInteractiveMessage.class);
    addCommand("SendShortcutMessage",SendShortcutMessage.class);
    addCommand("SendBroadcastMessage", SendBroadcastMessage.class);
    addCommand("SendUnicastMessage", SendUnicastMessage.class);
    addCommand("QueryHistory", QueryHistory.class);
    addCommand("BrowseHistory", BrowseHistory.class);
    addCommand("ListStartedMessages", ListStartedMessages.class);
    addCommand("AskSysguide",AskSysguide.class);
    
    //Group messaging commands
    addCommand("ListGroupInvitations", ListGroupInvitations.class);
    addCommand("ListGroupMemberships", ListGroupMemberships.class);
    addCommand("ListJoinableGroups", ListJoinableGroups.class);
    addCommand("JoinGroup", JoinGroup.class);
    addCommand("JoinInvitedGroups", JoinInvitedGroups.class);
    addCommand("LeaveGroup", LeaveGroup.class);
    addCommand("InviteToGroup", InviteToGroup.class);
    addCommand("KickFromGroup", KickFromGroup.class);
    addCommand("CreateTemporaryGroup", CreateTemporaryGroup.class);
    addCommand("CreateTemporaryGroupQuick", CreateTemporaryGroupQuick.class);
    addCommand("CreatePermanentGroup", CreatePermanentGroup.class);
    addCommand("RemovePermanentGroup", RemovePermanentGroup.class);
    addCommand("UpdatePermanentGroup", UpdatePermanentGroup.class);
    addCommand("ListGroups", ListGroups.class);
    addCommand("InspectGroup", InspectGroup.class);
    addCommand("UserInspectGroup", UserInspectGroup.class);

    //addCommand("", .class);
  }//prepare

}//class MessagingCommandProvider
