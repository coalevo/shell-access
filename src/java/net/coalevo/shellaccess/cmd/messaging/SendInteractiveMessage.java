/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.messaging;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.messaging.model.*;
import net.coalevo.presence.model.PresenceProxy;
import net.coalevo.shellaccess.cmd.presence.AvailableUserFilter;
import net.coalevo.shellaccess.cmd.presence.PresenceCommand;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.wimpi.telnetd.io.TemplateAttributes;
import net.wimpi.telnetd.io.toolkit.Editfield;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.IOException;
import java.util.Locale;
import java.util.Set;

/**
 * Sends an {@link InteractiveMessage}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class SendInteractiveMessage
    extends MessagingCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(SendInteractiveMessage.class.getName());

  protected String m_GroupName;

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell) throws CommandException {
    prepare(shell);
    EditableInteractiveMessage em = null;
    AgentIdentifier aid = null;
    try {
      if (!canSendMessage()) {
        m_IO.printTemplate(PREFIX + "unavailable");
        return;
      }
      //addressee
      aid = getAddressee();
      if (m_GroupName != null) {
        try {
          em = m_GroupMessagingService.createGroupMessage(m_Presence, m_GroupName, null);
          String msg = getMessage();
          if (msg != null && msg.length() > 0 && !msg.equals("\n") && !msg.equals("\n\n")) {
            em.setBody(msg);
            boolean confirm = shell.getSession().isGroupConfirmations();
            em.setConfirmationRequired(confirm);
            remPendingOut(em, !confirm); //add before sending CSAB-27
            m_GroupMessagingService.send(m_Presence, m_GroupName, em);
            m_IO.printTemplate(PREFIX + "sent");
            return;
          } else {
            m_IO.printTemplate(PREFIX + "aborted");
            return;
          }
        } catch (NoSuchGroupException ex) {
          m_IO.printTemplate(PREFIX + "groupnonexistant");
          return;
        } catch (MessagingException ex) {
          m_IO.printTemplate(PREFIX + "notmember");
          return;
        }
      }
      if (aid == null) {
        if (this instanceof ReplyInteractiveMessage) {
          m_IO.printTemplate(PREFIX + "noreply");
        } else if (this instanceof AskSysguide || this instanceof SendShortcutMessage) {
          return;
        } else {
          m_IO.printTemplate(PREFIX + "aborted");
        }
      } else {
        if (aid.equals(m_AgentIdentifier)) {
          m_IO.printTemplate(PREFIX + "gonecrazy");
          return;
        }
        if (!isAdvertisedAvailable(aid)) {

          if (!m_PresenceInfo.isPresent(aid)) {
            if (m_Presence.getSubscriptionsTo().contains(aid)
                && promptDecision(PREFIX + "askstoreoffline")) {
              try {
                em = m_MessagingService.create(m_Presence,
                    InteractiveMessageTypes.OFFLINE, null);
                //2. set the addressee
                em.setTo(aid);
                String msg = getMessage();
                if (msg != null && msg.length() > 0 && !msg.equals("\n") && !msg.equals("\n\n")) {
                  em.setBody(msg);
                  if (m_MessagingService.store(m_Presence, em)) {
                    m_IO.printTemplate(PREFIX + "msgstored");
                  } else {
                    m_IO.printTemplate(PREFIX + "msgnotstored");
                  }
                } else {
                  m_IO.printTemplate(PREFIX + "aborted");
                }

              } catch (MessagingException ex) {
                ex.getCause().printStackTrace();
                m_IO.printTemplate(PREFIX + "storefailed");
              }
            } else {
              m_IO.printTemplate(PREFIX + "notpresent", "to", getFormattedAgent(aid));
            }
            return;
          }

          if (!m_PresenceInfo.isAvailable(aid)) {
            PresenceProxy p = m_PresenceInfo.getPresenceFor(aid);
            TemplateAttributes attr = m_IO.leaseTemplateAttributes();
            attr.add("to", getFormattedAgent(aid));
            attr.add("type", m_IO.formatTemplate(
                PresenceCommand.STATUS_PREFIX + p.getStatusType().getIdentifier()
            ));
            attr.add("desc", p.getStatusDescription());
            m_IO.printTemplate(PREFIX + "notavailable", attr);
            return;
          }
        }
        //Warn on temp unavail.
        if (m_PresenceInfo.isTemporaryUnavailable(aid)) {
          PresenceProxy p = m_PresenceInfo.getPresenceFor(aid);
          TemplateAttributes attr = m_IO.leaseTemplateAttributes();
          attr.add("to", getFormattedAgent(aid));
          attr.add("type", m_IO.formatTemplate(
              PresenceCommand.STATUS_PREFIX + p.getStatusType().getIdentifier()
          ));
          attr.add("desc", p.getStatusDescription());
          addSince(attr,p.getLastStatusUpdate());
          m_IO.printTemplate(PREFIX + "tmpunavailable", attr);
        }

        if (this instanceof AskSysguide) {
          em = m_MessagingService.create(m_Presence,
              InteractiveMessageTypes.QUESTION, null);
        } else {
          em = m_MessagingService.create(m_Presence,
              InteractiveMessageTypes.CHAT, null);
        }
        //2. set the addressee
        em.setTo(aid);
        //3. use event notification
        m_MessagingService.started(m_Presence, em);
        //4. request confirmation
        boolean confirm = m_Shell.getSession().isConfirmations();
        em.setConfirmationRequired(confirm);
        //get message
        String msg = getMessage();
        if (msg != null && msg.length() > 0 && !msg.equals("\n") && !msg.equals("\n\n")) {
          em.setBody(msg);
          remPendingOut(em, !confirm); //add before sending CSAB-27
          m_MessagingService.send(m_Presence, em);
          m_IO.printTemplate(PREFIX + "sent");
        } else {
          m_IO.printTemplate(PREFIX + "aborted");
        }
      }
      //m_IO.println(m_MessagingHistory.toString());
    } catch (MessagingException mex) {
      log.error("run()", mex);
      try {
        TemplateAttributes attr = m_IO.leaseTemplateAttributes();
        attr.add("to", (aid.isLocal()) ? aid.getName() : aid.getIdentifier());
        m_IO.printTemplate(PREFIX + "cannot", attr);
      } catch (Exception ex) {
        log.error(c_LogMarker, "run()", ex);
      }
    } catch (Exception ex) {
      log.error(c_LogMarker, "run()", ex);
    } finally {
      //Make sure that the stop event is sent. The service will
      //take care that it is not sent twice anyway.
      if (em != null) {
        try {
          m_MessagingService.stopped(m_Presence, em);
        } catch (Exception exc) {
          log.error(c_LogMarker, "run()", exc);
        }
      }
    }
  }//run

  protected boolean canSendMessage() {
    return m_Presence.isAvailable();
  }//canSendMessage

  protected AgentIdentifier getAddressee() throws IOException {
    //1. Check group first
    String defadd = null;
    m_GroupName = m_MessagingHistory.getLastGroupAddressee();
    TemplateAttributes attr = null;
    AgentIdentifier lastadd = null;
    if (m_GroupName != null) {
      defadd = "#".concat(m_GroupName);
      attr = m_IO.leaseTemplateAttributes();
      attr.add("default", defadd);
    } else {
      //2. Check agent
      lastadd = m_MessagingHistory.getLastAddressee();
      if (lastadd != null) {
        defadd = resolveNickname(lastadd);
        attr = m_IO.leaseTemplateAttributes();
        attr.add("default", defadd);
      }
    }
    m_IO.printTemplate(PREFIX + "addresseelabel", attr);

    Editfield ef = new Editfield(m_IO.getTerminalIO(), "aid", 40, 255);
    ef.setInputFilter(new AvailableUserFilter(ef, m_PresenceInfo, getGroups(), "#"));
    ef.setIgnoreDelete(true);
    ef.run();
    String str = ef.getValue();
    if (str.length() == 0) {
      if (defadd != null) {
        str = defadd;
      }
    }
    if (str.startsWith("#")) {
      //group
      m_GroupName = str.substring(1);
      return null;
    } else {
      m_GroupName = null;
    }
    if((str.length() == 0  || str== defadd) && lastadd != null) {
      return lastadd;
    }
    return getAgentIdentifier(str);
  }//getAddressee

  protected String getMessage() throws IOException {
    return simpleEdit(m_IO.formatTemplate(PREFIX + "xflag"));
  }//getMessage

  protected void remPendingOut(InteractiveMessage m, boolean index)
      throws IOException {
    //System.out.println("Remembering Pending out");
    //add pending to history
    TemplateAttributes attr = m_IO.leaseTemplateAttributes();
    long st = System.currentTimeMillis();
    Locale l = m_IO.getLocale();
    String to;
    String fmsg = "";
    if (m.getType().equals(InteractiveMessageTypes.GROUPCHAT)) {
      to = String.format("#%s", m.getThread());
      fmsg = m_IO.formatTemplate("MessagingServiceListener_sentgroup",
          MessageConversion.toAttributes(
              st,
              m,
              getFormattedAgent(m.getFrom()),
              to,
              l,
              attr
          )
      );
    } else if (m.getType().equals(InteractiveMessageTypes.QUESTION)) {
      to = getFormattedAgent(m.getTo());
      fmsg = m_IO.formatTemplate("MessagingServiceListener_sentquestion",
          MessageConversion.toAttributes(
              st,
              m,
              getFormattedAgent(m.getFrom()),
              to,
              l,
              attr
          )
      );
    } else {
      to = getFormattedAgent(m.getTo());
      fmsg = m_IO.formatTemplate("MessagingServiceListener_sent",
          MessageConversion.toAttributes(
              st,
              m,
              getFormattedAgent(m.getFrom()),
              to,
              l,
              attr
          )
      );
    }
    m_MessagingHistory.addOutgoingMessage(m, st, l, fmsg, index);
  }//remPendingOut

  protected Set<String> getGroups() {
    try {
      return m_GroupMessagingService.listGroupMemberships(m_Agent, m_AgentIdentifier);
    } catch (MessagingException ex) {
      log.error(c_LogMarker, "getGroups()", ex);
    }
    return null;
  }//getGroups

  public void reset() {
    super.reset();
    m_GroupName = null;
  }//reset

  protected void addSince(TemplateAttributes attr, long since) {
    final long now = System.currentTimeMillis();
    long elapsedSeconds = (now - since) / 1000;
    double minutes = ((double)elapsedSeconds) / (double)60;
    attr.add("since", String.format("%.0f",minutes));
  }//addSince

  private static final String PREFIX = TS_PREFIX + "SendInteractiveMessage_";

}//class SendInteractiveMessage
