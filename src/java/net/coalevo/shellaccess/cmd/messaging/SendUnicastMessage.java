/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.messaging;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.messaging.model.EditableInteractiveMessage;
import net.coalevo.messaging.model.InteractiveMessageTypes;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;

import java.io.IOException;

import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * This class provides a command that allows to send a unicast message.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class SendUnicastMessage
    extends MessagingCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(SendUnicastMessage.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell) throws CommandException {
    if (!prepare(shell)) {
      return;
    }
    try {
      //1. Ok we need to get a user
      m_PromptWrap = false;
      AgentIdentifier aid = promptAgent(PREFIX + "agent");
      if(aid == null) {
        m_IO.printTemplate(PREFIX + "nosuchagent");
        return;
      }
      String reason = simpleEdit(null);
      if(reason==null || reason.length()==0) {
        m_IO.printTemplate(PREFIX + "aborted");
        return;
      }
      EditableInteractiveMessage edm =
          m_MessagingService.create(m_Presence, InteractiveMessageTypes.CHAT, null);
      edm.setTo(aid);
      edm.setBody(reason);
      edm.setConfirmationRequired(false);
      m_MessagingService.unicast(m_Presence, edm);
      m_IO.printTemplate(PREFIX + "confirm");

    } catch (Exception ex) {
      try {
        m_IO.printTemplate(PREFIX + "failed");
      } catch (IOException ioex) {
        log.error(c_LogMarker,"run(BaseShell)",ioex);
      }
      log.error(c_LogMarker,"run(BaseShell)", ex);
    }
  }//run

  private static final String PREFIX = TS_PREFIX + "SendUnicastMessage_";

}//class SendUnicastMessage
