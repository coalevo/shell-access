/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.messaging;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.util.crypto.DigestUtil;
import net.coalevo.shellaccess.impl.Activator;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.SimpleAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.IndexModifier;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;

/**
 * Implements a bounded in-memory index based on Lucene.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class BoundedDiskIndex implements BoundedIndex {

  private static Logger log = LoggerFactory.getLogger(BoundedDiskIndex.class);
  private Directory m_Index;
  private Analyzer m_Analyzer;
  private Sort m_Sort;
  private IndexModifier m_IndexModifier;

  private int m_MaxDocuments;
  private String m_Path;

  public BoundedDiskIndex(AgentIdentifier owner, int maxdocs) {
    m_MaxDocuments = maxdocs;

    m_Analyzer = new SimpleAnalyzer();
    m_Sort = new Sort(new SortField(FIELD_NUM, SortField.INT));
    prepareIndex(owner);
  }//constructor

  private void prepareIndex(AgentIdentifier owner) {
    try {
      String str = DigestUtil.digestToHexString(owner.getIdentifier().getBytes("utf-8"));
      //prepare index file path
      final File f = Activator.getMessageHistoryDir(str);
      m_Path = f.getAbsolutePath();
      boolean create = !f.exists();
      log.debug("DiskIndex::" + owner.getIdentifier() + "::" + m_Path + ((create) ? "::create" : "::exists"));
      m_Index = FSDirectory.getDirectory(f, create);
      try {
        m_IndexModifier = new IndexModifier(m_Index, m_Analyzer, create);
      } catch (IOException ex) {
        String lock = ex.getMessage();
        int idx = lock.indexOf("Lock@");
        if (idx > 0) {
          lock = lock.substring(idx + 5);
          //delete Lock file
          boolean del = new File(lock).delete();
          log.debug("Deleted stale lock file::" + lock + "::" + del);
          m_IndexModifier = new IndexModifier(m_Index, m_Analyzer, create);
        }
      }
    } catch (IOException ex) {
      log.error("BoundedDiskIndex()", ex);
    }
  }

  public void add(Document d) {
    try {
      //add index num field
      d.add(new Field(FIELD_NUM, Long.toString(System.currentTimeMillis()),
          Field.Store.NO, Field.Index.UN_TOKENIZED));

      synchronized (m_IndexModifier) {
        m_IndexModifier.addDocument(d);
        m_IndexModifier.flush();
        if (m_IndexModifier.docCount() > m_MaxDocuments) {
          m_IndexModifier.deleteDocument(0);
        }
        m_IndexModifier.optimize();
      }
    } catch (Exception ex) {
      log.error("add()", ex);
    }
  }//add

  public int size() {
    return m_IndexModifier.docCount();
  }//size

  public Sort getDefaultSort() {
    return m_Sort;
  }//getDefaultSort

  public IndexSearcher getSearcher() {
    try {
      return new IndexSearcher(m_Index);
    } catch (Exception ex) {
      log.error("getSearcher()", ex);
      return null;
    }
  }//getSearcher

  public IndexReader getReader() {
    try {
      return IndexReader.open(m_Index);
    } catch (Exception ex) {
      log.error("getReader()");
      return null;
    }
  }//getReader

  public Analyzer getAnalyzer() {
    return m_Analyzer;
  }//getAnalyzer

  public boolean delete() throws IOException {
    return deleteDir(new File(m_Path));
  }//delete

  public void close() {
    try {
      m_IndexModifier.close();
    } catch (Exception ex) {
      log.error("close()");
    }
  }//close

  private static boolean deleteDir(File dir) {
    if (dir.isDirectory()) {
      String[] children = dir.list();
      for (int i = 0; i < children.length; i++) {
        boolean success = deleteDir(new File(dir, children[i]));
        if (!success) {
          return false;
        }
      }
    }

    // The directory is now empty so delete it
    return dir.delete();
  }//deleteDir

}//class BoundedDiskIndex