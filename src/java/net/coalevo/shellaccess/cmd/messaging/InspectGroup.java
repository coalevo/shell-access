/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.messaging;

import net.coalevo.messaging.model.Group;
import net.coalevo.messaging.model.NoSuchGroupException;
import net.coalevo.text.util.TagUtility;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.shellaccess.impl.Formatter;
import net.coalevo.foundation.model.AgentIdentifier;
import net.wimpi.telnetd.io.TemplateAttributes;

import java.util.Date;
import java.util.Set;
import java.io.IOException;

import org.slf4j.MarkerFactory;
import org.slf4j.Marker;

/**
 * Provides a command for inspecting an existing group.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class InspectGroup
    extends MessagingCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(InspectGroup.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell) throws CommandException {
    prepare(shell);
    try {
      String name;
      Group g;
      m_PromptWrap = false;                                    //dont wrap first
      name = prompt(PREFIX + "name");
      if (name == null || name.length() == 0) {
        m_IO.printTemplate(PREFIX + "aborted");
        return;
      }
      try {
        g = m_GroupMessagingService.getGroup(m_Agent, name);
      } catch (SecurityException ex) {
        m_IO.printTemplate(PREFIX + "notallowed");
        return;
      } catch (NoSuchGroupException nsex) {
        m_IO.printTemplate(PREFIX + "nosuchgroup");
        return;
      }
      displayGroup(g);
    } catch (Exception ex) {
      log.error(c_LogMarker,"run()", ex);
    }
  }//run

  protected void displayGroup(Group g) throws IOException {
    String infoformat = g.getInfoFormat();
    StringBuilder sbuf = new StringBuilder();
    TemplateAttributes attr = m_IO.leaseTemplateAttributes();

    //Descriptor
    attr.add("name",g.getName());
    attr.add("moderator", getFormattedAgent(g.getModerator()));
    attr.add("language", g.getLanguage().getDisplayLanguage(m_IO.getLocale()));
    attr.add("infoformat",infoformat);
    attr.add("created", getStandardDateFormat().format(new Date(g.getCreated())));
    attr.add("tags", TagUtility.fromSet(g.getTags()));

    if (g.isPermanent()) {
      attr.add("permanent", "");
    }
    if (g.isInviteOnly()) {
      attr.add("inviteonly", "");
    }
    if (g.isInviteModerated()) {
      attr.add("invitemod", "");
    }
    sbuf.append(m_IO.formatTemplate(PREFIX + "descriptor", attr));

    //Info
    String info = g.getInfo();
    if (info != null && info.length() > 0) {
      sbuf.append(Formatter.format(infoformat,info));
    }

    //Members
    attr = m_IO.leaseTemplateAttributes();
    Set<AgentIdentifier> presmem = g.getPresentMembers();
    for(AgentIdentifier aid:presmem){
      attr.add("presentmembers",getFormattedAgent(aid));
    }

    if(g.isPermanent()) {
      Set<AgentIdentifier> mem = g.getMembers();
      for(AgentIdentifier aid:mem){
        if(!presmem.contains(aid)) {
          attr.add("members",getFormattedAgent(aid));
        }
      }
    }
    sbuf.append(m_IO.formatTemplate(PREFIX + "members", attr));

    m_IO.page(sbuf.toString());
    m_IO.println();

  }//displayGroup

  protected static final String PREFIX = TS_PREFIX + "InspectGroup_";

}//class InspectGroup
