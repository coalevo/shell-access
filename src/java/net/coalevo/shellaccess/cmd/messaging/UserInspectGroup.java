/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.messaging;

import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.shellaccess.filters.StringListCompletionFilter;
import net.coalevo.messaging.model.Group;
import net.coalevo.messaging.model.NoSuchGroupException;
import java.util.Set;
import java.util.TreeSet;

import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * Provides a command for inspecting an existing group.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class UserInspectGroup
    extends InspectGroup {

  private static Marker c_LogMarker = MarkerFactory.getMarker(UserInspectGroup.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell) throws CommandException {
    prepare(shell);
    try {
      String name;
      Group g;
      Set<String> groups = new TreeSet<String>();
      groups.addAll(
          m_GroupMessagingService.listGroupMemberships(m_Agent, m_AgentIdentifier));
      groups.addAll(
          m_GroupMessagingService.listJoinableGroups(m_Presence));
      if(groups.isEmpty()) {
        m_IO.printTemplate(PREFIX + "none");
        return;
      }
      m_PromptWrap = false;
      name = prompt(PREFIX + "name",null,new StringListCompletionFilter(groups));
      if (name == null || name.length() == 0) {
        m_IO.printTemplate(PREFIX + "aborted");
        return;
      }
      if(!groups.contains(name)) {
        m_IO.printTemplate(PREFIX + "notallowed");
        return;      
      }
      try {
        g = m_GroupMessagingService.getGroup(m_Agent, name);
      } catch (SecurityException ex) {
        m_IO.printTemplate(PREFIX + "notallowed");
        return;
      } catch (NoSuchGroupException nsex) {
        m_IO.printTemplate(PREFIX + "nosuchgroup");
        return;
      }
      displayGroup(g);
    } catch (Exception ex) {
      log.error(c_LogMarker,"run()", ex);
    }
  }//run

}//class UserInspectGroup
