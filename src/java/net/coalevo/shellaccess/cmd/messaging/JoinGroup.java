/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.messaging;

import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.wimpi.telnetd.io.toolkit.Selection;

import java.util.Set;

import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * Provides a command that allows you to join a group.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class JoinGroup
    extends MessagingCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(JoinGroup.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell) throws CommandException {
    prepare(shell);
    try {
      Set<String> joinable = m_GroupMessagingService.listJoinableGroups(m_Presence);

      if (joinable.isEmpty()) {
        m_IO.printTemplate(PREFIX + "none");
        return;
      }
      m_IO.printTemplate(PREFIX + "instruct");
      m_IO.printTemplate(PREFIX + "promptgroup");
      Selection s = new Selection(m_IO.getTerminalIO(), "aid");
      String none = m_IO.formatTemplate(PREFIX + "noneoption");
      s.addOption(none);
      for (String str : joinable) {
        s.addOption(str);
      }
      s.run();
      String sel = s.getOption(s.getSelected());
      if (none.equals(sel)) {
        m_IO.printTemplate(PREFIX + "unchanged");
      } else {
        try {
          m_GroupMessagingService.joinGroup(m_Presence, sel);
          m_IO.printTemplate(PREFIX + "confirm");
        } catch (Exception ex) {
          m_IO.printTemplate(PREFIX + "failed");
          return;
        }
      }
    } catch (Exception ex) {
      log.error(c_LogMarker,"run()", ex);
    }
  }//run

  private static final String PREFIX = TS_PREFIX + "JoinGroup_";

}//class JoinGroup
