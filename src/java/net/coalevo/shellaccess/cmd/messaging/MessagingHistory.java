/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.messaging;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.messaging.model.InteractiveMessage;
import net.coalevo.messaging.model.EventInfo;
import org.apache.lucene.search.Query;

import java.util.Locale;
import java.util.ListIterator;

/**
 * Defines a messaging history.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface MessagingHistory {

  /**
   * Adds an outgoing message to the history without indexing it.
   * <p/>
   * Any such message to be indexed, requires confirmation through
   * {@link #confirmOutgoingMessage(String id)}
   * </p>
   *
   * @param m     an {@link InteractiveMessage}.
   * @param ei    an {@link EventInfo}.
   * @param time  the time as UTC milliseconds.
   * @param l     the <tt>Locale</tt>.
   * @param fmsg  the formatted message.
   * @return the number of the message.
   */
  public int addIncomingMessage(InteractiveMessage m, EventInfo ei,long time, Locale l, String fmsg);

  /**
   * Adds an outgoing message to the history without indexing it.
   * <p/>
   * Any such message to be indexed, requires confirmation through
   * {@link #confirmOutgoingMessage(String id)}
   * </p>
   *
   * @param m     an {@link InteractiveMessage}.
   * @param time  the time as UTC milliseconds.
   * @param l     the <tt>Locale</tt>.
   * @param fmsg  the formatted message.
   * @param index true if to be indexed without confirmation, false otherwise.
   */
  public void addOutgoingMessage(InteractiveMessage m, long time, Locale l, String fmsg, boolean index);

  /**
   * Confirms an added outgoing message, finally indexing it.
   *
   * @param id the message identifier of the received message.
   * @return the count of sent messages.
   */
  public int confirmOutgoingMessage(String id);

  /**
   * Returns the sender of the last received/incoming message.
   *
   * @return an {@link AgentIdentifier}.
   */
  public AgentIdentifier getLastSender();

  /**
   * Returns the name of the last group message that
   * was received.
   *
   * @return the name of the last group a message was received from.
   */
  public String getLastGroupSender();

  /**
   * Returns the addressee of the last sent/outgoing message.
   *
   * @return an {@link AgentIdentifier}.
   */
  public AgentIdentifier getLastAddressee();

  /**
   * Returns the name of the last group that was addressed.
   *
   * @return the name of the last group if the last message was
   *         sent to a group or null otherwise.
   */
  public String getLastGroupAddressee();

  /**
   * Returns the total count of all incoming and outgoing messages.
   *
   * @return the total count of all messages.
   */
  public int getTotalMessageCount();

  /**
   * Returns the count of all incoming messages.
   *
   * @return the number of received/incoming messages.
   */
  public int getIncomingMessageCount();

  /**
   * Returns the count of all outgoing messages.
   *
   * @return the total number of sent/outgoing messages.
   */
  public int getOutgoingMessageCount();

  /**
   * Parses a given string into a query.
   *
   * @param query a <tt>String</tt> representing a query.
   * @return a Lucene <tt>Query</tt> instance.
   */
  public Query parseQuery(String query);

  /**
   * Returns the messages matched by the given <tt>Query</tt>.
   * To obtain a query, use {@link #parseQuery(String)}.
   *
   * @param q Lucene <tt>Query</tt> instance.
   * @return the matched messages in a pre-formatted <tt>String</tt>.
   *         It's recommended to use a pager for display.
   */
  public String getMessages(Query q);

  /**
   * Returns the history in a list iterator for browsing.
   * @return the message history in a list iterator.
   */
  public ListIterator<String> iterateMessages();

  /**
   * Returns the size of this history.
   *
   * @return the size of the history in number of stored messages.
   */
  public int size();

  /**
   * Sets the index of this history to memory or disk.
   * @param mem true if in memory, false if on disk.
   */
  public void setIndex(boolean mem);

  /**
   * Closes the index that stores this history.
   */
  public void closeIndex();

}//interface MessagingHistory
