/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.messaging;

import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.wimpi.telnetd.io.toolkit.Editfield;
import net.wimpi.telnetd.io.toolkit.InputValidator;
import net.wimpi.telnetd.io.toolkit.Pager;
import org.apache.lucene.search.Query;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * Command implementation that allows to query the {@link MessagingHistory}.
 * <p/>
 * The history is implemented as an in-memory Lucene index, which means
 * that the history is full-text searchable.
 * </p>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class QueryHistory
    extends MessagingCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(QueryHistory.class.getName());
  private Query m_Query;

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell) throws CommandException {
    prepare(shell);
    try {
      final String pwderr = m_IO.formatTemplate("inputerror_queryvalidator");
      m_IO.printTemplate(PREFIX + "querylabel");
      Editfield ef = new Editfield(m_IO.getTerminalIO(), "query", 40, 255);
      ef.setIgnoreDelete(true);
      ef.setInputValidator(new InputValidator() {
        public boolean validate(String string) {
          if (string == null || "".equals(string)) {
            m_Query = null;
            return true;
          }
          m_Query = m_MessagingHistory.parseQuery(string);
          return (m_Query != null);
        }//validate

        public String getErrorMessage() {
          return pwderr;
        }
      });
      ef.run();
      if (m_Query == null) {
        m_IO.printTemplate(PREFIX + "aborted");
      } else {
        String msgs = m_MessagingHistory.getMessages(m_Query);
        if (msgs != null && msgs.length() > 0) {
          Pager p = new Pager(m_IO.getTerminalIO());
          p.page(msgs);
        } else {
          m_IO.printTemplate(PREFIX + "nomatch");
        }
      }
    } catch (Exception ex) {
      log.error(c_LogMarker, "run()", ex);
    }
  }//run

  public void reset() {
    super.reset();
    m_Query = null;
  }//reset

  private static final String PREFIX = TS_PREFIX + "QueryHistory_";

}//class QueryHistory
