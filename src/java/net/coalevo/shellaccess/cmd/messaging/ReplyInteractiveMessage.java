/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.messaging;

import net.coalevo.foundation.model.AgentIdentifier;
import net.wimpi.telnetd.io.TemplateAttributes;

import java.io.IOException;

/**
 * Provides a command implementation that will reply to
 * the sender of the last received message.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class ReplyInteractiveMessage
    extends SendInteractiveMessage {

  protected AgentIdentifier getAddressee() throws IOException {
    //1. Check group first
    m_GroupName = m_MessagingHistory.getLastGroupSender();
    if (m_GroupName != null) {
      TemplateAttributes attr = m_IO.leaseTemplateAttributes();
      attr.add("to", m_GroupName);
      m_IO.printTemplate(PREFIX + "groupaddressee", attr);
      return null;
    }
    //2. Check agent
    AgentIdentifier addressee = m_MessagingHistory.getLastSender();
      if(addressee != null) {
      TemplateAttributes attr = m_IO.leaseTemplateAttributes();
      attr.add("to", getFormattedAgent(addressee));
      m_IO.printTemplate(PREFIX + "addressee", attr);
    }
    return addressee;
  }//getAddressee

  private static final String PREFIX = TS_PREFIX + "ReplyInteractiveMessage_";

}//class ReplyInteractiveMessage
