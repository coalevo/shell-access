/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.messaging;

import net.coalevo.text.util.TagUtility;
import net.coalevo.messaging.model.EditableGroup;
import net.coalevo.messaging.model.MessagingException;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.shellaccess.validators.LanguageInputValidator;
import net.coalevo.shellaccess.validators.StringValidator;
import net.coalevo.foundation.model.AgentIdentifier;

import java.util.Locale;

import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * Provides a command for creating a permanent group.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class CreatePermanentGroup
    extends MessagingCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(CreatePermanentGroup.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell) throws CommandException {
    prepare(shell);
    try {
      String name;
      boolean done = false;
      EditableGroup eg;
      m_PromptWrap = false;
      do {
        name = prompt(PREFIX + "name", new StringValidator(2));
        if (m_GroupMessagingService.existsGroup(m_Agent, name)) {
          if (!promptDecision(PREFIX + "changename")) {
            m_IO.printTemplate(PREFIX + "aborted");
            return;
          }
        } else {
          done = true;
        }
      } while (!done);
      try {
        eg = m_GroupMessagingService.beginCreatePermanentGroup(m_Agent, name);
      } catch (SecurityException ex) {
        m_IO.printTemplate(PREFIX + "notallowed");
        return;
      }
      AgentIdentifier aid = promptAgent(PREFIX + "moderator");
      if(aid == null) {
        m_IO.printTemplate(PREFIX + "nosuchagent");
        return;
      }
      eg.setModerator(aid);
      eg.setLanguage(
          new Locale(prompt(PREFIX + "language", new LanguageInputValidator(
              m_IO.formatTemplate("inputerror_languagevalidator")
          )))
      );


      String format = promptInputFormat(null);
      String txt = editTransformableContent(format, null);
      if (txt == null || txt.length() == 0) {
        m_IO.printTemplate(PREFIX + "aborted");
        return;
      }
      eg.setInfoFormat(format);
      eg.setInfo(txt.trim());

      eg.setTags(TagUtility.toSet(prompt(PREFIX + "tags")));
      boolean inviteonly = promptDecision(PREFIX + "inviteonly");
      boolean invitemod = false;
      if (inviteonly) {
        invitemod = promptDecision(PREFIX + "invitemoderated");
      }
      eg.setInviteOnly(inviteonly);
      eg.setInviteModerated(invitemod);

      if (promptDecision(PREFIX + "doit")) {
        try {
          m_GroupMessagingService.commitPermanentGroup(m_Agent, eg);
          m_IO.printTemplate(PREFIX + "confirm");

        } catch (SecurityException ex) {
          m_IO.printTemplate(PREFIX + "notallowed");
          return;
        } catch (MessagingException mex) {
          m_IO.printTemplate(PREFIX + "failed");
          m_GroupMessagingService.cancelPermanentGroupTransaction(m_Agent, eg);
          return;
        }
      } else {
        m_IO.printTemplate(PREFIX + "aborted");
        m_GroupMessagingService.cancelPermanentGroupTransaction(m_Agent, eg);
        return;
      }

    } catch (Exception ex) {
      log.error(c_LogMarker,"run()", ex);
    }
  }//run

  private static final String PREFIX = TS_PREFIX + "CreatePermanentGroup_";

}//class CreatePermanentGroup
