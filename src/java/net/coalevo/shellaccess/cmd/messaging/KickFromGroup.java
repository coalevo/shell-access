/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.messaging;

import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.messaging.model.NoSuchGroupException;
import net.coalevo.messaging.model.MessagingException;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * Provides a command for kicking another agent from a
 * messaging group.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class KickFromGroup
    extends MessagingCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(KickFromGroup.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell) throws CommandException {
    prepare(shell);
    try {
      //1. prompt agent
      m_PromptWrap = false;
      String gname = prompt(PREFIX + "groupname");
      AgentIdentifier aid = promptAgent(PREFIX + "agent");
      if(aid == null) {
        m_IO.printTemplate(PREFIX + "nosuchagent");
        return;
      }
      try {
        m_GroupMessagingService.kickFromGroup(m_Agent, gname, aid);
        m_IO.printTemplate(PREFIX + "confirm");
      } catch (NoSuchGroupException nsgex) {
        m_IO.printTemplate(PREFIX + "nosuchgroup");
        return;
      } catch (SecurityException ex) {
        m_IO.printTemplate(PREFIX + "notallowed");
        return;
      } catch (MessagingException mex) {
        m_IO.printTemplate(PREFIX + "failed");
        return;
      }

    } catch (Exception ex) {
      log.error(c_LogMarker,"run()", ex);
    }
  }//run

  private static final String PREFIX = TS_PREFIX + "KickFromGroup_";

}//class KickFromGroup
