/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.shellaccess.impl.CommandShell;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.wimpi.telnetd.io.TemplateAttributes;
import org.slf4j.MarkerFactory;
import org.slf4j.Marker;

import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

/**
 * Implements the logout command.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class Logout
    extends BaseCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(Logout.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell) throws CommandException {
    prepare(shell);
    try {

      Set s = m_Shell.getSession().listMessaging();
      if (!s.isEmpty()) {
        TemplateAttributes attr = m_IO.leaseTemplateAttributes();
        for (Iterator iter = s.iterator(); iter.hasNext();) {
          AgentIdentifier aid = (AgentIdentifier) iter.next();
          attr.add("agents", getFormattedAgent(aid));
        }
        m_IO.printTemplate(PREFIX + "list", attr);
        if(promptDecision(PREFIX + "askwait")) {
          m_IO.println();
          return;
        } else {
          m_IO.println();
        }        
      }
      if (shell instanceof CommandShell) {
        ((CommandShell) shell).logout(true);
      }
    } catch (IOException ex) {
      log.error(c_LogMarker,"run(BaseShell)", ex);
      throw new CommandException(ex.getMessage());
    }
  }//run

  public void reset() {
    //Clean up local resources
  }//reset

  private static final String PREFIX = "commands_net.coalevo.shellaccess.cmd.Logout_";

}//class Logout
