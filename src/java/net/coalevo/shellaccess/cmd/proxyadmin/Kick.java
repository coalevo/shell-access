/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.proxyadmin;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.shellaccess.cmd.BaseCommand;
import net.coalevo.shellaccess.impl.Activator;
import net.coalevo.shellaccess.impl.SessionManager;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.shellaccess.model.ShellAccessSession;
import net.coalevo.shellaccess.model.Services;
import net.coalevo.messaging.service.MessagingService;
import net.coalevo.messaging.model.InteractiveMessageTypes;
import net.coalevo.messaging.model.EditableInteractiveMessage;
import net.coalevo.messaging.model.MessagingException;
import net.coalevo.presence.model.Presence;
import net.coalevo.presence.model.PresenceConstants;

import java.io.IOException;
import java.util.Iterator;

import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * This class provides a command for kicking a user.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class Kick
    extends BaseCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(Kick.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell) throws CommandException {
    if (!prepare(shell)) {
      return;
    }
    try {
      //1. Ok we need to get a user
      m_PromptWrap = false;
      AgentIdentifier aid = promptAgent(PREFIX + "agent");
      if(aid == null) {
        m_IO.printTemplate(PREFIX + "nosuchagent");
        return;
      }      
      //2. and kick him if we can
      SessionManager smgr = Activator.getServices().getSessionManager();
      for (Iterator iterator = smgr.sessions(); iterator.hasNext();) {
        ShellAccessSession session = (ShellAccessSession) iterator.next();
        AgentIdentifier agent = session.getUserAgent().getAgentIdentifier();
        if (agent.equals(aid)) {
          //this is the one
          //Prompt reason
          m_IO.printTemplate(PREFIX+"reason");
          String reason = simpleEdit(null);
          //Confirm
          if (!promptDecision(PREFIX + "doit")) {
            m_IO.printTemplate(PREFIX + "abort");
            return;
          }
          //edge out message
          MessagingService ms= shell.getServices().getMessagingService(Services.NO_WAIT);
          Presence p = (Presence) m_Shell.getSession().getCoalevoSession().getAttribute(PresenceConstants.PRESENCE_SESSION_ATTRIBUTE);
          if (ms != null) {
            try {
              EditableInteractiveMessage edm = ms.create(p, InteractiveMessageTypes.NOTIFICATION,null);
              edm.setTo(agent);
              edm.setSubject("Kick");
              edm.setBody(reason);
              ms.unicast(p,edm);
              try {
                synchronized(this) {
                  this.wait(500);
                }
              } catch (InterruptedException e) {
                log.error("()", e);
              }
            } catch (MessagingException mex) {
              m_IO.printTemplate(PREFIX + "notificationfailed");
            }
          } else {
            m_IO.printTemplate(PREFIX + "notificationfailed");
          }
          session.invalidate();
          m_IO.printTemplate(PREFIX + "confirm");
          return;
        }
      }
      m_IO.printTemplate(PREFIX + "nosuch");
    } catch (IOException ex) {
      log.error(c_LogMarker,"run(BaseShell)", ex);
    }
  }//run

  private static final String PREFIX = "commands_net.coalevo.shellaccess.cmd.proxyadmin.Kick_";

}//class Kick
