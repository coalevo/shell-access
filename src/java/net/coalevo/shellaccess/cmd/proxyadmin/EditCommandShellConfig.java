/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.proxyadmin;

import net.coalevo.shellaccess.cmd.admin.AdminCommand;
import net.coalevo.shellaccess.impl.Activator;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.wimpi.telnetd.io.Templates;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;

/**
 * This class implements a command for editing
 * a the command shell configuration.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class EditCommandShellConfig
    extends AdminCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(EditCommandShellConfig.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell) throws CommandException {
    prepare(shell);
    try {
      Templates ts = Activator.getTemplates();
      final Iterator iter = ts.getLanguages();
      final StringBuilder sbuf = new StringBuilder();
      while (iter.hasNext()) {
        sbuf.append(iter.next());
        sbuf.append((iter.hasNext()) ? "," : "");
      }
      final File templates = new File(Activator.getConfigStore(), "commandshell-config.xml");
      editFile(templates);
      m_IO.printTemplate(PREFIX + "done");
    } catch (Exception ex) {
      try {
        m_IO.printTemplate(PREFIX + "failed");
      } catch (IOException iex) {
        Activator.log().error(c_LogMarker, "run(BaseShell)", iex);
      }
      Activator.log().error(c_LogMarker, "run(BaseShell)", ex);
      throw new CommandException(ex.getMessage());
    }
  }//run

  public void reset() {
    //Clean up local resources
  }//reset

  private static final String PREFIX = "commands_net.coalevo.shellaccess.cmd.proxyadmin.EditCommandShellConfig_";

}//class EditCommandShellConfig
