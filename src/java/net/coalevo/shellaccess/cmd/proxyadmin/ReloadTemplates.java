/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.proxyadmin;

import net.coalevo.shellaccess.impl.Activator;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.Command;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.shellaccess.model.ShellIO;
import net.wimpi.telnetd.io.Templates;

import java.io.IOException;

import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * Command reloading the proxy template
 * definitions for all languages on the fly.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class ReloadTemplates
    implements Command {

  private static Marker c_LogMarker = MarkerFactory.getMarker(ReloadTemplates.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell) throws CommandException {
    ShellIO io = shell.getShellIO();
    Templates ts = Activator.getTemplates();

    try {
      io.printTemplate(PREFIX + "progresslabel");
      shell.startProgressIndicator();
      if (!ts.reloadTemplates()) {
        throw new IOException("Reload failed.");
      }
      shell.stopProgressIndicator();
      io.printTemplate(PREFIX +"done");
    } catch (IOException ex) {
      shell.stopProgressIndicator();
      try {
        io.printTemplate(PREFIX + "failed");
      } catch (IOException iex) {
        Activator.log().error(c_LogMarker,"run(BaseShell)", iex);
      }
      Activator.log().error(c_LogMarker,"run(BaseShell)", ex);
      throw new CommandException(ex.getMessage());
    }
  }//run

  public void reset() {
    //Clean up local resources
  }//reset

  private static final String PREFIX = "commands_net.coalevo.shellaccess.cmd.proxyadmin.ReloadTemplates_";

}//class ReloadTemplates
