/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.proxyadmin;

import net.coalevo.shellaccess.command.CommandShellManager;
import net.coalevo.shellaccess.impl.Activator;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.shellaccess.cmd.BaseCommand;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.IOException;

/**
 * Reloads a command.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class ReloadCommand
    extends BaseCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(ReloadCommand.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell) throws CommandException {
    if(!prepare(shell)) {
      return;
    }
    CommandShellManager csmgr = Activator.getServices().getCommandShellManager();
    try {
      String id = prompt(PREFIX + "idlabel");
      m_IO.println();
      if (!csmgr.existsCommand(id)) {
        m_IO.printTemplate(PREFIX + "nonexistant");
      } else {
        if (!csmgr.reloadCommand(id)) {
          throw new IOException("Reload failed");
        }
        m_IO.printTemplate(PREFIX + "done");
      }
    } catch (IOException ex) {
      try {
        m_IO.printTemplate(PREFIX+ "failed");
      } catch (IOException iex) {
        Activator.log().error(c_LogMarker,"run(BaseShell)", iex);
      }
      Activator.log().error(c_LogMarker,"run(BaseShell)", ex);
      throw new CommandException(ex.getMessage());
    }
  }//run

  public void reset() {
    //Clean up local resources
  }//reset

  private static final String PREFIX = "commands_net.coalevo.shellaccess.cmd.proxyadmin.ReloadCommand_";

}//class ReloadCommand
