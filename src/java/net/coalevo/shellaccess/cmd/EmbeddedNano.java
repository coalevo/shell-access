/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd;

import net.coalevo.shellaccess.impl.Activator;
import net.coalevo.shellaccess.model.BasicShell;

import java.io.*;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.WritableByteChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * This class implements a wrapper for the native nano editor.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class EmbeddedNano {

  private BasicShell m_Shell;
  private Process m_Process;

  //Streams and buffer
  private InputStream m_Input;
  private OutputStream m_Output;
  private InputStream m_ProcessInput;
  private OutputStream m_ProcessOutput;
  private InputStream m_ProcessError;
  private byte[] m_Buffer;
  private String m_Content;

  private File m_File;
  private boolean m_Temporary;

  private boolean m_FirstRun = true;

  public EmbeddedNano() {
  }//constructor

  private boolean isAlive() {
    try {
      m_Process.exitValue();
      return false;
    } catch (IllegalThreadStateException e) {
      return true;
    }
  }//isAlive

  private boolean pumpStream(InputStream in, OutputStream out, byte[] buffer) throws IOException {
    if (in.available() > 0) {
      int len = in.read(buffer);
      if (len > 0) {
        out.write(buffer, 0, len);
        out.flush();
        return true;
      }
    }
    return false;
  }//pumpStream

  private void pumpStreams() {
    try {
      for (; ;) {
        if (!isAlive()) {
          return;
        }
        if (pumpStream(m_Input, m_ProcessOutput, m_Buffer)) {
          continue;
        }
        if (pumpStream(m_ProcessInput, m_Output, m_Buffer)) {
          continue;
        }
        if (pumpStream(m_ProcessError, m_Output, m_Buffer)) {
          continue;
        }

        // Sleep a bit.  This is not very good, as it consumes CPU, but the
        // input streams are not selectable for nio, and any other blocking
        // method would consume at least two threads
        Thread.sleep(1);
      }
    } catch (Exception e) {
      m_Process.destroy();
    }
  }//pumpStreams

  public void prepare(BasicShell shell) {
    m_Shell = shell;
    String fn = "/tmp/" + shell.getServices().getRndStringGeneratorService(0).getRandomString(10);
    m_Temporary = true;
    m_File = new File(fn);
  }//prepare

  public void backup() {
    try {
      File in = m_File;
      File out = new File(in.getAbsolutePath() + "~");

      FileChannel inChannel = new
          FileInputStream(in).getChannel();
      FileChannel outChannel = new
          FileOutputStream(out).getChannel();
      try {
        inChannel.transferTo(0, inChannel.size(),
            outChannel);
      } finally {
        if (inChannel != null) inChannel.close();
        if (outChannel != null) outChannel.close();
      }
    } catch (Exception ex) {
      Activator.log().error("EmbeddedNano::backup()", ex);
    }
  }//backup

  public void prepare(BasicShell shell, File f) {
    m_Shell = shell;
    m_File = f;
  }//prepare

  public String getContent() {
    return m_Content;
  }//getContent

  public void setContent(String str) {
    m_Content = str;
  }//setContent

  public void cleanup() {
    m_File.delete();
  }//cleanup

  public void run() {
    try {
      //Prepare content
      if (m_FirstRun && m_Temporary && (m_Content != null && m_Content.length() > 0)) {
        FileOutputStream fout = new FileOutputStream(m_File);
        fout.write(m_Content.getBytes("utf-8"));
      }
      m_FirstRun = false;

      //Prepare command
      List<String> l = new ArrayList<String>();
      l.add(System.getProperty(NANO_SYS_PROP));
      l.add("-R");
      if(m_Shell.getSession().isRebindDelete()) {
        l.add("-d");
      }
      l.add(m_File.getAbsolutePath());
      ProcessBuilder pb = new ProcessBuilder(l);

      //Prepare environment
      Map<String, String> env = pb.environment();
      env.put("COLUMNS", Integer.toString(m_Shell.getShellIO().getColumns()));
      env.put("LINES", Integer.toString(m_Shell.getShellIO().getRows()));

      //Prepare streams
      m_Input = m_Shell.getShellIO().getTerminalIO().getBaseIO().getInputStream();
      m_Output = m_Shell.getShellIO().getTerminalIO().getBaseIO().getOutputStream();

      //Start Process
      m_Process = pb.start();

      //Acquire streams
      m_ProcessInput = m_Process.getInputStream();
      m_ProcessOutput = m_Process.getOutputStream();
      m_ProcessError = m_Process.getErrorStream();

      //Buffer
      m_Buffer = new byte[(int) (m_Shell.getShellIO().getColumns() * m_Shell.getShellIO().getRows() * 1.3)];

      //start pumping
      pumpStreams();
      m_Process.waitFor();

      //get content and remove the file if temporary
      if (m_Temporary) {
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        //copy contents using nio
        FileChannel source = new FileInputStream(m_File).getChannel();
        WritableByteChannel dest = Channels.newChannel(bout);
        source.transferTo(0, m_File.length(), dest);
        source.close();
        dest.close();

        m_Content = new String(bout.toByteArray(), "utf-8");
      }
      //m_Shell.getShellIO().getTerminalIO().resetTerminal();
      m_Shell.getShellIO().blankTerminal();
      m_Shell.getShellIO().flush();
    } catch (Exception ex) {
      Activator.log().error("EmbeddedNano::run()", ex);
    } finally {      
    }
  }//run

  public static boolean isAvailable() {
    String str = System.getProperty(NANO_SYS_PROP);
    return (str != null && str.length() > 0);
  }//isAvailable

  private static final String NANO_SYS_PROP = "net.coalevo.shellaccess.nano";

}//class EmbeddedNano
