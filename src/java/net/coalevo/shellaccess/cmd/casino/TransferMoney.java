/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.casino;

import net.coalevo.bank.model.BankServiceException;
import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.shellaccess.validators.LongInputValidator;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * Command that allows to transfer money from the callers account
 * to another account.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class TransferMoney
    extends CasinoCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(TransferMoney.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell)
      throws CommandException {
    if (!prepare(shell)) {
      return;
    }
    try {
      m_PromptWrap = false;
      //just to make sure, report status
      reportAccountStatus();
      //prompt to
      AgentIdentifier to = this.promptAgent(PREFIX + "agentid");
      if (to == null) {
        m_IO.printTemplate(PREFIX + "nosuchagent");
        return;
      }
      long amount = Long.parseLong(
          this.prompt(
              PREFIX + "howmuch",
              new LongInputValidator(
                  0,
                  this.getMoney(),
                  m_IO.formatTemplate("intvalidator","min",Long.toString(0),"max", Long.toString(this.getMoney()))
              )
          )
      );
      if (amount == 0) {
        m_IO.printTemplate(PREFIX + "aborted");
        return;
      }
      try {
        m_Bank.transfer(m_Agent, m_AgentIdentifier, to, amount);
        m_IO.printTemplate(PREFIX + "confirm", "balance", Long.toString(getMoney()));
      } catch (SecurityException ex) {
        m_IO.println(PREFIX + "notallowed");
        log.error(c_LogMarker, "run()", ex);
      } catch (BankServiceException ex) {
        m_IO.println(PREFIX + "failed");
        log.error(c_LogMarker, "run()", ex);
      }

    } catch (Exception ex) {
      log.error(c_LogMarker, "run()", ex);
      throw new CommandException(ex.getMessage());
    }
  }//run

  private static final String PREFIX = TPREFIX + "TransferMoney_";

}//class TransferMoney
