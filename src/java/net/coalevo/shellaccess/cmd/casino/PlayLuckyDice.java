/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.casino;

import net.coalevo.shellaccess.cmd.casino.model.PairOfDice;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.shellaccess.validators.IntegerInputValidator;
import net.wimpi.telnetd.io.toolkit.Editfield;
import net.wimpi.telnetd.io.TemplateAttributes;

import java.io.IOException;

/**
 * Command for playing Lucky Dice.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class PlayLuckyDice
    extends CasinoCommand {

  private PairOfDice m_Dice;

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell) throws CommandException {
    if(!prepare(shell)) {
      return;
    }    
    boolean done = false;
    m_Dice = new PairOfDice();

    try {
      do {
        //1. ask and handle bet, max limit is 100.
        reportAccountStatus();
        int bet = getBet(100);
        if(bet == 0) {
          done = true;
          m_IO.printTemplate(super.PREFIX + "nobet");
          continue;
        }
        //2. ask guess
        int guess = getGuess();
        //3. roll and report
        m_Dice.roll();
        TemplateAttributes attr = m_IO.leaseTemplateAttributes();
        attr.add("fdice",String.valueOf(m_Dice.firstDice()));
        attr.add("sdice",String.valueOf(m_Dice.secondDice()));
        attr.add("total",String.valueOf(m_Dice.getValue()));
        m_IO.printTemplate(PREFIX + "dice", attr);
        if (m_Dice.isValue(guess)) {
          win(calculateAmountWon(bet));
        } else {
          loose(bet);
        }
        //4. Check if continue
        done = !checkContinue();
        //5. Check for events in the i/o buffer
        checkEvents();

      } while (!done);
      reportAccountStatus();
    } catch (IOException ex) {
      log.error("run()",ex);
      throw new CommandException(ex.getMessage());
    }
  }//run

  private int getGuess() throws IOException {
    m_IO.printTemplate(PREFIX + "askguess");
    Editfield ef = new Editfield(m_IO.getTerminalIO(), "guess", 15, 15);
    ef.setIgnoreDelete(true);
    ef.setInputValidator(
        new IntegerInputValidator(
            2,
            12,
            m_IO.formatTemplate("inputerror_intvalidator","min",Integer.toString(2),"max",Integer.toString(12))
        )
    );
    ef.run();
    //is already validated
    return Integer.parseInt(ef.getValue().trim());
  }//getGuess

  private int calculateAmountWon(int bet) {
    return (int)Math.round(
        bet * ((1/(double)m_Dice.getNumberOfCombinations())*2d)
    );
  }//calculateAmountWon

  public void reset() {
    super.reset();
    m_Dice = null;
  }//reset

  private static final String PREFIX = TPREFIX + "PlayLuckyDice_";

}//class PlayLuckyDice
