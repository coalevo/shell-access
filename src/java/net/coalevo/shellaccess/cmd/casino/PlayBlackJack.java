/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.casino;

import net.coalevo.shellaccess.cmd.casino.model.BlackjackHand;
import net.coalevo.shellaccess.cmd.casino.model.Card;
import net.coalevo.shellaccess.cmd.casino.model.Deck;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.wimpi.telnetd.io.TemplateAttributes;

import java.io.IOException;


/**
 * This is a command for playing blackjack.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class PlayBlackJack extends CasinoCommand {

  private Deck m_Deck;
  private BlackjackHand m_PlayerHand;
  private BlackjackHand m_DealerHand;
  private int m_Bet;

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell) throws CommandException {
    if(!prepare(shell)) {
      return;
    }
    boolean done = false;
    m_Deck = new Deck();
    m_PlayerHand = new BlackjackHand();
    m_DealerHand = new BlackjackHand();

    try {
      do {
        m_Deck.shuffle();
        m_PlayerHand.clear();
        m_DealerHand.clear();
        //1. ask and handle bet, max limit is 100.
        reportAccountStatus();
        m_Bet = getBet(100);
        if(m_Bet == 0) {
          done = true;
          m_IO.printTemplate(super.PREFIX + "nobet");
          continue;
        }
        //2. deal, dealer one visible
        m_PlayerHand.addCard(m_Deck.dealCard());
        m_DealerHand.addCard(m_Deck.dealCard());
        m_PlayerHand.addCard(m_Deck.dealCard());
        m_DealerHand.addCard(m_Deck.dealCard().setVisible(false));

        //3. Show status
        printStatus();

        if(m_PlayerHand.isBlackjack()) {
          m_DealerHand.turnCard();
          m_IO.printTemplate(PREFIX + "turns");
          //show dealer status
          TemplateAttributes attr = m_IO.leaseTemplateAttributes();
          m_DealerHand.toAttributes(attr,m_IO);
          attr.add("total", String.valueOf(m_DealerHand.getValue()));
          attr.add("dealer","");
          m_IO.printTemplate(PREFIX + "hand", attr);
          if(m_DealerHand.isBlackjack()) {
            m_IO.printTemplate(PREFIX + "push");
          } else {
            m_IO.printTemplate(PREFIX + "blackjack","player","");
            win(m_Bet);
          }
        } else {
          if (!doPlayerMove()) {
            //dealer moves
            m_DealerHand.turnCard(); //turn card
            m_IO.printTemplate(PREFIX + "turns");
            //check blackjack
            if(m_DealerHand.isBlackjack()) {
              m_IO.printTemplate(PREFIX + "blackjack","dealer","");
              loose(m_Bet);
            }
            while(evaluateDealerHand()) {
              dealerHits();
            }
          }
        }


        //4. Check if continue
        done = !checkContinue();

        //5. Check for events in the i/o buffer
        checkEvents();

      } while (!done);
      reportAccountStatus();
    } catch (IOException ex) {
      log.error("run()", ex);
      throw new CommandException(ex.getMessage());
    }
  }//run

  public void reset() {
    super.reset();
    m_Deck = null;
    m_PlayerHand = null;
    m_DealerHand = null;
    m_Bet = 0;
  }//reset

  private void printStatus() throws IOException {
    //1. My hand
    TemplateAttributes attr = m_IO.leaseTemplateAttributes();
    m_PlayerHand.toAttributes(attr,m_IO);
    attr.add("total", String.valueOf(m_PlayerHand.getValue()));
    attr.add("player","");
    m_IO.printTemplate(PREFIX + "hand", attr);
    m_IO.println();
    //2. dealer
    attr = m_IO.leaseTemplateAttributes();
    m_DealerHand.toAttributes(attr,m_IO);
    attr.add("total", String.valueOf(m_DealerHand.getValue()));
    attr.add("dealer","");
    m_IO.printTemplate(PREFIX + "hand", attr);
  }//printStatus

  private boolean doPlayerMove() throws IOException {
    boolean done = false;
    do {
      int key = m_IO.prompt(PREFIX + "askmove", null);
      switch (key) {
        case STAND:
          m_IO.printTemplate(PREFIX + "stands");
          return false; //game is not over
        case HIT:
          playerHits();
          if (m_PlayerHand.isBusted()) {
            m_IO.printTemplate(PREFIX + "busted",
                "player","",
                "total", String.valueOf(m_PlayerHand.getValue()));
            loose(m_Bet);
            return true;//game over
          }
          break;
        case DOUBLEDOWN:
          //check money
          if (getMoney() < (m_Bet * 2)) {
            m_IO.printTemplate(PREFIX + "cantdoubledown");
            continue;
          } else {
            //double bet, one hit and then stand
            m_Bet += m_Bet;
            m_IO.printTemplate(PREFIX + "doubledown");
            playerHits();

            if (m_PlayerHand.isBusted()) {
              m_IO.printTemplate(PREFIX + "busted",
                "player","",
                "total", String.valueOf(m_PlayerHand.getValue()));
              loose(m_Bet);
              return true;//game over
            } else {
              return false; //standing
            }
          }
        case SURRENDER:
          m_IO.printTemplate(PREFIX + "giveup");
          loose(Math.round(m_Bet/2));
          return true; //game over
        default:
          //ignore
          break;
      }
    } while (!done);
    return true;
  }//doPlayerMove

  private void playerHits() throws IOException {
    m_IO.printTemplate(PREFIX + "hits","player","");
    Card c = m_Deck.dealCard();
    TemplateAttributes attr = m_IO.leaseTemplateAttributes();
    c.toAttributes(attr,m_IO);
    attr.add("player","");
    m_IO.printTemplate(PREFIX + "gotcard", attr);
    m_PlayerHand.addCard(c);
    printStatus();
  }//playerHits

  private void dealerHits() throws IOException {
    m_IO.printTemplate(PREFIX + "hits","dealer","");
    Card c = m_Deck.dealCard();
    TemplateAttributes attr = m_IO.leaseTemplateAttributes();
    c.toAttributes(attr,m_IO);
    attr.add("dealer","");
    m_IO.printTemplate(PREFIX + "gotcard", attr);
    m_DealerHand.addCard(c);
  }//dealerHits

  private boolean evaluateDealerHand() throws IOException {
    //2. dealer
    TemplateAttributes attr = m_IO.leaseTemplateAttributes();
    m_DealerHand.toAttributes(attr,m_IO);
    attr.add("total", String.valueOf(m_DealerHand.getValue()));
    attr.add("dealer","");
    m_IO.printTemplate(PREFIX + "hand", attr);
    int player = m_PlayerHand.getValue();
    int dealer = m_DealerHand.getValue();
    if(m_DealerHand.isGood()) {
      if(dealer > player) {
        m_IO.printTemplate(PREFIX + "dealerwins");
        loose(m_Bet);
        return false;
      } else if (dealer == player) {
        m_IO.printTemplate(PREFIX + "dealerwins","tie","");
        loose(m_Bet);
        return false;
      }
      return true;
    } else {
      m_IO.printTemplate(PREFIX + "busted","dealer","","total",String.valueOf(dealer));
      win(m_Bet);
      return false;
    }
  }//evaluateDealerHand

  private static final int STAND = 's';
  private static final int HIT = 'h';
  private static final int DOUBLEDOWN = 'd';
  private static final int SURRENDER = 'g';

  private static final String PREFIX = TPREFIX + "PlayBlackJack_";

}//class PlayBlackJack
