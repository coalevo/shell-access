/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.casino.model;

import net.wimpi.telnetd.io.TemplateAttributes;
import net.coalevo.shellaccess.model.ShellIO;
import net.coalevo.shellaccess.cmd.casino.CasinoCommand;

import java.io.IOException;

/**
 * Anglo-american playing card (set of 52).
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class Card {

  private int m_Suit;
  private int m_Rank;
  private int m_Value;
  private boolean m_Visible = true;

  public Card(int suit, int rank) {
    if(suit >=0 && suit <= 3 && rank >=0 && rank <13) {
      m_Suit = suit;
      m_Rank = rank;
      if(rank > 0 && rank < 10) {
        m_Value = rank+1;
      } else if(rank > 9) {
        m_Value = 10;
      } else {
        m_Value = 1;
      }
    }
  }//coonstructor

  public int getRank() {
    return m_Rank;
  }//getRank

  public String getRankKey() {
    return RANK_KEYS[m_Rank];
  }
  public int getSuit() {
    return m_Suit;
  }//getSuit

  public String getSuitKey() {
    return SUIT_KEYS[m_Suit];
  }//getSuitKey

  public int getValue() {
    return m_Value;
  }//getValue

  public boolean isAce() {
    return m_Rank == 0;
  }//isAce

  public Card setVisible(boolean b) {
    m_Visible = b;
    return this;
  }//setVisible

  public boolean isVisible() {
    return m_Visible;
  }//isVisible

  public String toString() {
    return getSuitKey() + " " + getRankKey() + " " + getValue();
  }//toString

  public TemplateAttributes toAttributes(TemplateAttributes attr, ShellIO io)
      throws IOException {
    attr.add("card.{suit,rank}",new String[] {
        io.formatTemplate(CasinoCommand.PREFIX + getSuitKey()),
        io.formatTemplate(CasinoCommand.PREFIX + getRankKey())});
    return attr;
  }//toAttributes

  private static final String KEY_SPADES = "spades";
  private static final String KEY_HEARTS = "hearts";
  private static final String KEY_DIAMONDS = "diamonds";
  private static final String KEY_CLUBS = "clubs";

  private static final String[] SUIT_KEYS = {KEY_SPADES,KEY_HEARTS,KEY_DIAMONDS,KEY_CLUBS};


  private static final String KEY_ACE = "ace";    //0
  private static final String KEY_TWO = "two";    //1
  private static final String KEY_THREE = "three";//2
  private static final String KEY_FOUR = "four";  //3
  private static final String KEY_FIVE = "five";  //4
  private static final String KEY_SIX = "six";    //5
  private static final String KEY_SEVEN = "seven";//6
  private static final String KEY_EIGHT = "eight";//7
  private static final String KEY_NINE = "nine";  //8
  private static final String KEY_TEN = "ten";    //9
  private static final String KEY_JACK = "jack";  //10
  private static final String KEY_QUEEN = "queen";//11
  private static final String KEY_KING = "king";  //12

  private static final String[] RANK_KEYS = {
      KEY_ACE,KEY_TWO,KEY_THREE,KEY_FOUR,KEY_FIVE,KEY_SIX,KEY_SEVEN,
      KEY_EIGHT,KEY_NINE,KEY_TEN,KEY_JACK,KEY_QUEEN,KEY_KING
  };

}//class Card
