/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.casino.model;

import java.util.Random;
import java.security.SecureRandom;

/**
 * Provides two rolling dices.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class PairOfDice {

  private Random m_Random;
  private int m_One;
  private int m_Two;

  public PairOfDice() {
    m_Random = new SecureRandom();
    roll();
  }//constructor

  public void roll() {
    m_One = m_Random.nextInt(6) + 1;
    m_Two = m_Random.nextInt(6) + 1;
  }//roll

  public int firstDice() {
    return m_One;
  }//firstDice

  public int secondDice() {
   return m_Two;
  }//secondDice

  public int getValue() {
    return m_One + m_Two;
  }//getValue

  public boolean isValue(int v) {
    return v==getValue();
  }//isValue

  public int getNumberOfCombinations() {
    return getNumberOfCombinations(getValue());
  }//getNumberOfCombinations

  public double getProbability() {
    return getProbabilityOf(getValue());
  }//getProbability

  public String toString() {
    return "{" + m_One + "," + m_Two + ":" + getValue() + "}";
  }//toString

  public static int getNumberOfCombinations(int value) {
    if(value <=1 || value >12) {
      return 0;
    } else if(value >=1 && value <=7) {
      return (value - 1);
    } else {
      return (13-value);
    }
  }//getNumberOfCombinations

  public static double getProbabilityOf(int value) {
    if(value <=1 || value >12) {
      return 0d;
    } else if(value >=1 && value <=7) {
      return (double)(value - 1)/36;
    } else {
      return (double)(13-value)/36 ;
    }
  }//getProbabilityOf


  public static void main(String[] args) {
    PairOfDice dice = new PairOfDice();
    dice.roll();
    System.out.println(dice.toString());
    dice.roll();
    System.out.println(dice.toString());
    dice.roll();
    System.out.println(dice.toString());
    dice.roll();
    System.out.println(dice.toString());
    for(int i = 2; i<13; i++) {
      System.out.println("Value " + i +
          " #Combinations= " + getNumberOfCombinations(i) +
          " Probability= " + getProbabilityOf(i) +
          " Win = " + (1/(double)getNumberOfCombinations(i))*2d
      );
    }
  }//main

}//class PairOfDice
