/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.casino.model;

import net.wimpi.telnetd.io.TemplateAttributes;
import net.coalevo.shellaccess.model.ShellIO;

import java.util.*;
import java.io.IOException;

/**
 * Hand of cards.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public abstract class Hand {

  protected Set<Card> m_Cards;

  public Hand() {
  }//constructor

  public void addCard(Card c) {
    m_Cards.add(c);
  }//addCard

  public int cardCount() {
    return m_Cards.size();
  }//cardCount

  public void clear() {
    m_Cards.clear();
  }//clear

  public String toString() {
    return m_Cards.toString();
  }//toString

  public boolean turnCard() {
    for (Iterator<Card> iterator = m_Cards.iterator(); iterator.hasNext();) {
      Card card = iterator.next();
      if(!card.isVisible()) {
        card.setVisible(true);
        return true;
      }
    }
    return false;
  }//turnCard

  public void setAllVisible() {
    for (Iterator<Card> iterator = m_Cards.iterator(); iterator.hasNext();) {
      Card card = iterator.next();
      if(!card.isVisible()) {
        card.setVisible(true);
      }
    }
  }//setAllVisible

  public TemplateAttributes toAttributes(TemplateAttributes attr, ShellIO io)
      throws IOException {
    for (Iterator<Card> iterator = m_Cards.iterator(); iterator.hasNext();) {
      Card card = iterator.next();
      if(card.isVisible()) {
        card.toAttributes(attr,io);
      }
    }
    return attr;
  }//toAttributes

}//class Hand
