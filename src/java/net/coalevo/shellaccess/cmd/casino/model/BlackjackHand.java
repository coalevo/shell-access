/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.casino.model;

import java.util.Iterator;
import java.util.Comparator;
import java.util.TreeSet;

/**
 * A hand for blackjack, that can provide the hand value.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class BlackjackHand extends Hand {

  private int m_State;

  public BlackjackHand() {
     m_Cards = new TreeSet<Card>(new CardComparator());
  }//BlackjackHand

  public void addCard(Card c) {
    super.addCard(c);
    int v = getValue();
    if (v < 22) {
      m_State = STATE_GOOD;
      if (v == 21 && this.m_Cards.size() == 2) {
        m_State = STATE_BLACKJACK;
      }
    } else {
      m_State = STATE_BUSTED;
    }
  }//addCard

  public boolean isBusted() {
    return m_State == STATE_BUSTED;
  }//isBusted

  public boolean isGood() {
    return m_State == STATE_GOOD;
  }//isGood

  public boolean isBlackjack() {
    return m_State == STATE_BLACKJACK;
  }//isBlackjack

  public int getValue() {
    int val = 0;
    for (Iterator iterator = m_Cards.iterator(); iterator.hasNext();) {
      Card card = (Card) iterator.next();
      if (card.isVisible()) {
        if (card.isAce()) {
          if (val >= 11) {
            val += 1;
          } else {
            val += 11;
          }
        } else {
          val += card.getValue();
        }
      }
    }
    return val;
  }//getValue

  public String toString() {
    return super.toString() + ":" + getValue();
  }//toString

  private class CardComparator
       implements Comparator<Card> {

     public int compare(Card c1, Card c2) {
       int r1 = c1.getRank();
       int r2 = c2.getRank();

       int cmp = r2-r1;

       if(cmp == 0) {
         r1 =  c1.getSuit();
         r2 =  c2.getSuit();
         cmp = r2-r1;
       }
       return cmp;
     }//compare

   }//RankComparator

  public static final int STATE_GOOD = 0;
  public static final int STATE_BUSTED = -1;
  public static final int STATE_BLACKJACK = 1;

}//class BlackjackHand
