/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.casino.model;

import java.util.*;
import java.security.SecureRandom;

/**
 * An anglo-american playing cards deck of 52 cards.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class Deck {

  private List<Card> m_Cards;
  private Random m_Random;

  public Deck() {
    m_Random = new SecureRandom();
  }//Deck

  public void shuffle() {
    m_Cards = new ArrayList<Card>(52);
    for (int i = 0; i < 4; i++ ) {
      for (int j = 0; j < 13; j++ ) {
        m_Cards.add(new Card(i,j));
      }
    }
    //all good things are three
    Collections.shuffle(m_Cards,m_Random);
    Collections.shuffle(m_Cards,m_Random);
    Collections.shuffle(m_Cards,m_Random);
  }//shuffle

  public int cardsLeft() {
    return m_Cards.size();
  }//cardsLeft

  public boolean hasCards() {
    return !m_Cards.isEmpty();
  }//hasCards

  public Card dealCard() {
    if(m_Cards.isEmpty()) {
      throw new NoSuchElementException();
    }
    return (Card) m_Cards.remove(0);
  }//dealCard

  public static void main(String[] args) {
    Deck d = new Deck();
    System.out.println(d.m_Cards.size() + " cards");
    while(d.hasCards()) {
      System.out.println(d.dealCard().toString());
    }
    d.shuffle();
    System.out.println("Hand");
    BlackjackHand h = new BlackjackHand();
    boolean done = false;
    do {
      Card c = d.dealCard();
      System.out.println("Adding card " + c.toString());
      h.addCard(c);
      System.out.println(h.toString());
      if(h.isBlackjack()) {
        System.out.println("BlackJack!!!!!");
        done = true;
      } else if(h.isBusted()) {
         System.out.println("Busted!!!!!");
         done = true;
      }
    } while (!done);
  }//main

}//class Deck
