/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.casino;

import net.coalevo.bank.model.BankServiceException;
import net.coalevo.bank.service.BankService;
import net.coalevo.foundation.model.Agent;
import net.coalevo.shellaccess.cmd.BaseCommand;
import net.coalevo.shellaccess.impl.Activator;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.Services;
import net.coalevo.shellaccess.validators.IntegerInputValidator;
import net.wimpi.telnetd.io.toolkit.Editfield;
import org.slf4j.MarkerFactory;
import org.slf4j.Marker;

import java.io.IOException;

/**
 * A base class for casino commands.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public abstract class CasinoCommand
    extends BaseCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(CasinoCommand.class.getName());
  protected BankService m_Bank;
  protected Agent m_ServiceAgent;

  public boolean prepare(BasicShell shell) {
    super.prepare(shell);
    m_ServiceAgent = Activator.getServices().getServiceAgent();
    m_Bank = Activator.getServices().getBankService(Services.NO_WAIT);
    if (m_Bank == null) {
      try {
        m_IO.printTemplate(PREFIX + "bankunavailable");
      } catch (IOException e) {
        log.error(c_LogMarker,"prepare()", e);
      }
      return false;
    }
    return checkAccount();
  }//prepare

  public void reset() {
    super.reset();
    m_IO = null;
    m_Bank = null;
    m_ServiceAgent = null;
  }//reset

  public void reportAccountStatus() throws IOException {
    m_IO.printTemplate(PREFIX + "account", "sum", String.valueOf(getMoney()));
  }//reportAccountStatus

  public int getBet(int limit) throws IOException {
    //adjust limit
    limit = (int) Math.min(getMoney(), limit);

    m_IO.printTemplate(PREFIX + "betlabel", "limit", String.valueOf(limit));
    Editfield ef = new Editfield(m_IO.getTerminalIO(), "bet", 15, 15);
    ef.setIgnoreDelete(true);
    ef.setInputValidator(
        new IntegerInputValidator(
            0,
            limit,
            m_IO.formatTemplate("inputerror_intvalidator","min",Integer.toString(0),"max",Integer.toString(limit))
        )
    );
    ef.run();
    //is already validated
    return Integer.parseInt(ef.getValue().trim());
  }//getBet

  public void checkEvents() throws IOException {
    if (m_IO.hasEventsInBuffer()) {
      //2. prompt yes no
      m_IO.printTemplate(PREFIX + "askevents", "num", String.valueOf(m_IO.getEventsInBufferCount()));
      //out question, have count :)
      if (m_IO.getDecision()) {
        //yes
        m_IO.flushEventBuffer();
      }
    }
  }//checkEvents

  public boolean checkContinue() throws IOException {
    if (hasMoney()) {
      m_IO.printTemplate(PREFIX + "askcontinue");
      return m_IO.getDecision();
    } else {
      m_IO.printTemplate(PREFIX + "outofmoney");
      return false;
    }
  }//checkContinue

  public void loose(int bet) throws IOException {
    try {
      m_Bank.retrieve(m_ServiceAgent, m_AgentIdentifier, bet);
    } catch (BankServiceException bex) {
      log.error(c_LogMarker,"loose()", bex);
    }
    m_IO.printTemplate(PREFIX + "lost", "amount", String.valueOf(bet));
  }//loose

  public void win(int bet) throws IOException {
    try {
      m_Bank.deposit(m_ServiceAgent, m_AgentIdentifier, bet);
    } catch (BankServiceException bex) {
      log.error(c_LogMarker,"win()", bex);
    }
    m_IO.printTemplate(PREFIX + "won", "amount", String.valueOf(bet));
  }//win

  public long getMoney() {
    try {
      return m_Bank.getAccountBalance(m_ServiceAgent, m_AgentIdentifier);
    } catch (BankServiceException bex) {
      log.error(c_LogMarker,"getMoney()", bex);
    }
    return 0;
  }//getMoney

  public boolean hasMoney() {
    return getMoney() > 0;
  }//hasMoney

  protected boolean checkAccount() {
    try {
      if (!m_Bank.existsAccount(m_ServiceAgent, m_AgentIdentifier)) {
        if (promptDecision(PREFIX + "askopenaccount"))  {
          m_Bank.openAccount(m_ServiceAgent,m_AgentIdentifier);
          m_Bank.deposit(m_ServiceAgent,m_AgentIdentifier,100);
          m_IO.printTemplate(PREFIX + "confirmaccount");
          return true;
        } else {
          m_IO.printTemplate(PREFIX + "noaccount");
          return false;
        }
      } else {
        if(!hasMoney()) {
          m_IO.printTemplate(PREFIX + "outofmoney");
          return false;
        }
        return true;
      }
    } catch (Exception bex) {
      log.error(c_LogMarker,"checkAccount()", bex);
      return false;
    }
  }//checkAccount

  public static final String TPREFIX = "commands_net.coalevo.shellaccess.cmd.casino.";
  public static final String PREFIX = TPREFIX + "CasinoCommand_";

}//class CasinoCommand
