/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.postoffice;

import net.coalevo.postoffice.model.NoSuchPOBoxException;
import net.coalevo.postoffice.model.POBox;
import net.coalevo.postoffice.model.PostOfficeServiceException;
import net.coalevo.postoffice.service.PostOfficeService;
import net.coalevo.shellaccess.cmd.BaseCommand;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.Services;

/**
 * This class implements an abstract base class for post office commands.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public abstract class PostOfficeCommand
    extends BaseCommand {

  protected PostOfficeService m_PostOfficeService;
  protected POBox m_PostBox;

  public boolean prepare(BasicShell shell) {
    // Prepare superclass
    if (!super.prepare(shell)) {
      return false;
    }
    // Prepare post office
    m_PostOfficeService = shell.getServices().getPostOfficeService(Services.NO_WAIT);

    if (m_PostOfficeService == null) {
      try {
        m_IO.printTemplate(PREFIX + "serviceunavailable");
      } catch (Exception ex) {
        log.error(c_LogMarker, "prepare()", ex);
      }
      return false;
    }
    //Prepare POBox
    try {
      m_PostBox = m_PostOfficeService.getPostBox(m_Agent, m_AgentIdentifier);
    } catch (NoSuchPOBoxException pbex) {
      //try create
      try {
        m_PostBox = m_PostOfficeService.createPostBox(
            m_Agent,
            m_AgentIdentifier,
            m_IO.getLocale(),
            false,
            true,
            false
        );
      } catch (PostOfficeServiceException ex) {
        log.error(c_LogMarker, "prepare()", ex);
      }
    } catch (PostOfficeServiceException poex) {
      log.error(c_LogMarker, "prepare()", poex);
      return false;
    }

    return true;
  }//prepare

  public void reset() {
    super.reset();
    m_PostOfficeService = null;
    m_PostBox = null;
    m_IO = null;
  }//reset

  /*
  private void setMailPrompt() {
    if (m_PostBox != null) {
      HashMap<String, String> map = new HashMap<String, String>();
      map.put("numnew", Integer.toString(m_NewMessages.size()));
      m_Shell.setEnvironmentVariable(BasicShell.PROMPT_ATTRIBUTES_KEY, map);
    }
  }//setActualForumPrompt  

  */

  public static final String TS_PREFIX = "commands_net.coalevo.shellaccess.cmd.postoffice.";
  private static final String PREFIX = TS_PREFIX + "PostOfficeCommand_";

}//class PostOfficeCommand
