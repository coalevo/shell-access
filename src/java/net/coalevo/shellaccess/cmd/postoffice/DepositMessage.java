/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.postoffice;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.postoffice.model.EditableMessage;
import net.coalevo.postoffice.model.NoSuchPOBoxException;
import net.coalevo.postoffice.model.PostOfficeServiceException;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * This class implements a {@link PostOfficeCommand} that provides
 * the functionality to deposit a new message.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class DepositMessage
    extends PostOfficeCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(DepositMessage.class.getName());
  protected boolean m_Quick = false;

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell)
      throws CommandException {

    if (!prepare(shell)) {
      throw new CommandException();
    }

    try {
      EditableMessage emsg = null;
      try {
        m_PromptWrap = false;
        AgentIdentifier aid = promptAgent(PREFIX + "promptagent");
        if(aid == null) {
          m_IO.printTemplate(PREFIX + "nosuchagent");
          return;
        }
        emsg = m_PostOfficeService.beginMessage(m_Agent, aid);
        doMessage(emsg);
      } catch (IllegalArgumentException iex) {
        m_IO.printTemplate(PREFIX + "notself");
      } catch (NoSuchPOBoxException pex) {
        m_IO.printTemplate(PREFIX + "notpobox");
        return;
      } catch (PostOfficeServiceException dsex) {
        if (emsg != null) {
          m_PostOfficeService.cancelMessage(m_Agent, emsg);
        }
        m_IO.printTemplate(PREFIX + "failed");
        return;
      } catch (SecurityException ex) {
        m_IO.printTemplate(PREFIX + "notallowed");
        return;
      }
    } catch (Exception ex) {
      log.error(c_LogMarker, "run()", ex);
    }


  }//run

  public void reset() {
    m_Quick = false;
    super.reset();
  }//reset

  protected void doMessage(EditableMessage emsg) throws Exception {
    if (m_Quick) {
      emsg.setBodyFormat("");
      m_IO.printTemplate(PREFIX + "body");
      String val = simpleEdit(null);
      if (val == null || val.length() == 0) {
        m_IO.printTemplate(PREFIX + "aborted");
        return;
      } else {
        emsg.getMessageBody().setContent(val);
        m_IO.println();
      }
    } else {
      m_PromptWrap = true;
      String tmp = prompt(PREFIX + "subject");
      if (tmp == null || tmp.length() == 0) {
        m_IO.printTemplate(PREFIX + "aborted");
        return;
      } else {
        emsg.setSubject(tmp);
      }
      String f = promptInputFormat(null);
      emsg.setBodyFormat(f);

      String txt = editTransformableContent(f, null);
      if (txt == null || txt.length() == 0) {
        m_IO.printTemplate(PREFIX + "aborted");
        return;
      }
      emsg.getMessageBody().setContent(txt);
    }

    if (isUserExpert() || promptDecision(PREFIX + "doit")) {
      m_PostOfficeService.depositMessage(m_Agent, emsg);
      m_IO.printTemplate(PREFIX + "confirm");
    } else {
      m_PostOfficeService.cancelMessage(m_Agent, emsg);
      m_IO.printTemplate(PREFIX + "aborted");
    }
  }//addMessage

  protected static final String PREFIX = TS_PREFIX + "DepositMessage_";

}//class DepositMessage
