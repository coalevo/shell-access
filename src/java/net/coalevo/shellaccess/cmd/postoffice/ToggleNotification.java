/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.postoffice;

import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;

/**
 * This class provides a command that allows to toggle the
 * notification configuration of the postbox.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class ToggleNotification
    extends PostOfficeCommand {

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell)
      throws CommandException {
    prepare(shell);

    try {
      boolean notify = m_PostBox.isNotify();
      m_PromptWrap = false;
      m_IO.printTemplate(PREFIX + "ask", (notify) ? "on" : "off", "");
      if (m_IO.getDecision()) {
        //toggle
        m_PostBox.setNotify(!notify);
        m_IO.printTemplate(PREFIX + "confirm", (!notify) ? "on" : "off", "");
      } else {
        //not toggle
        m_IO.printTemplate(PREFIX + "unchanged");
      }

    } catch (Exception ex) {
      log.error(c_LogMarker, "ToggleNotification::run()", ex);
    }
  }//run

  protected static final String PREFIX = TS_PREFIX + "ToggleNotification_";

}//class ToggleNotification