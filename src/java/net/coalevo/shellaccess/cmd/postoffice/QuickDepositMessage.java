/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.postoffice;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.postoffice.model.EditableMessage;
import net.coalevo.postoffice.model.NoSuchPOBoxException;
import net.coalevo.postoffice.model.PostOfficeServiceException;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * This class implements a {@link PostOfficeCommand} that provides
 * the functionality to deposit a new message.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class QuickDepositMessage
    extends DepositMessage {

  private static Marker c_LogMarker = MarkerFactory.getMarker(QuickDepositMessage.class.getName());


  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell)
      throws CommandException {

    if (!prepare(shell)) {
      throw new CommandException();
    }

    try {
      EditableMessage emsg = null;
      try {
        m_PromptWrap = false;        
        AgentIdentifier aid = promptAgent(PREFIX + "promptagent");
        if(aid == null) {
          m_IO.printTemplate(PREFIX + "nosuchagent");
          return;
        }
        emsg = m_PostOfficeService.beginMessage(m_Agent, aid);
        m_Quick = true;
        doMessage(emsg);
      } catch (NoSuchPOBoxException pex) {
        m_IO.printTemplate(PREFIX + "notpobox");
        return;
      } catch (PostOfficeServiceException dsex) {
        if (emsg != null) {
          m_PostOfficeService.cancelMessage(m_Agent, emsg);
        }
        m_IO.printTemplate(PREFIX + "failed");
        return;
      } catch (SecurityException ex) {
        m_IO.printTemplate(PREFIX + "notallowed");
        return;
      }
    } catch (Exception ex) {
      log.error(c_LogMarker, "run()", ex);
    }

  }//run

}//class DepositMessage