/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.postoffice;

import net.coalevo.postoffice.model.MessageBody;
import net.coalevo.postoffice.model.MessageDescriptor;
import net.coalevo.postoffice.model.PostOfficeServiceException;
import net.coalevo.shellaccess.impl.CommandShell;
import net.coalevo.shellaccess.impl.Formatter;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.shellaccess.validators.IntegerInputValidator;
import net.wimpi.telnetd.io.BasicTerminalIO;
import net.wimpi.telnetd.io.TemplateAttributes;
import net.wimpi.telnetd.io.toolkit.Editfield;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;

/**
 * This class implements a base class for reading messages.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class ReadMessages
    extends PostOfficeCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(ReadMessages.class.getName());

  protected boolean m_Pending = false;
  protected boolean m_Backreading = false;
  protected String m_Prompt;

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell)
      throws CommandException {
    prepare(shell);
    try {
      try {

        int maxnum = m_PostBox.getTotalMessageCount();
        if (maxnum <= 0) {
          m_IO.printTemplate(PREFIX + "nomessages");
          return;
        }
        //prompt
        TemplateAttributes attr = m_IO.leaseTemplateAttributes();
        attr.add("max", Integer.toString(maxnum));
        m_IO.printTemplate(PREFIX + "nummessages", attr);

        Editfield ef = new Editfield(m_IO.getTerminalIO(), "input", 15, 50);
        ef.setIgnoreDelete(true);
        ef.setInputValidator(
            new IntegerInputValidator(
                0,
                maxnum,
                m_IO.formatTemplate(
                    "inputerror_intvalidator",
                    "min",
                    Integer.toString(0),
                    "max",
                    Integer.toString(maxnum)
                )
            )
        );
        ef.run();
        String val = ef.getValue();
        int nummsg = 0;
        if(val != null && val.trim().length() > 0) {
          nummsg = Integer.parseInt(ef.getValue().trim());
        }
        if (nummsg == 0) {
          m_IO.printTemplate(PREFIX + "aborted");
          return;
        }
        List<String> mids =
            m_PostBox.listMessages(0, nummsg);
        navigateMessages(mids);
      } catch (SecurityException ex) {
        m_IO.printTemplate(PREFIX + "notallowed");
      } catch (PostOfficeServiceException e) {
        m_IO.printTemplate(PREFIX + "failed");
      }
    } catch (Exception ex) {
      log.error(c_LogMarker, "run()", ex);
    }
  }//run

  protected void navigateMessages(List<String> mids) throws Exception {
    boolean done = false;
    boolean forward = true;
    String mid = null;

    //ensure a prompt is set
    if (m_Prompt == null) {
      m_Prompt = PREFIX + "navprompt";
    }
    ListIterator<String> iter = mids.listIterator();

    if (m_Backreading) {
      //1. spool iterator to last
      while (iter.hasNext()) iter.next();
      //2. set reading direction to backwards
      forward = false;
      mid = iter.previous();
    } else {
      mid = iter.next();
    }

    char fwd = m_IO.formatTemplate("forward").charAt(0);
    char bwd = m_IO.formatTemplate("backward").charAt(0);
    char stop = m_IO.formatTemplate("stop").charAt(0);
    char delete = m_IO.formatTemplate("delete").charAt(0);

    char next = ' '; //always space
    BasicTerminalIO bio = m_IO.getTerminalIO();
    boolean skipprompt = false;
    //display first entry
    displayMessage(mid);

    do {
      TemplateAttributes attr = null;
      if (!skipprompt) {
        //print prompt
        attr = m_IO.leaseTemplateAttributes();
        attr.add("nummsgs", Integer.toString(mids.size()));
        if (forward) {
          attr.add("numremain", Integer.toString(iter.nextIndex()));
          attr.add("forward", "");
        } else {
          attr.add("numremain", Integer.toString(iter.nextIndex() + 1));
          attr.add("backward", "");
        }
        //add subclass attr.
        addTemplateAttributes(attr);
      }
      //Allow async event I/O
      m_IO.setAsyncEventIO(true);
      char ch = (char) m_IO.prompt(m_Prompt, attr, skipprompt);
      m_Shell.getSession().activity();
      m_IO.setAsyncEventIO(false);

      if (ch == fwd) {
        if (!forward) {
          iter.next();
          forward = true;
        }
      } else if (ch == bwd) {
        if (forward) {
          iter.previous();
          forward = false;
        }
      } else if (ch == delete) {
        try {
          if (promptDecision(PREFIX + "dodelete")) {
            m_PostBox.removeMessage(mid);
            m_IO.printTemplate(PREFIX + "confirmdelete");
            iter.remove();
          } else {
            continue;
          }
        } catch (SecurityException ex) {
          m_IO.printTemplate(PREFIX + "notalloweddelete");
          continue;  //prompt
        } catch (PostOfficeServiceException e) {
          m_IO.printTemplate(PREFIX + "faileddelete");
          continue; //prompt
        }
        //drop through to get the next entry in actual direction!
      } else if (ch == stop) {
        done = true;
        continue;
      } else if (ch != next) {
        //try run longprompt command
        if (!((CommandShell) m_Shell).runLongPromptCommand((int) ch)) {
          bio.bell();
          skipprompt = true;
        }
        continue;
      }
      if (forward && iter.hasNext()) {
        mid = iter.next();
        skipprompt = false;
        //print entry
        displayMessage(mid);
        continue;
      }
      if (!forward && iter.hasPrevious()) {
        mid = iter.previous();
        skipprompt = false;
        //print entry
        displayMessage(mid);
        continue;
      }
      done = true;
    } while (!done);
    m_IO.println();
  }//navigateEntries

  protected void displayMessage(String mid) throws Exception {
    //1. Descriptor
    MessageDescriptor md = m_PostBox.getMessageDescriptor(mid);
    //2. Content
    MessageBody mb = m_PostBox.getMessageBody(mid);
    //3. Prepare date format
    DateFormat df = getStandardDateTimeFormat();
    //4. Attributes and page template
    TemplateAttributes attr = m_IO.leaseTemplateAttributes();

    attr.add("mid", md.getIdentifier());
    attr.add("from", getFormattedAgent(md.getFrom()));
    if(md.getTo() != null) {
      attr.add("to", getFormattedAgent(md.getTo()));
    }
    if (md.isListMessage()) {
      attr.add("via", md.getVia());
    }
    //flag received messages
    if (md.isReceived()) {
      attr.add("received", "");
    }
    String tmp = md.getSubject();
    if (tmp != null && tmp.length() > 0) {
      attr.add("subject", tmp);
    }
    attr.add("deposited", df.format(new Date(md.getTimestamp())));

    if (!mb.isEmpty()) {
      attr.add("body", Formatter.format(md.getBodyFormat(), mid, mb.getContent()));
    }

    boolean ewr = m_Shell.getSession().isEventsWhileReading();
    m_IO.setAsyncEventIO(ewr);
    m_IO.pageTemplate(PREFIX + "message", attr);
    m_IO.setAsyncEventIO(false);
    m_IO.flushEventBuffer();
    if (!md.isRead()) {
      m_PostBox.markRead(mid);
    }
  }//displayEntry

  protected void addTemplateAttributes(TemplateAttributes attr) {
    return;
  }//addTemplateAttributes

  public void reset() {
    m_Pending = false;
    m_Backreading = false;
    m_Prompt = null;
    super.reset();
  }//reset

  protected static final String PREFIX = TS_PREFIX + "ReadMessages_";

}//class ReadMessages
