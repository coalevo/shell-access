/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.postoffice;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * This class provides a command that allows to add a user agent to
 * the blacklisted senders (i.e. prevents them from further message
 * deposits).
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class AddBlacklisted
    extends PostOfficeCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(AddBlacklisted.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell)
      throws CommandException {

    if (!prepare(shell)) {
      throw new CommandException();
    }
    try {
      m_PromptWrap = false;
      AgentIdentifier aid = promptAgent(PREFIX + "promptagent");
      if(aid == null) {
        m_IO.printTemplate(PREFIX + "nosuchagent");
        return;
      }

      if (m_PostBox.addBlacklisted(aid)) {
        m_IO.printTemplate(PREFIX + "confirm", "agent", this.getFormattedAgent(aid));
      } else {
        m_IO.printTemplate(PREFIX + "unchanged");
      }

    } catch (Exception ex) {
      log.error(c_LogMarker, "AddBlacklisted::run()", ex);
    }

  }//run

  protected static final String PREFIX = TS_PREFIX + "AddBlacklisted_";

}//class AddBlacklisted
