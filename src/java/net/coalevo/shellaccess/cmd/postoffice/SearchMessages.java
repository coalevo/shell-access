/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.postoffice;

import net.coalevo.postoffice.model.MessageHit;
import net.coalevo.postoffice.model.PostOfficeServiceException;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.wimpi.telnetd.io.TemplateAttributes;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * This class provides a command for searching messages.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class SearchMessages
    extends ReadMessages {

  private static Marker c_LogMarker = MarkerFactory.getMarker(SearchMessages.class.getName());
  protected String m_Query;

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell)
      throws CommandException {
    prepare(shell);
    try {
      try {

        //prompt
        m_PromptWrap = false;
        if(m_PostBox.isIndexRebuild()) {
           m_IO.printTemplate(PREFIX + "indexrebuild");
           if (!m_IO.getDecision()) {
             return;
           } else {
             m_IO.println();
           }
        }
        String query = prompt(PREFIX + "query");
        if (query == null || query.length() == 0) {
          m_IO.printTemplate(PREFIX + "aborted");
          return;
        }
        List<MessageHit> hits =
            m_PostBox.searchMessages(query, 50);
        if (hits.isEmpty()) {
          m_IO.printTemplate(PREFIX + "nomessages");
          return;
        }
        List<String> mids = new ArrayList<String>(hits.size());
        for (MessageHit hit : hits) {
          mids.add(hit.getMessageIdentifier());
        }
        m_Prompt = PREFIX + "navprompt";
        navigateMessages(mids);
      } catch (SecurityException ex) {
        m_IO.printTemplate(PREFIX + "notallowed");
      } catch (PostOfficeServiceException e) {
        m_IO.printTemplate(PREFIX + "failed");
      }
    } catch (Exception ex) {
      log.error(c_LogMarker, "run()", ex);
    }
  }//run

  protected void addTemplateAttributes(TemplateAttributes attr) {
    attr.add("query", m_Query);
  }//addTemplateAttributes

  public void reset() {
    m_Query = null;
    m_Prompt = null;
    super.reset();
  }//reset

  protected static final String PREFIX = TS_PREFIX + "SearchMessages_";

}//class SearchMessages
