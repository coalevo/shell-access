/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.postoffice;

import net.coalevo.postoffice.model.PostOfficeServiceException;
import net.coalevo.shellaccess.impl.Activator;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.util.List;

/**
 * This class implements a command that will print info about new messages.
 * <p/>
 * May be run as start command.
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class CheckForNewMessages
    extends ReadMessages {

  private static Marker c_LogMarker = MarkerFactory.getMarker(CheckForNewMessages.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell)
      throws CommandException {
    if (!prepare(shell)) {
      throw new CommandException();
    }
    try {
      try {
        if (!m_PostBox.hasUnreadMessages()) {
          if (!shell.isEnviromentVariableSet("CommandShell.start")) {
            m_IO.printTemplate(PREFIX + "nonewmessages");
          }
          return;
        }
        List<String> newmsgs = m_PostBox.listUnreadMessages();
        if(newmsgs.size() == 1) {
          m_IO.printTemplate(PREFIX + "newmessages");
        } else {
          m_IO.printTemplate(PREFIX + "newmessages", "num", Integer.toString(newmsgs.size()));
        }
        if (promptDecision(PREFIX + "readnow")) {
          navigateMessages(newmsgs);
        } else {
          m_IO.println();
        }
      } catch (PostOfficeServiceException poex) {
        m_IO.printTemplate(PREFIX + "failed");
      }

    } catch (Exception ex) {
      Activator.log().error(c_LogMarker, "run()", ex);
    }
  }//run

  protected static final String PREFIX = TS_PREFIX + "CheckForNewMessages_";

}//class CheckForNewMessages
