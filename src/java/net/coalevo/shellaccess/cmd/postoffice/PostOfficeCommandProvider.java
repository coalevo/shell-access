/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.postoffice;

import net.coalevo.shellaccess.model.BaseShellCommandProvider;

/**
 * A {@link net.coalevo.shellaccess.model.ShellCommandProvider}
 * for the post office commands.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class PostOfficeCommandProvider
    extends BaseShellCommandProvider {

  public PostOfficeCommandProvider() {
    super(PostOfficeCommandProvider.class.getPackage().getName());
    prepare();
  }//constructor

  private void prepare() {
    //Configuration
    addCommand("ToggleForwarding", ToggleForwarding.class);
    addCommand("TogglePrivacy", TogglePrivacy.class);
    addCommand("ToggleNotification", ToggleNotification.class);

    //Messages
    addCommand("ReadMessages", ReadMessages.class);
    addCommand("CheckForNewMessages", CheckForNewMessages.class);
    addCommand("SearchMessages", SearchMessages.class);
    addCommand("DepositMessage", DepositMessage.class);
    addCommand("QuickDepositMessage", QuickDepositMessage.class);

    //List
    addCommand("DepositListMessage", DepositListMessage.class);
    addCommand("ShowListMemberships", ShowListMemberships.class);

    //Blacklist
    addCommand("AddBlacklisted", AddBlacklisted.class);
    addCommand("RemoveBlacklisted", RemoveBlacklisted.class);
    addCommand("ListBlacklisted", ListBlacklisted.class);

    //admin
    addCommand("RebuildIndex", RebuildIndex.class);

    //Yell
    addCommand("Yell", Yell.class);
    
    //addCommand("", .class);
  }//prepare

}//class PostOfficeCommandProvider
