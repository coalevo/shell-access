/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.postoffice;

import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.wimpi.telnetd.io.TemplateAttributes;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.util.Iterator;
import java.util.List;

/**
 * This class provides a command that shows all list memberships.
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class ShowListMemberships
    extends PostOfficeCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(ShowListMemberships.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell)
      throws CommandException {

    if (!prepare(shell)) {
      throw new CommandException();
    }
    try {
      m_PromptWrap = false;
      List<String> list = m_PostOfficeService.getListMemberships(m_Agent,m_AgentIdentifier);
      if (list.isEmpty()) {
        m_IO.printTemplate(PREFIX + "none");
        return;
      }
      TemplateAttributes attr = m_IO.leaseTemplateAttributes();
      for (Iterator<String> iter = list.iterator(); iter.hasNext();) {
        String ln = iter.next();
        String[] items;
        if(m_PostOfficeService.isSendToListAllowed(m_Agent, ln)) {
          items = new String[] {
              ln,
              ""
          };
        } else {
          items = new String[] {
              ln,
              null
          };
        }
        attr.add("lists.{name,isallowed}", items);

      }
      m_IO.pageTemplate(PREFIX + "list", attr);

    } catch (Exception ex) {
      log.error(c_LogMarker, "ShowListMemberships::run()", ex);
    }
  }//run

  protected static final String PREFIX = TS_PREFIX + "ShowListMemberships_";

}//class ShowListMemberships