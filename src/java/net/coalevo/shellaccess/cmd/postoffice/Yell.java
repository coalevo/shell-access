/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.postoffice;

import net.coalevo.postoffice.model.EditableMessage;
import net.coalevo.postoffice.model.PostOfficeServiceException;
import net.coalevo.postoffice.model.NoSuchListException;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.shellaccess.impl.Activator;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * This class implements a {@link PostOfficeCommand} that provides
 * the functionality to deposit a yell message to the admins mailing list.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class Yell
    extends DepositMessage {

  private static Marker c_LogMarker = MarkerFactory.getMarker(Yell.class.getName());


  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell)
      throws CommandException {

    if (!prepare(shell)) {
      throw new CommandException();
    }

    try {
      EditableMessage emsg = null;
      try {
        m_IO.printTemplate(TS_PREFIX + "Yell_warn");
        m_PromptWrap = false;
        emsg = m_PostOfficeService.beginListMessage(
            Activator.getServices().getServiceAgent(),
            "admins",
            m_AgentIdentifier
        );
        emsg.setSubject("Yell");
        m_Quick = true;
        doMessage(emsg);
      } catch (NoSuchListException pex) {
        m_IO.printTemplate(TS_PREFIX + "Yell_noadminslist");
        return;
      } catch (PostOfficeServiceException dsex) {
        if (emsg != null) {
          m_PostOfficeService.cancelMessage(m_Agent, emsg);
        }
        m_IO.printTemplate(PREFIX + "failed");
        return;
      } catch (SecurityException ex) {
        m_IO.printTemplate(PREFIX + "notallowed");
        return;
      }
    } catch (Exception ex) {
      log.error(c_LogMarker, "run()", ex);
    }

  }//run

}//class Yell