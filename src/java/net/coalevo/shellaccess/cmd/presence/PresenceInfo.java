/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.presence;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.presence.model.Presence;
import net.coalevo.presence.model.PresenceProxy;

import java.util.Iterator;

/**
 * Defines an interface to access presence information.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface PresenceInfo {

  /**
   * Returns the owner of this instance as {@link Presence}.
   *
   * @return the owner as {@link Presence}.
   */
  public Presence getOwner();

  /**
   * Tests if a specific {@link AgentIdentifier} is present.
   *
   * @param aid an {@link AgentIdentifier}.
   * @return true if present, false otherwise.
   */
  public boolean isPresent(AgentIdentifier aid);

  /**
   * Tests if an agent with the given {@link AgentIdentifier}
   * is present.
   *
   * @param aid an {@link AgentIdentifier}.
   * @return true if available, false otherwise.
   */
  public boolean isAvailable(AgentIdentifier aid);

  /**
   * Tests if an agent with the given {@link AgentIdentifier}
   * is temporarily unavailable.
   *
   * @param aid an {@link AgentIdentifier}.
   * @return true if temp. unavailable, false otherwise.
   */
  public boolean isTemporaryUnavailable(AgentIdentifier aid);

  /**
   * Returns the {@link PresenceProxy} for a given
   * {@link AgentIdentifier} if available.
   *
   * @param aid an {@link AgentIdentifier}.
   * @return a {@link PresenceProxy} for the available agent.
   */
  public PresenceProxy getPresenceFor(AgentIdentifier aid);

  /**
   * Returns an <tt>Iterator</tt> over all {@link AgentIdentifier}
   * instances of all present agents.
   *
   * @param advertised flag that indicates whether or not to include advertised presences.
   * @return an <tt>Iterator</tt> over {@link AgentIdentifier} instances.
   */
  public Iterator listPresent(boolean advertised);

  /**
   * Resolves the nickname for an {@link AgentIdentifier}.
   *
   * @param aid an {@link AgentIdentifier}.
   * @return the nickname.
   */
  public String resolveNickname(AgentIdentifier aid);

}//interface PresenceInfo
