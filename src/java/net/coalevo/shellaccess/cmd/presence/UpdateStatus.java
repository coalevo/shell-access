/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.presence;

import net.coalevo.presence.model.PresenceStatus;
import net.coalevo.presence.model.PresenceStatusType;
import net.coalevo.presence.model.PresenceStatusTypes;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.wimpi.telnetd.io.TemplateAttributes;
import net.wimpi.telnetd.io.toolkit.BufferOverflowException;
import net.wimpi.telnetd.io.toolkit.Editline;
import net.wimpi.telnetd.io.toolkit.MaskedSelection;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.IOException;

/**
 * Updates the presence status.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class UpdateStatus
    extends PresenceCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(UpdateStatus.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell) throws CommandException {
    if (!prepare(shell)) {
      return;
    }
    try {
      //1. output actual status
      TemplateAttributes attr = m_IO.leaseTemplateAttributes();
      PresenceStatus st = m_Presence.getStatus();
      String type = m_IO.formatTemplate(
          STATUS_PREFIX + st.getType().getIdentifier()
      );
      attr.add("type", type);
      attr.add("desc", st.getDescription());
      m_IO.printTemplate(PREFIX + "actualstatus", attr);

      //2. get decision
      //m_IO.printTemplate(PREFIX + "decidechange");
      //if (!m_IO.getDecision()) {
      //outta here
      //  m_IO.printTemplate(PREFIX + "nochange", attr);
      //  return;
      //}

      //3. Get type and description
      m_IO.printTemplate(PREFIX + "typelabel");
      MaskedSelection sel = getTypeSelection();
      sel.initialSelect(st.getType());
      sel.run();
      PresenceStatusType newtype = (PresenceStatusType) sel.getSelectedValue();
      //handle no change option
      if (NO_CHANGE.equals(newtype)) {
        m_IO.printTemplate(PREFIX + "nochange");
        return;
      }

      m_IO.printTemplate(PREFIX + "desclabel");
      Editline el = new Editline(m_IO.getTerminalIO(), "description");
      el.setIgnoreDelete(true);
      try {
        el.setValue(st.getDescription());
      } catch (BufferOverflowException ex) {
        //handling?
      }
      el.run();
      String newdesc = el.getValue();

      //4. Update status if required
      if (!newtype.equals(st.getType()) || !newdesc.equals(st.getDescription())) {
        st.setType(newtype);
        st.setDescription(newdesc);
        m_Presence.updateStatus(st);
        //report success
        attr = m_IO.leaseTemplateAttributes();
        type = m_IO.formatTemplate(
            STATUS_PREFIX + st.getType().getIdentifier()
        );
        attr.add("type", type);
        attr.add("desc", st.getDescription());
        m_IO.printTemplate(PREFIX + "updated", attr);
      } else {
        m_IO.printTemplate(PREFIX + "nochange");
      }
    } catch (IOException ex) {
      try {
        m_IO.printTemplate(PREFIX + "failed");
      } catch (IOException iex) {
        log.error(c_LogMarker, "run(BaseShell)", iex);
      }
      log.error(c_LogMarker, "run(BaseShell)", ex);
      throw new CommandException(ex.getMessage());
    }
  }//run

  private MaskedSelection getTypeSelection() throws IOException {
    MaskedSelection sel = new MaskedSelection(m_IO.getTerminalIO(), "type");
    sel.addOption(
        PresenceStatusTypes.Available,
        m_IO.formatTemplate(
            STATUS_PREFIX + PresenceStatusTypes.Available.getIdentifier()
        )
    );
    sel.addOption(
        PresenceStatusTypes.AdvertisedAvailable,
        m_IO.formatTemplate(
            STATUS_PREFIX + PresenceStatusTypes.AdvertisedAvailable.getIdentifier()
        )
    );
    sel.addOption(
        PresenceStatusTypes.AdvertisedTemporarilyUnavailable,
        m_IO.formatTemplate(
            STATUS_PREFIX + PresenceStatusTypes.AdvertisedTemporarilyUnavailable.getIdentifier()
        )
    );
    sel.addOption(
        PresenceStatusTypes.TemporarilyUnavailable,
        m_IO.formatTemplate(
            STATUS_PREFIX + PresenceStatusTypes.TemporarilyUnavailable.getIdentifier()
        )
    );
    sel.addOption(
        PresenceStatusTypes.Unavailable,
        m_IO.formatTemplate(
            STATUS_PREFIX + PresenceStatusTypes.Unavailable.getIdentifier()
        )
    );
    sel.addOption(
        NO_CHANGE,
        m_IO.formatTemplate(PREFIX + "noneoption")
    );
    return sel;
  }//getTypeSelection

  private static final PresenceStatusType NO_CHANGE = new PresenceStatusType() {
    public String getIdentifier() {
      return "none";
    }//getIdentifier
  };

  public static final String PREFIX = TS_PREFIX + "UpdateStatus_";

}//class UpdateStatus