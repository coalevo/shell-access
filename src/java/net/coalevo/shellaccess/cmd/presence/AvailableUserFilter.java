/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.presence;

import net.coalevo.foundation.model.AgentIdentifier;
import net.wimpi.telnetd.io.toolkit.Editfield;
import net.wimpi.telnetd.io.toolkit.TabCompletionFilter;
import net.wimpi.telnetd.util.StringCompletions;
import org.apache.commons.collections.iterators.LoopingIterator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

/**
 * Provides a <tt>TabCompletionFilter</tt> that will
 * complete available (presence related) users.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class AvailableUserFilter
    extends TabCompletionFilter {

  private PresenceInfo m_PresenceInfo;
  private boolean m_Capitalize = true;
  private boolean m_Clearfirst = true;
  private Set<String> m_AddCompletions;
  private String m_EPrefix;

  public AvailableUserFilter(Editfield editfield, PresenceInfo pi) {
    super(editfield);
    m_PresenceInfo = pi;
  }//AvailableUserFilter

  public AvailableUserFilter(Editfield editfield, PresenceInfo pi, Set<String> addcomp, String eprefix) {
    this(editfield, pi);
    m_AddCompletions = addcomp;
    m_EPrefix = eprefix;
  }//constructor

  public int filterInput(int key) throws IOException {
    if (m_Clearfirst) {
      m_Editfield.clear();
      m_Clearfirst = false;
    }
    if ((m_Capitalize && key > 64 && key < 91)) {
      key = key + 32;
      m_Capitalize = false;
    } else if (m_Capitalize && key > 96 & key < 123) {
      key = key - 32;
      m_Capitalize = false;
    } else if (m_Capitalize && key == 35) {
      m_Capitalize = false;
    } else if (key == 32) {
      m_Capitalize = true;
    }
    return super.filterInput(key);
  }//filterInput

  public StringCompletions complete(String string) {
    //prepare al
    final ArrayList<String> al = new ArrayList<String>();
    Iterator iter = m_PresenceInfo.listPresent(true);
    while (iter.hasNext()) {
      AgentIdentifier aid = (AgentIdentifier) iter.next();
      if (m_PresenceInfo.isAvailable(aid)) {
        String str = (aid.isLocal()) ? aid.getName() : aid.getIdentifier();
        String nick = m_PresenceInfo.resolveNickname(aid);
        if (str.startsWith(string)) {
          al.add(str);
        }
        if (!nick.equals(str) && nick.startsWith(string)) {
          al.add(nick);
        }
      }
    }
    // Additional completions
    if (m_AddCompletions != null) {
      for (String str : m_AddCompletions) {
        str = (m_EPrefix != null) ? m_EPrefix.concat(str) : str;
        if (str.startsWith(string)) {
          al.add(str);
        }
      }
    }
    if (al.isEmpty()) {
      iter = EMPTY_ITERATOR;
    } else {
      //add prefix
      al.add(string);
      iter = new LoopingIterator(al);
    }
    final Iterator completions = iter;

    return new StringCompletions() {
      public String nextCompletion() {
        if (completions.hasNext()) {
          return (String) completions.next();
        } else {
          return null;
        }
      }
    };
  }//complete

  public String getErrorMessage() {
    return "";
  }//getErrorMessage

  private static final Iterator EMPTY_ITERATOR = new Iterator() {

    public boolean hasNext() {
      return false;
    }//hasNext

    public Object next() {
      throw new IllegalStateException();
    }//next

    public void remove() {
      throw new UnsupportedOperationException();
    }//remove
  };

}//class AvailableUserFilter
