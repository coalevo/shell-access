/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.presence;

import net.coalevo.foundation.model.Agent;
import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.presence.model.*;
import net.coalevo.presence.service.PresenceService;
import net.coalevo.security.service.SecurityService;
import net.coalevo.shellaccess.impl.Activator;
import net.coalevo.shellaccess.impl.RoleUtility;
import net.coalevo.shellaccess.model.ShellAccessSession;
import net.coalevo.shellaccess.model.ShellIO;
import net.coalevo.text.util.DateFormatter;
import net.coalevo.userdata.service.UserdataService;
import net.wimpi.telnetd.io.TemplateAttributes;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.IOException;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.Comparator;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Provides an implementation of a {@link PresenceServiceListener}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class PresenceServiceListenerImpl
    implements PresenceServiceListener, PresenceInfo {

  private static Marker c_LogMarker = MarkerFactory.getMarker(PresenceServiceListenerImpl.class.getName());

  private UserdataService m_UserdataService;
  private SecurityService m_SecurityService;
  private AtomicBoolean m_Active;
  private Presence m_Owner;
  private ShellIO m_IO;
  protected ConcurrentHashMap<AgentIdentifier, PresenceProxy> m_Present;
  private Agent m_Agent;
  private ShellAccessSession m_Session;

  public PresenceServiceListenerImpl(ShellIO shellio, ShellAccessSession session) {
    m_IO = shellio;
    m_Session = session;
    m_Active = new AtomicBoolean(true);
    m_Present = new ConcurrentHashMap<AgentIdentifier, PresenceProxy>();
    m_UserdataService = Activator.getServices().getUserdataService(0);
    m_SecurityService = Activator.getServices().getSecurityService(0);
  }//PresenceServiceListenerImpl

  public Presence getOwner() {
    return m_Owner;
  }//getOwner

  public void setOwner(Presence owner) {
    if (m_Owner == null) {
      m_Owner = owner;
      m_Agent = m_Owner.getAgent();
    }
  }//setOwner

  public void found(PresenceProxy p) {
    if (m_Session.isPresenceNotification()) {
      try {
        String str = m_IO.formatTemplate(PREFIX + "found", toAttributes(p));
        m_IO.printEvent(str);
      } catch (IOException ex) {
        Activator.log().error(c_LogMarker, "found()", ex);
      }
    }
  }//found

  public void subscriptionAllowed(AgentIdentifier aid) {
    try {
      String str = m_IO.formatTemplate(PREFIX + "subscription_allowed", toAttributes(aid));
      m_IO.printEvent(str);
    } catch (IOException ex) {
      Activator.log().error(c_LogMarker, "subscriptionAllowed()", ex);
    }
  }//subscriptionAllowed

  public void subscriptionDenied(AgentIdentifier aid, String reason) {
    try {
      TemplateAttributes attr = toAttributes(aid);
      if (reason != null && reason.length() > 0) {
        attr.add("reason", reason);
      }
      String str = m_IO.formatTemplate(PREFIX + "subscription_denied", attr);
      m_IO.printEvent(str);
    } catch (IOException ex) {
      Activator.log().error(c_LogMarker, "subscriptionDenied()", ex);
    }
  }//subscriptionDenied

  public void subscriptionCanceled(AgentIdentifier aid) {
    try {
      String str = m_IO.formatTemplate(PREFIX + "subscription_canceled", toAttributes(aid));
      m_IO.printEvent(str);
    } catch (IOException ex) {
      Activator.log().error(c_LogMarker, "subscriptionCanceled()", ex);
    }
  }//subscriptionCanceled

  public void unsubscribed(AgentIdentifier aid) {
    try {
      String str = m_IO.formatTemplate(PREFIX + "subscription_unsubscribed", toAttributes(aid));
      m_IO.printEvent(str);
    } catch (IOException ex) {
      Activator.log().error(c_LogMarker, "subscriptionAllowed()", ex);
    }
  }//unsubscribed

  public void becamePresent(PresenceProxy p) {
    try {
      String str =
          m_IO.formatTemplate(
              PREFIX + "presence_becamepresent",
              toAttributes(p)
          );
      m_IO.printEvent(str);
    } catch (IOException ex) {
      Activator.log().error(c_LogMarker, "becomePresent()", ex);
    }
    m_Present.put(p.getAgentIdentifier(), p);
  }//becamePresent

  public void becameAbsent(PresenceProxy p) {
    try {
      String str =
          m_IO.formatTemplate(
              PREFIX + "presence_becameabsent",
              toAttributes(p)
          );
      m_IO.printEvent(str);
    } catch (IOException ex) {
      Activator.log().error(c_LogMarker, "becomeAbsent()", ex);
    }
    m_Present.remove(p.getAgentIdentifier());
  }//becameAbsent

  public void statusUpdated(PresenceProxy p) {
    if (m_Session.isPresenceNotification()) {
      try {
        String str =
            m_IO.formatTemplate(
                PREFIX + "presence_statusupdated",
                toAttributes(p)
            );
        m_IO.printEvent(str);
      } catch (IOException ex) {
        Activator.log().error(c_LogMarker, "statusUpdated()", ex);
      }
    }
    m_Present.put(p.getAgentIdentifier(), p);
  }//statusUpdated

  public void resourcesUpdated(PresenceProxy p) {
    if (m_Session.isPresenceNotification()) {
      try {
        String str =
            m_IO.formatTemplate(
                PREFIX + "presence_resourcesupdated",
                toAttributes(p)
            );
        m_IO.printEvent(str);
      } catch (IOException ex) {
        Activator.log().error(c_LogMarker, "resourcesUpdated()", ex);
      }
    }
    m_Present.put(p.getAgentIdentifier(), p);
  }//resourcesUpdated

  public void requestedSubscription(PresenceProxy p) {
    try {
      String str =
          m_IO.formatTemplate(
              PREFIX + "subscription_requested",
              toAttributes(p)
          );
      m_IO.printEvent(str);
    } catch (IOException ex) {
      Activator.log().error(c_LogMarker, "requestedSubscription()", ex);
    }
  }//requestedSubscription

  public void receivedPresence(PresenceProxy p) {
    if (m_Session.isPresenceNotification()) {
      try {
        String str =
            m_IO.formatTemplate(
                PREFIX + "presence_received",
                toAttributes(p)
            );
        m_IO.printEvent(str);
      } catch (IOException ex) {
        Activator.log().error(c_LogMarker, "receivedPresence()", ex);
      }
    }
    m_Present.put(p.getAgentIdentifier(), p);
  }//receivedPresence

  public boolean isActive() {
    return m_Active.get();
  }//isActive

  public void setActive(boolean b) {
    m_Active.set(b);
  }//setActive

  public boolean isPresent(AgentIdentifier aid) {
    return m_Present.containsKey(aid);
  }//isPresent

  public boolean isAvailable(AgentIdentifier aid) {
    Object o = m_Present.get(aid);
    return o != null && ((PresenceProxy) o).isAvailable();
  }//isAvailable

  public boolean isTemporaryUnavailable(AgentIdentifier aid) {
    Object o = m_Present.get(aid);
    if (o != null) {
      PresenceStatusType pst = ((PresenceProxy) o).getStatusType();
      return pst.equals(PresenceStatusTypes.TemporarilyUnavailable) ||
          pst.equals(PresenceStatusTypes.AdvertisedTemporarilyUnavailable);
    } else {
      return false;
    }
  }//isTemporarilyUnavailable

  public Iterator listPresent(boolean advertised) {
    TreeSet<AgentIdentifier> present = new TreeSet<AgentIdentifier>(AID_COMPARATOR);
    for (AgentIdentifier aid : m_Present.keySet()) {
      PresenceProxy p = m_Present.get(aid);
      if(m_Owner.getSubscriptionsTo().contains(aid)) {
        present.add(aid);
        continue;
      }
      if (advertised && (p.getStatusType().equals(PresenceStatusTypes.AdvertisedTemporarilyUnavailable) ||
          p.getStatusType().equals(PresenceStatusTypes.AdvertisedAvailable))) {
        present.add(aid);
        continue;
      }
    }
    return present.iterator();
  }//listPresent

  public PresenceProxy getPresenceFor(AgentIdentifier aid) {
    return (PresenceProxy) m_Present.get(aid);
  }//getPresenceFor

  private String getString(long in) {
    final StringBuilder sbuf = new StringBuilder();
    if (in < 10) {
      sbuf.append('0');
    }
    return sbuf.append(in).toString();
  }//getString

  private TemplateAttributes toAttributes(PresenceProxy p)
      throws IOException {
    TemplateAttributes attr = m_IO.leaseTemplateAttributes();
    AgentIdentifier aid = p.getAgentIdentifier();
    attr.add("agent", getFormattedAgent(aid));

    //System.err.println("Added identifier.");
    //add resources
    for (Iterator iter = p.listResources().listIterator(); iter.hasNext();) {
      attr.add("res", iter.next().toString());
    }
    //System.err.println("Added resources.");
    //add status
    String type = m_IO.formatTemplate(
        PresenceCommand.STATUS_PREFIX + p.getStatusType().getIdentifier()
    );
    //System.err.println("Formatted type.");
    attr.add("type", type);
    //System.err.println("Added type.");
    String description = p.getStatusDescription();
    if (description != null && description.length() > 0) {
      attr.add("desc", description);
    }
    //System.err.println("Added desc.");

    //add presence since
    long since = p.presentSince();
    final long now = System.currentTimeMillis();
    long elapsedSeconds = (now - since) / 1000;
    long days = elapsedSeconds / 86400;
    elapsedSeconds -= (days * 86400);
    long hours = elapsedSeconds / 3600;
    elapsedSeconds -= (hours * 3600);
    long minutes = elapsedSeconds / 60;
    elapsedSeconds -= (minutes * 60);

    attr.add("days", String.valueOf(days));
    attr.add("hours", getString(hours));
    attr.add("minutes", getString(minutes));
    attr.add("seconds", getString(elapsedSeconds));

    //add now
    attr.add("time", TIME_FORMAT.format(now));

    return attr;
  }//toAttributes

  private TemplateAttributes toAttributes(AgentIdentifier aid)
      throws IOException {
    TemplateAttributes attr = m_IO.leaseTemplateAttributes();
    attr.add("agent", getFormattedAgent(aid));
    return attr;
  }//toAttributes

  private String getFormattedAgent(AgentIdentifier aid)
      throws IOException {
    if (aid.getIdentifier().startsWith("net.coalevo.")) {
      //need to resolve a service
      return resolveFormattedServicename(aid);
    }
    if (m_Agent == null || m_Owner == null) {
      System.out.println("===> OWNER or AGENT was never set?!!!");
    }
    TemplateAttributes attr = m_IO.leaseTemplateAttributes();
    attr.add("aid", aid.getIdentifier());
    attr.add("name", resolveFormattedNickname(aid));
    if (!aid.isLocal()) {
      attr.add("remote", "");
    }
    return m_IO.formatTemplate("agent", attr);
  }//getFormattedAgent

  private String resolveFormattedNickname(AgentIdentifier aid) {
    if (m_SecurityService == null) {
      return resolveNickname(aid);
    }
    return RoleUtility.getUserWithPriorityRoleStyle(
        resolveNickname(aid),
        m_SecurityService.getAgentRolesByName(m_Agent, aid));
  }//resolveFormattedNickname

  private String resolveFormattedServicename(AgentIdentifier aid)
      throws IOException {
    String snick = m_IO.formatTemplate(String.format("services_%s", aid.getName()));
    if (m_SecurityService == null) {
      return snick;
    }
    return RoleUtility.getUserWithPriorityRoleStyle(
        snick,
        m_SecurityService.getAgentRolesByName(m_Agent, aid));
  }//resolveFormattedServicename


  public String resolveNickname(AgentIdentifier aid) {
    if (aid.isLocal() || m_UserdataService == null) {
      try {
        return m_UserdataService.getNickname(m_Agent, aid);
      } catch (Exception ex) {
        Activator.log().error(c_LogMarker, "resolveNickname()", ex);
        return aid.getName();
      }
    } else {
      return aid.getName();
    }
  }//resolveNickname

  public PresenceListener getPublicListener(PresenceService ps) {
    return new PublicListener(ps);
  }//getPublicListener

  private class PublicListener extends PresenceListenerAdapter {

    public PublicListener(PresenceService ps) {
      try {
        if (PresenceServiceListenerImpl.this.m_Agent != null) {
          Iterator<AgentIdentifier> iter = ps.listAdvertisedPresent(PresenceServiceListenerImpl.this.m_Agent, PresenceFilter.NONE);
          while(iter != null && iter.hasNext()) {
            AgentIdentifier aid = iter.next();
            if(aid == null) {
              Activator.log().error(c_LogMarker, "$PublicListener::PublicListener()::listAdvertisedPresent returned a null instance in iterator.");
            } else {
              PublicListener.this.becamePresent(ps.getAdvertisedPresent(PresenceServiceListenerImpl.this.m_Agent, aid));
            }
          }
        } else {
          Activator.log().error(c_LogMarker, "PublicListener(PresenceService)::NO AGENT");
        }
      } catch (Exception ex) {
        Activator.log().error(c_LogMarker, "PublicListener(PresenceService)", ex);
      }
    }//constructor

    public Presence getOwner() {
      return PresenceServiceListenerImpl.this.m_Owner;
    }//getOwner

    public void becamePresent(PresenceProxy p) {
      if(p == null) {
        Activator.log().error(c_LogMarker, "becamePresent::PresenceProxy p = NULL");
        return;
      }
      if (!PresenceServiceListenerImpl.this.m_Present.containsKey(p.getAgentIdentifier())
          && !getOwner().getAgentIdentifier().equals(p.getAgentIdentifier())
          ) {
        PresenceServiceListenerImpl.this.m_Present.put(p.getAgentIdentifier(), p);
      }
    }//becamePresent

    public void becameAbsent(PresenceProxy p) {
      if (PresenceServiceListenerImpl.this.m_Present.containsKey(p.getAgentIdentifier()) &&
          !getOwner().getSubscriptionsTo().contains(p.getAgentIdentifier()) &&
          !getOwner().getAgentIdentifier().equals(p.getAgentIdentifier())
          ) {
        PresenceServiceListenerImpl.this.m_Present.remove(p.getAgentIdentifier());
      }
    }//becameAbsent

    public void statusUpdated(PresenceProxy p) {
      if (PresenceServiceListenerImpl.this.m_Present.containsKey(p.getAgentIdentifier())
          && !getOwner().getAgentIdentifier().equals(p.getAgentIdentifier())) {
        PresenceServiceListenerImpl.this.m_Present.put(p.getAgentIdentifier(), p);
      }
    }//statusUpdated

  }//PublicListener

  private static String PREFIX = "PresenceServiceListener_";
  public static final DateFormatter TIME_FORMAT = new DateFormatter("HH:mm:ss", 10);

  private static Comparator<AgentIdentifier> AID_COMPARATOR = new Comparator<AgentIdentifier>() {
    public int compare(AgentIdentifier o1, AgentIdentifier o2) {
      return o1.getIdentifier().compareTo(o2.getIdentifier());
    }//compare
  };

}//class PresenceServiceListenerImpl
