/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.presence;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.presence.model.PresenceProxy;
import net.coalevo.shellaccess.cmd.config.ConfigurationCommand;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.shellaccess.model.Shortcuts;
import net.wimpi.telnetd.io.TemplateAttributes;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.IOException;
import java.util.Date;
import java.util.Iterator;

/**
 * Lists the presences that the caller is subscribed to.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class ListPresent
    extends PresenceCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(ListPresent.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell) throws CommandException {
    if (!prepare(shell)) {
      return;
    }
    try {
      Iterator iter = m_PresenceInfo.listPresent(false);
      if (iter == null || !iter.hasNext()) {
        m_IO.printTemplate(PREFIX + "none");
        return;
      }
      //obtain shortcuts
      Shortcuts shortcuts;
      Object o = m_Shell.getEnvironmentVariable(ConfigurationCommand.KEY_SHORTCUTS);
      if (o == null) {
        shortcuts = new Shortcuts();
      } else {
        shortcuts = (Shortcuts) o;
      }
      TemplateAttributes attr = m_IO.leaseTemplateAttributes();
      int count = 0;
      while (iter.hasNext()) {
        AgentIdentifier aid = (AgentIdentifier) iter.next();
        String sc = null;
        if (shortcuts.contains(aid.getIdentifier())) {
          sc = String.format("%02d", shortcuts.get(aid.getIdentifier()));
        }
        PresenceProxy p = m_PresenceInfo.getPresenceFor(aid);
        String desc = p.getStatusDescription();
        if (desc != null && desc.length() == 0) {
          desc = null;
        }
        String[] items = new String[]{
            getFormattedAgent(aid),
            m_IO.formatTemplate(
                PresenceCommand.STATUS_PREFIX + p.getStatusType().getIdentifier()
            ),
            p.getStatusDescription(),
            sc,
            getSince(p.getLastStatusUpdate())
        };

        attr.add("agents.{aid,status,desc,shortcut,since}", items);
        count++;
      }
      attr.add("now", getStandardTimeFormat().format(new Date()));
      attr.add("numonline", Integer.toString(count));

      m_IO.pageTemplate(PREFIX + "list", attr);

    } catch (IOException ex) {
      try {
        m_IO.printTemplate(PREFIX + "failed");
      } catch (IOException iex) {
        log.error(c_LogMarker, "run(BaseShell)", iex);
      }
      log.error(c_LogMarker, "run(BaseShell)", ex);
      throw new CommandException(ex.getMessage());
    }
  }//run

  protected String getSince(long since) {
    final long now = System.currentTimeMillis();
    long elapsedSeconds = (now - since) / 1000;
    double minutes = ((double)elapsedSeconds) / (double)60;
    return String.format("%.0f",minutes);
  }//addSince

  private static final String PREFIX = TS_PREFIX + "ListPresent_";

}//class ListPresent
