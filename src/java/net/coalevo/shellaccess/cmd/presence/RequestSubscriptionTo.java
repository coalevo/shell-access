/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.presence;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.wimpi.telnetd.io.TemplateAttributes;

import java.io.IOException;

/**
 * Command for blocking inbound notificiations or subscription requests
 * from another agent.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class RequestSubscriptionTo
    extends PresenceCommand {

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell) throws CommandException {
    if (!prepare(shell)) {
      return;
    }
    try {
      m_PromptWrap = false;
      AgentIdentifier aid = promptAgent(PREFIX + "reqlabel");
      if (aid == null) {
        m_IO.printTemplate(PREFIX + "nosuchagent");
        return;
      }
      if (m_PresenceService.requestSubscriptionTo(m_Presence, aid)) {
        TemplateAttributes attr = m_IO.leaseTemplateAttributes();
        attr.add("agent", getFormattedAgent(aid));
        m_IO.printTemplate(PREFIX + "requested", attr);
      } else {
        m_IO.printTemplate(PREFIX + "notrequested");
      }
    } catch (IOException ex) {
      try {
        m_IO.printTemplate(PREFIX + "failed");
      } catch (IOException iex) {
        log.error("run(BaseShell)", iex);
      }
      log.error("run(BaseShell)", ex);
      throw new CommandException(ex.getMessage());
    }
  }//run

  private static final String PREFIX = TS_PREFIX + "RequestSubscriptionTo_";

}//class RequestSubscriptionTo