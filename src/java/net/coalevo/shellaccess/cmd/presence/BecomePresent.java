/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.presence;

import net.coalevo.foundation.model.Session;
import net.coalevo.shellaccess.impl.Activator;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.shellaccess.model.Services;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * Initiates the presence of the caller.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class BecomePresent
    extends PresenceCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(BecomePresent.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell) throws CommandException {
    m_PresenceService = Activator.getServices().getPresenceService(Services.NO_WAIT);
    if (m_PresenceService == null) {
      log.error(c_LogMarker, "PresenceService not available.");
      return;
      //probably notify that the presence service isn't available
    }
    PresenceServiceListenerImpl psl = new PresenceServiceListenerImpl(shell.getShellIO(), shell.getSession());
    //store in environment (don't forget to purge it?)
    shell.setEnvironmentVariable(PresenceCommand.ENVKEY_PRESENCELISTENER, psl);
    m_Presence = m_PresenceService.register(
        shell.getSession().getUserAgent(),
        psl
    );
    //Register public listener (once registered)
    m_PresenceService.registerPublicPresenceListener(psl.getPublicListener(m_PresenceService));

    Session s = shell.getSession().getUserAgent().getSession();
    if (s == null || !s.isValid()) {
      shell.setEnvironmentVariable(PresenceCommand.ENVKEY_PRESENCE, m_Presence);
    }
    //Get status type from session
    m_Presence.getStatus().setType(shell.getSession().getLoginPresenceStatusType());
    m_Presence.begin();
  }//run

}//class BecomePresent