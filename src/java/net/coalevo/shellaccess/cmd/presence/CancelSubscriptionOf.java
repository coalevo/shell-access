/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.presence;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.presence.model.AgentIdentifierList;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import net.wimpi.telnetd.io.TemplateAttributes;
import net.wimpi.telnetd.io.toolkit.Editfield;
import net.wimpi.telnetd.io.toolkit.MaskedSelection;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.IOException;
import java.util.Iterator;

/**
 * Cancel the subscription of another presence to the caller.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class CancelSubscriptionOf
    extends PresenceCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(CancelSubscriptionOf.class.getName());

  public String getIdentifier() {
    return getClass().getName();
  }//getIdentifier

  public void run(BasicShell shell) throws CommandException {
    if (!prepare(shell)) {
      return;
    }
    try {
      AgentIdentifierList list = m_Presence.getSubscriptionsFrom();
      if (list.isEmpty()) {
        m_IO.printTemplate(PREFIX + "nosubscriptions");
        return;
      }
      m_IO.printTemplate(PREFIX + "instruct");
      m_IO.printTemplate(PREFIX + "agentprompt");
      MaskedSelection s = new MaskedSelection(m_IO.getTerminalIO(), "aid");
      String none = m_IO.formatTemplate(PREFIX + "noneoption");
      s.addOption(NONE, none);
      for (Iterator iter = list.iterator(); iter.hasNext();) {
        AgentIdentifier aid = (AgentIdentifier) iter.next();
        s.addOption(aid, getFormattedAgent(aid));
      }
      s.run();
      Object o = s.getSelectedValue();
      if (NONE.equals(o)) {
        m_IO.printTemplate(PREFIX + "unchanged");
      } else {
        //check if notification is wished
        boolean notify = false;
        m_IO.printTemplate(PREFIX + "decidenotification");
        notify = m_IO.getDecision();
        String reason = null;
        if (notify) {
          m_IO.printTemplate(PREFIX + "reasonlabel");
          Editfield ef = new Editfield(m_IO.getTerminalIO(), "reason", 60, 255);
          ef.setIgnoreDelete(true);
          ef.run();
          reason = ef.getValue();
        }
        AgentIdentifier aid = (AgentIdentifier) o;
        m_PresenceService.cancelSubscriptionOf(m_Presence, aid, notify);
        TemplateAttributes attr = m_IO.leaseTemplateAttributes();
        attr.add("agent", getFormattedAgent(aid));
        if (reason != null && reason.length() > 0) {
          attr.add("reason", reason);
        }
        m_IO.printTemplate(PREFIX + "canceled", attr);
      }
    } catch (IOException ex) {
      try {
        m_IO.printTemplate(PREFIX + "failed");
      } catch (IOException iex) {
        log.error("run(BaseShell)", iex);
      }
      log.error(c_LogMarker, "run(BaseShell)", ex);
      throw new CommandException(ex.getMessage());
    }
  }//run

  private static final String PREFIX = TS_PREFIX + "CancelSubscriptionOf_";
  private static Object NONE = new Object();

}//class CancelSubscriptionOf