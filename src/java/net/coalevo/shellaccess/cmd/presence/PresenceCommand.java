/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.presence;

import net.coalevo.foundation.model.Session;
import net.coalevo.presence.model.Presence;
import net.coalevo.presence.model.PresenceConstants;
import net.coalevo.presence.service.PresenceService;
import net.coalevo.shellaccess.cmd.BaseCommand;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.Services;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.util.Iterator;


/**
 * Abstract base class for presence related commands.
 * <p/>
 * Provides the functionality common to the service access.
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public abstract class PresenceCommand
    extends BaseCommand {

  private static Marker c_LogMarker = MarkerFactory.getMarker(PresenceCommand.class.getName());
  protected PresenceService m_PresenceService;
  protected Presence m_Presence;
  protected PresenceInfo m_PresenceInfo;

  public boolean prepare(BasicShell shell) {
    if (!super.prepare(shell)) {
      return false;
    }
    m_PresenceService = shell.getServices().getPresenceService(Services.NO_WAIT);
    if (m_PresenceService == null) {
      //System.err.println("PresenceService not available.");
      log.error(c_LogMarker, "PresenceService null");
      return false;
    }
    //user coalevo session
    Session s = shell.getSession().getUserAgent().getSession();
    if (s != null && s.isValid()) {
      m_Presence = (Presence) s.getAttribute(PresenceConstants.PRESENCE_SESSION_ATTRIBUTE);
    } else if (shell.isEnviromentVariableSet(ENVKEY_PRESENCE)) {
      m_Presence = (Presence) shell.getEnvironmentVariable(ENVKEY_PRESENCE);
    } else {
      log.error(c_LogMarker, "PresenceService null");
      return false;
    }
    if (!m_Presence.isPresent()) {
      log.error(c_LogMarker, "Presence not present.");
      return false;
    }
    m_PresenceInfo = (PresenceInfo) shell.getEnvironmentVariable(
        PresenceCommand.ENVKEY_PRESENCEINFO);
    return true;
  }//prepare

  public void reset() {
    super.reset();
    m_PresenceService = null;
    m_Presence = null;
    m_PresenceInfo = null;
  }//reset

  public Iterator listPresent() {
    Object o = m_Shell.getEnvironmentVariable(PresenceCommand.ENVKEY_PRESENCELISTENER);
    if (o == null) {
      return null;
    } else {
      PresenceServiceListenerImpl psl = (PresenceServiceListenerImpl) o;
      return psl.listPresent(false);
    }
  }//listPresent

  protected static final String TS_PREFIX = "commands_net.coalevo.shellaccess.cmd.presence.";
  public static final String STATUS_PREFIX = "presencestatus_";

  public static final String ENVKEY_PRESENCE = "net.coalevo.presence.model.Presence";
  protected static final String ENVKEY_PRESENCELISTENER = "net.coalevo.presence.model.PresenceServiceListener";
  public static final String ENVKEY_PRESENCEINFO = ENVKEY_PRESENCELISTENER;  //note that's due to our implementation

}//class PresenceCommand
