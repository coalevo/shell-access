/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.cmd.presence;

import net.coalevo.shellaccess.model.BaseShellCommandProvider;

/**
 * A {@link net.coalevo.shellaccess.model.ShellCommandProvider}
 * for the presence commands.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class PresenceCommandProvider
    extends BaseShellCommandProvider {

  public PresenceCommandProvider() {
    super(PresenceCommandProvider.class.getPackage().getName());
    prepare();
  }//constructor

  private void prepare() {
    addCommand("AllowInbound", AllowInbound.class);
    addCommand("AllowOutbound", AllowOutbound.class);
    addCommand("AllowRequestedSubscription", AllowRequestedSubscription.class);
    addCommand("BecomePresent", BecomePresent.class);
    addCommand("BecomeAbsent", BecomeAbsent.class);
    addCommand("BlockInbound", BlockInbound.class);
    addCommand("BlockOutbound", BlockOutbound.class);
    addCommand("CancelSubscriptionOf", CancelSubscriptionOf.class);
    addCommand("DenyRequestedSubscription", DenyRequestedSubscription.class);
    addCommand("ListAdvertisedAvailable", ListAdvertisedAvailable.class);
    addCommand("ListInboundBlocked", ListInboundBlocked.class);
    addCommand("ListOutboundBlocked", ListOutboundBlocked.class);
    addCommand("ListRequestedSubscriptions", ListRequestedSubscriptions.class);
    addCommand("ListSubscriptionsFrom", ListSubscriptionsFrom.class);
    addCommand("ListSubscriptionsTo", ListSubscriptionsTo.class);
    addCommand("ListPresent", ListPresent.class);
    addCommand("RequestSubscriptionTo", RequestSubscriptionTo.class);
    addCommand("UnsubscribeFrom", UnsubscribeFrom.class);
    addCommand("UpdateStatus", UpdateStatus.class);

  }//prepare

}//class PresenceCommandProvider
