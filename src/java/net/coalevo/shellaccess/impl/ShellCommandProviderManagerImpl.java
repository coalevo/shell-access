/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.impl;

import net.coalevo.shellaccess.model.ScriptShellCommandProvider;
import net.coalevo.shellaccess.model.ShellCommandProvider;
import net.coalevo.shellaccess.service.ShellCommandProviderManager;
import org.osgi.framework.*;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.util.*;


/**
 * Provides an implementation of {@link ShellCommandProviderManager}
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class ShellCommandProviderManagerImpl
    implements ShellCommandProviderManager {

  private static Marker c_LogMarker = MarkerFactory.getMarker(ShellCommandProviderManagerImpl.class.getName());
  private BundleContext m_BundleContext;
  private Map<String, ShellCommandProvider> m_ShellCommandProviders;
  private ShellCommandProviderListener m_ProviderListener;

  public ShellCommandProviderManagerImpl() {
    m_ShellCommandProviders = new HashMap<String, ShellCommandProvider>();
  }//ShellCommandProvidersManagerImpl


  /**
   * Activates this <tt>ShellCommandProviderManagerImpl</tt>.
   * The logic will automatically register all {@link net.coalevo.shellaccess.model.ShellCommandProvider} class
   * objects, whether registered before or after the activation (i.e. white board
   * model implementation).
   *
   * @param bc the <tt>BundleContext</tt>.
   */
  public void activate(BundleContext bc) {
    //get the context
    m_BundleContext = bc;
    //prepare listener
    m_ProviderListener = new ShellCommandProviderListener();
    //prepare the filter
    String filter = "(objectclass=" + ShellCommandProvider.class.getName() + ")";

    try {
      //add the listener to the bundle context.
      bc.addServiceListener(m_ProviderListener, filter);
      //ensure that already registered Provider instances are registered with
      //the manager
      ServiceReference[] srl = bc.getServiceReferences(null, filter);
      for (int i = 0; srl != null && i < srl.length; i++) {
        m_ProviderListener.serviceChanged(new ServiceEvent(ServiceEvent.REGISTERED, srl[i]));
      }
    } catch (InvalidSyntaxException ex) {
      Activator.log().error(c_LogMarker, "activate()", ex);
    }
  }//activate

  /**
   * Deactivates this <tt>ShellCommandProviderManagerImpl</tt>.
   * The logic will remove the listener and release all
   * references.
   */
  public void deactivate() {
    //remove the listener
    m_BundleContext.removeServiceListener(m_ProviderListener);
    //null out the references
    m_ShellCommandProviders.clear();

    m_ShellCommandProviders = null;
    m_ProviderListener = null;
    m_BundleContext = null;
  }//deactivate


  public boolean register(ShellCommandProvider scp) {
    final String id = scp.getNamespace();
    if (m_ShellCommandProviders.containsKey(id)) {
      return false;
    } else {
      m_ShellCommandProviders.put(id, scp);
      Activator.log().info(c_LogMarker, "Registered ShellCommandProvider " + id);
      return true;
    }
  }//register


  public boolean unregister(String ns) {
    if (!m_ShellCommandProviders.containsKey(ns)) {
      return false;
    } else {
      m_ShellCommandProviders.remove(ns);
      Activator.log().info(c_LogMarker, "Unregistered ShellCommandProvider " + ns);
      return true;
    }
  }//unregister

  public ShellCommandProvider get(String ns) {
    Object o = m_ShellCommandProviders.get(ns);
    if (o != null) {
      return (ShellCommandProvider) o;
    } else {
      throw new NoSuchElementException(ns);
    }
  }//get

  public boolean isAvailable(String id) {
    return m_ShellCommandProviders.containsKey(id);
  }//isAvailable

  public Iterator listAvailable() {
    return m_ShellCommandProviders.keySet().iterator();
  }//listAvailable

  public Iterator<String> listAvailableScriptProviders() {
    ArrayList<String> al = new ArrayList<String>();
    for (Iterator<Map.Entry<String, ShellCommandProvider>> iter = m_ShellCommandProviders.entrySet().iterator(); iter.hasNext();) {
      Map.Entry<String, ShellCommandProvider> entry = iter.next();
      if (entry.getValue() instanceof ScriptShellCommandProvider) {
        al.add(entry.getKey());
      }
    }
    //sorted so that they are not shuffled in each call
    Collections.sort(al);
    return al.listIterator();
  }//listAvailableScriptProviders

  private class ShellCommandProviderListener
      implements ServiceListener {

    public void serviceChanged(ServiceEvent ev) {
      ServiceReference sr = ev.getServiceReference();
      Object o = null;
      switch (ev.getType()) {
        case ServiceEvent.REGISTERED:
          o = m_BundleContext.getService(sr);
          if (o == null) {
            Activator.log().error(c_LogMarker, "ServiceListener:serviceChanged:registered:null");
          } else if (!(o instanceof ShellCommandProvider)) {
            Activator.log().error(c_LogMarker, "ServiceListener:serviceChanged:registered:Reference not a ShellService instance.");
          } else {
            register((ShellCommandProvider) o);
          }
          break;
        case ServiceEvent.UNREGISTERING:
          o = m_BundleContext.getService(sr);
          if (o == null) {
            Activator.log().error(c_LogMarker, "ServiceListener:serviceChanged:unregistering:null");
          } else if (!(o instanceof ShellCommandProvider)) {
            Activator.log().error(c_LogMarker, "ServiceListener:serviceChanged:unregistering:Reference not a ShellService instance.");
          } else {
            unregister(((ShellCommandProvider) o).getNamespace());
          }
          break;
      }
    }
  }//inner class ShellServiceListener

}//interface ShellCommandProviderManager
