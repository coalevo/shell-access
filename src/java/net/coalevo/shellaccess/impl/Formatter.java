/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.impl;

import net.coalevo.text.model.TransformationException;
import net.coalevo.text.model.UnsupportedTransformException;
import net.coalevo.text.service.TransformationService;
import net.coalevo.text.util.TransformCache;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Provides formatting and a cache for formatted text with unique
 * identification. Mediates to the TransformationService
 * (net.coalevo.text.service.TransformationService).
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class Formatter {

  private static final Marker c_LogMarker = MarkerFactory.getMarker(Formatter.class.getName());

  private static TransformationService c_TransformationService;
  private static Map<String, TransformCache> m_Caches;

  static {
    m_Caches = new HashMap<String, TransformCache>();
    c_TransformationService = Activator.getServices().getTransformationService(ServiceMediator.WAIT_UNLIMITED);
  }//static

  public static final Set<String> getAvailableFormats() {
    return c_TransformationService.getInputFormats(TELNETD);
  }//getAvailableFormats

  public static final String format(String inf, String id, String str) {
    if (inf == null || PLAIN.equals(inf)) {
      return str;
    }
    TransformCache tc = getCacheFor(inf);
    String out = tc.get(id);
    if (out == null) {
      out = format(inf, str);
      tc.put(id, out);
    }
    return out;
  }//format

  public static final String format(String inf, String str) {
    if (inf == null || PLAIN.equals(inf)) {
      return str;
    }
    try {
      return c_TransformationService.transform(inf, TELNETD, str);
    } catch (TransformationException ex) {
      Activator.log().error(c_LogMarker, "format(String,String)", ex);
      return EMPTY;
    } catch (UnsupportedTransformException ex) {
      Activator.log().error(c_LogMarker, "format(String,String)", ex);
      return EMPTY;
    }
  }//format

  public static final String formatPreview(String inf, String str)
      throws TransformationException {
    return c_TransformationService.transform(inf, TELNETD, str);
  }//formatPreview

  public static final void invalidateCache(String inf, String id) {
    if (PLAIN.equals(inf)) {
      return;
    }
    TransformCache tc = getCacheFor(inf);
    tc.remove(id);
  }//invalidateCache;

  private static final TransformCache getCacheFor(String inf) {
    TransformCache tc = m_Caches.get(inf);
    if (tc == null) {
      tc = new TransformCache(50);
      m_Caches.put(inf, tc);
    }
    return tc;
  }//getCacheFor

  public static final boolean isAvailableFormat(String inf) {
    return getAvailableFormats().contains(inf);
  }//isAvailableFormat

  private static final String EMPTY = "";
  private static final String TELNETD = "telnetd";
  private static final String PLAIN = "";

}//class Formatter
