/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.impl;

import net.coalevo.bank.service.BankService;
import net.coalevo.discussion.service.DiscussionService;
import net.coalevo.discussion.service.ForumModerationService;
import net.coalevo.foundation.model.ServiceAgent;
import net.coalevo.foundation.service.MessageResourceService;
import net.coalevo.foundation.service.RndStringGeneratorService;
import net.coalevo.foundation.util.ConfigurationMediator;
import net.coalevo.iplocator.service.IPLocatorService;
import net.coalevo.logging.service.LoggingService;
import net.coalevo.messaging.service.GroupMessagingService;
import net.coalevo.messaging.service.MessagingService;
import net.coalevo.postoffice.service.PostOfficeService;
import net.coalevo.presence.service.PresenceService;
import net.coalevo.security.model.ServiceAgentProxy;
import net.coalevo.security.service.PolicyService;
import net.coalevo.security.service.SecurityManagementService;
import net.coalevo.security.service.SecurityService;
import net.coalevo.shellaccess.command.CommandShellManager;
import net.coalevo.shellaccess.model.Services;
import net.coalevo.shellaccess.service.ShellCommandProviderManager;
import net.coalevo.statistics.service.StatisticsService;
import net.coalevo.system.service.ExecutionService;
import net.coalevo.system.service.MaintenanceService;
import net.coalevo.system.service.SessionService;
import net.coalevo.text.service.TransformationService;
import net.coalevo.userdata.service.UserdataService;
import net.wimpi.telnetd.service.TemplatesService;
import org.osgi.framework.*;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.prefs.PreferencesService;
import org.slf4j.Logger;

import javax.xml.parsers.SAXParserFactory;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * Provides a mediator for required coalevo services.
 * Allows to obtain fresh and latest references by using
 * the whiteboard model to track the services at all times.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class ServiceMediator implements Services {

  private BundleContext m_BundleContext;

  private SecurityService m_SecurityService;
  private SecurityManagementService m_SecurityManagementService;
  private PolicyService m_PolicyService;
  private RndStringGeneratorService m_RndStringGeneratorService;
  private ConfigurationAdmin m_ConfigurationAdmin;
  private MessageResourceService m_MessageResourceService;
  private ExecutionService m_ExecutionService;
  private TemplatesService m_TemplatesService;
  private ConfigurationMediator m_ConfigurationMediator;
  private IPLocatorService m_IPLocatorService;
  private DiscussionService m_DiscussionService;
  private ForumModerationService m_ForumModerationService;
  private StatisticsService m_StatisticsService;
  private GroupMessagingService m_GroupMessagingService;
  private TransformationService m_TransformationService;
  private BankService m_BankService;
  private SAXParserFactory m_SAXParserFactory;
  private LoggingService m_LoggingService;
  private MaintenanceService m_MaintenanceService;
  private PostOfficeService m_PostOfficeService;
  private Logger m_Logger;

  //these are kind of "internal" services
  private ShellCommandProviderManager m_ShellCommandProviderManager;
  private CommandShellManager m_CommandShellManager;
  private SessionManager m_SessionManager;
  private UserdataService m_UserdataService;
  private PresenceService m_PresenceService;
  private MessagingService m_MessagingService;
  private SessionService m_SessionService;

  private CountDownLatch m_SecurityServiceLatch;
  private CountDownLatch m_SecurityManagementServiceLatch;
  private CountDownLatch m_PolicyServiceLatch;
  private CountDownLatch m_RndStringGeneratorLatch;
  private CountDownLatch m_MessageResourceServiceLatch;
  private CountDownLatch m_ConfigurationAdminLatch;
  private CountDownLatch m_ExecutionServiceLatch;
  private CountDownLatch m_UserdataServiceLatch;
  private CountDownLatch m_PresenceServiceLatch;
  private CountDownLatch m_TemplatesServiceLatch;
  private CountDownLatch m_MessagingServiceLatch;
  private CountDownLatch m_SessionServiceLatch;
  private CountDownLatch m_IPLocatorServiceLatch;
  private CountDownLatch m_DiscussionServiceLatch;
  private CountDownLatch m_ForumModerationServiceLatch;
  private CountDownLatch m_StatisticsServiceLatch;
  private CountDownLatch m_GroupMessagingServiceLatch;
  private CountDownLatch m_TransformationServiceLatch;
  private CountDownLatch m_BankServiceLatch;
  private CountDownLatch m_LoggingServiceLatch;
  private CountDownLatch m_SAXParserFactoryLatch;
  private CountDownLatch m_MaintenanceServiceLatch;
  private CountDownLatch m_PostOfficeServiceLatch;

  private ServiceAgentProxy m_ServiceAgentProxy;

  public ServiceMediator() {

  }//constructor

  public void setServiceAgentProxy(ServiceAgentProxy a) {
    m_ServiceAgentProxy = a;
  }//setServiceAgentProxy

  public ServiceAgent getServiceAgent() {
    return m_ServiceAgentProxy.getAuthenticPeer();
  }//getServiceAgent

  public SAXParserFactory getSAXParserFactory(long wait) {
    try {
      if (wait < 0) {
        m_SAXParserFactoryLatch.await();
      } else if (wait > 0) {
        m_SAXParserFactoryLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      e.printStackTrace(System.err);
    }

    return m_SAXParserFactory;
  }//getSAXParserFactory

  public PreferencesService getPreferencesService()
      throws Exception {
    ServiceReference ref = m_BundleContext.getServiceReference(
        PreferencesService.class.getName()
    );
    if (ref == null) {
      throw new Exception("Could not obtain preferences service.");
    }
    return (PreferencesService) m_BundleContext.getService(ref);
  }//getPreferencesService

  public SecurityService getSecurityService(long wait) {
    try {
      if (wait < 0) {
        m_SecurityServiceLatch.await();
      } else if (wait > 0) {
        m_SecurityServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      e.printStackTrace(System.err);
    }
    return m_SecurityService;
  }//getSecurityService

  public SecurityManagementService getSecurityManagementService(long wait) {
    try {
      if (wait < 0) {
        m_SecurityManagementServiceLatch.await();
      } else if (wait > 0) {
        m_SecurityManagementServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      e.printStackTrace(System.err);
    }
    return m_SecurityManagementService;
  }//getSecurityManagementService

  public PolicyService getPolicyService(long wait) {
    try {
      if (wait < 0) {
        m_PolicyServiceLatch.await();
      } else if (wait > 0) {
        m_PolicyServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      e.printStackTrace(System.err);
    }
    return m_PolicyService;
  }//getPolicyService

  public MessageResourceService getMessageResourceService(long wait) {
    try {
      if (wait < 0) {
        m_MessageResourceServiceLatch.await();
      } else if (wait > 0) {
        m_MessageResourceServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      e.printStackTrace(System.err);
    }
    return m_MessageResourceService;
  }//getMessageResourceService

  public ConfigurationAdmin getConfigurationAdmin(long wait) {
    try {
      if (wait < 0) {
        m_ConfigurationAdminLatch.await();
      } else if (wait > 0) {
        m_ConfigurationAdminLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      e.printStackTrace(System.err);
    }

    return m_ConfigurationAdmin;
  }//getConfigurationAdmin

  public RndStringGeneratorService getRndStringGeneratorService(long wait) {
    try {
      if (wait < 0) {
        m_RndStringGeneratorLatch.await();
      } else if (wait > 0) {
        m_RndStringGeneratorLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      e.printStackTrace(System.err);
    }

    return m_RndStringGeneratorService;
  }//getRndStringGeneratorService

  public UserdataService getUserdataService(long wait) {
    try {
      if (wait < 0) {
        m_UserdataServiceLatch.await();
      } else if (wait > 0) {
        m_UserdataServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      e.printStackTrace(System.err);
    }

    return m_UserdataService;
  }//getUserdataService

  public DiscussionService getDiscussionService(long wait) {
    try {
      if (wait < 0) {
        m_DiscussionServiceLatch.await();
      } else if (wait > 0) {
        m_DiscussionServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      e.printStackTrace(System.err);
    }

    return m_DiscussionService;
  }//getDiscussionService

  public ForumModerationService getForumModerationService(long wait) {
    try {
      if (wait < 0) {
        m_ForumModerationServiceLatch.await();
      } else if (wait > 0) {
        m_ForumModerationServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      e.printStackTrace(System.err);
    }

    return m_ForumModerationService;
  }//getForumModerationService

  public PresenceService getPresenceService(long wait) {
    try {
      if (wait < 0) {
        m_PresenceServiceLatch.await();
      } else if (wait > 0) {
        m_PresenceServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      e.printStackTrace(System.err);
    }

    return m_PresenceService;
  }//getPresenceService

  public MessagingService getMessagingService(long wait) {
    try {
      if (wait < 0) {
        m_MessagingServiceLatch.await();
      } else if (wait > 0) {
        m_MessagingServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      e.printStackTrace(System.err);
    }

    return m_MessagingService;
  }//getMessagingService

  public GroupMessagingService getGroupMessagingService(long wait) {
    try {
      if (wait < 0) {
        m_GroupMessagingServiceLatch.await();
      } else if (wait > 0) {
        m_GroupMessagingServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      e.printStackTrace(System.err);
    }

    return m_GroupMessagingService;
  }//getGroupMessagingService

  public ExecutionService getExecutionService(long wait) {
    try {
      if (wait < 0) {
        m_ExecutionServiceLatch.await();
      } else if (wait > 0) {
        m_ExecutionServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      e.printStackTrace(System.err);
    }

    return m_ExecutionService;
  }//getExecutionService

  public SessionService getSessionService(long wait) {
    try {
      if (wait < 0) {
        m_SessionServiceLatch.await();
      } else if (wait > 0) {
        m_SessionServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      e.printStackTrace(System.err);
    }

    return m_SessionService;
  }//getSessionService

  public TemplatesService getTemplatesService(long wait) {
    try {
      if (wait < 0) {
        m_TemplatesServiceLatch.await();
      } else if (wait > 0) {
        m_TemplatesServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      e.printStackTrace(System.err);
    }

    return m_TemplatesService;
  }//getTemplatesService

  public IPLocatorService getIPLocatorService(long wait) {
    try {
      if (wait < 0) {
        m_IPLocatorServiceLatch.await();
      } else if (wait > 0) {
        m_IPLocatorServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      e.printStackTrace(System.err);
    }
    return m_IPLocatorService;
  }//getIPLocatorService

  public StatisticsService getStatisticsService(long wait) {
    try {
      if (wait < 0) {
        m_StatisticsServiceLatch.await();
      } else if (wait > 0) {
        m_StatisticsServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      e.printStackTrace(System.err);
    }
    return m_StatisticsService;
  }//getStatisticsService

  public TransformationService getTransformationService(long wait) {
    try {
      if (wait < 0) {
        m_TransformationServiceLatch.await();
      } else if (wait > 0) {
        m_TransformationServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      e.printStackTrace(System.err);
    }

    return m_TransformationService;
  }//getTransformationService

  public BankService getBankService(long wait) {
    try {
      if (wait < 0) {
        m_BankServiceLatch.await();
      } else if (wait > 0) {
        m_BankServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      e.printStackTrace(System.err);
    }
    return m_BankService;
  }//getBankService

  public MaintenanceService getMaintenanceService(long wait) {
    try {
      if (wait < 0) {
        m_MaintenanceServiceLatch.await();
      } else if (wait > 0) {
        m_MaintenanceServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      e.printStackTrace(System.err);
    }
    return m_MaintenanceService;
  }//getMaintenanceService

  public PostOfficeService getPostOfficeService(long wait) {
    try {
      if (wait < 0) {
        m_PostOfficeServiceLatch.await();
      } else if (wait > 0) {
        m_PostOfficeServiceLatch.await(wait, TimeUnit.MILLISECONDS);
      }
    } catch (InterruptedException e) {
      e.printStackTrace(System.err);
    }
    return m_PostOfficeService;
  }//getPostOfficeService

  public ConfigurationMediator getConfigMediator() {
    return m_ConfigurationMediator;
  }//getConfigMediator

  public void setConfigMediator(ConfigurationMediator configMediator) {
    m_ConfigurationMediator = configMediator;
  }//setConfigMediator


  public ShellCommandProviderManager getShellCommandProviderManager() {
    return m_ShellCommandProviderManager;
  }///getShellCommandProviderManager

  public void setShellCommandProviderManager(ShellCommandProviderManager shellCommandProviderManager) {
    m_ShellCommandProviderManager = shellCommandProviderManager;
  }//setShellCommandProviderManager

  public CommandShellManager getCommandShellManager() {
    return m_CommandShellManager;
  }//getCommandShellManager

  public void setCommandShellManager(CommandShellManager csm) {
    m_CommandShellManager = csm;
  }//setCommandShellManager

  public SessionManager getSessionManager() {
    return m_SessionManager;
  }//getSessionManager

  public void setSessionManager(SessionManager sessionManager) {
    m_SessionManager = sessionManager;
  }//setSessionManager

  public boolean activate(BundleContext bc) {
    //get the context
    m_BundleContext = bc;
    //prepare waits if required
    m_SecurityServiceLatch = createWaitLatch();
    m_SecurityManagementServiceLatch = createWaitLatch();
    m_PolicyServiceLatch = createWaitLatch();
    m_RndStringGeneratorLatch = createWaitLatch();
    m_MessageResourceServiceLatch = createWaitLatch();
    m_ConfigurationAdminLatch = createWaitLatch();
    m_ExecutionServiceLatch = createWaitLatch();
    m_UserdataServiceLatch = createWaitLatch();
    m_PresenceServiceLatch = createWaitLatch();
    m_TemplatesServiceLatch = createWaitLatch();
    m_MessagingServiceLatch = createWaitLatch();
    m_SessionServiceLatch = createWaitLatch();
    m_IPLocatorServiceLatch = createWaitLatch();
    m_DiscussionServiceLatch = createWaitLatch();
    m_ForumModerationServiceLatch = createWaitLatch();
    m_StatisticsServiceLatch = createWaitLatch();
    m_GroupMessagingServiceLatch = createWaitLatch();
    m_TransformationServiceLatch = createWaitLatch();
    m_BankServiceLatch = createWaitLatch();
    m_LoggingServiceLatch = createWaitLatch();
    m_SAXParserFactoryLatch = createWaitLatch();
    m_MaintenanceServiceLatch = createWaitLatch();
    m_PostOfficeServiceLatch = createWaitLatch();

    //prepareDefinitions listener
    ServiceListener serviceListener = new ServiceListenerImpl();

    //prepareDefinitions the filter
    String filter =
        "(|(|(|(|(|(|(|(|(|(|(|(|(|(|(|(|(|(|(|(|(|(|(objectclass=" + SecurityService.class.getName() + ")" +
            "(objectclass=" + SecurityManagementService.class.getName() + "))" +
            "(objectclass=" + PolicyService.class.getName() + "))" +
            "(objectclass=" + BankService.class.getName() + "))" +
            "(objectclass=" + RndStringGeneratorService.class.getName() + "))" +
            "(objectclass=" + MessageResourceService.class.getName() + "))" +
            "(objectclass=" + ConfigurationAdmin.class.getName() + "))" +
            "(objectclass=" + ExecutionService.class.getName() + "))" +
            "(objectclass=" + UserdataService.class.getName() + "))" +
            "(objectclass=" + PresenceService.class.getName() + "))" +
            "(objectclass=" + MessagingService.class.getName() + "))" +
            "(objectclass=" + GroupMessagingService.class.getName() + "))" +
            "(objectclass=" + SessionService.class.getName() + "))" +
            "(objectclass=" + IPLocatorService.class.getName() + "))" +
            "(objectclass=" + DiscussionService.class.getName() + "))" +
            "(objectclass=" + ForumModerationService.class.getName() + "))" +
            "(objectclass=" + StatisticsService.class.getName() + "))" +
            "(objectclass=" + TransformationService.class.getName() + "))" +
            "(objectclass=" + TemplatesService.class.getName() + "))" +
            "(objectclass=" + LoggingService.class.getName() + "))" +
            "(objectclass=" + SAXParserFactory.class.getName() + "))" +
            "(objectclass=" + PostOfficeService.class.getName() + "))" +
            "(objectclass=" + MaintenanceService.class.getName() + "))";


    try {
      //add the listener to the bundle context.
      bc.addServiceListener(serviceListener, filter);

      //ensure that already registered Service instances are registered with
      //the manager
      ServiceReference[] srl = bc.getServiceReferences(null, filter);
      for (int i = 0; srl != null && i < srl.length; i++) {
        serviceListener.serviceChanged(new ServiceEvent(ServiceEvent.REGISTERED, srl[i]));
      }
    } catch (InvalidSyntaxException ex) {
      ex.printStackTrace(System.err);
      return false;
    }
    return true;
  }//activate

  public void deactivate() {
    //null out the references
    m_SecurityService = null;
    m_SecurityManagementService = null;
    m_PolicyService = null;
    m_ConfigurationAdmin = null;
    m_MessageResourceService = null;
    m_UserdataService = null;
    m_ExecutionService = null;
    m_RndStringGeneratorService = null;
    m_PresenceService = null;
    m_MessagingService = null;
    m_SessionService = null;
    m_TemplatesService = null;
    m_ConfigurationMediator = null;
    m_IPLocatorService = null;
    m_DiscussionService = null;
    m_ForumModerationService = null;
    m_StatisticsService = null;
    m_GroupMessagingService = null;
    m_TransformationService = null;
    m_BankService = null;
    m_LoggingService = null;
    m_Logger = null;
    m_SAXParserFactory = null;
    m_MaintenanceService = null;
    m_PostOfficeService = null;

    //Latches
    m_SecurityServiceLatch = null;
    m_SecurityManagementServiceLatch = null;
    m_PolicyServiceLatch = null;
    m_RndStringGeneratorLatch = null;
    m_MessageResourceServiceLatch = null;
    m_ConfigurationAdminLatch = null;
    m_ExecutionServiceLatch = null;
    m_UserdataServiceLatch = null;
    m_PresenceServiceLatch = null;
    m_TemplatesServiceLatch = null;
    m_MessagingServiceLatch = null;
    m_SessionServiceLatch = null;
    m_IPLocatorServiceLatch = null;
    m_DiscussionServiceLatch = null;
    m_ForumModerationServiceLatch = null;
    m_StatisticsServiceLatch = null;
    m_GroupMessagingServiceLatch = null;
    m_TransformationServiceLatch = null;
    m_BankServiceLatch = null;
    m_LoggingServiceLatch = null;
    m_SAXParserFactoryLatch = null;
    m_MaintenanceServiceLatch = null;
    m_PostOfficeServiceLatch = null;

    m_BundleContext = null;
  }//deactivate

  private CountDownLatch createWaitLatch() {
    return new CountDownLatch(1);
  }//createWaitLatch

  private class ServiceListenerImpl
      implements ServiceListener {

    public void serviceChanged(ServiceEvent ev) {
      //System.err.println(ev.toString());
      ServiceReference sr = ev.getServiceReference();
      Object o = null;
      switch (ev.getType()) {
        case ServiceEvent.REGISTERED:
          o = m_BundleContext.getService(sr);
          if (o == null) {
            return;
          } else if (o instanceof SecurityService) {
            m_SecurityService = (SecurityService) o;
            m_SecurityServiceLatch.countDown();
          } else if (o instanceof SecurityManagementService) {
            m_SecurityManagementService = (SecurityManagementService) o;
            m_SecurityManagementServiceLatch.countDown();
          } else if (o instanceof PolicyService) {
            m_PolicyService = (PolicyService) o;
            m_PolicyServiceLatch.countDown();
          } else if (o instanceof RndStringGeneratorService) {
            m_RndStringGeneratorService = (RndStringGeneratorService) o;
            m_RndStringGeneratorLatch.countDown();
          } else if (o instanceof MessageResourceService) {
            m_MessageResourceService = (MessageResourceService) o;
            m_MessageResourceServiceLatch.countDown();
          } else if (o instanceof ConfigurationAdmin) {
            m_ConfigurationAdmin = (ConfigurationAdmin) o;
            m_ConfigurationAdminLatch.countDown();
          } else if (o instanceof ExecutionService) {
            m_ExecutionService = (ExecutionService) o;
            m_ExecutionServiceLatch.countDown();
          } else if (o instanceof UserdataService) {
            m_UserdataService = (UserdataService) o;
            m_UserdataServiceLatch.countDown();
          } else if (o instanceof PresenceService) {
            m_PresenceService = (PresenceService) o;
            m_PresenceServiceLatch.countDown();
          } else if (o instanceof MessagingService) {
            m_MessagingService = (MessagingService) o;
            m_MessagingServiceLatch.countDown();
          } else if (o instanceof GroupMessagingService) {
            m_GroupMessagingService = (GroupMessagingService) o;
            m_GroupMessagingServiceLatch.countDown();
          } else if (o instanceof TemplatesService) {
            m_TemplatesService = (TemplatesService) o;
            m_TemplatesServiceLatch.countDown();
          } else if (o instanceof SessionService) {
            m_SessionService = (SessionService) o;
            m_SessionServiceLatch.countDown();
          } else if (o instanceof IPLocatorService) {
            m_IPLocatorService = (IPLocatorService) o;
            m_IPLocatorServiceLatch.countDown();
          } else if (o instanceof DiscussionService) {
            m_DiscussionService = (DiscussionService) o;
            m_DiscussionServiceLatch.countDown();
          } else if (o instanceof ForumModerationService) {
            m_ForumModerationService = (ForumModerationService) o;
            m_ForumModerationServiceLatch.countDown();
          } else if (o instanceof StatisticsService) {
            m_StatisticsService = (StatisticsService) o;
            m_StatisticsServiceLatch.countDown();
          } else if (o instanceof TransformationService) {
            m_TransformationService = (TransformationService) o;
            m_TransformationServiceLatch.countDown();
          } else if (o instanceof BankService) {
            m_BankService = (BankService) o;
            m_BankServiceLatch.countDown();
          } else if (o instanceof LoggingService) {
            m_LoggingService = (LoggingService) o;
            //prepare Log
            m_Logger = m_LoggingService.getLogger(m_BundleContext.getBundle().getSymbolicName());
            m_LoggingServiceLatch.countDown();
          } else if (o instanceof SAXParserFactory) {
            m_SAXParserFactory = (SAXParserFactory) o;
            m_SAXParserFactory.setValidating(false);
            m_SAXParserFactory.setNamespaceAware(true);
            m_SAXParserFactoryLatch.countDown();
          } else if (o instanceof MaintenanceService) {
            m_MaintenanceService = (MaintenanceService) o;
            m_MaintenanceServiceLatch.countDown();
          } else if (o instanceof PostOfficeService) {
            m_PostOfficeService = (PostOfficeService) o;
            m_PostOfficeServiceLatch.countDown();
          } else {
            m_BundleContext.ungetService(sr);
          }

          break;
        case ServiceEvent.UNREGISTERING:
          o = m_BundleContext.getService(sr);
          if (o == null) {
            return;
          } else if (o instanceof SecurityService) {
            m_SecurityService = null;
            m_SecurityServiceLatch = createWaitLatch();
          } else if (o instanceof SecurityManagementService) {
            m_SecurityManagementService = null;
            m_SecurityManagementServiceLatch = createWaitLatch();
          } else if (o instanceof PolicyService) {
            m_PolicyService = null;
            m_PolicyServiceLatch = createWaitLatch();
          } else if (o instanceof RndStringGeneratorService) {
            m_RndStringGeneratorService = null;
            m_RndStringGeneratorLatch = createWaitLatch();
          } else if (o instanceof MessageResourceService) {
            m_MessageResourceService = null;
            m_MessageResourceServiceLatch = createWaitLatch();
          } else if (o instanceof ConfigurationAdmin) {
            m_ConfigurationAdmin = null;
            m_ConfigurationAdminLatch = createWaitLatch();
          } else if (o instanceof ExecutionService) {
            m_ExecutionService = null;
            m_ExecutionServiceLatch = createWaitLatch();
          } else if (o instanceof UserdataService) {
            m_UserdataService = null;
            m_UserdataServiceLatch = createWaitLatch();
          } else if (o instanceof PresenceService) {
            m_PresenceService = null;
            m_PresenceServiceLatch = createWaitLatch();
          } else if (o instanceof MessagingService) {
            m_MessagingService = null;
            m_MessagingServiceLatch = createWaitLatch();
          } else if (o instanceof GroupMessagingService) {
            m_GroupMessagingService = null;
            m_GroupMessagingServiceLatch = createWaitLatch();
          } else if (o instanceof TemplatesService) {
            m_TemplatesService = null;
            m_TemplatesServiceLatch = createWaitLatch();
          } else if (o instanceof SessionService) {
            m_SessionService = null;
            m_SessionServiceLatch = createWaitLatch();
          } else if (o instanceof IPLocatorService) {
            m_IPLocatorService = null;
            m_IPLocatorServiceLatch = createWaitLatch();
          } else if (o instanceof DiscussionService) {
            m_DiscussionService = null;
            m_DiscussionServiceLatch = createWaitLatch();
          } else if (o instanceof ForumModerationService) {
            m_ForumModerationService = null;
            m_ForumModerationServiceLatch = createWaitLatch();
          } else if (o instanceof StatisticsService) {
            m_StatisticsService = null;
            m_StatisticsServiceLatch = createWaitLatch();
          } else if (o instanceof TransformationService) {
            m_TransformationService = null;
            m_TransformationServiceLatch = createWaitLatch();
          } else if (o instanceof BankService) {
            m_BankService = null;
            m_BankServiceLatch = createWaitLatch();
          } else if (o instanceof LoggingService) {
            m_LoggingService = null;
            m_Logger = null;
            m_LoggingServiceLatch = createWaitLatch();
          } else if (o instanceof SAXParserFactory) {
            m_SAXParserFactory = null;
            m_SAXParserFactoryLatch = createWaitLatch();
          } else if (o instanceof MaintenanceService) {
            m_MaintenanceService = null;
            m_MaintenanceServiceLatch = createWaitLatch();
          } else if (o instanceof PostOfficeService) {
            m_PostOfficeService = null;
            m_PostOfficeServiceLatch = createWaitLatch();
          } else {
            m_BundleContext.ungetService(sr);
          }
          break;
      }
    }
  }//inner class ServiceListenerImpl

  public static long WAIT_UNLIMITED = -1;
  public static long NO_WAIT = 0;

}//class ServiceMediator
