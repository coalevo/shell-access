/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.impl;

import net.coalevo.asciicaptcha.Captcha;
import net.coalevo.asciicaptcha.CaptchaGenerator;
import net.coalevo.foundation.model.Agent;
import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.iplocator.model.GeoLocation;
import net.coalevo.iplocator.service.IPLocatorService;
import net.coalevo.security.service.SecurityManagementService;
import net.coalevo.shellaccess.filters.ASCIIInputFilter;
import net.coalevo.shellaccess.filters.PasswordFilter;
import net.coalevo.shellaccess.model.Services;
import net.coalevo.shellaccess.validators.NewAIDValidator;
import net.coalevo.shellaccess.validators.NewEMailValidator;
import net.coalevo.shellaccess.validators.NewNickValidator;
import net.coalevo.userdata.model.EditableUserdata;
import net.coalevo.userdata.service.UserdataService;
import net.wimpi.telnetd.io.toolkit.BufferOverflowException;
import net.wimpi.telnetd.io.toolkit.Editfield;
import net.wimpi.telnetd.io.toolkit.InputFilter;
import net.wimpi.telnetd.io.toolkit.InputValidator;
import net.wimpi.telnetd.net.Connection;
import net.wimpi.telnetd.net.ConnectionEvent;
import net.wimpi.telnetd.shell.Shell;
import org.osgi.service.prefs.Preferences;
import org.osgi.service.prefs.PreferencesService;
import org.slf4j.Logger;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.IOException;
import java.net.InetAddress;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Provides a {@link Shell} implementation for creating new users.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class NewUserShell
    extends BaseShell {

  private static Marker c_LogMarker = MarkerFactory.getMarker(NewUserShell.class.getName());
  private Marker m_PolicyMarker = MarkerFactory.getMarker("POLICY");

  /**
   * Overrides the run method to handle the login
   * process.
   */
  public void run(Connection con) {
    try {
      UserdataService ud = getServices().getUserdataService(Services.NO_WAIT);
      SecurityManagementService secs = getServices().getSecurityManagementService(Services.NO_WAIT);

      if (ud == null || secs == null) {
        m_ShellIO.printTemplate("newuser_missingservices");
        return;
      }

      //1. prepare shell
      prepare(con, this);

      //2. set language
      Integer newlang = (Integer) m_Environment.get(NEWLANGUAGE_KEY);
      m_ShellIO.setLanguageIndex(newlang);

      //3. Print welcome
      m_ShellIO.pageTemplate("newuser_welcome");
      m_ShellIO.printTemplate("newuser_promptcreate");
      //ask if user wants to continue
      if (!m_ShellIO.getDecision()) {
        returnToLogin(con);
        return;
      }

      //4. Tell humans and computers apart
      m_ShellIO.printTemplate("newuser_captcha");
      if (!passesCaptcha()) {
        m_ShellIO.printTemplate("newuser_captchafailed");
        return;
      } else {
        m_ShellIO.printTemplate("newuser_registrationinfo");
      }

      //5. Identifier
      AgentIdentifier aid = new AgentIdentifier(
          prompt(
              "newuser_promptidentifier",
              null,
              new ASCIIInputFilter(false, m_ShellIO.formatTemplate("inputerror_asciifilter")),
              new NewAIDValidator(
                  getServices(),
                  m_ShellIO.formatTemplate("newuser_nouds"),
                  m_ShellIO.formatTemplate("newuser_aidsyntax"),
                  m_ShellIO.formatTemplate("newuser_aidduplicate")),
              50,
              255
          )
      );
      //6. Nickname
      String nickname =
          prompt(
              "newuser_promptnick",
              null,
              new ASCIIInputFilter(false, m_ShellIO.formatTemplate("inputerror_asciifilter")),
              new NewNickValidator(
                  getServices(),
                  m_ShellIO.formatTemplate("newuser_nouds"),
                  m_ShellIO.formatTemplate("newuser_nicksyntax"),
                  m_ShellIO.formatTemplate("newuser_nickduplicate")),
              50,
              255
          );

      //7. eMail
      String email =
          prompt(
              "newuser_promptemail",
              null,
              new ASCIIInputFilter(false, m_ShellIO.formatTemplate("inputerror_asciifilter")),
              new NewEMailValidator(
                  getServices(),
                  m_ShellIO.formatTemplate("newuser_nouds"),
                  m_ShellIO.formatTemplate("newuser_emailsyntax"),
                  m_ShellIO.formatTemplate("newuser_emailduplicate"),
                  m_ShellIO.formatTemplate("newuser_emailanon")
              ),
              60,
              255
          );

      //8. Name
      String firstname = prompt("newuser_promptfirstname", null, null, null, 60, 255);
      String lastname = prompt("newuser_promptlastname", null, null, null, 60, 255);

      //9. Address
      InetAddress addr = con.getConnectionData().getInetAddress();
      if (addr.isLoopbackAddress() || addr.isAnyLocalAddress() || addr.isSiteLocalAddress()) {
        addr = InetAddress.getByName("www.karanet.at");
      }
      IPLocatorService iploc = getServices().getIPLocatorService(ServiceMediator.NO_WAIT);
      GeoLocation geoloc = iploc.getGeoLocation(addr);
      String country = prompt(
          "newuser_promptcountry",
          geoloc.getCountry(),
          new ASCIIInputFilter(true, m_ShellIO.formatTemplate("inputerror_asciifilter")),
          null,
          2,
          2
      );

      String city = prompt(
          "newuser_promptcity",
          geoloc.getCity(),
          null,
          null,
          50,
          255
      );
      String pcode = prompt(
          "newuser_promptpcode",
          null,
          new ASCIIInputFilter(false, m_ShellIO.formatTemplate("inputerror_asciifilter")),
          null,
          30,
          50
      );

      //10. Password
      String password;
      String passagain;
      do {
        m_ShellIO.printTemplate("newuser_password");
        m_ShellIO.printTemplate("newuser_promptpass");
        Editfield ef = new Editfield(m_ShellIO.getTerminalIO(), "newpass", 30, 255);
        ef.setIgnoreDelete(true);
        ef.setInputFilter(new PasswordFilter(ef));
        final String pwderr = m_ShellIO.formatTemplate("inputerror_passwordvalidator");
        ef.setInputValidator(new InputValidator() {

          public boolean validate(String string) {
            if (string == null || string.length() < 8 || string.length() > 25) {
              return false;
            }
            //todo: probably add other password checks?
            return true;
          }

          public String getErrorMessage() {
            return pwderr;
          }
        });
        ef.setHiddenInput(true); //if tabbed, will be set to false!
        ef.run();
        password = ef.getValue();

        m_ShellIO.printTemplate("newuser_promptpassagain");
        ef = new Editfield(m_ShellIO.getTerminalIO(), "newpassagain", 30, 255);
        ef.setHiddenInput(true);
        ef.setIgnoreDelete(true);
        ef.run();
        passagain = ef.getValue();

      } while (!password.equals(passagain));

      //11. Accept policy
      m_ShellIO.printTemplate("newuser_promptpolicy");
      //ask if user wants to continue
      if (!m_ShellIO.getDecision()) {
        returnToLogin(con);
        return;
      }
      Activator.log().info(
          m_PolicyMarker,
          Activator.getBundleMessages().get("NewUserShell.policy", "aid", aid.getIdentifier())
      );

      //12. Create agent and userdata
      try {
        Agent agent = secs.registerAgent(getServices().getServiceAgent(), aid, true);
        secs.createAuthentication(getServices().getServiceAgent(), agent, password);
        secs.grantRole(
            getServices().getServiceAgent(),
            agent,
            secs.getRole(
                getServices().getServiceAgent(),
                "User"
            )
        );
        m_ShellIO.printTemplate("newuser_createdaccount");

        //13. Create userdata
        EditableUserdata udata = ud.beginUserdataCreate(
            getServices().getServiceAgent(),
            aid
        );
        udata.setNickname(nickname);
        udata.setEmail(email);
        udata.setFirstnames(firstname);
        udata.setLastnames(lastname);
        udata.setCountry(country);
        udata.setCity(city);
        udata.setPostalCode(pcode);

        ud.commitUserdata(getServices().getServiceAgent(), udata);
        m_ShellIO.printTemplate("newuser_createduserdata");

      } catch (Exception ex) {
        m_ShellIO.printTemplate("newuser_createerror");
        Activator.log().error(c_LogMarker, "run()::create", ex);
        return;
      }

      //14. Store language preference
      try {
        PreferencesService ps = getServices().getPreferencesService();
        if (ps != null) {
          Preferences p = ps.getUserPreferences(aid.getIdentifier());
          p.put("options.session.language", m_ShellIO.getLocale().getLanguage());
          p.flush();
        }
      } catch (Exception ex) {
        Activator.log().error(c_LogMarker, "run()::languageprefs", ex);
      }

      //15. Return to login shell
      con.setNextShell("net.coalevo.shellaccess.impl.LoginShell");
      m_Connection.removeConnectionListener(this);
      return;

    } catch (IOException ex) {
      Activator.log().info(
          c_LogMarker,
          "run()",
          ex
      );
    } finally {

    }
  }//run

  private void returnToLogin(Connection con) throws IOException {
    m_ShellIO.blankTerminal();
    con.setNextShell("net.coalevo.shellaccess.impl.LoginShell");
    m_Connection.removeConnectionListener(this);
  }//returnToLogin

  //*** Helper methods ***//
  private boolean passesCaptcha() throws IOException {
    final CountDownLatch latch = new CountDownLatch(1);
    final AtomicBoolean passedCaptcha = new AtomicBoolean(false);
    final CaptchaGenerator cg = Activator.getCaptchaGenerator();
    final Logger log = Activator.log();
    //Needs to be run on ServiceAgent (no authenticated user!)
    getServices().getExecutionService(0).execute(
        getServices().getServiceAgent(),
        new Runnable() {
          public void run() {
            try {
              //1. Display Captcha
              Captcha c = cg.generate();
              m_ShellIO.println(c.getDisplay());
              m_ShellIO.printTemplate("newuser_captchaprompt");
              m_ShellIO.flush();
              Editfield eField = new Editfield(m_TermIO, "captcha", 4);
              eField.run();
              if (!c.equals(eField.getValue())) {
                log.error(
                    Activator.getBundleMessages().get("NewUserShell.captchafailed")
                );
              } else {
                passedCaptcha.set(true);
                log.info(
                    Activator.getBundleMessages().get("NewUserShell.captchapassed")
                );
              }
            } catch (Exception ex) {
              log.error("passesCaptcha()", ex);
            }
            finally {
              latch.countDown();
            }
          }
        }
    );

    try {
      latch.await(60 * 2, TimeUnit.SECONDS);
    } catch (Exception ex) {
      Activator.log().error("passesCaptcha()", ex);
      return false;
    }
    return passedCaptcha.get();
  }//passesCaptcha

  protected String prompt(String key, String value, InputFilter inf, InputValidator iv, int visize, int size)
      throws IOException {

    m_ShellIO.printTemplate(key);
    Editfield ef = new Editfield(m_ShellIO.getTerminalIO(), "input", visize, size);
    ef.setIgnoreDelete(true);
    if (value != null) {
      try {
        ef.setValue(value);
      } catch (BufferOverflowException e) {
        Activator.log().error(c_LogMarker, "prompt()", e);
      }
    }
    if (inf != null) {
      ef.setInputFilter(inf);
    }
    if (iv != null) {
      ef.setInputValidator(iv);
    }

    ef.run();
    return ef.getValue();
  }//prompt


  //*** Connection Event Handlers ***//
  public void connectionIdle(ConnectionEvent event) {
    getServices().getExecutionService(0).execute(
        getServices().getServiceAgent(),
        new Runnable() {
          public void run() {
            try {
              Activator.log().debug(c_LogMarker, "connectionIdle()");
              m_ShellIO.printTemplate("connection_event_idle_warning");
            } catch (Throwable t) {
              t.printStackTrace(System.err);
            }
          }
        });
  }//connectionIdle

  public void connectionTimedOut(ConnectionEvent ce) {
    getServices().getExecutionService(0).execute(
        getServices().getServiceAgent(),
        new LogoutHandler("connectionTimedOut()", "connection_event_idle_loggedout"));
  }//connectionTimedOut

  public void connectionLogoutRequest(ConnectionEvent ce) {
    getServices().getExecutionService(0).execute(
        getServices().getServiceAgent(),
        new LogoutHandler("connectionLogoutRequest()", "connection_event_logoutrequest"));
  }//connectionLogoutRequest

  public void connectionSentBreak(ConnectionEvent ce) {
    Activator.log().debug(c_LogMarker, "connectionSentBreak");
  }//connectionSentBreak


  final class LogoutHandler implements Runnable {

    private String m_Debug;
    private String m_Reason;

    public LogoutHandler(String debug, String reason) {
      m_Debug = debug;
      m_Reason = reason;
    }//constructor

    public void run() {
      try {
        Activator.log().debug(m_Debug);
        m_ShellIO.pageTemplate(m_Reason);
      } catch (Throwable t) {
        t.printStackTrace(System.err);
      }
      try {
        forceTelnetClose();
      } catch (Throwable t) {
        t.printStackTrace(System.err);
      }      
    }//run

  }//inner class LogoutHandler

}//class NewUserShell
