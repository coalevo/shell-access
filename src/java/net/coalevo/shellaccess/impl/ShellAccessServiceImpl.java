/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.impl;

import net.coalevo.foundation.model.Action;
import net.coalevo.foundation.model.BaseService;
import net.coalevo.security.model.ServiceAgentProxy;
import net.coalevo.shellaccess.service.ShellAccessService;
import org.osgi.framework.BundleContext;

/**
 * Implements {@link ShellAccessService}.
 * <p/>
 * Note at the moment this is for auth purposes only.
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class ShellAccessServiceImpl
    extends BaseService
    implements ShellAccessService {

  private ServiceAgentProxy m_ServiceAgentProxy;

  public ShellAccessServiceImpl() {
    super(ShellAccessService.class.getName(), new Action[0]);
  }//constructor

  public boolean activate(BundleContext bc) {
    //1. Authenticate ourself
    m_ServiceAgentProxy = new ServiceAgentProxy(this, Activator.log());
    m_ServiceAgentProxy.activate(bc);
    return true;
  }//activate

  public boolean deactivate() {
    if (m_ServiceAgentProxy != null) {
      m_ServiceAgentProxy.deactivate();
      m_ServiceAgentProxy = null;
    }
    return true;
  }//deactivate

  public ServiceAgentProxy getServiceAgentProxy() {
    return m_ServiceAgentProxy;
  }//getServiceAgent

}//class ShellAccessServiceImpl
