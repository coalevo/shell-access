/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.impl;

import net.wimpi.telnetd.shell.Shell;
import net.wimpi.telnetd.shell.ShellService;
import org.osgi.framework.BundleContext;

/**
 * This class implements a {@link ShellService} for
 * the {@link CommandShell}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class CommandShellService
    implements ShellService {

  private final String m_Identifier;

  public CommandShellService() {
    m_Identifier = CommandShell.class.getName();
  }//constructor

  public boolean activate(BundleContext bundleContext) {
    return false;  //To change body of implemented methods use File | Settings | File Templates.
  }

  public void deactivate() {
    //To change body of implemented methods use File | Settings | File Templates.
  }

  public String getIdentifier() {
    return m_Identifier;
  }//getIdentifier

  public Shell createShell() {
    return new CommandShell();
  }//createShell

}//class CommandShellService
