/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.impl;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.model.Session;
import net.coalevo.foundation.model.UserAgent;
import net.coalevo.iplocator.model.GeoLocation;
import net.coalevo.iplocator.service.IPLocatorService;
import net.coalevo.presence.model.Presence;
import net.coalevo.presence.model.PresenceConstants;
import net.coalevo.presence.model.PresenceStatus;
import net.coalevo.presence.model.PresenceStatusType;
import net.coalevo.security.model.AuthenticationException;
import net.coalevo.security.service.SecurityService;
import net.coalevo.shellaccess.model.Services;
import net.coalevo.shellaccess.model.ShellAccessSession;
import net.coalevo.system.service.SessionService;

import java.net.InetAddress;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Class implementing a dummy test session.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class ShellAccessSessionImpl
    implements ShellAccessSession {

  private ServiceMediator m_Services;
  private AtomicBoolean m_LoggedIn = new AtomicBoolean(false);
  private boolean m_Expert = false;
  private UserAgent m_UserAgent;
  private final long m_StartTime;
  private String m_Host;
  private String m_Doing = "Nothing.";
  private String m_BossKeyPrompt = "Fangorn: ~root$";
  private SessionManager m_SessionManager;
  private Map<AgentIdentifier, String> m_Messaging;
  private InetAddress m_Address;
  private boolean m_Host2Country = false;
  private boolean m_EventsWhileReading = true;
  private long m_AutoAwayTime = 1000 * 60 * 30; //30 mins
  private boolean m_Away;
  private PresenceStatusType m_LoginPresenceStatusType;
  private boolean m_GroupConfirmations;
  private boolean m_SysguideActive = false;
  private boolean m_GroupPresenceNotification = true;
  private TimeZone m_TimeZone = TimeZone.getDefault();
  private boolean m_Whiner = false;
  private boolean m_PresenceNotification = true;
  private boolean m_Confirmations = true;
  private Set<String> m_AutoJoinGroups;
  private boolean m_RebindDelete;

  public ShellAccessSessionImpl(SessionManager smgr) {
    m_SessionManager = smgr;
    m_StartTime = System.currentTimeMillis();
    m_Services = Activator.getServices();
    m_Messaging = new ConcurrentHashMap<AgentIdentifier, String>();
  }//ShellAccessSessionImpl

  public boolean isLoggedIn() {
    return m_LoggedIn.get();
  }//isLoggedIn

  public String getUsername() {
    return m_UserAgent.getAgentIdentifier().getName();
  }//getUsername

  public void login(String username, String password) throws AuthenticationException {
    SessionService sesservice = m_Services.getSessionService(5000);
    Session session = sesservice.initiateSession(
        username, password, 60 * 60 * 10, TimeUnit.SECONDS); //2h idle telnet, 4*longIdle Recht
    m_UserAgent = session.getAgent();
    m_SessionManager.addSession(this);
    m_LoggedIn.set(true);
  }//login

  public UserAgent getUserAgent() {
    return m_UserAgent;
  }//getUserAgent

  public boolean isAway() {
    return m_Away;
  }//isAway

  public void setAway(boolean away) {
    m_Away = away;
  }//setAway

  public void setExpert(boolean b) {
    m_Expert = b;
  }//setExpert

  public boolean isExpert() {
    return m_Expert;
  }//isExpert

  public long getStartTime() {
    return m_StartTime;
  }//getStartTime

  public InetAddress getAddress() {
    return m_Address;
  }//getAddress

  public void setAddress(InetAddress addr) {
    m_Address = addr;
  }//setAddress

  public boolean isHostToCountry() {
    return m_Host2Country;
  }//isHost2Country

  public void setHostToCountry(boolean b) {
    if (m_Host2Country && !b) {
      m_Host = m_Address.getHostName();
    }
    if (!m_Host2Country && b) {
      if (m_Address.isLoopbackAddress() || m_Address.isAnyLocalAddress() || m_Address.isSiteLocalAddress()) {
        m_Host = m_Address.getHostName();
      } else {
        try {
          IPLocatorService ipls = m_Services.getIPLocatorService(Services.NO_WAIT);
          GeoLocation gloc = ipls.getGeoLocation(m_Address);
          m_Host = String.format("%s, %s", gloc.getCity(), gloc.getCountry());
          m_TimeZone = gloc.getTimeZone();
        } catch (Exception ex) {
          m_Host = m_Address.getHostName();
        }
      }
    }
    m_Host2Country = b;
  }//setHostToCountry

  public String getHost() {
    if (m_Host == null) {
      if (m_Address != null) {
        //will capture also cases when country couldnt be resolved
        m_Host = m_Address.getHostName();
      } else {
        m_Host = "";
      }
    }
    return m_Host;
  }//geTHost

  public void setHost(String host) {
    m_Host = host;
  }//setHost

  public String getDoing() {
    return m_Doing;
  }//getDoing

  public void setDoing(String doing) {
    m_Doing = doing;
  }//setDoing

  public boolean isGroupConfirmations() {
    return m_GroupConfirmations;
  }//isGroupConfirmations

  public void setGroupConfirmations(boolean b) {
    m_GroupConfirmations = b;
  }//setGroupConfirmations;

  public boolean isConfirmations() {
    return m_Confirmations;
  }//isConfirmations

  public void setConfirmations(boolean confirmations) {
    m_Confirmations = confirmations;
  }//setConfirmations

  public boolean isEventsWhileReading() {
    return m_EventsWhileReading;
  }//isEventsWhileReading

  public void setEventsWhileReading(boolean b) {
    m_EventsWhileReading = b;
  }//setEventsWhileReading

  public String getBossKeyPrompt() {
    return m_BossKeyPrompt;
  }//getBossKeyPrompt

  public void setBossKeyPrompt(String bossKeyPrompt) {
    m_BossKeyPrompt = bossKeyPrompt;
  }//setBossKeyPrompt

  public long getAutoAwayTime() {
    return m_AutoAwayTime;
  }//getAutoAwayTime

  public void setAutoAwayTime(long autoAwayTime) {
    m_AutoAwayTime = autoAwayTime;
  }//setAutoAwayTime

  public PresenceStatusType getLoginPresenceStatusType() {
    return m_LoginPresenceStatusType;
  }//getLoginPresenceStatusType

  public void setLoginPresenceStatusType(PresenceStatusType lpst) {
    m_LoginPresenceStatusType = lpst;
  }//getLoginPresenceStatusType

  public String[] getRoles() {
    SecurityService secs = m_Services.getSecurityService(0);
    if (secs != null) {
      Set<String> s = secs.getAgentRolesByName(
          m_UserAgent,
          m_UserAgent.getAgentIdentifier()
      );
      return s.toArray(new String[s.size()]);
    }
    return new String[0];
  }//getRoles

  public boolean isSysguideActive() {
    return m_SysguideActive;
  }//isSysguideActive

  public void setSysguideActive(boolean sysguideActive) {
    if (sysguideActive) {
      //check role
      SecurityService secs = m_Services.getSecurityService(0);
      //Activator.log().debug("setSysguideActive=" + sysguideActive + "::haveSecService=" + (secs!=null));
      if (secs != null) {
        m_SysguideActive = secs.getAuthorizations(m_UserAgent).hasRole("Sysguide");
        //Activator.log().debug("setSysguideActive=" + m_SysguideActive);
      }
    } else {
      m_SysguideActive = false;
    }
  }//setSysguideActive

  public boolean isGroupPresenceNotification() {
    return m_GroupPresenceNotification;
  }//isGroupPresenceNotification

  public void setGroupPresenceNotification(boolean groupPresenceNotification) {
    m_GroupPresenceNotification = groupPresenceNotification;
  }//setGroupPresenceNotification

  public boolean isPresenceNotification() {
    return m_PresenceNotification;
  }//isPresenceNotification

  public void setPresenceNotification(boolean presenceNotification) {
    m_PresenceNotification = presenceNotification;
  }//setPresenceNotification

  /**
   * Invalidates the session removing the reference
   * from the {@link SessionManager}.
   * The call will also invalidate the coalevo session,
   * if the session is valid.
   */
  public void invalidate() {
    if (m_LoggedIn.getAndSet(false)) {
      Session s = m_UserAgent.getSession();
      if (s.isValid()) {
        try {
          m_UserAgent.getSession().invalidate();
        } catch (Exception ex) {
        }
      }
      m_SessionManager.removeSession(this);
    } else {
      m_SessionManager.removeSession(this);
    }
  }//invalidate

  public void activity() {
    if (!m_LoggedIn.get()) {
      return;
    }
    if (m_UserAgent != null) {
      net.coalevo.foundation.model.Session s = m_UserAgent.getSession();
      if (s != null) {
        s.access();
        if (m_Away) {
          try {
            if (s != null && s.isValid()) {
              Presence p = (Presence)
                  s.getAttribute(PresenceConstants.PRESENCE_SESSION_ATTRIBUTE);
              PresenceStatus ps = p.getStatus();
              //store old
              PresenceStatusType pst = (PresenceStatusType) s.getAttribute("net.coalevo.presence.model.PresenceStatusType_last");
              ps.setType(pst);
              p.updateStatus(ps);
              m_Away = false;
            }
          } catch (Exception ex) {
          }
        }
      }
    }
  }//activity

  public Set listMessaging() {
    return m_Messaging.keySet();
  }//listMessaging

  public void addMessaging(AgentIdentifier aid, String msgid) {
    m_Messaging.put(aid, msgid);
  }//addMessaging

  public void removeMessaging(AgentIdentifier aid, String msgid) {
    m_Messaging.remove(aid);
  }//removeMessaging

  public boolean isMessaging(AgentIdentifier aid) {
    return m_Messaging.containsKey(aid);
  }//isMessaging

  public Session getCoalevoSession() {
    if (m_UserAgent != null) {
      return m_UserAgent.getSession();
    }
    return null;
  }//getCoalevoSession

  public TimeZone getTimeZone() {
    return m_TimeZone;
  }//getTimeZone

  public boolean isWhiner() {
    return m_Whiner;
  }//isWhiner

  public void setWhiner(boolean whiner) {
    m_Whiner = whiner;
  }//setWhiner

  public boolean isRebindDelete() {
    return m_RebindDelete;
  }//isRebindDelete

  public void setRebindDelete(boolean rebindDelete) {
    m_RebindDelete = rebindDelete;
  }//setRebindDelete

  public void setAutoJoinGroups(String str) {
    String[] groups = str.split(",");
    if (groups != null && groups.length > 0) {
      m_AutoJoinGroups = new TreeSet<String>();
      m_AutoJoinGroups.addAll(Arrays.asList(groups));
    }
  }//setAutoJoinGroups

  public Set<String> getAutoJoinGroups() {
    return m_AutoJoinGroups;
  }//getAutoJoinGroups

  public boolean isAutoJoinGroup(String str) {
    return m_AutoJoinGroups != null && m_AutoJoinGroups.contains(str);
  }//isAutoJoinGroup

}//class ShellAccessSessionImpl
