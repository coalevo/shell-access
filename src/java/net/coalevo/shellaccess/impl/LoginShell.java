/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.impl;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.security.model.AuthenticationException;
import net.coalevo.shellaccess.filters.NicknameInputFilter;
import net.coalevo.shellaccess.model.ShellAccessSession;
import net.coalevo.shellaccess.validators.AIDNickValidator;
import net.coalevo.userdata.service.UserdataService;
import net.wimpi.telnetd.io.toolkit.Editfield;
import net.wimpi.telnetd.net.Connection;
import net.wimpi.telnetd.net.ConnectionEvent;
import net.wimpi.telnetd.shell.Shell;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Provides the shell access login {@link Shell} implementation.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class LoginShell
    extends BaseShell {

  private static final Marker c_LogMarker = MarkerFactory.getMarker(LoginShell.class.getName());
  private AtomicBoolean m_LoginInProgress = new AtomicBoolean(false);
  private ServiceMediator m_Services = Activator.getServices();

  /**
   * Clears resources for the shell to be recycled.
   */
  public void reset() {
    m_LoginInProgress.set(false);
  }//reset

  /**
   * Overrides the run method to handle the login
   * process.
   */
  public void run(Connection con) {
    try {
      Thread.currentThread().setContextClassLoader(getClass().getClassLoader());
      //debug
      Activator.log().debug(c_LogMarker, "run():" + this.toString() + ":" + con.toString());

      //1. prepare shell and output
      prepare(con, this);
      Activator.log().debug(c_LogMarker, "run(): Environment=" + m_ConData.getEnvironment());

      m_TermIO.setAutoflushing(false);  //controlled flush on all outputs

      //m_ShellIO.blankTerminal(); //start on a new line
      m_ShellIO.println();
      m_ShellIO.println();
      
      //2. Output banner
      m_ShellIO.printTemplate(
          Activator.getServices().getConfigMediator().getConfiguration().getString("banner.key")
      );

      //3. get login
      ShellAccessSession ses = m_Services.getSessionManager().createSession();
      int attempts = 3;
      do {
        String username = getUsername();
        m_ShellIO.println();

        // Handle user creation
        if ("new".equals(username)) {
          //System.out.println("===> handling new user");
          m_Connection.setNextShell("net.coalevo.shellaccess.impl.NewUserShell");
          m_Connection.removeConnectionListener(this);
          return;
        }

        // Handle login
        String password = getPassword();
        try {
          ses.login(username, password);
        } catch (AuthenticationException ex) {
          m_ShellIO.printTemplate("login_authfailure");
        }
        m_ShellIO.println();
        m_ShellIO.flush();
      } while (!ses.isLoggedIn() && --attempts > 0);

      if (ses.isLoggedIn()) {
        ses.setAddress(m_ConData.getInetAddress());
        setSession(ses);
        m_Connection.setNextShell("net.coalevo.shellaccess.impl.CommandShell");
        m_Connection.removeConnectionListener(this);
        return;
      } else {
        m_ShellIO.printTemplate("login_goodbye");
      }
    } catch (Exception ex) {
      Activator.log().error(c_LogMarker, "run()", ex);
    }
  }//run

  public String getUsername() throws IOException {
    if (this.isEnviromentVariableSet("login.username")) {
      String tmp = (String) getEnvironmentVariable("login.username");
      //unset it to allow editing second time around
      unsetEnvironmentVariable("login.username");
      //ignore that default many may use
      if (!"bbs".equals(tmp) && !"bbsnew".equals(tmp) && !"new".equals(tmp)) {
        m_ShellIO.printTemplate("login_username");
        m_ShellIO.print(tmp);
        UserdataService ud = Activator.getServices().getUserdataService(0);
        AgentIdentifier[] aids = ud.getIdentifiers(Activator.getServices().getServiceAgent(), tmp);
        if (aids != null && aids.length > 0) {
          tmp = aids[0].getIdentifier();
        }
        m_ShellIO.flush();
        return tmp;
      }
    }

    //output label
    m_ShellIO.printTemplate("login_username");
    Editfield eField = new Editfield(m_TermIO, "username", 20);
    eField.setInputValidator(
        new AIDNickValidator(m_Services, m_ShellIO.formatTemplate("inputerror_aidnickvalidator"))
    );
    eField.setInputFilter(
        new NicknameInputFilter(false, m_ShellIO.formatTemplate("inputerror_asciifilter"))
    );
    eField.setInsertMode(true);
    eField.setIgnoreDelete(true);
    eField.run();

    String uname = eField.getValue();
    String usernew = uname.toUpperCase();
    // handle new account
    List<String> newuser = Arrays.asList(
        m_ShellIO.formatAllTemplates("login_new")
    );
    if (newuser.contains(usernew)) {
      m_Environment.put(NEWLANGUAGE_KEY, newuser.indexOf(usernew));
      return "new";
    }
    if (uname.contains("@")) {
      //agent login
      return uname;
    } else {
      //assume nickname login
      UserdataService ud = Activator.getServices().getUserdataService(0);
      AgentIdentifier[] aids = ud.getIdentifiers(Activator.getServices().getServiceAgent(), uname);
      if (aids == null || aids.length == 0 || aids.length > 1) {
        return uname;
      } else {
        return aids[0].getIdentifier();
      }
    }
  }//getUsername

  public String getPassword() throws IOException {
    //output password label
    m_ShellIO.printTemplate("login_password");
    m_ShellIO.flush();
    //get password
    Editfield eField = new Editfield(m_TermIO, "password", 40);
    eField.setInsertMode(false);
    eField.setHiddenInput(true);
    eField.setIgnoreDelete(true);
    eField.run();
    return eField.getValue();
  }//getString


  public void connectionIdle(ConnectionEvent ce) {
/*
    getServices().getExecutionService(0).execute(
        getServices().getServiceAgent(),
        new Runnable() {
          public void run() {
            try {
              Activator.log().debug(c_LogMarker, "connectionIdle()");
              m_ShellIO.printTemplate("connection_event_idle_warning");
            } catch (Throwable t) {
              t.printStackTrace(System.err);
            }
          }
        });
*/
  }//connectionIdle

  public void connectionTimedOut(ConnectionEvent ce) {
    getServices().getExecutionService(0).execute(
        getServices().getServiceAgent(), new LogoutHandler("connectionTimedOut()", "connection_event_idle_loggedout"));
  }//connectionTimedOut

  public void connectionLogoutRequest(ConnectionEvent ce) {
    getServices().getExecutionService(0).execute(
        getServices().getServiceAgent(), new LogoutHandler("connectionLogoutRequest()", "connection_event_logoutrequest"));
  }//connectionLogoutRequest

  public void connectionSentBreak(ConnectionEvent ce) {
    Activator.log().debug(c_LogMarker, "connectionSentBreak");
  }//connectionSentBreak


  final class LogoutHandler implements Runnable {

    private String m_Debug;
    private String m_Reason;

    public LogoutHandler(String debug, String reason) {
      m_Debug = debug;
      m_Reason = reason;
    }//constructor

    public void run() {
      Activator.log().debug(m_Debug + "::" + m_Reason);
      try {
        forceTelnetClose();
      } catch (Throwable t) {
        t.printStackTrace(System.err);
      }

    }//run
  }//inner class LogoutHandler

}//class LoginShell
