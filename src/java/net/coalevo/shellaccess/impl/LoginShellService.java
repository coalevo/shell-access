/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.impl;

import net.wimpi.telnetd.shell.PoolingShellService;
import net.wimpi.telnetd.shell.Shell;
import net.wimpi.telnetd.shell.ShellService;
import org.apache.commons.pool.BasePoolableObjectFactory;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.osgi.framework.BundleContext;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * This class implements a {@link ShellService} for
 * the {@link LoginShell}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class LoginShellService
    implements PoolingShellService {

  private static final Marker c_LogMarker = MarkerFactory.getMarker(LoginShellService.class.getName());
  private final String m_Identifier;
  private GenericObjectPool.Config m_PoolConfig;
  private GenericObjectPool m_Shells;

  public LoginShellService() {
    //1. set id
    m_Identifier = LoginShell.class.getName();
    //2. prepare pool
    m_PoolConfig = new GenericObjectPool.Config();
    m_PoolConfig.minIdle = 3;
    m_PoolConfig.maxActive = 5;
    m_PoolConfig.maxWait = -1;
    m_PoolConfig.whenExhaustedAction = GenericObjectPool.WHEN_EXHAUSTED_GROW;
    m_Shells = new GenericObjectPool(new LoginShellFactory(), m_PoolConfig);
  }//consttuctor

  public Shell leaseShell() throws Exception {
    try {
      return (Shell) m_Shells.borrowObject();
    } catch (Exception ex) {
      Activator.log().error(c_LogMarker, "leaseShell()::Pool broken::", ex);
      return createShell();
    }
  }//leaseShell

  public void releaseShell(Shell shell) throws Exception {
    try {
      m_Shells.returnObject(shell);
    } catch (Exception ex) {
      Activator.log().error(c_LogMarker, "leaseShell()::Pool broken::", ex);
    }
  }//releaseShell

  public boolean activate(BundleContext bundleContext) {
    return true;
  }//activate

  public void deactivate() {
  }//deactivate

  public String getIdentifier() {
    return m_Identifier;
  }//getIdentifier

  public Shell createShell() {
    return new LoginShell();
  }//createShell

  class LoginShellFactory extends BasePoolableObjectFactory {

    public Object makeObject() throws Exception {
      return createShell();
    }//makeObject

    public void passivateObject(Object o) throws Exception {
      ((LoginShell) o).reset();
    }//returnObject

  }//class LoginShellFactory

}//class LoginShellService
