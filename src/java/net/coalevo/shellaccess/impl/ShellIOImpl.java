/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.impl;

import net.coalevo.shellaccess.model.ShellIO;
import net.wimpi.telnetd.io.BasicTerminalIO;
import net.wimpi.telnetd.io.TemplateAttributes;
import net.wimpi.telnetd.io.Templates;
import net.wimpi.telnetd.io.terminal.ColorHelper;
import net.wimpi.telnetd.io.toolkit.Pager;
import org.apache.commons.collections.buffer.UnboundedFifoBuffer;

import java.io.EOFException;
import java.io.IOException;
import java.util.Iterator;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Class encapsulating all output to the shell.
 *
 * @author Dieter Wimberger
 * @version @version@ (@date@)
 */
class ShellIOImpl implements ShellIO {

  private BasicTerminalIO m_TermIO;
  private Templates m_Templates;
  private Locale m_Locale;
  private int m_LangIdx;
  private boolean m_Bold;
  private UnboundedFifoBuffer m_EventBuffer;
  private AtomicBoolean m_AsyncEventIO = new AtomicBoolean(true);
  private String m_ProgressIndicator = ".";
  private ReentrantLock m_WriteLock;
  private boolean m_Autoflush = false;
  private Pager m_Pager;
  private String m_LastPrompt = "";

  /**
   * Constructs a new <tt>ShellIOImpl</tt> instance.
   *
   * @param tio a <tt>BasicTerminalIO</tt> instance.
   * @param l   the user locale.
   */
  public ShellIOImpl(BasicTerminalIO tio, Locale l) {
    //obtain templates service
    m_Templates = Activator.getTemplates();
    m_TermIO = tio;
    setLocale(l);
    m_EventBuffer = new UnboundedFifoBuffer();
    m_WriteLock = new ReentrantLock(true);
    updateProgressIndicator();
    m_Pager = new Pager(m_TermIO);
    m_Pager.setShowPosition(true);
    m_Pager.setDefaultFGColor(ColorHelper.GREEN);
    updatePager();
  }//ShellIOImpl

  public BasicTerminalIO getTerminalIO() {
    return m_TermIO;
  }//getTerminalIO

  /**
   * Returns the number of columns of the terminal.
   * <p/>
   * Convenience method.
   *
   * @return the number of columns as <tt>int</tt>.
   * @see net.wimpi.telnetd.io.BasicTerminalIO#getColumns()
   */
  public int getColumns() {
    return m_TermIO.getColumns();
  }//getWidth

  /**
   * Returns the number of rows of the terminal.
   * <p/>
   * Convenience method.
   *
   * @return the number of columns as <tt>int</tt>.
   * @see net.wimpi.telnetd.io.BasicTerminalIO#getRows()
   */
  public int getRows() {
    return m_TermIO.getRows();
  }//getHeight

  /**
   * Returns the <tt>java.util.Locale</tt> of
   * this <tt>ShellIOImpl</tt>.
   * <p/>
   *
   * @return the <tt>java.util.Locale</tt>.
   */
  public Locale getLocale() {
    return m_Locale;
  }//getLocale

  /**
   * Sets the <tt>java.util.Locale</tt> of
   * this <tt>ShellIOImpl</tt>
   *
   * @param l <tt>java.util.Locale</tt> instance.
   */
  public void setLocale(Locale l) {
    m_Locale = l;
    m_LangIdx = m_Templates.getIndexOfLanguage(l.getLanguage());
    if (m_LangIdx < 0) {
      m_LangIdx = 0;
    }
  }//setLocale

  public void setLanguageIndex(int idx) {
    m_LangIdx = idx;
    m_Locale = new Locale(m_Templates.getLanguageCode(idx));
  }//setLanguage

  /**
   * Returns the index of the actual language.
   *
   * @return a language index as <tt>int</tt>.
   */
  public int getLanguageIndex() {
    return m_LangIdx;
  }//getLanguageIndex

  /**
   * Sets the terminal type.
   * <p/>
   * If the given type is not supported, the default terminal
   * type will be set (as configured).
   * <p/>
   * <em>WARNING:</em><br/>
   * This should be used with care; a negotiated terminal
   * type might be much more useful and correct.
   *
   * @param type the type of terminal to be set.
   */
  public void setTerminalType(String type)
      throws IOException {
    //(wimpi) this is a bit of a hack because of the cast
    //I am not sure if the user should insist on something that
    //could not be negotiated? Anyway, if fails, will set default
    //terminal type as defined in telnetd properties, terminals section
    ((net.wimpi.telnetd.io.TerminalIO) m_TermIO).setTerminal(type);
  }//setTerminalType

  /**
   * Tests if I/O should be done with bold colors.
   *
   * @return true if with bold colors, false otherwise.
   */
  public boolean isBold() {
    return m_Bold;
  }//isBold

  /**
   * Sets the flag for bold color output.
   * <p/>
   *
   * @param bold true if bold color output, false otherwise.
   */
  public void setBold(boolean bold) {
    m_Bold = bold;
    m_TermIO.forceBold(bold);
  }//setBold

  /**
   * Sets the flag for asynchronous or synchronous event
   * output.
   * <p/>
   * If set, event outputs will be immediately written to
   * the terminal, or buffered otherwise.
   *
   * @param b true if asynchronous, false otherwise.
   */
  public synchronized void setAsyncEventIO(boolean b) {
    m_AsyncEventIO.set(b);
  }//setAsyncEventIO

  public boolean isAsyncEventIO() {
    return m_AsyncEventIO.get();
  }//isAsyncEventIO

  public void updateProgressIndicator() {
    m_ProgressIndicator = m_Templates.format(m_LangIdx, "progressindicator");
  }//updateProgressIndicator

  public boolean isAutoflushing() {
    return m_Autoflush;
  }//isAutoflush

  public void setAutoflushing(boolean b) {
    m_Autoflush = b;
  }//setAutoflushing

  //Real output methods

  protected void printAndFlush(String str) throws IOException {
    try {
      m_WriteLock.lockInterruptibly();
      m_TermIO.write(str);
      m_TermIO.flush();
    } catch (InterruptedException ex) {
      //ignore
    } finally {
      m_WriteLock.unlock();
    }
  }//printAndFlush

  public void print(String str) throws IOException {
    try {
      m_WriteLock.lockInterruptibly();
      m_TermIO.write(str);
      if (m_Autoflush) {
        m_TermIO.flush();
      }
    } catch (InterruptedException ex) {
      //ignore
    } finally {
      m_WriteLock.unlock();
    }
  }//print

  /**
   * Prints a newline.
   */
  public void println() throws IOException {
    try {
      m_WriteLock.lockInterruptibly();
      m_TermIO.write(BasicTerminalIO.CRLF);
      if (m_Autoflush) {
        m_TermIO.flush();
      }
    } catch (InterruptedException ex) {
      //ignore
    } finally {
      m_WriteLock.unlock();
    }
  }//println

  public void println(String str) throws IOException {
    try {
      m_WriteLock.lockInterruptibly();
      m_TermIO.write(str);
      m_TermIO.write(BasicTerminalIO.CRLF);
      if (m_Autoflush) {
        m_TermIO.flush();
      }
    } catch (InterruptedException ex) {
      //ignore
    } finally {
      m_WriteLock.unlock();
    }
  }//println

  public void page(String str) throws IOException {
    if (m_AsyncEventIO.get()) {
      try {
        m_WriteLock.lockInterruptibly();
        m_Pager.page(str);
      } catch (InterruptedException ex) {
        //ignore
      } finally {
        m_WriteLock.unlock();
      }
    } else {
      m_Pager.page(str);
    }
  }//page

  private void updatePager() {
    //set the prompt as given by the language index.
    //set the stop key as given by the language index.
    m_Pager.setStopKey(m_Templates.format(m_LangIdx, "pager_stopkey").charAt(0));
    m_Pager.setPrompt(m_Templates.format(m_LangIdx, "pager_prompt"));
  }//updatePager

  /**
   * Prints or queues the given content.
   * <p/>
   * The given content will be printed immediately if
   * the event I/O is set to be asynchronous and the output unlocked,
   * and queued in the event buffer otherwise.
   *
   * @param content the content.
   */
  public void printEvent(String content) throws IOException {
    if (m_AsyncEventIO.get() && !m_WriteLock.isLocked()) {
      m_TermIO.bell();
      //flush buffer
      flushEventBuffer();
      printAndFlush(content + m_LastPrompt);
    } else {
      synchronized (m_EventBuffer) {
        m_EventBuffer.add(content);
      }
    }
  }//printEvent

  public int getEventsInBufferCount() {
    return m_EventBuffer.size();
  }//getEventsInBufferCount

  public boolean hasEventsInBuffer() {
    return m_EventBuffer.size() > 0;
  }//hasEventsInBuffer

  /**
   * Flushes buffered event outputs.
   */
  public void flushEventBuffer() throws IOException {
    synchronized (m_EventBuffer) {
      if (m_EventBuffer.size() > 0) {
        m_TermIO.bell();
        for (Iterator iter = m_EventBuffer.iterator(); iter.hasNext();) {
          printAndFlush((String) iter.next());
          iter.remove();
        }
      }
    }
  }//flushEventBuffer


  public void flush() throws IOException {
    try {
      m_WriteLock.lockInterruptibly();
      m_TermIO.flush();
    } catch (InterruptedException ex) {
      //ignore
    } finally {
      m_WriteLock.unlock();
    }
  }//flush

  public void indicateProgress() throws IOException {
    try {
      m_WriteLock.lockInterruptibly();
      m_TermIO.write(m_ProgressIndicator);
      m_TermIO.flush();
    } catch (InterruptedException ex) {
      //ignore
    } finally {
      m_WriteLock.unlock();
    }
  }//indicateProgress

  public void scrollLines(int num) throws IOException {
    StringBuilder sbuf = new StringBuilder();
    for (int i = 0; i < num; i++) {
      sbuf.append(BasicTerminalIO.CRLF);
    }
    print(sbuf.toString());
  }//scrollLines

  /**
   * Blanks out everything on the terminal.
   *
   * @throws IOException
   */
  public void blankTerminal()
      throws IOException {
    try {
      m_WriteLock.lockInterruptibly();
      m_TermIO.eraseScreen();
      m_TermIO.homeCursor();
      m_TermIO.flush();
    } catch (InterruptedException ex) {
      //ignore
    } finally {
      m_WriteLock.unlock();
    }
  }//blankTerminal

  /**
   * Get a YES/NO decision, accepting only the respective
   * yes and no denoting characters as input.
   *
   * @return true if yes, false otherwise.
   */
  public boolean getDecision() throws IOException {
    do {
      char c = (char) read();
      if (c == m_Templates.format(m_LangIdx, "yes").charAt(0)) {
        return true;
      } else if (c == m_Templates.format(m_LangIdx, "no").charAt(0)) {
        return false;
      }
    } while (true);
  }//getDecision

  /**
   * Prompts for a key with the given prompt.
   *
   * @param promptKey the key of the prompt to be formatted.
   * @param attr      the {@link TemplateAttributes} for formatting.
   * @return the read character.
   * @throws IOException if an I/O error occurs.
   */
  public int prompt(String promptKey, TemplateAttributes attr) throws IOException {
    //print prompt
    if (attr == null) {
      m_LastPrompt = formatTemplate(promptKey);
    } else {
      m_LastPrompt = formatTemplate(promptKey, attr);
    }
    printAndFlush(m_LastPrompt);
    return read();
  }//prompt

  public int prompt(String promptKey, TemplateAttributes attr, boolean suppress) throws IOException {
    //print prompt
    if (!suppress) {
      if (attr == null) {
        m_LastPrompt = formatTemplate(promptKey);
      } else {
        m_LastPrompt = formatTemplate(promptKey, attr);
      }
      printAndFlush(m_LastPrompt);
    }
    return read();
  }//prompt

  /**
   * Print a formatted template.
   *
   * @param key a <tt>String</tt> representing the template's key.
   */
  public void printTemplate(String key) throws IOException {
    print(m_Templates.format(m_LangIdx, key));
  }//printTemplate(String)

  /**
   * Convenience method that will print a template with a given
   * attribute.
   *
   * @param key     the template key.
   * @param attrkey the attribute key.
   * @param attrval the attribute value.
   * @throws IOException if an I/O error occurs.
   */
  public void printTemplate(String key, String attrkey, String attrval)
      throws IOException {

    final TemplateAttributes attr = leaseTemplateAttributes();
    attr.add(attrkey, attrval);
    printTemplate(key, attr);
  }//printTemplate

  /**
   * Convenience method that will print a template with two given
   * attributes.
   *
   * @param key      the template key.
   * @param attrkey  the attribute key.
   * @param attrval  the attribute value.
   * @param attr2key the attribute key.
   * @param attr2val the attribute value.
   * @throws IOException if an I/O error occurs.
   */
  public void printTemplate(String key, String attrkey, String attrval, String attr2key, String attr2val)
      throws IOException {

    final TemplateAttributes attr = leaseTemplateAttributes();
    attr.add(attrkey, attrval);
    attr.add(attr2key, attr2val);
    printTemplate(key, attr);
  }//printTemplate

  /**
   * Print a formatted template with the given
   * attributes.
   *
   * @param key  <tt>String</tt> representing the template's key.
   * @param attr the attributes to fill the "holes" in the template.
   */
  public void printTemplate(String key, TemplateAttributes attr) throws IOException {
    print(m_Templates.format(m_LangIdx, key, attr));
  }//printTemplate

  /**
   * Print a formatted template.
   *
   * @param key a <tt>String</tt> representing the template's key.
   */
  public void pageTemplate(String key) throws IOException {
    page(m_Templates.format(m_LangIdx, key));
  }//printTemplate(String)

  /**
   * Print a formatted template with the given
   * attributes.
   *
   * @param key  <tt>String</tt> representing the template's key.
   * @param attr the attributes to fill the "holes" in the template.
   * @throws IOException if an I/O error occurs.
   */
  public void pageTemplate(String key, TemplateAttributes attr) throws IOException {
    page(m_Templates.format(m_LangIdx, key, attr));
  }//printTemplate

  /**
   * Print a formatted template with the given
   * attributes.
   *
   * @param key      <tt>String</tt> representing the template's key.
   * @param attr     the attributes to fill the "holes" in the template.
   * @param fixwidth the width of the template is preformatted and therefor fix.
   * @throws IOException if an I/O error occurs.
   */
  public void pageTemplate(String key, TemplateAttributes attr, boolean fixwidth) throws IOException {
    if (fixwidth) {
      m_Pager.setPreFormatted(true);
    }
    try {
      page(m_Templates.format(m_LangIdx, key, attr));
    } finally {
      m_Pager.setPreFormatted(false);
    }
  }//printTemplate

  /**
   * Convenience method that will format a message with
   * the given template key and attributes, in the actual
   * I/O language.
   *
   * @param key  <tt>String</tt> representing the template's key.
   * @param attr the attributes to fill the "holes" in the template.
   * @return the formatted String.
   */
  public String formatTemplate(String key, TemplateAttributes attr) throws IOException {
    return m_Templates.format(m_LangIdx, key, attr);
  }//formatTemplate

  /**
   * Convenience method that will format a message with
   * the given template key in the actual
   * I/O language.
   *
   * @param key <tt>String</tt> representing the template's key.
   * @return the formatted String.
   */
  public String formatTemplate(String key) throws IOException {
    return m_Templates.format(m_LangIdx, key, null);
  }//formatTemplate

  /**
   * Convenience method that will format a template with a given
   * attribute.
   *
   * @param key     the template key.
   * @param attrkey the attribute key.
   * @param attrval the attribute value.
   * @return the formatted String.
   * @throws IOException if an I/O error occurs.
   */
  public String formatTemplate(String key, String attrkey, String attrval)
      throws IOException {

    final TemplateAttributes attr = leaseTemplateAttributes();
    attr.add(attrkey, attrval);
    return formatTemplate(key, attr);
  }//formatTemplate

  /**
   * Convenience method that will format a template with a given
   * attribute.
   *
   * @param key      the template key.
   * @param attrkey  the attribute key.
   * @param attrval  the attribute value.
   * @param attr2key the attribute key.
   * @param attr2val the attribute value.
   * @return the formatted String.
   * @throws IOException if an I/O error occurs.
   */
  public String formatTemplate(String key, String attrkey, String attrval, String attr2key, String attr2val)
      throws IOException {

    final TemplateAttributes attr = leaseTemplateAttributes();
    attr.add(attrkey, attrval);
    attr.add(attr2key, attr2val);
    return formatTemplate(key, attr);
  }//formatTemplate

  /**
   * Convenience method that will format a given string with
   * a given style.
   *
   * @param key <tt>String</tt> representing the template's key.
   * @param str the <tt>String</tt> to be formatted with the style.
   * @return the formatted String.
   * @throws IOException
   */
  public String formatStyle(String key, String str) throws IOException {
    return m_Templates.formatStyle(str, key);
  }//formatStyle

  /**
   * Leases a {@link TemplateAttributes} instance.
   *
   * @return a leased {@link TemplateAttributes} instance.
   */
  public TemplateAttributes leaseTemplateAttributes() {
    return m_Templates.leaseAttributes();
  }//leaseTemplateAttributes

  /**
   * Returns all templates formatted for all available locales.
   *
   * @param key the key of the template.
   * @return a <tt>String[]</tt> with the formatted templates.
   */
  public String[] formatAllTemplates(String key) {
    return m_Templates.formatAll(key);
  }//getAllTemplates

  /**
   * Reads a character from the terminal I/O.
   *
   * @return the character read from stream.
   * @throws IOException if an I/O error occurs or end of stream is detected.
   */
  public int read() throws IOException {
    int ch = m_TermIO.read();
    if (ch == -1) {
      throw new EOFException();
    } else {
      return ch;
    }
  }//read

}//ShellIOImpl
