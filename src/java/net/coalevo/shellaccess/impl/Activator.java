/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.impl;

import net.coalevo.asciicaptcha.CaptchaGenerator;
import net.coalevo.foundation.model.Messages;
import net.coalevo.foundation.service.MessageResourceService;
import net.coalevo.foundation.util.BundleConfiguration;
import net.coalevo.foundation.util.metatype.MetaTypeDictionary;
import net.coalevo.logging.model.LogProxy;
import net.coalevo.shellaccess.cmd.ShellCommandProviderImpl;
import net.coalevo.shellaccess.cmd.admin.AdminCommandProvider;
import net.coalevo.shellaccess.cmd.casino.CasinoCommandProvider;
import net.coalevo.shellaccess.cmd.config.ConfigCommandProvider;
import net.coalevo.shellaccess.cmd.discussion.DiscussionCommandProvider;
import net.coalevo.shellaccess.cmd.messaging.MessagingCommandProvider;
import net.coalevo.shellaccess.cmd.postoffice.PostOfficeCommandProvider;
import net.coalevo.shellaccess.cmd.presence.PresenceCommandProvider;
import net.coalevo.shellaccess.cmd.proxyadmin.ProxyAdminCommandProvider;
import net.coalevo.shellaccess.cmd.scripting.ScriptingCommandProvider;
import net.coalevo.shellaccess.cmd.userdata.UserdataCommandProvider;
import net.coalevo.shellaccess.command.BeanShellCommandProvider;
import net.coalevo.shellaccess.command.CommandShellManager;
import net.coalevo.shellaccess.command.PythonShellCommandProvider;
import net.coalevo.shellaccess.model.Services;
import net.coalevo.shellaccess.model.ShellCommandProvider;
import net.coalevo.shellaccess.service.ShellAccessConfiguration;
import net.coalevo.shellaccess.service.ShellCommandProviderManager;
import net.wimpi.telnetd.io.Templates;
import net.wimpi.telnetd.shell.ShellService;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.*;
import java.net.URL;
import java.util.*;

/**
 * This class implements the <tt>BundleActivator</tt>.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class Activator
    implements BundleActivator {

  private static Marker c_LogMarker;
  private static LogProxy c_Log;
  private static BundleContext c_BundleContext;
  private static ServiceMediator c_Services;
  private static long c_StartTime;
  private static File c_ScriptStore;
  private static File c_ConfigStore;
  private static Messages c_BundleMessages;
  private static BundleConfiguration c_BundleConfiguration;
  private static Templates c_Templates;
  private static Properties c_DefaultPrefs;
  private static ShellAccessServiceImpl c_ShellAccessService;
  private static CaptchaGenerator c_CaptchaGenerator;
  private static File c_MessagingHistoryStore;

  private Thread m_StartThread;

  public void start(BundleContext bundleContext)
      throws Exception {

    c_BundleContext = bundleContext;
    if (m_StartThread != null && m_StartThread.isAlive()) {
      throw new Exception();
    }
    m_StartThread = new Thread(new Runnable() {

      public void run() {
        try {
          //1. Log
          c_LogMarker = MarkerFactory.getMarker(Activator.class.getName());
          c_Log = new LogProxy();
          c_Log.activate(c_BundleContext);

          //2. Services
          c_Services = new ServiceMediator();
          c_Services.activate(c_BundleContext);

          //3. Bundle Messages
          MessageResourceService mrs =
              c_Services.getMessageResourceService(ServiceMediator.WAIT_UNLIMITED);
          if (mrs == null) {
            log().error(c_LogMarker, "MessageResourceService unavailable.");
            return;
          }
          c_BundleMessages = mrs.getBundleMessages(c_BundleContext.getBundle());

          //4. Bundle Configuration
          c_BundleConfiguration = new BundleConfiguration(ShellAccessConfiguration.class.getName());
          c_BundleConfiguration.activate(c_BundleContext);
          c_Services.setConfigMediator(c_BundleConfiguration.getConfigurationMediator());

          if (c_BundleMessages == null) {
            log().error(c_LogMarker, "Bundle Message Resources could not be resolved.");
            return;
          }

          //5. Service
          c_ShellAccessService = new ShellAccessServiceImpl();
          c_ShellAccessService.activate(c_BundleContext);
          c_Services.setServiceAgentProxy(c_ShellAccessService.getServiceAgentProxy());

          //6. Configuration
          final File root = c_BundleContext.getDataFile("");
          c_ConfigStore = new File(root, "config_store");
          if (!c_ConfigStore.exists()) {
            try {
              installConfigStore();
            } catch (Exception ex) {
              log().error(c_LogMarker, c_BundleMessages.get("Activator.configstore.exception"), ex);
            }
          }
          log().info(c_LogMarker, c_BundleMessages.get("Activator.configstore.prepared"));

          c_DefaultPrefs = new Properties();
          c_DefaultPrefs.load(
              new FileInputStream(
                  new File(c_ConfigStore, "default-preferences.properties")
              )
          );

          //7. Templates Service
          try {
            c_Templates = prepareTemplates();
            log().info(c_LogMarker, "Have Templates " + c_Templates.toString());
          } catch (Exception ex) {
            log().error(c_LogMarker, c_BundleMessages.get("Activator.exception"), ex);
          }

          //8. Register command providers
          ShellCommandProviderManagerImpl mgr = new ShellCommandProviderManagerImpl();
          mgr.activate(c_BundleContext);
          c_BundleContext.registerService(
              ShellCommandProviderManager.class.getName(),
              mgr,
              null);
          c_Services.setShellCommandProviderManager(mgr);

          //standard shell commands
          c_BundleContext.registerService(
              ShellCommandProvider.class.getName(),
              new ShellCommandProviderImpl(),
              null);

          //admin shell commands
          c_BundleContext.registerService(
              ShellCommandProvider.class.getName(),
              new AdminCommandProvider(),
              null
          );

          //proxyadmin shell commands
          c_BundleContext.registerService(
              ShellCommandProvider.class.getName(),
              new ProxyAdminCommandProvider(),
              null
          );

          //scripting shell commands
          c_BundleContext.registerService(
              ShellCommandProvider.class.getName(),
              new ScriptingCommandProvider(),
              null
          );

          //presence command provider
          c_BundleContext.registerService(
              ShellCommandProvider.class.getName(),
              new PresenceCommandProvider(),
              null
          );

          //messaging command provider
          c_BundleContext.registerService(
              ShellCommandProvider.class.getName(),
              new MessagingCommandProvider(),
              null
          );

          //config command provider
          c_BundleContext.registerService(
              ShellCommandProvider.class.getName(),
              new ConfigCommandProvider(),
              null
          );

          //userdata command provider
          c_BundleContext.registerService(
              ShellCommandProvider.class.getName(),
              new UserdataCommandProvider(),
              null
          );

          //discussion command provider
          c_BundleContext.registerService(
              ShellCommandProvider.class.getName(),
              new DiscussionCommandProvider(),
              null
          );

          //postoffice command provider
          c_BundleContext.registerService(
              ShellCommandProvider.class.getName(),
              new PostOfficeCommandProvider(),
              null
          );

          //casino command provider
          c_BundleContext.registerService(
              ShellCommandProvider.class.getName(),
              new CasinoCommandProvider(),
              null
          );

          //Prepare the script command support
          c_ScriptStore = new File(root, "script_store");
          if (!c_ScriptStore.exists()) {
            try {
              installScriptStore();
            } catch (Exception ex) {
              log().error(c_LogMarker, c_BundleMessages.get("Activator.scriptstore.exception"), ex);
            }
          }
          log().info(c_LogMarker, c_BundleMessages.get("Activator.scriptstore.prepared"));

          // python provider
          PythonShellCommandProvider pr = new PythonShellCommandProvider("net.coalevo.shellaccess.cmd.py");
          pr.activate(c_BundleContext);
          c_BundleContext.registerService(
              ShellCommandProvider.class.getName(),
              pr,
              null
          );
          // beanshell provider
          BeanShellCommandProvider bpr = new BeanShellCommandProvider("net.coalevo.shellaccess.cmd.bsh");
          bpr.activate(c_BundleContext);
          c_BundleContext.registerService(
              ShellCommandProvider.class.getName(),
              bpr,
              null
          );

          //b) commandshell manager
          CommandShellManager csmgr = new CommandShellManager();
          csmgr.initialize();
          c_Services.setCommandShellManager(csmgr);

          //create and register the shell services
          c_BundleContext.registerService(
              ShellService.class.getName(), new LoginShellService(), null);
          c_BundleContext.registerService(
              ShellService.class.getName(), new NewUserShellService(), null);
          c_BundleContext.registerService(
              ShellService.class.getName(), new CommandShellService(), null);

          //Special shellaccess Session manager
          SessionManager smgr = new SessionManager();
          c_Services.setSessionManager(smgr);
          smgr.startAwayHandler();

          //hacky-hack ?! :)
          net.wimpi.telnetd.io.toolkit.Component.setAutoLocating(true);

          //Prepare Captcha Generator
          c_CaptchaGenerator = new CaptchaGenerator();

          //Prepare Messaging History Store
          c_MessagingHistoryStore = new File(c_BundleContext.getDataFile(""), "msghist_store");


          c_StartTime = System.currentTimeMillis();
          log().info(c_LogMarker, c_BundleMessages.get("Activator.started"));
        } catch (Exception ex) {
          ex.printStackTrace(System.err);
          log().error(c_LogMarker, "start()", ex);
        }
      }//run
    }//Runnable
    );//thread
    m_StartThread.setContextClassLoader(bundleContext.getBundle().getClass().getClassLoader());
    m_StartThread.start();
  }//start

  public void stop(BundleContext bundleContext) throws Exception {

    //wait start
    if (m_StartThread != null && m_StartThread.isAlive()) {
      m_StartThread.join();
      m_StartThread = null;
    }

    SessionManager smgr = c_Services.getSessionManager();
    if (smgr != null) {
      smgr.stopAwayHandler();
    }
    if (c_ShellAccessService != null) {
      c_ShellAccessService.deactivate();
      c_ShellAccessService = null;
    }
    if (c_Services != null) {
      c_Services.deactivate();
      c_Services = null;
    }
    if (c_Log != null) {
      c_Log.deactivate();
      c_Log = null;
    }
    c_ScriptStore = null;
    c_ConfigStore = null;
    c_Templates = null;
    c_BundleConfiguration = null;
    c_BundleMessages = null;
    c_LogMarker = null;
    c_BundleContext = null;
  }//stop

  public static File getMessageHistoryDir(String indexname) {
    return new File(c_MessagingHistoryStore, indexname);
  }//getMessageHistoryDir

  private void installConfigStore()
      throws IOException {
    //1. make the directory
    c_ConfigStore.mkdir();
    //2. copy over template
    Enumeration e = c_BundleContext.getBundle().findEntries("net/coalevo/shellaccess/conf", "*", false);
    while (e != null && e.hasMoreElements()) {
      URL url = (URL) e.nextElement();
      log().debug(url.toString());
      //log().debug(url.getFile());
      String file = url.getFile();
      int idx = file.lastIndexOf("/");
      if (idx > 0) {
        file = file.substring(idx + 1, file.length());
      }
      Reader r = new InputStreamReader(url.openStream());

      FileWriter w = new FileWriter(new File(c_ConfigStore, file));

      char[] buf = new char[1024 * 2];  //2k buffer
      boolean done = false;
      do {
        int i = r.read(buf, 0, buf.length);
        if (i < 0) {
          done = true;
        } else {
          w.write(buf, 0, i);
        }
      } while (!done);
      r.close();
      w.flush();
      w.close();
    }
    log().info(c_LogMarker, c_BundleMessages.get("Activator.configstore.installed"));

  }//installConfigStore

  private void installScriptStore()
      throws IOException {
    //1. make the directory
    c_ScriptStore.mkdir();
    File pyd = new File(c_ScriptStore, "net.coalevo.shellaccess.cmd.py");
    if (!pyd.exists()) {
      pyd.mkdir();
    }
    File bshd = new File(c_ScriptStore, "net.coalevo.shellaccess.cmd.bsh");
    if (!bshd.exists()) {
      bshd.mkdir();
    }
    //2. copy over python
    Enumeration e = c_BundleContext.getBundle().findEntries("net/coalevo/shellaccess/script", "*.py", false);
    while (e != null && e.hasMoreElements()) {
      URL url = (URL) e.nextElement();
      log().debug(url.toString());
      //log().debug(url.getFile());
      String file = url.getFile();
      int idx = file.lastIndexOf("/");
      if (idx > 0) {
        file = file.substring(idx + 1, file.length());
      }
      Reader r = new InputStreamReader(url.openStream());

      FileWriter w = new FileWriter(new File(pyd, file));

      char[] buf = new char[1024 * 2];  //2k buffer
      boolean done = false;
      do {
        int i = r.read(buf, 0, buf.length);
        if (i < 0) {
          done = true;
        } else {
          w.write(buf, 0, i);
        }
      } while (!done);
      r.close();
      w.flush();
      w.close();
    }
    log().info(c_LogMarker, c_BundleMessages.get("Activator.scriptstore.python"));

    //2. copy over beanshell
    e = c_BundleContext.getBundle().findEntries("net/coalevo/shellaccess/script", "*.bsh", false);
    while (e != null && e.hasMoreElements()) {
      URL url = (URL) e.nextElement();
      log().debug(url.toString());
      //log().debug(url.getFile());
      String file = url.getFile();
      int idx = file.lastIndexOf("/");
      if (idx > 0) {
        file = file.substring(idx + 1, file.length());
      }
      Reader r = new InputStreamReader(url.openStream());

      FileWriter w = new FileWriter(new File(bshd, file));

      char[] buf = new char[1024 * 2];  //2k buffer
      boolean done = false;
      do {
        int i = r.read(buf, 0, buf.length);
        if (i < 0) {
          done = true;
        } else {
          w.write(buf, 0, i);
        }
      } while (!done);
      r.close();
      w.flush();
      w.close();
    }
    log().info(c_LogMarker, c_BundleMessages.get("Activator.scriptstore.beanshell"));


    log().info(c_LogMarker, c_BundleMessages.get("Activator.scriptstore.installed"));

  }//installConfigStore

  private Templates prepareTemplates() throws Exception {
    //System.err.println("prepareTemplates()");
    MetaTypeDictionary config = c_BundleConfiguration.getConfigurationMediator().getConfiguration();
    Dictionary d = config.getDictionary();

    //replace defaults with the actual values
    String str = config.getString("templates.path");
    if (str == null || str.equals("")) {
      config.setString("templates.path", c_ConfigStore.getAbsolutePath());
    }
    str = config.getString("styles.file");
    config.setString("styles.file",
        new File(c_ConfigStore, str).getAbsolutePath());
    //System.err.println("prepareTemplates()::Have Dictionary " + d.toString());

    return c_Services.getTemplatesService(Services.WAIT_UNLIMITED).createTemplates(d);
  }//prepareTemplates


  public static ServiceMediator getServices() {
    return c_Services;
  }//getServices

  public static long getStartTime() {
    return c_StartTime;
  }//getStartTime

  public static File getConfigStore() {
    return c_ConfigStore;
  }//getConfigStore

  public static File getScriptStore() {
    return c_ScriptStore;
  }//getScriptStore

  public static Templates getTemplates() {
    return c_Templates;
  }//getTemplates

  public static Iterator getBundleImports() {
    return Arrays.asList(
        ((String) c_BundleContext.getBundle().getHeaders().get(Constants.IMPORT_PACKAGE))
            .split(",")).listIterator();
  }//getBundleImports

  public static Iterator getBundleExports() {
    return Arrays.asList(
        ((String) c_BundleContext.getBundle().getHeaders().get(Constants.EXPORT_PACKAGE))
            .split(",")).listIterator();
  }//getBundleExports

  public static CaptchaGenerator getCaptchaGenerator() {
    if (c_CaptchaGenerator == null) {
      c_CaptchaGenerator = new CaptchaGenerator();
    }
    return c_CaptchaGenerator;
  }//getCaptchaGenerator

  public static Properties getDefaultPreferences() {
    return c_DefaultPrefs;
  }//getDefaultPreferences

  /**
   * Return the bundles logger.
   *
   * @return the <tt>Logger</tt>.
   */
  public static Logger log() {
    return c_Log;
  }//log

  public static Messages getBundleMessages() {
    return c_BundleMessages;
  }//getBundleMessages

  public static Bundle getBundle() {
    return c_BundleContext.getBundle();
  }//getBundle

}//class Activator

