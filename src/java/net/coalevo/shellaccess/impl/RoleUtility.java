/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.impl;

import net.coalevo.security.model.Role;
import net.wimpi.telnetd.io.Templates;
import net.wimpi.telnetd.io.TemplatesUpdateListener;

import java.util.Iterator;
import java.util.Set;

/**
 * Provides utility methods for dealing with roles.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class RoleUtility {

  private static String[] c_Roles = new String[0];

  static {
    Templates ts = Activator.getTemplates();

    ts.registerUpdateListener(new TemplatesUpdateListener() {
      public void updatedTemplates() {
        templatesUpdate();
      }//updated
    });
    templatesUpdate();
  }//static initializer

  private RoleUtility() {
  }

  ;

  /**
   * Selects the priority role from the given set of role names.
   *
   * @param s the set of role names.
   * @return the name of the role with highest priority.
   */
  public static String getPriorityRole(Set s) {
    for (int i = 0; i < c_Roles.length; i++) {
      if (s.contains(c_Roles[i])) {
        return c_Roles[i];
      }
    }
    return "ERROR"; //shouldnt happen
  }//getPriorityRole

  /**
   * Formats the user with the role style of the given role.
   *
   * @param name the user name.
   * @param role the role name.
   * @return the formatted user name.
   */
  public static String getUserWithRoleStyle(String name, String role) {
    Templates ts = Activator.getTemplates();
    synchronized (ts) {
      return ts.formatStyle(name, role); //key seems to be second!
    }
  }//getUserWithRoleStyle

  /**
   * Formats the user with the role style of the role with highest priority.
   * Note that this is a convenience method.
   *
   * @param name the user name.
   * @param s    the set of role names.
   * @return the formatted user name.
   */
  public static String getUserWithPriorityRoleStyle(String name, Set s) {
    return getUserWithRoleStyle(name, getPriorityRole(s));
  }//getUserWithPriorityRoleStyle

  /**
   * Returns the priority role name formatted with its
   * corresponding style.
   *
   * @param s the set of role names.
   * @return the priority role name formatted with the corresponding style.
   */
  public static String getPriorityRoleWithStyle(Set s) {
    Templates ts = Activator.getTemplates();
    String role = getPriorityRole(s);
    synchronized (ts) {
      return ts.formatStyle(role, role); //key is second.
    }
  }//getPriorityRoleWithStyle

  /**
   * Returns the roles with style applied in a handy string.
   *
   * @param s the set of role names.
   * @return the string that lists the roles with their style.
   */
  public static String getRolesWithStyle(Set s) {
    Templates ts = Activator.getTemplates();
    StringBuilder sb = new StringBuilder();
    for (Iterator iterator = s.iterator(); iterator.hasNext();) {
      String role = (String) iterator.next();
      sb.append(ts.formatStyle(String.format("[%s]", role), role)); //key is second.
      if (iterator.hasNext()) {
        sb.append(" ");
      }
    }
    return sb.toString();
  }//getRolesWithStyle

  /**
   * Returns the role with style applied in a handy string.
   *
   * @param r the role.
   * @return the string representing the role with its style.
   */
  public static String getRoleWithStyle(Role r) {
    Templates ts = Activator.getTemplates();
    String role = r.getIdentifier();
    return ts.formatStyle(String.format("%s", role), role); //key is second.
  }//getRoleWithStyle

  /**
   * Updates the role priority list when templates are reloaded.
   */
  private static void templatesUpdate() {
    Templates ts = Activator.getTemplates();
    if (ts == null) {
      return;
    } else {
      String list = ts.format(0, "RolePriorityList");
      if (list != null && list.length() > 0) {
        c_Roles = list.split(",");
      }
    }
  }//updateTemplatesUpdate

}//RoleUtility