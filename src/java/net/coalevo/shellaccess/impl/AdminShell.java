/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.impl;

import bsh.NameSpace;
import net.wimpi.telnetd.shell.BeanScriptShell;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * This class implements an admin shell with Java access to most of
 * the system.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class AdminShell
    extends BeanScriptShell {

  private static final Marker c_LogMarker = MarkerFactory.getMarker(AdminShell.class.getName());

  /**
   * Method that can be overridden to initialize the
   * Interpreter or it's namespace with package and
   * class imports, as well as variables required.
   */
  protected void initializeInterpreter() {
    //pre-imports
    NameSpace ns = m_Interpreter.getNameSpace();
    //telnetd
    ns.importPackage("net.wimpi.telnetd.io.terminal");
    ns.importPackage("net.wimpi.telnetd.io.util");
    //shellaccess
    ns.importPackage("net.coalevo.shellaccess.command");
    ns.importPackage("net.coalevo.shellaccess.io");
    ns.importPackage("net.coalevo.shellaccess.util");
    ns.importPackage("net.coalevo.shellaccess.model");
    ns.importPackage("net.coalevo.shellaccess.impl");

    //variables
    try {
      m_Interpreter.set("termio", m_TermIO);
      m_Interpreter.set("cdata", m_ConData);
      m_Interpreter.set("env", m_Environment);
      m_Interpreter.set("services", Activator.getServices());

    } catch (Exception ex) {
      Activator.log().error(c_LogMarker, "initializeInterpreter():", ex);
    }
  }//initializeInterpreter

}//class AdminShell
