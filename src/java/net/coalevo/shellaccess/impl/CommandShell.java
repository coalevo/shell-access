/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.impl;

import net.coalevo.asciicaptcha.Captcha;
import net.coalevo.asciicaptcha.CaptchaGenerator;
import net.coalevo.foundation.model.Session;
import net.coalevo.foundation.model.SessionListener;
import net.coalevo.foundation.model.UserAgent;
import net.coalevo.security.service.SecurityService;
import net.coalevo.shellaccess.command.*;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.Command;
import net.coalevo.shellaccess.model.CommandException;
import net.wimpi.telnetd.io.TemplateAttributes;
import net.wimpi.telnetd.io.toolkit.Component;
import net.wimpi.telnetd.io.toolkit.Editfield;
import net.wimpi.telnetd.net.Connection;
import net.wimpi.telnetd.net.ConnectionEvent;
import org.slf4j.Logger;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Class implementing a generic key command
 * shell.
 * <p/>
 *
 * @author Dieter Wimberger
 * @version @version@ (@date@)
 */
public class CommandShell
    extends BaseShell implements ConfigurationListener {

  private static Logger log = Activator.log();
  private static final Marker c_LogMarker = MarkerFactory.getMarker(CommandShell.class.getName());
  protected KeySchema m_Schema;
  protected MenuKeyEntry m_DefaultMenu;
  protected Stack<MenuKeyEntry> m_MenuStack;
  private boolean m_ConfigurationUpdate = true;
  private CommandShellManager m_CommandShellManager;
  private int m_IdleWarnings = 0;
  private int m_CommandKey = -1;
  private long m_LastCaptcha = -1;
  private boolean m_IsNonHuman = false;

  static {
    Component.setAutoLocating(true);
  }

  public CommandShell() {
    m_MenuStack = new Stack<MenuKeyEntry>();
    m_CommandShellManager = Activator.getServices().getCommandShellManager();
  }//constructor


  public void run(Connection con) {
    //prepare shell
    prepare(con, this);
    m_CommandShellManager.registerConfigurationListener(this);
    //add session listener
    Session cs = m_Session.getCoalevoSession();
    if (cs != null) {
      cs.addSessionListener(new SessionListener() {
        public void invalidated(Session s) {
          try {
            CommandShell.this.m_Session.invalidate();
            CommandShell.this.forceTelnetClose();
          } catch (Exception ex) {
            log.error(c_LogMarker, "SessionListener::invalidated", ex);
          }
        }
      });
    }

    MenuKeyEntry mke = null;
    CommandKeyEntry cke = null;
    KeyEntry ke = null;
    m_ShellIO.setAutoflushing(true);

    try {
      //run start commands, which might init stuff or do things or whatever :)
      setEnvironmentVariable("CommandShell.start", "true");
      for (Iterator iter = m_CommandShellManager.listStartCommands(); iter.hasNext();) {
        try {
          runCommand((String) iter.next(), true);
        } catch (Exception cmex) {
          log.error(c_LogMarker, "logout()", cmex);
        }
      }
      unsetEnvironmentVariable("CommandShell.start");

      //Captcha for NonHuman
      SecurityService ssrv = CommandShell.this.getServices().getSecurityService(0);
      if (ssrv.getAuthorizations(this.getSession().getUserAgent()).hasRole("ScriptKiddie")) {
        m_IsNonHuman = true;
        if (checkScripting()) return;
      }

      do {
        //configuration update
        if (m_ConfigurationUpdate) {
          m_Schema = m_CommandShellManager.getKeySchema("standard");
          m_DefaultMenu = m_CommandShellManager.getDefaultMenu();
          if (m_MenuStack.isEmpty()) {
            pushMenu(m_DefaultMenu);
          } else if (!m_CommandShellManager.existsMenu(getActualMenu().getIdentifier())) {
            m_MenuStack.clear();
            pushMenu(m_DefaultMenu);
          }
          m_ConfigurationUpdate = false;
        }

        //flush event buffer
        m_ShellIO.flushEventBuffer();

        //scripting captcha
        if (checkScripting()) return;

        if (!inMainMenu()) {
          offerActualMenu(m_Session.isExpert());
        }
        mke = getActualMenu();
        //prompt (this will block the loop
        Object o = getEnvironmentVariable(BasicShell.PROMPT_ATTRIBUTES_KEY);
        TemplateAttributes attr = m_ShellIO.leaseTemplateAttributes();
        if (o != null) {
          Map<String, String> map = (Map<String, String>) o;
          for (Iterator<Map.Entry<String, String>> iterator =
              map.entrySet().iterator(); iterator.hasNext();) {
            Map.Entry<String, String> entry = iterator.next();
            attr.add(entry.getKey(), entry.getValue());
          }
        }
        m_CommandKey = m_ShellIO.prompt(mke.getPromptKey(), attr);
        if (m_CommandKey == -1 || !cs.isValid() || !m_Session.isLoggedIn()) {
          return;
        }
        //Activity
        m_Session.activity();
        m_IdleWarnings = 0;
        //m_ShellIO.printTemplate(mke.getPromptKey());
        try {
          m_ShellIO.setAsyncEventIO(false);
          try {
            ke = m_Schema.entryForKey(mke.getIdentifier(), m_CommandKey, m_Session.getUserAgent());
          } catch (NoSuchMenuException nsmex) {
            //log.debug("run(Connection)", nsmex);
            continue;
          } catch (NoSuchEntryException nseex) {
            //log.debug("run(Connection)", nseex);
            m_ShellIO.printTemplate("commands_nosuch");
            m_ShellIO.println();
            continue;
          } catch (SecurityException ex) {
            m_ShellIO.printTemplate("commands_nosuch");
            m_ShellIO.println();
            continue;
          }
          if (ke.isMenuEntry()) {
            //log.debug("run()::called menu.");
            //put on the stack
            pushMenu((MenuKeyEntry) ke);
            m_ShellIO.printTemplate(ke.getConfirmationKey());
            m_ShellIO.println();
          } else {
            cke = (CommandKeyEntry) ke;
            m_ShellIO.printTemplate(ke.getConfirmationKey());
            m_ShellIO.println();
            Command cmd = cke.leaseCommand();
            try {
              cmd.run(this);
            } catch (CommandException cmdex) {
              log.error(c_LogMarker, "run(Connection)", cmdex);
            } finally {
              cke.releaseCommand(cmd);
            }
          }
        } finally {
          m_ShellIO.setAsyncEventIO(true);
        }
      } while (cs.isValid() && m_Session.isLoggedIn());

    } catch (Exception ex) {
      log.error(c_LogMarker, "run()::" + con.toString(), ex);
    } finally {
      log.debug(c_LogMarker, "run()::finally::Removing event listener and session registrations.");
      m_CommandShellManager.removeConfigurationListener(this);
      con.removeConnectionListener(this);
      m_Session.invalidate();
    }

  }//run

  public int getCommandKey() {
    return m_CommandKey;
  }//getCommandKey

  public boolean runLongPromptCommand(int key) throws CommandException, IOException {
    m_CommandKey = key;
    if (checkScripting()) return false;
    //Activity
    m_Session.activity();
    m_IdleWarnings = 0;
    KeyEntry ke = null;
    try {
      ke = m_Schema.entryForKey(m_DefaultMenu.getIdentifier(), m_CommandKey, m_Session.getUserAgent());
    } catch (Exception ex) {
      return false;
    }
    if (ke.isMenuEntry() || !ke.isAvailableOnLongPrompt()) {
      return false;
    } else {
      CommandKeyEntry cke = (CommandKeyEntry) ke;
      m_ShellIO.printTemplate(ke.getConfirmationKey());
      m_ShellIO.println();
      Command cmd = cke.leaseCommand();
      try {
        m_ShellIO.setAsyncEventIO(false);
        cmd.run(this);
      } catch (CommandException cmdex) {
        log.error(c_LogMarker, "runLongPromptCommand(Connection)", cmdex);
      } finally {
        cke.releaseCommand(cmd);
        m_ShellIO.setAsyncEventIO(true);
      }
    }
    return true;
  }//runLongPromptCommand

  public void runCommand(String identifier) throws CommandException {
    runCommand(identifier, true);
  }//runCommand

  private void runCommand(String identifier, boolean checks)
      throws CommandException {
    //log.debug(c_LogMarker, "runCommand()::" + identifier);
    if (checks) {
      //make sure the command can be executed or return silently
      try {
        if (!m_CommandShellManager.getSecurityManager()
            .canExecuteCommand(identifier, m_Session.getUserAgent())) {
          return;
        }
      } catch (SecurityException ex) {
        log.error(c_LogMarker, "User not authorized to run command.");
        return;
      }
    }
    Command c = null;
    boolean async = m_ShellIO.isAsyncEventIO();
    try {
      c = m_CommandShellManager.leaseCommand(identifier);
      if (c != null) {
        m_ShellIO.setAsyncEventIO(false);
        c.run(this);
      }
    } finally {
      if (c != null) {
        m_CommandShellManager.releaseCommand(c);
        m_ShellIO.setAsyncEventIO(async);  //restore to previous setting
      }
    }
  }//runCommand

  public void updatedConfiguration() {
    m_ConfigurationUpdate = true;
  }//updateConfiguration

  public void pushMenu(MenuKeyEntry mke) {
    m_MenuStack.push(mke);
  }//pushMenu

  public void popMenu() {
    if (!inMainMenu()) {
      m_MenuStack.pop();
    }
  }//popMenu

  public MenuKeyEntry getActualMenu() {
    return (MenuKeyEntry) m_MenuStack.peek();
  }//getActualMenu

  public void offerActualMenu(boolean expert)
      throws Exception {
    if (expert) {
      return;
    }
    //output of menu
    MenuKeyEntry am = getActualMenu();

    StringBuilder sbuf = new StringBuilder();

    TemplateAttributes attr = m_ShellIO.leaseTemplateAttributes();
    attr.add("menu", m_ShellIO.formatTemplate(am.getNameKey()));
    sbuf.append(m_ShellIO.formatTemplate("menus_menuheader", attr));

    MenuEntries entries = m_Schema.menuEntries(am.getIdentifier(), m_Session.getUserAgent());
    //1. submenus
    List mke = entries.getMenuKeyEntries();
    if (!mke.isEmpty()) {
      attr = m_ShellIO.leaseTemplateAttributes();
      attr.add("blockname", m_ShellIO.formatTemplate("menus_submenusblockname"));

      for (Iterator iter = mke.iterator(); iter.hasNext();) {
        Map.Entry me = (Map.Entry) iter.next();
        KeyEntry ke = (KeyEntry) me.getValue();
        Character c = (Character) me.getKey();
        String key = "";
        if (m_Schema.isSpecialKey(c)) {
          key = m_ShellIO.formatTemplate(m_Schema.translateSpecialKey(c));
        } else {
          key = c.toString();
        }
        String[] items = new String[]{
            key,
            m_ShellIO.formatTemplate(ke.getDescriptionKey())
        };
        attr.add("items.{key,description}", items);
      }
      sbuf.append(m_ShellIO.formatTemplate("menus_entryblock", attr));
    }

    //2. Namespace organized entries.
    Set namespaces = entries.listNamespaces();
    for (Iterator iter = namespaces.iterator(); iter.hasNext();) {
      attr = m_ShellIO.leaseTemplateAttributes();
      String nsk = (String) iter.next();
      attr.add("blockname", m_ShellIO.formatTemplate(nsk));
      mke = entries.getCommandKeyEntries(nsk);
      for (Iterator iterator = mke.iterator(); iterator.hasNext();) {
        Map.Entry me = (Map.Entry) iterator.next();
        KeyEntry ke = (KeyEntry) me.getValue();
        Character c = (Character) me.getKey();
        String key = "";
        if (m_Schema.isSpecialKey(c)) {
          key = m_ShellIO.formatTemplate(m_Schema.translateSpecialKey(c));
        } else {
          key = c.toString();
        }
        String[] items = new String[]{
            key,
            m_ShellIO.formatTemplate(ke.getDescriptionKey())
        };
        attr.add("items.{key,description}", items);
      }
      sbuf.append(m_ShellIO.formatTemplate("menus_entryblock", attr));
    }
    m_ShellIO.page(sbuf.toString());
    //m_ShellIO.println();
  }//offerActualMenu

  public void printCommandHelp(int key, String prefix) throws IOException {
    //output of menu
    MenuKeyEntry mke = getActualMenu();
    try {
      KeyEntry ke = m_Schema.entryForKey(mke.getIdentifier(), key, m_Session.getUserAgent());
      TemplateAttributes attr = m_ShellIO.leaseTemplateAttributes();
      Character c = new Character((char) key);
      String thekey = "";
      if (m_Schema.isSpecialKey(c)) {
        thekey = m_ShellIO.formatTemplate(m_Schema.translateSpecialKey(c));
      } else {
        thekey = c.toString();
      }
      attr.add((ke.isMenuEntry()) ? "menu" : "cmd", m_ShellIO.formatStyle("cmdkey", thekey));
      attr.add("desc", m_ShellIO.formatTemplate(ke.getDescriptionKey()));
      attr.add("help", m_ShellIO.formatTemplate(ke.getHelpKey()));
      m_ShellIO.pageTemplate(prefix + "helptext", attr);

    } catch (Exception ex) {
      m_ShellIO.printTemplate(prefix + "nohelp");
    }
  }//printCommandHelp

  public boolean inMainMenu() {
    return m_MenuStack.size() == 1;
  }//isMainMenu

  public void logout
      (boolean ask) throws IOException {
    try {
      m_ShellIO.flush();
    } catch (Exception ex) {
      //this means likely that the io is down
      ask = false;
    }
    if (ask) {
      m_ShellIO.printTemplate("logout_prompt");
      if (!m_ShellIO.getDecision()) {
        m_ShellIO.printTemplate("logout_stay");
        return;
      } else {
        TemplateAttributes attr = m_ShellIO.leaseTemplateAttributes();
        attr.add("username", m_Session.getUsername());
        m_ShellIO.printTemplate("logout_goodbye", attr);
      }
    }
    if (m_Session.isLoggedIn()) {
      setEnvironmentVariable("CommandShell.end", "true");
      for (Iterator iter = m_CommandShellManager.listEndCommands(); iter.hasNext();) {
        try {
          runCommand((String) iter.next(), false);
        } catch (Exception cmex) {
          log.error(c_LogMarker, "logout()", cmex);
        }
      }
      unsetEnvironmentVariable("CommandShell.end");
      try {
        m_Session.invalidate();
      } catch (Exception ex) {
        log.error(c_LogMarker, "logout()", ex);
      }

    }
    forceTelnetClose();
  }//logout

  private boolean checkScripting() throws IOException {
    if (m_IsNonHuman) {
      long lastcheck = System.currentTimeMillis() - m_LastCaptcha;
      if (lastcheck > c_IdleCaptcha) {
        if (!passesCaptcha()) {
          m_ShellIO.printTemplate("commands_captchafailed");
          logout(false);
          return true;
        } else {
          m_ShellIO.println();
        }
      }
    }
    return false;
  }//checkScripting

  private boolean passesCaptcha() throws IOException {
    final CountDownLatch latch = new CountDownLatch(1);
    final AtomicBoolean passedCaptcha = new AtomicBoolean(false);
    final CaptchaGenerator cg = Activator.getCaptchaGenerator();

    getServices().getExecutionService(0).execute(
        this.getSession().getUserAgent(),
        new Runnable() {
          public void run() {
            try {
              //1. Display Captcha
              Captcha c = cg.generate();
              m_ShellIO.println(c.getDisplay());
              m_ShellIO.printTemplate("commands_captchaprompt");
              m_ShellIO.flush();
              Editfield eField = new Editfield(m_TermIO, "captcha", 4);
              eField.run();
              if (!c.equals(eField.getValue())) {
                log.error(
                    Activator.getBundleMessages().get("CommandShell.captchafailed")
                );
              } else {
                passedCaptcha.set(true);
                log.info(
                    Activator.getBundleMessages().get("CommandShell.captchapassed")
                );
              }
            } catch (Throwable t) {
              log.error("passesCaptcha()", t);
            } finally {
              latch.countDown();
            }
          }
        }
    );

    try {
      latch.await(60 * 2, TimeUnit.SECONDS);
    } catch (Exception ex) {
      log.error("passesCaptcha()", ex);
      return false;
    }
    return passedCaptcha.get();
  }//passesCaptcha

  //ConnectionListener Implementation
  public void connectionIdle(ConnectionEvent ce) {
    getServices().getExecutionService(0).execute(
        getSession().getUserAgent(),
        new Runnable() {
          public void run() {
            log.debug(c_LogMarker, "connectionIdle()");
            UserAgent ua = CommandShell.this.m_Session.getUserAgent();
            SecurityService ssrv = CommandShell.this.getServices().getSecurityService(0);
            if (ssrv.getAuthorizations(ua).hasPermission("longIdle")) {
              //check if was idled already
              if (m_IdleWarnings++ < 3) {
                CommandShell.this.getConnectionData().activity();
                return;
              }
            }
            try {
              m_ShellIO.printTemplate("connection_event_idle_warning");
              m_ShellIO.flush();
            } catch (Exception ex) {
              //ignore
            }
          }
        });
  }//connectionIdle

  public void connectionTimedOut(ConnectionEvent ce) {
    getServices().getExecutionService(0).execute(
        getSession().getUserAgent(), new LogoutHandler(
            "connectionTimedOut()",
            "connection_event_idle_loggedout",
            false));
  }//connectionTimedOut

  public void connectionLogoutRequest(ConnectionEvent ce) {

    //execute(new LogoutHandler("connectionLogoutRequest()", "connection_event_logoutrequest", true));
    //ignore. 
  }//connectionLogoutRequest

  public void connectionSentBreak(ConnectionEvent ce) {
    getServices().getExecutionService(0).execute(
        getSession().getUserAgent(), new Runnable() {
          public void run() {
            try {
              log.debug(c_LogMarker, "connectionSentBreak()");
            } catch (Throwable t) {
              t.printStackTrace(System.err);              
            }
          }
        });
  }//connectionSentBreak

  final class LogoutHandler implements Runnable {

    private String m_Debug;
    private String m_Reason;
    private boolean m_Ask;

    public LogoutHandler(String debug, String reason, boolean b) {
      m_Debug = debug;
      m_Reason = reason;
      m_Ask = b;
    }//constructor

    public void run() {
      try {
        log.debug(c_LogMarker, m_Debug);

        m_ShellIO.pageTemplate(m_Reason);
        CommandShell.this.logout(m_Ask);
      } catch (Throwable t) {
        t.printStackTrace(System.err);
      }
      try {
        forceTelnetClose();
      } catch (Throwable t) {
        t.printStackTrace(System.err);
      }
    }//run
  }//inner class LogoutHandler

  private static final long c_IdleCaptcha = 1000 * 60 * 60; // 1h

}//class CommandShell
