/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.impl;


import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.Services;
import net.coalevo.shellaccess.model.ShellAccessSession;
import net.coalevo.shellaccess.model.ShellIO;
import net.coalevo.system.service.ExecutionService;
import net.wimpi.telnetd.net.Connection;
import net.wimpi.telnetd.shell.AbstractBaseShell;

import java.util.Iterator;
import java.util.Locale;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Superclass for <tt>BaseShell</tt> implementations.
 *
 * @author Dieter Wimberger
 * @see net.wimpi.telnetd.shell.AbstractBaseShell
 */
abstract class BaseShell
    extends AbstractBaseShell
    implements BasicShell {

  protected ShellAccessSession m_Session = null;
  protected ShellIO m_ShellIO = null;
  protected AtomicBoolean m_InProgress;
  protected CountDownLatch m_StoppedProgress;
  private ServiceMediator m_Services;


  public BaseShell() {
    m_InProgress = new AtomicBoolean(false);
    m_StoppedProgress = new CountDownLatch(0);
    m_Services = Activator.getServices();
  }//constructor

  public void prepare(Connection con, BaseShell shell) {
    prepare(con);
    con.addConnectionListener(shell);
    if (m_Session == null && m_Environment.containsKey(SESSION_KEY)) {
      m_Session = (ShellAccessSession) getVariable(SESSION_KEY);
    }
    if (m_ShellIO == null && m_Environment.containsKey(SHELLIO_KEY)) {
      m_ShellIO = (ShellIO) getVariable(SHELLIO_KEY);
    } else {
      m_ShellIO = new ShellIOImpl(m_TermIO, Locale.ENGLISH);
      //store I/O reference
      setVariable(SHELLIO_KEY, m_ShellIO);
    }
  }//prepare

  /**
   * Returns the {@link ShellAccessSession} instance.
   *
   * @return a {@link ShellAccessSession} instance.
   */
  public ShellAccessSession getSession() {
    return m_Session;
  }//getSession

  /**
   * Sets the {@link ShellAccessSession} instance.
   * Note that it will as well be stored in the
   * environment as <tt>PROXY_SESSION</tt>.
   *
   * @param ps the {@link ShellAccessSession} instance.
   */
  public void setSession(ShellAccessSession ps) {
    setVariable(SESSION_KEY, ps);
    m_Session = ps;
  }//setSession

  /**
   * Returns the {@link net.coalevo.shellaccess.impl.ShellIOImpl}
   * instance of the connection.
   *
   * @return the shell io.
   */
  public ShellIO getShellIO() {
    return m_ShellIO;
  }//getShellIO

  /**
   * Starts a progress indicator.
   */
  public void startProgressIndicator() {
    return;
    //m_InProgress.set(true);
    //ExecutionService es = getExecutionService();
    //es.execute(m_Services.getServiceAgent(), new ProgressIndicator());
  }//startProgressIndicator

  /**
   * Stops a progress indicator.
   */
  public void stopProgressIndicator() {
    if (m_InProgress.get()) {
      m_StoppedProgress = new CountDownLatch(1);
      m_InProgress.set(false);
      try {
        //in case something goes wrong, after 5 seconds we should
        //be through with the waiting
        m_StoppedProgress.await(5000, TimeUnit.MILLISECONDS);
      } catch (InterruptedException ex) {
        Activator.log().error("stopProgressIndicator()", ex);
      }
    }
  }//stopProgressIndicator

  class ProgressIndicator implements Runnable {

    public void run() {
      try {
        while (m_InProgress.get()) {
          m_ShellIO.indicateProgress();
          Thread.sleep(500);
        }
      } catch (Exception ex) {
        Activator.log().error("ProgressIndicator::run()", ex);
      } finally {
        m_StoppedProgress.countDown();
        m_InProgress.set(false);
      }
    }//run

  }//ProgressIndicator

  public ExecutionService getExecutionService() {
    return m_Services.getExecutionService(ServiceMediator.WAIT_UNLIMITED);
  }//getExecutionService

  public void unsetEnvironmentVariable(String key) {
    m_Environment.remove(key);
  }//dropEnvironmentVariable

  public Object getEnvironmentVariable(String key) {
    return m_Environment.get(key);
  }//getEnvironmentVariable

  public void setEnvironmentVariable(String key, Object value) {
    if (value == null) {
      m_Environment.remove(key);
    } else {
      m_Environment.put(key, value);
    }
  }//setEnvironmentVariable

  public boolean isEnviromentVariableSet(String key) {
    return m_Environment.containsKey(key);
  }//isEnvironmentVariableSet

  public Iterator environmentVariables() {
    return m_Environment.keySet().iterator();
  }//environmentVariable

  public Services getServices() {
    return m_Services;
  }//getServices


  public static String SESSION_KEY = "BASE_SESSION";
  public static String SHELLIO_KEY = "BASE_SHELLIO";
  public static String NEWLANGUAGE_KEY = "BASE_NEWLANG";

}//class BaseShell
