/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.impl;

import net.coalevo.foundation.model.Session;
import net.coalevo.presence.model.*;
import net.coalevo.shellaccess.model.ShellAccessSession;
import org.slf4j.Logger;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 * This class implements a manager for a node-local
 * who list.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class SessionManager {

  private static Logger log = Activator.log();
  private static final Marker c_LogMarker = MarkerFactory.getMarker(SessionManager.class.getName());
  private List<ShellAccessSession> m_Sessions;
  private Future m_Future = null;

  public SessionManager() {
    m_Sessions = new CopyOnWriteArrayList<ShellAccessSession>();
  }//constructor

  public ShellAccessSession createSession() {
    return new ShellAccessSessionImpl(this);
  }//createSession

  public boolean addSession(ShellAccessSession ses) {
    log.debug(c_LogMarker, "addSession()::" + ses.toString());
    return m_Sessions.add(ses);
  }//addSession

  public boolean removeSession(ShellAccessSession ses) {
    log.debug(c_LogMarker, "removeSession()::" + ses.toString());
    return m_Sessions.remove(ses);
  }//removeSession

  public int getSessionCount() {
    return m_Sessions.size();
  }//getSessionCount

  public Iterator sessions() {
    return m_Sessions.listIterator();
  }//sessions

  public void startAwayHandler() {
    try {
      ServiceMediator sm = Activator.getServices();
      m_Future = sm.getExecutionService(ServiceMediator.NO_WAIT).scheduleAtFixedRate(
          sm.getServiceAgent(),
          new AutoAwayTask(),
          60000,
          60000,
          TimeUnit.MILLISECONDS
      );
    } catch (Exception ex) {
      log.error(c_LogMarker, "startAwayHandler()", ex);
    }

  }//startAwayHandler

  public void stopAwayHandler() {
    try {
      if (m_Future != null) {
        m_Future.cancel(false);
      }
    } catch (Exception ex) {
      log.error(c_LogMarker, "stopAwayHandler()", ex);
    }
  }//stopAwayHandler

  class AutoAwayTask implements Runnable {

    public void run() {
      try {
        //log.debug("Running ShellAccess AutoAwayTask.");
        long now = System.currentTimeMillis();
        ArrayList<ShellAccessSession> invses = new ArrayList<ShellAccessSession>();
        for (ListIterator<ShellAccessSession> iter = m_Sessions.listIterator(); iter.hasNext();) {
          ShellAccessSession ses = iter.next();
          if (!ses.isLoggedIn()) {
            invses.add(ses);
            log.debug(c_LogMarker, "AutoAwayTask::run()::will invalidate" + ses.getUserAgent().getIdentifier());
            continue;
          }
          if (ses.isAway()) {
            continue;
          }
          try {
            Session s = ses.getUserAgent().getSession();
            if (s != null && s.isValid()) {
              long lac = s.getLastAccessedTime();
              long aat = ses.getAutoAwayTime();
              if (now > lac + aat) {
                Presence p = (Presence)
                    s.getAttribute(PresenceConstants.PRESENCE_SESSION_ATTRIBUTE);
                if (p.isUnavailable()) {
                  //no status update needed
                  ses.setAway(true);
                  continue;
                }
                PresenceStatus ps = p.getStatus();
                //store old
                PresenceStatusType pst = ps.getType();
                s.setAttribute("net.coalevo.presence.model.PresenceStatusType_last", pst);
                if (pst.equals(PresenceStatusTypes.AdvertisedAvailable)) {
                  ps.setType(PresenceStatusTypes.AdvertisedTemporarilyUnavailable);
                } else {
                  ps.setType(PresenceStatusTypes.TemporarilyUnavailable);
                }
                p.updateStatus(ps);
                ses.setAway(true);
              }
            }
          } catch (IllegalStateException ise) {
            log.debug(c_LogMarker, "AutoAwayTask::run()::will invalidate" + ses.getUserAgent().getIdentifier(), ise);
            invses.add(ses);
          } catch (Exception ex) {
            log.debug(c_LogMarker, "AutoAwayTask::run()::" + ses.getUserAgent().getIdentifier(), ex);
          }
        }//for

        //Invalid sessions that are no longer logged in if still, or that have an illegal presence state.
        for (Iterator<ShellAccessSession> iter = invses.iterator(); iter.hasNext();) {
          ShellAccessSession ses = iter.next();
          try {
            ses.invalidate();
          } catch (Exception ex) {
            log.error(c_LogMarker, "AutoAwayTask::run()::invalidate failed::" + ses.getUserAgent().getIdentifier(), ex);
          }
        }
      } catch (Exception ex) {
        log.error(c_LogMarker, "AutoAwayTask::run()", ex);
      }
    }//run

  }//AutoAwayTask

}//class SessionManager
