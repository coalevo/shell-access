/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.command;

import net.coalevo.foundation.model.Agent;

/**
 * This interface defines the contract for
 * <tt>CommandShellSecurityManager</tt> instances.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface CommandShellSecurityManager {

  /**
   * Tests if a given command can be executed by a specific {@link Agent}.
   *
   * @param id the identifier of the command as <tt>String</tt>.
   * @param a  an {@link Agent} instance.
   * @return false if a given agent is not authorized.
   * @throws SecurityException if the given agent is not authentic.
   */
  public boolean canExecuteCommand(String id, Agent a)
      throws SecurityException;

  /**
   * Tests if a given menu can be entered by a specific {@link Agent}.
   *
   * @param id the identifier of the command as <tt>String</tt>.
   * @param a  an {@link Agent} instance.
   * @return false if a given agent is not authorized.
   * @throws SecurityException if the given agent is not authentic.
   */
  public boolean canEnterMenu(String id, Agent a)
      throws SecurityException;

}//interface CommandShellSecurityManager
