/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.command;

/**
 * Class implementing a <tt>KeyEntry</tt>
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public abstract class KeyEntry {


  protected String m_Identifier;
  protected String m_DescriptionKey;
  protected String m_HelpKey;
  protected String m_CommentsKey;
  protected String m_ConfirmationKey;
  protected boolean m_Hidden = false;
  protected CommandShellManager m_CommandShellManager;
  protected boolean m_AvailableOnLongPrompt = false;

  /**
   * Constructs a new <tt>KeyEntry</tt> instance
   * with the given identifier and type.
   *
   * @param csm    the {@link CommandShellManager}.
   * @param id     the identifier as <tt>String</tt>
   * @param hidden true if to be hidden from the menu, false otherwise.
   */
  protected KeyEntry(CommandShellManager csm, String id, boolean hidden) {
    m_Identifier = id;
    m_Hidden = hidden;
    m_CommandShellManager = csm;
  }//Entry

  /**
   * Returns the key for the description
   * template.
   *
   * @return a template key as <tt>String</tt>.
   */
  public String getDescriptionKey() {
    return m_DescriptionKey;
  }//getDescriptionKey


  /**
   * Returns the key for the help template.
   *
   * @return a template key as <tt>String</tt>.
   */
  public String getHelpKey() {
    return m_HelpKey;
  }//getHelpKey

  /**
   * Returns the key for the comments
   * template.
   *
   * @return a template key as <tt>String</tt>.
   */
  public String getCommentsKey() {
    return m_CommentsKey;
  }//getCommentsKey

  /**
   * Returns the key for the confirmation
   * template.
   *
   * @return a template key as <tt>String</tt>.
   */
  public String getConfirmationKey() {
    return m_ConfirmationKey;
  }//getConfirmationKey

  /**
   * Returns the identifier of this <tt>KeyEntry</tt>.
   *
   * @return an identifier as <tt>String</tt>.
   */
  public String getIdentifier() {
    return m_Identifier;
  }//getIdentifier

  /**
   * Tests if this <tt>KeyEntry</tt> is flagged as
   * hidden.
   *
   * @return true if hidden, false otherwise.
   */
  public boolean isHidden() {
    return m_Hidden;
  }//getHidden

  protected void setHidden(boolean hidden) {
    m_Hidden = hidden;
  }//setHidden


  /**
   * Tests if this <tt>KeyEntry</tt> instance is
   * a {@link MenuKeyEntry}.
   *
   * @return true if {@link MenuKeyEntry}, false otherwise.
   */
  public boolean isMenuEntry() {
    return this instanceof MenuKeyEntry;
  }//isMenuEntry

  /**
   * Tests if this <tt>KeyEntry</tt> instance is
   * a {@link CommandKeyEntry}.
   *
   * @return true if {@link CommandKeyEntry}, false otherwise.
   */
  public boolean isCommandEntry() {
    return this instanceof CommandKeyEntry;
  }//isCommandEntry

  /**
   * Returns if the entry is available on a default level long prompt.
   *
   * @return true if available on default level long prompt, false otherwise.
   */
  public boolean isAvailableOnLongPrompt() {
    return m_AvailableOnLongPrompt;
  }//isAvailableOnLongPrompt

  /**
   * Factory method for creating <tt>KeyEntry</tt> instances
   * that refer to a menu.
   *
   * @param csm    the {@link CommandShellManager}.
   * @param id     the identifier of the menu as <tt>String</tt>
   * @param hidden true if to be hidden from the menu, false otherwise.
   * @return the new <tt>KeyEntry</tt> instance.
   */
  public static KeyEntry createMenuEntry(CommandShellManager csm, String id, boolean hidden) {
    return new MenuKeyEntry(csm, id, hidden);
  }//createMenuEntry

  /**
   * Factory method for creating <tt>KeyEntry</tt> instances
   * that refer to a command.
   *
   * @param csm    the {@link CommandShellManager}.
   * @param id     the identifier of the command as <tt>String</tt>
   * @param hidden true if to be hidden from the menu, false otherwise.
   * @param lp     true if avalable on default long prompt, false otherwise.
   * @return the new <tt>KeyEntry</tt> instance.
   */
  public static KeyEntry createCommandEntry(CommandShellManager csm, String id, boolean hidden, boolean lp) {
    return new CommandKeyEntry(csm, id, hidden, lp);
  }//createCommandEnry

  protected static final String CONFIRMATION_POSTFIX = "_confirmation";
  protected static final String DESCRIPTION_POSTFIX = "_description";
  protected static final String HELP_POSTFIX = "_help";
  protected static final String COMMENTS_POSTFIX = "_comments";

}//class KeyEntry
