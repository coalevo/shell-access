/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.command;

import net.coalevo.shellaccess.model.Command;

/**
 * This class defines a {@link KeyEntry} for
 * a {@link Command}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class CommandKeyEntry
    extends KeyEntry {

  protected String m_NamespaceKey;

  /**
   * Constructs a new <tt>CommandKeyEntry</tt> instance.
   *
   * @param csm    the {@link CommandShellManager}.
   * @param id     the unique identifier as <tt>String</tt>.
   * @param hidden true if hidden, false otherwise.
   * @param lp     true if available on default level longprompt, false otherwise.
   */
  public CommandKeyEntry(CommandShellManager csm, String id, boolean hidden, boolean lp) {
    super(csm, id, hidden);
    m_AvailableOnLongPrompt = lp;
    m_ConfirmationKey =
        new StringBuilder(COMMANDS_PREFIX).append(id).append(CONFIRMATION_POSTFIX).toString();
    m_DescriptionKey =
        new StringBuilder(COMMANDS_PREFIX).append(id).append(DESCRIPTION_POSTFIX).toString();
    m_HelpKey =
        new StringBuilder(COMMANDS_PREFIX).append(id).append(HELP_POSTFIX).toString();
    m_CommentsKey =
        new StringBuilder(COMMANDS_PREFIX).append(id).append(COMMENTS_POSTFIX).toString();
    m_NamespaceKey = new StringBuilder(NAMESPACES_PREFIX).append(id.substring(0, id.lastIndexOf("."))).toString();
  }//CommandKeyEntry

  /**
   * Returns the key for the namespace template.
   *
   * @return a template key as <tt>String</tt>.
   */
  public String getNamespaceKey() {
    return m_NamespaceKey;
  }//getNamespaceKey

  /**
   * Leases the {@link Command} that this <tt>CommandKeyEntry</tt>
   * is mapping to.
   *
   * @return a {@link Command} instance.
   */
  public Command leaseCommand() {
    return m_CommandShellManager.leaseCommand(m_Identifier);
  }//leaseCommand

  /**
   * Releases a previously leased {@link Command} instance.
   *
   * @param cmd a {@link Command} instance.
   * @see #leaseCommand()
   */
  public void releaseCommand(Command cmd) {
    m_CommandShellManager.releaseCommand(cmd);
  }//releaseCommand

  /**
   * Constant defining the commands prefix for creating
   * template keys.
   */
  private static final String COMMANDS_PREFIX = "commands_";

  /**
   * Constant defining the namespaces prefix for creating
   * template keys.
   */
  private static final String NAMESPACES_PREFIX = "namespaces_";

}//class CommandKeyEntry
