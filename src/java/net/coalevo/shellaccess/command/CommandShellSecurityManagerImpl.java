/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.command;

import net.coalevo.foundation.model.Agent;
import net.coalevo.security.model.AuthorizationRule;
import net.coalevo.security.service.PolicyService;
import net.coalevo.security.service.SecurityService;
import net.coalevo.shellaccess.impl.Activator;
import net.coalevo.shellaccess.impl.ServiceMediator;
import net.coalevo.shellaccess.model.Services;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * This class implements a simple {@link CommandShellSecurityManager}.
 * <p/>
 * It will be configured through the {@link CommandShellConfigHandler}
 * that handles the command shell configuration file.
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class CommandShellSecurityManagerImpl
    implements CommandShellSecurityManager {

  private static final Marker c_LogMarker = MarkerFactory.getMarker(CommandShellSecurityManagerImpl.class.getName());
  private Map<String,AuthorizationRule> m_CommandRules;
  private Map<String,AuthorizationRule> m_MenuRules;
  private ServiceMediator m_Services;

  /**
   * Constructs a new <tt>CommandShellSecurityManagerImpl</tt>
   * instance.
   */
  public CommandShellSecurityManagerImpl() {
    m_CommandRules = new HashMap<String,AuthorizationRule>();
    m_MenuRules = new HashMap<String,AuthorizationRule>();
    m_Services = Activator.getServices();
  }//constructor

  public boolean canExecuteCommand(String id, Agent a)
      throws SecurityException {
    Activator.log().debug(c_LogMarker,"canExecuteCommand()::" + id + "::" + a.getIdentifier() + "::" + m_CommandRules.containsKey(id));
    AuthorizationRule r = m_CommandRules.get(id);
    return r != null && r.isAuthorized(getSecurityService().getAuthorizations(a));
  }//canExecuteCommand

  public boolean canEnterMenu(String id, Agent a)
      throws SecurityException {
    Activator.log().debug(c_LogMarker,"canEnterMenu()::" + id + "::" + a.getIdentifier() + "::" + m_MenuRules.containsKey(id));
    AuthorizationRule r = m_MenuRules.get(id);
    if (r == null) {
      return (id == "main");
    } else {
      return ((AuthorizationRule) r).isAuthorized(getSecurityService().getAuthorizations(a));
    }
  }//canEnterMenu

  /**
   * This will add a permission required for a menu.
   * For one and the same permission set, only one
   * type of set can be used (i.e. AND or OR).
   *
   * @param id   the unique identifier of the menu.
   * @param rule a SARL expression.
   * @throws Exception if the rule cannot be created because it isn't valid SARL.
   */
  public void addMenuAuthRule(String id, String rule)
      throws Exception {
    PolicyService ps = m_Services.getPolicyService(Services.WAIT_UNLIMITED);
    if (ps != null) {
      AuthorizationRule ar = ps.createAuthorizationRule(rule);
      m_MenuRules.put(id, ar);
    }
  }//addMenuAuthRule

  /**
   * This will add a permission required for a command.
   * For one and the same permission set, only one
   * type of set can be used (i.e. AND or OR).
   *
   * @param id   the unique identifier of the command.
   * @param rule a SARL expression.
   * @throws Exception if the rule cannot be created because it isn't valid SARL.
   */
  public void addCommandAuthRule(String id, String rule)
      throws Exception {

    PolicyService ps = m_Services.getPolicyService(Services.WAIT_UNLIMITED);
    if (ps != null) {
      AuthorizationRule ar = ps.createAuthorizationRule(rule);
      m_CommandRules.put(id, ar);
    }
  }//addCommandAuthRule

  private SecurityService getSecurityService() {
    SecurityService s = m_Services.getSecurityService(Services.NO_WAIT);
    if (s == null) {
      s = m_Services.getSecurityService(Services.WAIT_UNLIMITED);
    }
    return s;
  }//getSecurityService

}//class CommandShellSecurityManagerImpl
