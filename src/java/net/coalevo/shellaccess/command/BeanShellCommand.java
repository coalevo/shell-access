/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.command;

import bsh.Interpreter;
import net.coalevo.shellaccess.impl.Activator;
import net.coalevo.shellaccess.model.BasicShell;
import net.coalevo.shellaccess.model.CommandException;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * This class implements a wrapper for Beanshell based
 * <tt>ScriptCommands</tt>.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class BeanShellCommand implements ScriptCommand {

  private static final Marker c_LogMarker = MarkerFactory.getMarker(BeanShellCommand.class.getName());
  private String m_Script;
  private final String m_Identifier;
  private BeanShellCommandProvider m_Provider;
  private Interpreter m_Interpreter;

  public BeanShellCommand(BeanShellCommandProvider p, String id, String script) {
    m_Identifier = id;
    m_Provider = p;
    m_Script = script;
  }//constructor

  public String getIdentifier() {
    return m_Identifier;
  }//getIdentifier

  public String getScript() {
    return m_Script;
  }//getScript

  public void run(BasicShell shell) throws CommandException {
    try {
      m_Interpreter = m_Provider.leaseInterpreter();
      m_Interpreter.set(KEY_SHELL, shell);
      m_Interpreter.set(KEY_SERVICES, Activator.getServices());
      m_Interpreter.eval(m_Script);
    } catch (Exception ex) {
      Activator.log().error(c_LogMarker,"run(BaseShell)::", ex);
    }
  }//run

  public void reset() {
    try {
      //unset variables
      if (m_Interpreter != null) {
        m_Interpreter.unset(KEY_SHELL);
        m_Interpreter.unset(KEY_SERVICES);
        m_Provider.releaseInterpreter(m_Interpreter);
        m_Interpreter = null;
      }
    } catch (Exception ex) {
      Activator.log().error(c_LogMarker,"reset()", ex);
    }

  }//reset

}//class BeanShellCommand
