/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.command;

/**
 * This class defines a {@link KeyEntry} for a menu.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class MenuKeyEntry
    extends KeyEntry {

  protected String m_PromptKey;
  protected String m_NameKey;

  /**
   * Constructs a new <tt>MenuKeyEntry</tt> instance.
   *
   * @param csm the {@link CommandShellManager}.
   * @param id     the unique identifier as <tt>String</tt>.
   * @param hidden true if hidden, false otherwise.
   */
  public MenuKeyEntry(CommandShellManager csm, String id, boolean hidden) {
    super(csm, id, hidden);
    m_ConfirmationKey =
        new StringBuilder(MENUS_PREFIX).append(id).append(CONFIRMATION_POSTFIX).toString();
    m_DescriptionKey =
        new StringBuilder(MENUS_PREFIX).append(id).append(DESCRIPTION_POSTFIX).toString();
    m_HelpKey =
        new StringBuilder(MENUS_PREFIX).append(id).append(HELP_POSTFIX).toString();
    m_CommentsKey =
        new StringBuilder(MENUS_PREFIX).append(id).append(COMMENTS_POSTFIX).toString();
    m_PromptKey =
        new StringBuilder(MENUS_PREFIX).append(id).append(PROMPT_POSTFIX).toString();
    m_NameKey =
        new StringBuilder(MENUS_PREFIX).append(id).append(NAME_POSTFIX).toString();
  }//MenuKeyEntry

  /**
   * Returns the key for the prompt template
   * of this menu.
   *
   * @return a template key as <tt>String</tt>.
   */
  public String getPromptKey() {
    return m_PromptKey;
  }//getPromptKey

  /**
   * Returns the key for the name template
   * of this menu.
   *
   * @return a template key as <tt>String</tt>.
   */
  public String getNameKey() {
    return m_NameKey;
  }//getNameKey

  /**
   * Constant defining the menu prefix for creating
   * template keys.
   */
  private static final String MENUS_PREFIX = "menus_";
  private static final String PROMPT_POSTFIX = "_prompt";
  private static final String NAME_POSTFIX = "_name";

}//class MenuKeyEntry
