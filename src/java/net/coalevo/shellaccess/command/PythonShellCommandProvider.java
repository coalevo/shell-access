/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.command;

import net.coalevo.foundation.util.LRUCacheMap;
import net.coalevo.shellaccess.impl.Activator;
import net.coalevo.shellaccess.model.Command;
import net.coalevo.shellaccess.model.CommandException;
import net.coalevo.shellaccess.model.ScriptShellCommandProvider;
import org.apache.commons.pool.BasePoolableObjectFactory;
import org.apache.commons.pool.ObjectPool;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.osgi.framework.BundleContext;
import org.python.core.Py;
import org.python.core.PySystemState;
import org.python.util.PythonInterpreter;

import java.io.*;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Provides an implementation of {@link ScriptShellCommandProvider} for
 * Python script commands.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class PythonShellCommandProvider
    implements ScriptShellCommandProvider {

  private String m_Namespace;
  private GenericObjectPool.Config m_PoolConfig;
  private InterpreterPool m_Interpreters;
  private File m_Store;
  private LRUCacheMap<String,String> m_ScriptCache;

  public PythonShellCommandProvider(String namespace) {
    m_Namespace = namespace;
  }//constructor

  public void activate(BundleContext bc) {
    m_ScriptCache = new LRUCacheMap<String,String>(25);
    //1. prepare commands
    m_Store = new File(Activator.getScriptStore(), m_Namespace);
    if (!m_Store.exists()) {
      m_Store.mkdir();
    }
    //2. prepare interpreters
    prepareInterpreters();
  }//activate

  public void deactivate() {
    m_ScriptCache.clear(false);
  }//deactivate

  public String getNamespace() {
    return m_Namespace;
  }//getNamespace


  public String getCommandScript(String id)
      throws IOException {
    //cache
    Object o = m_ScriptCache.get(id);
    if (o != null) {
      return (String) o;
    }
    //file
    final File f = new File(m_Store, id.concat(FILE_ENDING));
    if (!f.exists()) {
      throw new NoSuchElementException();
    } else {
      FileReader fr = new FileReader(f);
      StringWriter strw = new StringWriter();
      char[] buf = new char[1024 * 2];  //2k buffer
      boolean done = false;
      do {
        int i = fr.read(buf, 0, buf.length);
        if (i < 0) {
          done = true;
        } else {
          strw.write(buf, 0, i);
        }
      } while (!done);
      fr.close();
      strw.flush();
      final String script = strw.toString();
      m_ScriptCache.put(id, script);
      return script;
    }
  }//getCommandScript

  public void setCommandScript(String id, String script)
      throws IOException {
    //file
    final File f = new File(m_Store, id.concat(FILE_ENDING));
    if (f.exists()) {
      f.renameTo(new File(m_Store, f.getName() + "~"));
    }
    FileWriter w = new FileWriter(f);
    w.write(script);
    w.flush();
    w.close();
    //replace in cache
    m_ScriptCache.put(id, script);
  }//setCommandScript

   public String[] listCommandScriptNames() {
    File[] files = m_Store.listFiles(new FilenameFilter() {
      public boolean accept(File dir, String name) {
        return name.endsWith(FILE_ENDING);
      }//accept
    });
    String[] ret = new String[files.length];
    for (int i = 0; i < files.length; i++) {
      ret[i] = files[i].getName();
    }
    return ret;
  }//listCommandScriptNames

  public boolean provides(String cid) {
    return m_ScriptCache.containsKey(cid) || new File(m_Store, cid.concat(FILE_ENDING)).exists();
  }//provides

  public Command createCommand(String cid)
      throws CommandException {
    try {
      final String script = getCommandScript(cid);
      return new PythonCommand(this, m_Namespace.concat(".").concat(cid), script);
    } catch (IOException ex) {
      throw new CommandException(ex.getMessage());
    }
  }//createCommand


  public PythonInterpreter leaseInterpreter()
      throws Exception {
    return m_Interpreters.lease();
  }//leaseInterpreter

  public void releaseInterpreter(PythonInterpreter pi)
      throws Exception {
    m_Interpreters.release(pi);
  }//releaseInterpreter

  private void prepareInterpreters() {
    //pool config
    m_PoolConfig = new GenericObjectPool.Config();
    //set for
    m_PoolConfig.maxActive = 25;
    m_PoolConfig.maxIdle = 5;
    m_PoolConfig.maxWait = -1;
    m_PoolConfig.testOnBorrow = false;
    m_PoolConfig.testOnReturn = false;
    //make it growable so it adapts
    m_PoolConfig.whenExhaustedAction = GenericObjectPool.WHEN_EXHAUSTED_GROW;
    m_Interpreters = new InterpreterPool();

    Py.initPython();
    PySystemState sys = Py.getSystemState();
    if (sys == null) {
      //System.err.println("No systemstate");
      return;
    }
    //set the correct classloader
    sys.setClassLoader(getClass().getClassLoader());
    //import all packages that are imported by the bundle itself.
    for (Iterator i = Activator.getBundleImports(); i.hasNext();) {
      String pkg = i.next().toString().trim();
      //System.err.println("Adding " + pkg);
      PySystemState.add_package(pkg);
    }
    //import all packages that are exported by the bundle itself.
    for (Iterator i = Activator.getBundleExports(); i.hasNext();) {
      String pkg = i.next().toString().trim();
      //System.err.println("Adding " + pkg);
      PySystemState.add_package(pkg);
    }


  }//prepareInterpreters


  /**
   * Inner class implementing a pool of
   * <tt>Interpreter</tt> instances.
   */
  private class InterpreterPool {

    private ObjectPool m_Pool;

    public InterpreterPool() {
      m_Pool = new GenericObjectPool(new InterpreterFactory(), m_PoolConfig);
    }//constructor

    public PythonInterpreter lease()
        throws Exception {
      return (PythonInterpreter) m_Pool.borrowObject();
    }//lease

    public void release(PythonInterpreter i) throws Exception {
      m_Pool.returnObject(i);
    }//release

  }//InterpreterPool

  /**
   * Inner class implementing a factory for the
   * {@link net.coalevo.shellaccess.command.CommandShellManager.CommandPool} instances.
   */
  private class InterpreterFactory
      extends BasePoolableObjectFactory {

    public InterpreterFactory() {
    }//constructor

    public Object makeObject() throws Exception {
      //System.err.println(getClass().getClassLoader());
      //System.err.println(Activator.getServices().getClass().getClassLoader());
      // System.err.println(Thread.currentThread().getContextClassLoader());
      return new PythonInterpreter();
    }//makeObject

  }//class InterpreterFactory


  private static final String FILE_ENDING = ".py";


}//class PythonShellCommandProvider
