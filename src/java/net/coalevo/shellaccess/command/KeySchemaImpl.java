/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.command;

import net.coalevo.foundation.model.Agent;
import net.coalevo.shellaccess.impl.Activator;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.util.*;

/**
 * Class implementing a <tt>KeySchemaImpl</tt>.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class KeySchemaImpl implements KeySchema {

  private static final Marker c_LogMarker = MarkerFactory.getMarker(KeySchemaImpl.class.getName());

  protected Map<String, SortedMap<Character, KeyEntry>> m_EntryMaps;
  private String m_Identifier;
  private CommandShellSecurityManager m_SecurityManager;
  protected Map<Character, String> m_SpecialKeys;
  protected CommandShellManager m_CommandShellManager;

  /**
   * Constructs a new <tt>KeySchema</tt> instance
   * with the given identifier.
   *
   * @param csm the {@link CommandShellManager}.
   * @param id  a <tt>String</tt> identifier.
   */
  public KeySchemaImpl(CommandShellManager csm, String id) {
    m_Identifier = id;
    m_EntryMaps = new HashMap<String, SortedMap<Character, KeyEntry>>();
    m_SpecialKeys = new HashMap<Character, String>();
    m_CommandShellManager = csm;
  }//constructor


  public String getIdentifier() {
    return m_Identifier;
  }//getIdentifier

  /**
   * Returns the actual SecurityManager instance.
   *
   * @param smgr a {@link CommandShellSecurityManager} instance.
   */
  public void setSecurityManager(CommandShellSecurityManager smgr) {
    m_SecurityManager = smgr;
  }//setSecurityManager

  public KeyEntry entryForKey(String mid, int key, Agent a)
      throws SecurityException, NoSuchEntryException, NoSuchMenuException {
    Object o = m_EntryMaps.get(mid);
    if (o != null) {
      Character ch = new Character((char) key);
      o = ((Map) o).get(ch);
      if (o != null) {
        KeyEntry ke = (KeyEntry) o;
        if ((ke.isMenuEntry() && !m_SecurityManager.canEnterMenu(ke.getIdentifier(), a))
            || (ke.isCommandEntry() && !m_SecurityManager.canExecuteCommand(ke.getIdentifier(), a))) {
          throw new SecurityException();
        }
        return ke;
      } else {
        throw new NoSuchEntryException();
      }
    } else {
      Activator.log().error(c_LogMarker, "entryForKey():: Wrong menu identifier passed as parameter. " + mid);
      throw new NoSuchMenuException();
    }
  }//entryForKey

  public String translateSpecialKey(Character c) {
    String rval = c.toString();
    Object o = m_SpecialKeys.get(c);
    if (o != null) {
      return o.toString();
    }
    return rval;
  }//translateSpecialKey

  public boolean isSpecialKey(Character c) {
    return m_SpecialKeys.containsKey(c);
  }//isSpecialKey

  public MenuEntries menuEntries(String mid, Agent a)
      throws NoSuchMenuException, SecurityException {
    SortedMap<Character, KeyEntry> m = m_EntryMaps.get(mid);
    if (m == null) {
      throw new NoSuchMenuException();
    }
    //ensure required menu permissions
    m_SecurityManager.canEnterMenu(mid, a);
    //put together accesible menu and command entries
    MenuEntriesImpl entries = new MenuEntriesImpl();
    for (Iterator<Map.Entry<Character, KeyEntry>> iterator = m.entrySet().iterator(); iterator.hasNext();) {
      Map.Entry<Character, KeyEntry> entry = iterator.next();
      KeyEntry ke = entry.getValue();
      try {
        if (ke.isMenuEntry() && !ke.isHidden()) {
          if (m_SecurityManager.canEnterMenu(ke.getIdentifier(), a)) {
            entries.putMenuKeyEntry(entry);
          }
        } else if (ke.isCommandEntry() && !ke.isHidden()) {
          if (m_SecurityManager.canExecuteCommand(ke.getIdentifier(), a)) {
            entries.putCommandKeyEntry(entry);
          }
        }
      } catch (SecurityException ex) {
      }
    }
    return entries;
  }//menuEntries

  /**
   * Adds a {@link KeyEntry} instance that will refer to a
   * command.
   *
   * @param mid    a menu identifier as <tt>String</tt>.
   * @param cid    a command identifier as <tt>String</tt>.
   * @param c      the key this entry is mapped to as <tt>Character</tt>.
   * @param hidden true if hidden, false otherwise.
   * @param lp     true if available on long prompt in forum commands actually, false otherwise.
   */
  public void addCommandEntry(String mid, String cid, Character c, boolean hidden, boolean lp) {
    Activator.log().debug(c_LogMarker, "addCommandEntry()::mid=" + mid + "::cid=" + cid + "::Character=" + c.toString());
    if (m_EntryMaps.containsKey(mid)) {
      SortedMap<Character, KeyEntry> m = m_EntryMaps.get(mid);
      m.put(c, KeyEntry.createCommandEntry(m_CommandShellManager, cid, hidden, lp));
    } else {
      SortedMap<Character, KeyEntry> m = new TreeMap<Character, KeyEntry>();
      m.put(c, KeyEntry.createCommandEntry(m_CommandShellManager, cid, hidden, false));
      m_EntryMaps.put(mid, m);
    }
  }//addCommandEntry

  /**
   * Adds a {@link KeyEntry} instance that will refer to a
   * menu.
   *
   * @param mid    a menu identifier as <tt>String</tt>.
   * @param submid a submenu identifier as <tt>String</tt>.
   * @param c      the key this entry is mapped to as <tt>Character</tt>.
   * @param hidden true if hidden, false otherwise.
   */
  public void addMenuEntry(String mid, String submid, Character c, boolean hidden) {
    Activator.log().debug(c_LogMarker, "addMenuEntry()::mid=" + mid + "::submid=" + submid + "::Character=" + c.toString());

    if (m_EntryMaps.containsKey(mid)) {
      SortedMap<Character, KeyEntry> m = m_EntryMaps.get(mid);
      m.put(c, KeyEntry.createMenuEntry(m_CommandShellManager, submid, hidden));
    } else {
      SortedMap<Character, KeyEntry> m = new TreeMap<Character, KeyEntry>();
      m.put(c, KeyEntry.createMenuEntry(m_CommandShellManager, submid, hidden));
      m_EntryMaps.put(mid, m);
    }
  }//addMenuEntry

  public void addSpecialKeyTranslation(Character c, String str) {
    Activator.log().debug(c_LogMarker, "addSpecialKeyTranslation():: Character = " + (int) c.charValue() + ":: tkey=" + str);
    m_SpecialKeys.put(c, str);
  }//addSpecialKeyTranslation

  private class MenuEntriesImpl implements MenuEntries {

    private List<Map.Entry<Character, KeyEntry>> m_MenuKeyEntries;
    private Map<String, List<Map.Entry<Character, KeyEntry>>> m_Namespaces;

    public MenuEntriesImpl() {
      m_MenuKeyEntries = new ArrayList<Map.Entry<Character, KeyEntry>>();
      m_Namespaces = new TreeMap<String, List<Map.Entry<Character, KeyEntry>>>();
    }//constructor

    public void putMenuKeyEntry(Map.Entry<Character, KeyEntry> entry) {
      m_MenuKeyEntries.add(entry);
    }//putMenuEntry

    public void putCommandKeyEntry(Map.Entry<Character, KeyEntry> entry) {
      String nsk = ((CommandKeyEntry) entry.getValue()).getNamespaceKey();
      if (m_Namespaces.containsKey(nsk)) {
        List<Map.Entry<Character, KeyEntry>> l = m_Namespaces.get(nsk);
        l.add(entry);
      } else {
        List<Map.Entry<Character, KeyEntry>> l = new ArrayList<Map.Entry<Character, KeyEntry>>();
        l.add(entry);
        m_Namespaces.put(nsk, l);
      }
    }//putCommandKeyEntry

    public Set<String> listNamespaces() {
      return m_Namespaces.keySet();
    }//listNamespaces

    public List<Map.Entry<Character, KeyEntry>> getCommandKeyEntries(String namespace) {
      return m_Namespaces.get(namespace);
    }//getCommandKeyEntries

    public List<Map.Entry<Character, KeyEntry>> getMenuKeyEntries() {
      return m_MenuKeyEntries;
    }//getMenuKeyEntries

  }//inner class MenuEntriesImpl


}//class KeySchemaImpl
