/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.command;

import net.coalevo.shellaccess.model.Command;

/**
 * Provides the base interface for a <tt>ScriptCommand</tt>.
 * <p/>
 * At the moment this interface is only for tagging purposes
 * and defines common constants.
 * </p>
 * <p/>
 * There are two defined variables that every implementation
 * should set on the script interpreter to allow the script
 * to obtain references to I/O and services:
 * <ul>
 * <li>The corresponding {@link net.coalevo.shellaccess.model.BasicShell} stored
 * using the key {@link #KEY_SHELL}.</li>
 * <li>The corresponding {@link net.coalevo.shellaccess.model.Services} instance,
 * using the key {@link #KEY_SERVICES}.</li>
 * </ul>
 * </p>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface ScriptCommand
    extends Command {

  /**
   * Defines the key for the {@link net.coalevo.shellaccess.model.BasicShell}
   * that should be used to set the variable on the script interpreter. (e.g.
   * =variable name).
   */
  public static String KEY_SHELL = "shell";

  /**
   * Defines the key for the {@link net.coalevo.shellaccess.model.Services}
   * that should be used to set the variable on the script interpreter. (e.g.
   * =variable name).
   */
  public static String KEY_SERVICES = "services";


}//interface ScriptCommand
