/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.command;


import net.coalevo.shellaccess.impl.Activator;
import net.coalevo.shellaccess.model.Command;
import net.coalevo.shellaccess.model.ShellCommandProvider;
import net.coalevo.shellaccess.service.ShellCommandProviderManager;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.Stack;

/**
 * Class implementing a SAX handler for the command
 * shell config document.
 * <p/>
 * This code is hand written, based on a simple state
 * machine.
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class CommandShellConfigHandler
    extends DefaultHandler {

  private static final Marker c_LogMarker = MarkerFactory.getMarker(CommandShellConfigHandler.class.getName());

  //state
  protected int m_State = STATE_UNDEFINED;
  protected int m_AuthRuleState = STATE_UNDEFINED;
  protected KeySchemaImpl m_KeySchema;
  protected String m_CommandId;
  protected String m_CommandType;
  protected GenericObjectPool.Config m_CommandPoolConfig;

  protected StringBuilder m_Buffer;
  protected StringBuilder m_FormatBuffer;
  protected Stack<String> m_MenuPrefix = new Stack<String>();
  protected CommandShellManager m_CSMgr;
  protected CommandShellSecurityManagerImpl m_SecurityManager;
  protected String m_Namespace;
  protected ShellCommandProviderManager m_CmdProviderManager;
  protected ShellCommandProvider m_CmdProvider;
  protected Command m_Command;

  /**
   * Constructor for the handler.
   *
   * @param csm a {@link CommandShellManager} instance.
   */
  public CommandShellConfigHandler(CommandShellManager csm) {
    m_CSMgr = csm;
    m_SecurityManager = (CommandShellSecurityManagerImpl) m_CSMgr.getSecurityManager();
    m_CmdProviderManager = csm.getCommandProviderManager();
  }//constructor

  public void startElement(String uri, String name,
                           String qualifiedName, Attributes attributes)
      throws SAXException {
    Activator.log().debug(c_LogMarker, "startElement()::" + m_State + "::" + name);
    try {
      switch (m_State) {
        case STATE_UNDEFINED:
          if (Tokens.COMMANDSHELLCONFIG.equals(name)) {
            m_State = STATE_COMMANDSHELLCONFIG;
          } else {
            throw new SAXException("Root element not found.");
          }
          break;
        case STATE_COMMANDSHELLCONFIG:
          if (Tokens.COMMANDSET.equals(name)) {
            handleCommandSetAttributes(attributes);
            m_State = STATE_COMMANDSET;
          } else if (Tokens.MENU.equals(name)) {
            handleMenuAttributes(attributes);
            m_State = STATE_MENU;
          } else if (Tokens.KEYSCHEMA.equals(name)) {
            pushMenuPrefix(m_CSMgr.getDefaultMenu().getIdentifier());
            handleCmdKeySchemaAttributes(attributes);
            m_State = STATE_KEYSCHEMA;
          } else if (Tokens.SPECIALKEY.equals(name)) {
            handleSpecialKeyAttributes(attributes);
            m_State = STATE_SPECIALKEY;
            break;
          } else if (Tokens.AUTOCOMMANDS.equals(name)) {
            m_State = STATE_AUTOCOMMANDS;
            break;
          }
          break;
        case STATE_COMMANDSET:
          if (Tokens.COMMAND.equals(name)) {
            handleCommandAttributes(attributes);
            m_State = STATE_COMMAND;
          }
          break;
        case STATE_COMMAND:
          if (Tokens.POOLCONFIG.equals(name)) {
            handlePoolConfigAttributes(attributes);
            m_State = STATE_POOLCONFIG;
          } else if (Tokens.AUTHRULE.equals(name)) {
            m_Buffer = new StringBuilder();
            m_AuthRuleState = STATE_COMMAND; //store to return
            m_State = STATE_AUTHRULE;
          }
          break;
        case STATE_MENU:
          if (Tokens.AUTHRULE.equals(name)) {
            m_Buffer = new StringBuilder();
            m_AuthRuleState = STATE_MENU; //store to return
            m_State = STATE_AUTHRULE;
          } else if (Tokens.MENU.equals(name)) {
            handleMenuAttributes(attributes);
            m_State = STATE_MENU;
          }
          break;
        case STATE_KEYSCHEMA:
        case STATE_MENUKEY:
          //pushMenuPrefix(m_CSMgr.getDefaultMenu().getIdentifier());
          if (Tokens.COMMANDKEY.equals(name)) {
            handleCommandKeyAttributes(attributes);
            m_State = STATE_COMMANDKEY;
          } else if (Tokens.MENUKEY.equals(name)) {
            handleMenuKeyAttributes(attributes);
            m_State = STATE_MENUKEY;
          }
          break;
        case STATE_AUTOCOMMANDS:
          if (Tokens.AUTOCOMMAND.equals(name)) {
            handleAutoCommand(attributes);
            m_State = STATE_AUTOCOMMAND;
          }
      }

    } catch (Exception e) {
      throw new SAXException(e);
    }
  }//startElement

  public void endElement(String namespaceURI, String localName, String qName)
      throws SAXException {
    Activator.log().debug(c_LogMarker, "endElement()::" + m_State + "::" + localName);
    switch (m_State) {
      case STATE_COMMANDSHELLCONFIG:
        //parsing should be over
        //do cleanups?
        break;
      case STATE_COMMANDSET:
        m_State = STATE_COMMANDSHELLCONFIG;
        break;
      case STATE_KEYSCHEMA:
        m_KeySchema.setSecurityManager(m_SecurityManager);
        m_CSMgr.addKeySchema(m_KeySchema.getIdentifier(), m_KeySchema);
        m_State = STATE_COMMANDSHELLCONFIG;
        break;
      case STATE_COMMAND:
        //Add command with provider, namespace and pool config
        m_CSMgr.addCommand(m_CmdProvider, m_CommandId, m_CommandPoolConfig);
        m_CommandPoolConfig = null;
        m_State = STATE_COMMANDSET;
        break;
      case STATE_POOLCONFIG:
        m_State = STATE_COMMAND;
        break;
      case STATE_AUTHRULE:
        if (m_AuthRuleState == STATE_COMMAND) {
          try {
            String cmdid = m_CmdProvider.getNamespace().concat(".").concat(m_CommandId);
            m_SecurityManager.addCommandAuthRule(cmdid, m_Buffer.toString());
          } catch (Exception ex) {
            throw new SAXException(ex);
          }
        } else if (m_AuthRuleState == STATE_MENU) {
          try {
            String menu = "";
            if (!m_MenuPrefix.isEmpty()) {
              menu = m_MenuPrefix.peek().toString();
            }
            m_SecurityManager.addMenuAuthRule(menu, m_Buffer.toString());
          } catch (Exception ex) {
            throw new SAXException(ex);
          }

        }
        m_State = m_AuthRuleState; //return to command or to menu
        break;
      case STATE_MENU:
        if (!m_MenuPrefix.isEmpty()) {
          String menu = m_MenuPrefix.pop().toString();
          m_CSMgr.addMenu(menu);
        }
        if (m_MenuPrefix.isEmpty()) {
          m_State = STATE_COMMANDSHELLCONFIG;
        }
        break;
      case STATE_COMMANDKEY:
        if (m_MenuPrefix.size() > 1) {
          m_State = STATE_MENUKEY;
        } else {
          m_State = STATE_KEYSCHEMA;
        }
        break;
      case STATE_MENUKEY:
        Activator.log().debug(c_LogMarker, m_MenuPrefix.toString());
        if (m_MenuPrefix.size() > 1) {
          m_MenuPrefix.pop();

        }
        if (m_MenuPrefix.size() > 1) {
          m_State = STATE_MENUKEY;
        } else {
          m_State = STATE_KEYSCHEMA;
        }
        break;
      case STATE_SPECIALKEY:
        m_State = STATE_COMMANDSHELLCONFIG;
        break;
      case STATE_AUTOCOMMANDS:
        m_State = STATE_COMMANDSHELLCONFIG;
        break;
      case STATE_AUTOCOMMAND:
        m_State = STATE_AUTOCOMMANDS;
        break;
    }
  }//endElement

  public void characters(char[] chars, int start, int length)
      throws SAXException {
    switch (m_State) {
      case STATE_AUTHRULE:
        m_Buffer.append(chars, start, length);
        break;
    }
  }//characters

  private final void handleCommandSetAttributes(Attributes attr)
      throws SAXException {
    //1. package
    String str = attr.getValue("", Tokens.NAMESPACE);
    if (str == null || str.length() == 0) {
      throw new SAXException("namespace missing.");
    } else if (!m_CmdProviderManager.isAvailable(str)) {
      throw new SAXException("No provider available for namespace.");
    } else {
      m_Namespace = str;
      m_CmdProvider = m_CmdProviderManager.get(str);
    }
  }//handleCommandSetAttributes

  private final void handleMenuAttributes(Attributes attr)
      throws SAXException {
    //1. id
    String str = attr.getValue("", Tokens.ID);
    if (str == null || str.length() == 0) {
      throw new SAXException("id missing");
    } else {
      if (m_MenuPrefix.isEmpty()) {
        m_CSMgr.setDefaultMenu(str);
      }
      pushMenuPrefix(str);
    }
  }//handleMenuSetAttributes

  private final void handleCmdKeySchemaAttributes(Attributes attr)
      throws SAXException {
    //1. id
    String str = attr.getValue("", Tokens.ID);
    if (str == null || str.length() == 0) {
      throw new SAXException("id missing");
    } else {
      m_KeySchema = new KeySchemaImpl(m_CSMgr, str);
    }
  }//handleCmdKeySchemaAttributes

  private final void handleCommandAttributes(Attributes attr)
      throws SAXException {
    //1. id
    String str = attr.getValue("", Tokens.ID);
    if (str == null || str.length() == 0) {
      throw new SAXException("id missing");
    } else if (!m_CmdProvider.provides(str)) {
      throw new SAXException(str + " is not available in namespace " + m_Namespace);
    } else {
      m_CommandId = str;
    }
  }//handleCommandAttributes

  private final void handlePoolConfigAttributes(Attributes attr) {
    GenericObjectPool.Config pcfg = new GenericObjectPool.Config();

    String str = attr.getValue("", Tokens.MINIDLE);
    if (str != null && str.length() == 0) {
      try {
        pcfg.minIdle = Integer.parseInt(str);
      } catch (NumberFormatException ex) {
        Activator.log().error(c_LogMarker, "handlePoolConfigAttributes()", ex);
      }
    }
    str = attr.getValue("", Tokens.MAXIDLE);
    if (str != null && str.length() == 0) {
      try {
        pcfg.maxIdle = Integer.parseInt(str);
      } catch (NumberFormatException ex) {
        Activator.log().error(c_LogMarker, "handlePoolConfigAttributes()", ex);
      }
    }
    str = attr.getValue("", Tokens.MINIDLETIME);
    if (str != null && str.length() == 0) {
      try {
        pcfg.minEvictableIdleTimeMillis = Integer.parseInt(str);
      } catch (NumberFormatException ex) {
        Activator.log().error(c_LogMarker, "handlePoolConfigAttributes()", ex);
      }
    }
    str = attr.getValue("", Tokens.EVICTIONINTERVAL);
    if (str != null && str.length() == 0) {
      try {
        pcfg.timeBetweenEvictionRunsMillis = Integer.parseInt(str);
      } catch (NumberFormatException ex) {
        Activator.log().error(c_LogMarker, "handlePoolConfigAttributes()", ex);
      }
    }
    str = attr.getValue("", Tokens.EXHAUSTACTION);
    if (str != null && str.length() == 0) {
      if (Tokens.EXHAUST_FAIL.equals(str)) {
        pcfg.whenExhaustedAction = GenericObjectPool.WHEN_EXHAUSTED_FAIL;
      } else if (Tokens.EXHAUST_GROW.equals(str)) {
        pcfg.whenExhaustedAction = GenericObjectPool.WHEN_EXHAUSTED_GROW;
      } else if (Tokens.EXHAUST_BLOCK.equals(str)) {
        pcfg.whenExhaustedAction = GenericObjectPool.WHEN_EXHAUSTED_BLOCK;
      }
    } else {
      pcfg.whenExhaustedAction = GenericObjectPool.WHEN_EXHAUSTED_GROW;
    }
    str = attr.getValue("", Tokens.MAXWAIT);
    if (str != null && str.length() == 0) {
      try {
        pcfg.maxWait = Integer.parseInt(str);
      } catch (NumberFormatException ex) {
        Activator.log().error(c_LogMarker, "handlePoolConfigAttributes()", ex);
        pcfg.maxWait = -1;
      }
    }
    m_CommandPoolConfig = pcfg;
  }//handlePoolConfigAttributes

  private final void handleCommandKeyAttributes(Attributes attr)
      throws SAXException {
    String str = attr.getValue("", Tokens.KEY);
    if (str == null || str.length() == 0) {
      throw new SAXException("commandkey key is missing.");
    } else {
      char keychar = handleCharMod(attr, str);
      str = attr.getValue("", Tokens.REF);
      boolean lp = new Boolean(attr.getValue("", Tokens.LP)).booleanValue();
      if (str == null || str.length() == 0) {
        throw new SAXException("commandkey ref is missing.");
      } else if (!m_CSMgr.providesCommand(str)) {
        throw new SAXException("commandkey command ref is not defined.");
      } else {
        m_KeySchema.addCommandEntry(m_MenuPrefix.peek().toString(), str, new Character(keychar),
            new Boolean(attr.getValue("", Tokens.HIDDEN)).booleanValue(), lp);
      }
    }
  }//handleCommandKeyAttributes

  private final void handleMenuKeyAttributes(Attributes attr)
      throws SAXException {
    String str = attr.getValue("", Tokens.KEY);
    if (str == null || str.length() == 0) {
      throw new SAXException("menukey key is missing.");
    } else {
      char keychar = handleCharMod(attr, str);
      str = attr.getValue("", Tokens.REF);
      if (str == null || str.length() == 0) {
        throw new SAXException("menukey ref is missing.");
      } else {
        String actualmenu = m_MenuPrefix.peek().toString();
        str = pushMenuPrefix(str);
        if (!m_CSMgr.existsMenu(str)) {
          throw new SAXException("menukey menu ref is not defined.");
        } else {
          m_KeySchema.addMenuEntry(actualmenu, str, new Character(keychar),
              new Boolean(attr.getValue("", Tokens.HIDDEN)).booleanValue());
        }
      }
    }
  }//handleCommandKeyAttributes

  private final void handleSpecialKeyAttributes(Attributes attr)
      throws SAXException {
    Activator.log().debug(c_LogMarker, "handleSpecialKeyAttributes()");
    String str = attr.getValue("", Tokens.KEY);
    if (str == null || str.length() == 0) {
      throw new SAXException("specialkey key is missing.");
    } else {
      char keychar = handleCharMod(attr, str);
      str = attr.getValue("", Tokens.TKEY);
      if (str == null || str.length() == 0) {
        throw new SAXException("specialkey tkey is missing.");
      } else {
        m_KeySchema.addSpecialKeyTranslation(new Character(keychar), "specialkeys_" + str);
      }
    }

  }//handleSpecialKeyAttributes

  private final char handleCharMod(Attributes attr, String str) {
    char keychar = 0;
    keychar = str.charAt(0);
    /*
    try {
      int cnum = Integer.parseInt(str);
      keychar = (char) cnum;
    } catch (NumberFormatException ex) {
      //not given as number
      
    }
    */
    str = attr.getValue("", Tokens.MODIFIER);
    if (str != null && str.length() != 0) {
      //do modification
      if (Tokens.SHIFT.equals(str)) {
        keychar = Character.toUpperCase(keychar);
      } else if (Tokens.CTRL.equals(str)) {
        keychar = (char) (Character.toLowerCase(keychar) - 96);
      }
    }
    return keychar;
  }//handleCharMod

  private final void handleAutoCommand(Attributes attr)
      throws SAXException {
    Activator.log().debug(c_LogMarker, "handleAutoCommand()");
    String str = attr.getValue("", Tokens.REF);
    if (str == null || str.length() == 0) {
      throw new SAXException("commandkey ref is missing.");
    } else if (!m_CSMgr.providesCommand(str)) {
      throw new SAXException("commandkey command ref is not defined.");
    } else {
      String ref = str;
      str = attr.getValue("", Tokens.WHEN);
      if (str == null || str.length() == 0) {
        throw new SAXException("when specification is missing.");
      } else {
        if (Tokens.START.equals(str)) {
          //add start command
          m_CSMgr.addStartCommand(ref);
        } else if (Tokens.END.equals(str)) {
          //add end command
          m_CSMgr.addEndCommand(ref);
        } else {
          throw new SAXException("when not -start- or -end-.");
        }
      }

    }
  }//handleAutoCommand

  private final String pushMenuPrefix(String id) {
    Activator.log().debug(c_LogMarker, "pushMenuPrefix()::" + id);
    if (m_MenuPrefix == null) {
      m_MenuPrefix = new Stack<String>();
    }
    if (m_MenuPrefix.isEmpty()) {
      m_MenuPrefix.push(id);
      return id;
    } else {
      String str = new StringBuilder(m_MenuPrefix.peek().toString())
          .append('_').append(id).toString();
      m_MenuPrefix.push(str);
      return str;
    }
  }//pushMenuPrefix

  private static final int STATE_UNDEFINED = -1;
  private static final int STATE_COMMANDSHELLCONFIG = 1;
  private static final int STATE_COMMANDSET = 3;
  private static final int STATE_COMMAND = 4;
  private static final int STATE_POOLCONFIG = 5;
  private static final int STATE_MENU = 6;
  private static final int STATE_AUTHRULE = 7;
  private static final int STATE_KEYSCHEMA = 9;
  private static final int STATE_COMMANDKEY = 10;
  private static final int STATE_MENUKEY = 11;
  private static final int STATE_SPECIALKEY = 13;
  private static final int STATE_AUTOCOMMANDS = 14;
  private static final int STATE_AUTOCOMMAND = 15;

  /**
   * Inner class defining the tokens to be encountered in
   * the document.
   */
  private final class Tokens {

    static final String COMMANDSHELLCONFIG = "commandshell-config";
    static final String COMMANDSET = "command-set";
    static final String NAMESPACE = "namespace";
    static final String COMMAND = "command";
    static final String AUTOCOMMAND = "auto-cmd";
    static final String AUTOCOMMANDS = "auto-commands";
    static final String ID = "id";
    static final String REF = "ref";
    static final String KEY = "key";
    static final String TYPE = "type";
    static final String CLAZZ = "class";
    static final String AUTHRULE = "auth-rule";
    static final String MENU = "menu";
    static final String KEYSCHEMA = "key-schema";
    static final String COMMANDKEY = "commandkey";
    static final String MODIFIER = "modifier";
    static final String SPECIALKEY = "specialkey";
    static final String TKEY = "tkey";
    static final String MENUKEY = "menukey";
    static final String AND = "and";
    static final String OR = "or";
    static final String POOLCONFIG = "pool-config";
    static final String MINIDLE = "minIdle";
    static final String MAXIDLE = "maxIdle";
    static final String MINIDLETIME = "minIdleTime";
    static final String MAXACTIVE = "maxActive";
    static final String EVICTIONINTERVAL = "evictionInterval";
    static final String EXHAUSTACTION = "exhaustAction";
    static final String EXHAUST_BLOCK = "block";
    static final String EXHAUST_GROW = "grow";
    static final String EXHAUST_FAIL = "fail";
    static final String MAXWAIT = "maxWait";
    static final String SHIFT = "shift";
    static final String CTRL = "ctrl";
    static final String HIDDEN = "hidden";
    static final String WHEN = "when";
    static final String START = "start";
    static final String END = "end";
    static final String LP = "lp";

  }//inner class Tokens


}//class CommandShellConfigHandler
