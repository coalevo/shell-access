/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.command;

import net.coalevo.shellaccess.impl.Activator;
import net.coalevo.shellaccess.model.Command;
import net.coalevo.shellaccess.model.ScriptShellCommandProvider;
import net.coalevo.shellaccess.model.ShellCommandProvider;
import net.coalevo.shellaccess.model.Services;
import net.coalevo.shellaccess.service.ShellCommandProviderManager;
import org.apache.commons.pool.BasePoolableObjectFactory;
import org.apache.commons.pool.ObjectPool;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import org.slf4j.MarkerFactory;
import org.slf4j.Marker;
import org.slf4j.Logger;

import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.FileInputStream;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Class implementing the command shell manager.
 * This class manages schemas and pools of commands.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class CommandShellManager {

  private static Logger log = Activator.log();
  private static Marker c_LogMarker = MarkerFactory.getMarker(CommandShellManager.class.getName());
  private static GenericObjectPool.Config c_PoolConfig;

  protected Map<String,KeySchema> m_KeySchemas;
  protected Map<String,CommandPool> m_CommandPools;
  protected CommandShellSecurityManager m_SecurityManager;
  protected SortedSet<String> m_Menus;
  protected MenuKeyEntry m_DefaultMenu;
  private boolean m_Initialized = false;
  private Set<ConfigurationListener> m_ConfigListeners;
  private AtomicBoolean m_Reload = new AtomicBoolean(false);
  private ShellCommandProviderManager m_CmdProviderManager;
  private File m_ConfigFile;
  private List<String> m_StartCommands;
  private List<String> m_EndCommands;

  static {
    //prepare a default pool config
    c_PoolConfig = new GenericObjectPool.Config();
    //set for
    c_PoolConfig.maxActive = 10;
    c_PoolConfig.maxIdle = 5;
    c_PoolConfig.maxWait = -1;
    c_PoolConfig.testOnBorrow = false;
    c_PoolConfig.testOnReturn = false;
    //make it growable so it adapts
    c_PoolConfig.whenExhaustedAction = GenericObjectPool.WHEN_EXHAUSTED_GROW;
  }//static initializer

  public CommandShellManager() {
    m_SecurityManager = new CommandShellSecurityManagerImpl();
    m_KeySchemas = new HashMap<String,KeySchema>();
    m_CommandPools = new HashMap<String,CommandPool>();
    m_Menus = new TreeSet<String>();
    m_ConfigListeners = new HashSet<ConfigurationListener>(100);
    m_CmdProviderManager = Activator.getServices().getShellCommandProviderManager();
    m_ConfigFile = new File(Activator.getConfigStore(), "commandshell-config.xml");
    m_StartCommands = new ArrayList<String>();
    m_EndCommands = new ArrayList<String>();
  }//CommandShellManager

  public void initialize() {
    if (!m_Initialized) {
      loadConfiguration();
    }
  }//initialize

  public boolean reload() {
    if (m_Initialized) {
      m_Reload.set(true);
      loadConfiguration();
      if (m_Reload.get()) {
        for (Iterator iterator = m_ConfigListeners.iterator(); iterator.hasNext();) {
          ConfigurationListener l = (ConfigurationListener) iterator.next();
          l.updatedConfiguration();
        }
      }
    }
    return m_Reload.get();
  }//reload

  private void loadConfiguration() {
    //TODO: Configuration!
    log.debug(c_LogMarker,"loadConfiguration()");

    //backup old references
    Map<String,KeySchema> backupKS = m_KeySchemas;
    Map<String,CommandPool> backupCmdPools = m_CommandPools;
    SortedSet<String> backupMenus = m_Menus;

    m_KeySchemas = new HashMap<String,KeySchema>();
    m_CommandPools = new HashMap<String,CommandPool>();
    m_Menus = new TreeSet<String>();
    m_StartCommands = new ArrayList<String>();
    m_EndCommands = new ArrayList<String>();

    try {
      SAXParserFactory fac = Activator.getServices().getSAXParserFactory(Services.WAIT_UNLIMITED);
      XMLReader parser = fac.newSAXParser().getXMLReader();
      //Activate Namespaces
      parser.setFeature("http://xml.org/sax/features/namespaces", true);
      if (new Boolean(System.getProperty("sax.parser.verify")).booleanValue()) {
        parser.setFeature("http://xml.org/sax/features/validation", true);
      } else {
        log.debug(c_LogMarker,"loadConfiguration()::Disabled DTD Validation.");
        parser.setFeature("http://xml.org/sax/features/validation", false);
      }
      log.debug(c_LogMarker,"initialize()::have parser = " + parser.toString());
      InputSource src = new InputSource(new FileInputStream(m_ConfigFile));
      log.debug(c_LogMarker,"initialize()::have InputSource = " + src.toString());
      parser.setContentHandler(new CommandShellConfigHandler(this));
      parser.parse(src);
    } catch (Exception ex) {
      log.error(c_LogMarker,"initialize()", ex);
      if (m_Reload.get()) {//place old references that should work
        m_KeySchemas = backupKS;
        m_CommandPools = backupCmdPools;
        m_Menus = backupMenus;
        log.info(c_LogMarker,"loadConfiguration():: Restored last working instances.");
        m_Reload.set(false);
      }
      return;
    }
    m_Initialized = true;
  }//loadConfiguration

  public void registerConfigurationListener(ConfigurationListener cl) {
    m_ConfigListeners.add(cl);
    log.debug(c_LogMarker,"registerConfigurationListener() ::" + cl.toString());
  }//registerConfigurationListener

  public void removeConfigurationListener(ConfigurationListener cl) {
    m_ConfigListeners.remove(cl);
    log.debug(c_LogMarker,"removeConfigurationListener() ::" + cl.toString());
  }//removeConfigurationListener


  public void setDefaultMenu(String mid) {
    m_DefaultMenu = (MenuKeyEntry) KeyEntry.createMenuEntry(this, mid, true);
  }//setDefaultMenu

  public MenuKeyEntry getDefaultMenu() {
    return m_DefaultMenu;
  }//getDefaultMenu

  public void addKeySchema(String id, KeySchema cs) {
    m_KeySchemas.put(id, cs);
  }//addKeySchema

  public void removeKeySchema(String id) {
    m_KeySchemas.remove(id);
  }//removeKeySchema

  public boolean existsKeySchema(String id) {
    return m_KeySchemas.get(id) != null;
  }//existsKeySchema

  public KeySchema getKeySchema(String id) {
    return (KeySchema) m_KeySchemas.get(id);
  }//getKeySchema

  public CommandShellSecurityManager getSecurityManager() {
    return m_SecurityManager;
  }//getSecurityManager

  public ShellCommandProviderManager getCommandProviderManager() {
    return m_CmdProviderManager;
  }//getCommandProviderManager

  public void addCommand(ShellCommandProvider p, String cmdid, GenericObjectPool.Config pcfg) {
    final String fcid = p.getNamespace().concat(".").concat(cmdid);
    m_CommandPools.put(fcid, new CommandPool(p, cmdid, (pcfg != null) ? pcfg : c_PoolConfig));
  }//addCommand

  public void addStartCommand(String ref) {
    //Adding start command
    log.debug(c_LogMarker,"addStartCommand()::" + ref);
    m_StartCommands.add(ref);
  }//addStartCommand

  public Iterator listStartCommands() {
    return m_StartCommands.listIterator();
  }//listStartCommands

  public void addEndCommand(String ref) {
    log.debug(c_LogMarker,"addEndCommand()::" + ref);
    m_EndCommands.add(ref);
  }//addEndCommands

  public Iterator listEndCommands() {
    return m_EndCommands.listIterator();
  }//listEndCommands

  public boolean providesCommand(String fid) {
    return m_CommandPools.containsKey(fid);
  }//providesCommand

  /**
   * This just makes sense for script commands, given that
   * the classloading has been delegated to the OSGi framework.
   * TODO: Probably add some mechanism that makes reloading of class commands
   * possible, in the sense that the commandprovider instance reference
   * changes
   *
   * @param id the identifier of the command.
   * @return true if reloaded, false otherwise.
   */
  public boolean reloadCommand(String id) {
    log.debug(c_LogMarker,"reloadCommand()::" + id);
    if (!m_CommandPools.containsKey(id)) {
      log.debug(c_LogMarker,"reloadCommand()::" + id + " is not a command identifier.");
      return false;
    }
    //get pool
    CommandPool cp = (CommandPool) m_CommandPools.get(id);
    cp.flush();
    return true;
  }//reloadCommand

  private String[] splitId(String id) throws Exception {
    int idx = id.lastIndexOf('.');
    if(idx > 0 ) {
      String[] rv = new String[2];
      rv[0] = id.substring(0,idx);
      rv[1] = id.substring(idx+1, id.length());
      return rv;
    } else {
      throw new Exception();
    }
  }//splitId

  public String loadScript(String id) throws Exception {
    String[] s = splitId(id);

    ShellCommandProvider p = this.m_CmdProviderManager.get(s[0]);
    if(p != null && p instanceof ScriptShellCommandProvider) {
      ScriptShellCommandProvider sscp = (ScriptShellCommandProvider) p;
      return sscp.getCommandScript(s[1]);
    } else {
      throw new Exception();
    }
  }//loadScript

  public void saveScript(String id, String script) throws Exception {
    String[] s = splitId(id);

    ShellCommandProvider p = this.m_CmdProviderManager.get(s[0]);
    if(p != null && p instanceof ScriptShellCommandProvider) {
      ScriptShellCommandProvider sscp = (ScriptShellCommandProvider) p;
      sscp.setCommandScript(s[1],script);
    } else {
      throw new Exception();
    }
  }//saveScript

  public Command getScriptCommand(String id) throws Exception {
   String[] s = splitId(id);

    ShellCommandProvider p = this.m_CmdProviderManager.get(s[0]);
    if(p != null && p instanceof ScriptShellCommandProvider) {
      ScriptShellCommandProvider sscp = (ScriptShellCommandProvider) p;
      return sscp.createCommand(s[1]);
    } else {
      throw new Exception();
    }
  }//getScriptCommand

  public String[] listScriptCommandNames(String namespace)
      throws Exception {
    ShellCommandProvider p = this.m_CmdProviderManager.get(namespace);
    if(p != null && p instanceof ScriptShellCommandProvider) {
      ScriptShellCommandProvider sscp = (ScriptShellCommandProvider) p;
      return sscp.listCommandScriptNames();
    } else {
      throw new Exception();
    }
  }//listScriptCommandNames

  public void addMenu(String mid) {
    log.debug(c_LogMarker,"addMenu()::" + mid);
    m_Menus.add(mid);
  }//addMenu

  public boolean existsMenu(String mid) {
    log.debug(c_LogMarker,"existsMenu()::" + mid);
    return m_Menus.contains(mid);
  }//existsMenu

  public boolean existsCommand(String cid) {
    return m_CommandPools.containsKey(cid);
  }//existsCommand

  public boolean isScriptCommand(String id) {
    try {
      String[] s = splitId(id);
      ShellCommandProvider p = this.m_CmdProviderManager.get(s[0]);
      if(p != null && p instanceof ScriptShellCommandProvider) {
       ScriptShellCommandProvider sscp = (ScriptShellCommandProvider) p;
        return sscp.provides(s[1]);
      }
    } catch (Exception ex) {
    }
    return false;
  }//isScriptCommand

  public Command leaseCommand(String id) {
    Object o = m_CommandPools.get(id);
    if (o == null) {
      log.error(c_LogMarker,"leaseCommand()::No pool for this identifier available " + id);
      return null;
    } else {
      try {
        return ((CommandPool) o).lease();
      } catch (Exception ex) {
        log.error(c_LogMarker,"leaseCommand()::" + id);
        return null;
      }
    }
  }//leaseCommand

  public void releaseCommand(Command cmd) {
    Object o = m_CommandPools.get(cmd.getIdentifier());
    if (o == null) {
      log.error(c_LogMarker,"releaseCommand()::No pool for this identifier available " + cmd.getIdentifier());
    } else {
      try {
        ((CommandPool) o).release(cmd);
      } catch (Exception ex) {
        log.error(c_LogMarker,"releaseCommand()", ex);
      }
    }
  }//releaseCommand

  /**
   * Inner class implementing a pool of
   * <tt>Command</tt> instances.
   */
  private static class CommandPool {

    private ObjectPool m_Pool;
    private ShellCommandProvider m_Provider;
    private String m_CommandId;

    public CommandPool(ShellCommandProvider provider, String cid, GenericObjectPool.Config pcfg) {
      m_Pool = new GenericObjectPool(new CommandFactory(provider, cid), pcfg);
      m_Provider = provider;
      m_CommandId = cid;
    }//constructor

    /**
     * Flushes all instances in the pool.
     */
    public void flush() {
      try {
        m_Pool.clear();
      } catch (Exception ex) {
        log.error(c_LogMarker,"CommandPool.update()", ex);
      }
    }//update

    public Command lease()
        throws Exception {
      Command cmd = (Command) m_Pool.borrowObject();
      log.debug(c_LogMarker,"lease():" + cmd.toString());
      return cmd;
    }//lease

    public void release(Command c) throws Exception {
      log.debug(c_LogMarker,"release()::" + c.toString());
      m_Pool.returnObject(c);
    }//release

    public ShellCommandProvider getProvider() {
      return m_Provider;
    }//getProvider

    public String getCommandIdentifier() {
      return m_CommandId;
    }//getCommandIdentifier

  }//CommandPool

  /**
   * Inner class implementing a factory for the
   * {@link CommandPool} instances.
   */
  private static class CommandFactory
      extends BasePoolableObjectFactory {

    private ShellCommandProvider m_CommandProvider;
    private String m_CommandId;

    public CommandFactory(ShellCommandProvider scp, String fid) {
      m_CommandProvider = scp;
      m_CommandId = fid;
    }//constructor

    public Object makeObject() throws Exception {
      return m_CommandProvider.createCommand(m_CommandId);
    }//makeObject

    public void passivateObject(Object o) {
      ((Command) o).reset();
    }//passivateObject


  }//class CommandFactory

}//class CommandShellManager
