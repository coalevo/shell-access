/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.command;


import net.coalevo.foundation.model.Agent;

import java.util.SortedMap;

/**
 * This interface defines the contract for a
 * <tt>KeySchema</tt>, which maps keys in form
 * of input characters with {@link KeyEntry}
 * instances.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface KeySchema {

  /**
   * Returns the identifier of this <tt>KeySchemaImpl</tt>
   * intance.
   *
   * @return the identifier as <tt>String</tt>.
   */
  String getIdentifier();

  /**
   * Returns a {@link KeyEntry} instance.
   *
   * @param mid the menu identifier.
   * @param key the key as <tt>int</tt>.
   * @param a   an {@link Agent} instance.
   * @return a {@link KeyEntry} instance if exists.
   * @throws SecurityException    if an entry
   *                              exists, but cannot be used by the given agent.
   * @throws NoSuchEntryException if the given menu exists, but does not
   *                              have any entry for the given key.
   * @throws NoSuchMenuException  if the given menu does not exist.
   */
  public KeyEntry entryForKey(String mid, int key, Agent a)
      throws SecurityException, NoSuchEntryException, NoSuchMenuException;

  /**
   * Returns a <tt>SortedMap</tt> instance containing
   * all entries in a given menu identifier that are
   * accessible by the given {@link Agent}.
   *
   * @param mid a unique menu identifier as <tt>String</tt>.
   * @param a   an {@link Agent} instance.
   * @return a {@link MenuEntries} instance.
   * @throws NoSuchMenuException if the given menu identifier does
   *                             not identify any menu.
   * @throws SecurityException   if the menu cannot be used
   *                             with the given set of permissions.
   */
  public MenuEntries menuEntries(String mid, Agent a)
      throws NoSuchMenuException, SecurityException;

  /**
   * Tests if the given key is a special key.
   *
   * @return true if special key, false otherwise.
   */
  public boolean isSpecialKey(Character c);

  /**
   * Translates a special key to a <tt>String</tt> representation.
   *
   * @param c the character to be converted.
   * @return the <tt>String</tt> representation.
   */
  public String translateSpecialKey(Character c);

}//interface KeySchema
