/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.shellaccess.service;


import net.coalevo.shellaccess.model.ShellCommandProvider;
import net.coalevo.shellaccess.model.ScriptShellCommandProvider;

import java.util.Iterator;

/**
 * Defines the interface for the <tt>ShellCommandProviderManager</tt>.
 * <p/>
 * Note that this service is implemented using the whiteboard model.
 * It is not strictly required to register and unregister {@link ShellCommandProvider}
 * instances for specific namespaces; the mechanism works as well through the OSGi
 * framework registry.
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface ShellCommandProviderManager {

  /**
   * Directly registers a {@link ShellCommandProvider} instance.
   *
   * @param scp a {@link ShellCommandProvider} instance.
   * @return true if registered, false otherwise.
   */
  public boolean register(ShellCommandProvider scp);

  /**
   * Directly unregisters the {@link ShellCommandProvider} that was
   * bound to the given namespace.
   *
   * @param ns the namespace a {@link ShellCommandProvider} was
   *           bound to through registration.
   * @return true if unregistered, false otherwise.
   */
  public boolean unregister(String ns);

  /**
   * Returns the {@link ShellCommandProvider} bound to the given
   * namespace.
   *
   * @param ns the namespace as <tt>String</tt>.
   * @return a {@link ShellCommandProvider} instance.
   */
  public ShellCommandProvider get(String ns);

  /**
   * Tests if a {@link ShellCommandProvider} is bound to the given
   * namespace.
   *
   * @param ns the namespace as <tt>String</tt>.
   * @return true if there is a {@link ShellCommandProvider} bound to the
   *         given namespace, false otherwise.
   */
  public boolean isAvailable(String ns);

  /**
   * Lists the available namespaces that have {@link ShellCommandProvider} instances
   * bound to it.
   *
   * @return an <tt>Iterator</tt> over {@link ShellCommandProvider} instances.
   */
  public Iterator listAvailable();

  /**
   * Lists the available namespaces that have {@link ScriptShellCommandProvider} instances
   * bound to it.
   *
   */
  public Iterator listAvailableScriptProviders();
  
}//interface ShellCommandProviderManager
