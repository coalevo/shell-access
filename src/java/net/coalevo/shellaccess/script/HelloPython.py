# This is a Python script command.
# There are two defined variables that you can use
# to obtain references to I/O and services:
# The variable "shell" (see interface BaseShell) and the
# variable "services" (see interface Services).
#
# Class loading:
# The python provider included in this package is
# written in a way that it will automatically import all packages
# available to the bundle that it belongs to (in case of the standard
# provider this is the shell-access bundle + its imports).
#
# @author Dieter Wimberger
# @version @version@ (@date@)
#
from net.coalevo.shellaccess.model import ShellIO
from net.wimpi.telnetd.io import TemplateAttributes
from net.coalevo.shellaccess.model import ShellAccessSession

sio = shell.getShellIO()
session = shell.getSession()

attr = sio.leaseTemplateAttributes()
attr.add("username",session.getUsername())
sio.printTemplate("commands_net.coalevo.shellaccess.cmd.py.HelloPython_test",attr)
